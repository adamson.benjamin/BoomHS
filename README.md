# BoomHS

A sandbox on-line role-playing game focusing on emergent game play through player-to-player and player-to-environment interactions. Development focuses on creating an immersive environment for players to play a game in without all the modern day game features that ruin immersion *(cash-shops, loot boxes.., pay-to-win, etc...)*

**This project is open-source.**

![cone](./assets/cone.png?raw=true)`WARNING: Under Construction! This project is in active development.`

Follow the link below for more information about the project. It is currently separated into two documents. The first link describes the game-play, the second is for developers interested in building the project or understanding the source code.

- [Game-play Information](./gameplay.md)

- [Developer Information](./developer-information.md)

NOTE: Please upvote this [gitlab issue](https://gitlab.com/gitlab-org/gitlab-ce/issues/45388) to get automatic TOC generation prioritized.

## Website
The [website](http://www.blog.foreheadwallgames.com/) is WIP.

## Screenshots
You can see screen-shots of development in the [screenshots/](./screenshots) directory.

## Github History
Originally this project was developed on [github](https://github.com/bjadamson/BoomHS), which you can visit if you are interested. Please note the github project is **no longer updated or maintained.** All development takes place on gitlab now.
