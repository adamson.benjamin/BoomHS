#include <common/algorithm.hpp>
#include <common/log.hpp>
#include <common/macro_util.hpp>
#include <common/os.hpp>

#include <opengl/global.hpp>

#include <cstdlib>
#include <filesystem>
#include <iostream>
#include <optional>
#include <string>
#include <vector>

// Application that does post-processing on the OpenGL shader code (after the main executable has
// been compiled). The scripts included with the project automatically execute this application on
// your behalf when starting the main executable.

namespace fs = std::filesystem;
using namespace common;

namespace
{

void
delete_if_exists(fs::path const& path)
{
  if (fs::exists(path)) {
    fs::remove(path);
  }
}

struct Paths
{
  fs::path const read_shaders_path, write_shaders_path;
  fs::path const log_path;
};

inline auto
make_paths()
{
  auto const determine_root = []() -> fs::path {
    auto const CWD = fs::current_path();
    if (auto const it = CWD.string().find("build-system"); it != std::string::npos) {
      return CWD.parent_path();
    }
    return CWD;
  };

  auto const root = determine_root();
  auto const bin  = root / "build-system" / "bin";

  auto constexpr shaders_path   = "shaders";
  auto const read_shaders_path  = root / shaders_path;
  auto const write_shaders_path = bin / shaders_path;

  auto const log_path = bin / "logs";
  return Paths{read_shaders_path, write_shaders_path, log_path};
}

} // namespace

template <typename FN, typename... Args>
auto
iter_directories(fs::path const& path_to_shaders, char const* file_extension, FN const& fn,
                 Args&&... args)
{
  bool result = true;
  for (fs::directory_iterator it{path_to_shaders}; it != fs::directory_iterator{}; ++it) {
    if (!common::string_contains(it->path().filename().string(), file_extension)) {
      continue;
    }
    result &= fn(it, FORWARD(args));
  }
  return result;
}
std::optional<std::pair<std::string, fs::path>>
copy_common(fs::directory_iterator const& it, fs::path const& path_to_shaders,
            fs::path const& outdir)
{
  auto const path = it->path();

  auto const shader_read_result = common::read_file(path_to_shaders / path);
  if (!shader_read_result) {
    return std::nullopt;
  }

  auto const filename_comment = "// SHADER FILENAME: '" + path.string() + "'\n";
  auto const shader_contents  = filename_comment + shader_read_result.expect("shader contents");
  auto const output_file      = outdir / path.filename();

  // remove the file if it exists already (we don't care about success here, unless
  // failure to delete becomes a problem someday ...)
  ::delete_if_exists(output_file);
  return std::make_optional(PAIR(shader_contents, output_file));
}

Result<NOTHING, std::string>
copy_shaders_to_outdir(fs::path const& path_to_shaders, fs::path const& outdir)
{
  if (!fs::exists(path_to_shaders)) {
    return Err(std::string{"no shader at path '" + path_to_shaders.string() + "' exists."});
  }
  else if (!fs::is_directory(path_to_shaders)) {
    return Err(std::string{"found 'shader' to not be a directory at this path."});
  }

  auto const copy_vert_files = [&](auto const& it, auto const& both_shared,
                                   auto const& vert_shared) {
    auto cresult = copy_common(it, path_to_shaders, outdir);
    if (!cresult) {
      return false;
    }

    auto       pair            = *cresult;
    auto const shader_contents = MOVE(pair.first);
    auto const output_file     = MOVE(pair.second);

    // write the modified copy out to the file
    common::write_file(output_file, both_shared, vert_shared, shader_contents);
    return true;
  };

  auto const copy_frag_files = [&](auto const& it, auto const& shared_library_code,
                                   auto const& frag_library_code, auto const& frag_pl_code) {
    auto cresult = copy_common(it, path_to_shaders, outdir);
    if (!cresult) {
      return false;
    }

    auto       pair            = *cresult;
    auto const shader_contents = MOVE(pair.first);
    auto const output_file     = MOVE(pair.second);

    auto const libcode = shared_library_code + frag_library_code;

    // write the modified copy out to the file
    bool const contains_pointlights =
        common::string_contains(shader_contents, "MAX_NUM_POINTLIGHTS");
    if (contains_pointlights) {
      common::write_file(output_file, libcode, frag_pl_code, shader_contents);
    }
    else {
      common::write_file(output_file, libcode, shader_contents);
    }
    return true;
  };

  auto const path_to_shared = path_to_shaders / "shared";
  auto const both_rr        = path_to_shared / "3d_common.glsl_both";
  auto const vert_rr        = path_to_shared / "3d_common.glsl_vert";
  auto const frag_rr        = path_to_shared / "3d_common.glsl_frag";
  auto const frag_plrr      = path_to_shared / "3d_common_pointlights.glsl_frag";

  auto const both   = TRY(common::read_file(both_rr));
  auto const vert   = TRY(common::read_file(vert_rr));
  auto const frag   = TRY(common::read_file(frag_rr));
  auto const fragpl = TRY(common::read_file(frag_plrr));

  // Copy the shaders (prepended with their shared code) to the output directories.
  bool result = true;
  result |= iter_directories(path_to_shaders, ".vert", copy_vert_files, both, vert);
  result |= iter_directories(path_to_shaders, ".frag", copy_frag_files, both, frag, fragpl);
  if (!result) {
    return Err(std::string{"Error copying files."});
  }
  return OK_NONE;
}

int
main(int, char**)
{
  auto        LOGGER    = log_factory::make_stderr(spdlog::level::info);
  auto const  paths     = make_paths();
  auto const& read_from = paths.read_shaders_path;
  auto const& write_too = paths.write_shaders_path;
  auto const& log_path  = paths.log_path;

  auto const on_error = [&](auto const msg) {
    LOG_ERROR("Error running post-build program, problem: '%s'", msg);
    return EXIT_FAILURE;
  };
  {
    fs::create_directory(write_too);

    auto const result = copy_shaders_to_outdir(read_from, write_too);
    if (result.is_err()) {
      return on_error(result.unwrap_error());
    }
    LOG_INFO("Shaders successfully copied.");
  }

  fs::create_directory(log_path);
  return EXIT_SUCCESS;
}
