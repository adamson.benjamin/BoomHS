if (DEBUG_MODE)
  list(APPEND OSFLAGS -O0)
  list(APPEND OSFLAGS -g0)
  list(APPEND OSFLAGS -fno-inline)
else()
  list(APPEND OSFLAGS -g3)
endif()

set (OS_SPECIFIC_LINKER_LIBRARIES
  -L/usr/local/opt/sdl2/lib SDL2
  -L/usr/local/lib SOIL
  -L/usr/local/lib glew
)
 list(APPEND OSFLAGS -framework CoreFoundation)
 list(APPEND OSFLAGS -framework OpenAL)
 list(APPEND OSFLAGS -framework OpenGL)

###################################################################################################
# Preprocessor definitions
add_compile_definitions(OS_IS_MACOS=1)
add_compile_definitions(BOOST_STACKTRACE_USE_BACKTRACE=1)
add_compile_definitions(BOOST_STACKTRACE_USE_ADDR2LINE=1)
