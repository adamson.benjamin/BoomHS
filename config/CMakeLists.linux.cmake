if (DEBUG_MODE)
  # Optimize debugging experience. -Og should be the optimization level of choice for the standard edit-compile-debug
  # cycle, offering a reasonable level of optimization while maintaining fast compilation and a good debugging experience.
  # It is a better choice than -O0 for producing debuggable code because some compiler passes that collect
  # debug information are disabled at -O0.
  list(APPEND OS_FLAGS -Og)

  # Level 3 includes extra information, such as all the macro definitions present in the program.
  # Some debuggers support macro expansion when you use -g3.
  # https://gcc.gnu.org/onlinedocs/gcc/Debugging-Options.html
  list(APPEND OS_FLAGS -g3)
else()
  # Optimize yet more. -O3 turns on all optimizations specified by -O2 and also turns on the following optimization flags:
  list(APPEND OS_FLAGS -O3)


  # g0 - no debug information
  list(APPEND OS_FLAGS -g0)
endif()

set(OS_LIBRARIES
    -static-libgcc
    -static-libstdc++

    dw
    dl
    bfd
    openal
    udev
    m
    pthread
    dl
    backtrace
    boost_stacktrace_addr2line
    boost_stacktrace_backtrace
    ${SDL2_LIBRARIES}
    ${SDL2IMAGE_LIBRARIES}
    )

###################################################################################################
# Preprocessor definitions
add_compile_definitions(OS_IS_LINUX=1)
add_compile_definitions(BOOST_STACKTRACE_USE_BACKTRACE=1)
add_compile_definitions(BOOST_STACKTRACE_USE_ADDR2LINE=1)
