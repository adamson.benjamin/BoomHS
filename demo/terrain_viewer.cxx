#include <demo/common.hpp>

#include <boomhs/fog.hpp>
#include <boomhs/material.hpp>
#include <boomhs/terrain.hpp>
#include <opengl/terrain_renderer.hpp>
#include <opengl/texture.hpp>

using namespace math;
using namespace common;
using namespace demo;
using namespace gl_sdl;
using namespace opengl;

namespace
{
int constexpr WIDTH           = 1024;
int constexpr HEIGHT          = 768;
auto constexpr HEIGHTMAP_PATH = "assets/terrain/Area0-HM.png";

} // namespace

int
main(int, char**)
{
  auto LOGGER = common::log_factory::make_stderr(spdlog::level::info);
  MAKE_DEMO_WINDOW_BOILERPLATE(LOGGER, "Terrain Demo", WIDTH, HEIGHT);
  auto& window = LOGGER_GLSL_PAIR.window;
  window.make_current();

  TerrainConfig const     tc;
  TerrainGridConfig const tgc{2, 2};

  opengl::texture_storage ttable;

  struct TerrainTexture
  {
    GLenum      const format;
    char const* const wrap;
    char const* const nickname;
    char const* const path;
  };

  auto const load_2dtex = [&](TerrainTexture const& tt) -> Result<none_t, std::string> {
    auto tn = opengl::texture_and_filenames{tt.nickname, {tt.path}};

    texture_cfg cfg{GL_TEXTURE_2D};
    cfg.wrap   = texture::wrap_mode_from_string(LOGGER, tt.wrap);
    cfg.format = tt.format;
    auto t     = TRY(texture::upload_2d_texture(LOGGER, tn.filenames[0], cfg));
    ttable.add(MOVE(tn), MOVE(t));
    return OK_NONE;
  };

  auto const on_load_error = [&LOGGER](auto const& err_str) {
    LOG_ERROR("Error loading '%s', msg: '%s", HEIGHTMAP_PATH, err_str);
  };

  std::array constexpr vals = {
      TerrainTexture{GL_RGBA, "clamp_edge", "Area0-HM", HEIGHTMAP_PATH},
      TerrainTexture{GL_RGB, "repeat", "floor", "assets/terrain/Floor0.png"},
      TerrainTexture{GL_RGB, "clamp_edge", "grass", "assets/terrain/Grass0.png"},
      TerrainTexture{GL_RGB, "clamp_edge", "mud", "assets/terrain/mud.png"},
      TerrainTexture{GL_RGB, "clamp_edge", "brick_path", "assets/terrain/brick_path.png"},
      TerrainTexture{GL_RGB, "clamp_edge", "blendmap", "assets/terrain/blendmap.png"},
  };
  for (auto const& v : vals) {
    load_2dtex(v).expect_fn(on_load_error);
  }

  auto heightmap = heightmap::parse(LOGGER, HEIGHTMAP_PATH).expect_fn(on_load_error);

  GlobalLight const global_light{LOC3::YELLOW};
  Fog const         fog{0.007f, 1.0f, LOC4::BLUE};

  auto constexpr PL_Y = 1;
  Transform              t0{1, PL_Y, -1};
  PointLight             p0{Light{LOC3::BLUE, LOC3::BLUE}};
  PointLightAndTransform plt0{t0, p0};

  PointLightList const pointlights{{plt0}};

  DirectionalLight           dlight;
  DirectionalLightList const dirlights{{DirectionalLightReference{dlight}}};

  NumLights const num_lights{dirlights.size(), pointlights.size()};
  auto            sp           = demo::make_terrain_program(LOGGER, num_lights);
  auto            terrain_grid = terrain::generate_grid(LOGGER, tgc, tc, heightmap, sp);

  Material const           material;
  terrain_renderer_default terrain_renderer;

  auto const PERS_FORWARD = -constants::Z_UNIT_VECTOR;
  auto constexpr PERS_UP  = constants::Y_UNIT_VECTOR;
  CameraArcball arcball{PERS_UP};

  {
    SphericalCoordinates sc;
    sc.radius       = 3.8f;
    sc.theta        = glm::radians(-0.229f);
    sc.phi          = glm::radians(38.2735f);
    arcball.scoords = sc;
  }

  Transform nothing;
  arcball.set_target(nothing);

  auto constexpr NEAR    = 0.001f;
  auto constexpr FAR     = 5000.0f;
  auto const window_rect = rect::create(0, 0, WIDTH, HEIGHT);
  auto const frustum     = Frustum::from_rect_and_nearfar(window_rect, NEAR, FAR);

  auto constexpr FOV = glm::radians(110.0f);
  ViewSettings const vs{FOV};
  auto               cmatrices = camera::calc_cm(arcball, vs, frustum, PERS_FORWARD);

  draw_call_counter ds;
  auto const        draw_fn = [&](draw_call_counter&, auto&&...) {
    OR::clear_screen(LOC4::WHITE);
    bool constexpr DRAW_NORMALS = false;

    auto const& vm = cmatrices.view;
    auto const  cm = cmatrices.proj * vm;

    glm::vec4 const NOCULL_VECTOR{0, 0, 0, 0};
    terrain_renderer.render(LOGGER, ttable, cm, vm, material, dirlights, pointlights, terrain_grid,
                            global_light, fog, NOCULL_VECTOR, DRAW_NORMALS, ds);
  };

  auto const update_fn = [&](auto&&...) {
    cmatrices = camera::calc_cm(arcball, vs, frustum, arcball.position());
    LOG_ERROR("position: %s", glm::to_string(arcball.position()));
    LOG_ERROR("local_position: %s", glm::to_string(arcball.local_position()));
    LOG_ERROR("target_position: %s", glm::to_string(arcball.target_position()));
  };

  Mouse      ms;
  auto const process_event_fn = [&](SDL_Event& event, DeltaTime const& dt, auto&&...) {
    auto const type = event.type;
    switch (type) {
    case SDL_KEYDOWN: {
      switch (event.key.keysym.sym) {
      case SDLK_w:
        nothing.move(-constants::Z_UNIT_VECTOR);
        break;
      case SDLK_s:
        nothing.move(constants::Z_UNIT_VECTOR);
        break;
      case SDLK_a:
        nothing.move(constants::X_UNIT_VECTOR);
        break;
      case SDLK_d:
        nothing.move(-constants::X_UNIT_VECTOR);
        break;
      case SDLK_e:
        nothing.move(constants::Y_UNIT_VECTOR);
        break;
      case SDLK_q:
        nothing.move(-constants::Y_UNIT_VECTOR);
        break;
      }
    } break;
    case SDL_MOUSEMOTION: {
      auto const& motion = event.motion;
      if (ms.left_pressed()) {
        auto xrel = static_cast<float>(motion.xrel);
        auto yrel = static_cast<float>(motion.yrel);
        xrel *= dt();
        yrel *= dt();
        camera::rotate_radians(arcball, xrel, yrel, dt);
      }
    } break;
    case SDL_MOUSEWHEEL: {
      float constexpr ZOOM_FACTOR = 0.2f;
      auto const& wheel           = event.wheel;
      if (wheel.y > 0) {
        camera::zoom_out(arcball, ZOOM_FACTOR, dt);
      }
      else {
        camera::zoom_in(arcball, ZOOM_FACTOR, dt);
      }
    } break;
    }
  };

  return demo::main_loop(window, update_fn, draw_fn, process_event_fn);
}
