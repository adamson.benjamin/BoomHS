#pragma once
#include <boomhs/camera.hpp>
#include <boomhs/camera_algorithm.hpp>
#include <boomhs/collision.hpp>
#include <boomhs/color.hpp>
#include <boomhs/components.hpp>
#include <boomhs/delta_time.hpp>
#include <boomhs/mouse.hpp>

#include <boomhs/transform.hpp>
#include <boomhs/vertex_factory.hpp>
#include <boomhs/world_object.hpp>

#include <math/cube.hpp>
#include <math/raycast.hpp>
#include <math/rectangle.hpp>
#include <math/viewport.hpp>

#include <opengl/draw_info.hpp>
#include <opengl/global.hpp>
#include <opengl/gpu.hpp>
#include <opengl/renderer.hpp>
#include <opengl/shader.hpp>
#include <opengl/ui_renderer.hpp>
#include <opengl/vertex_attribute.hpp>

#include <gl_sdl/global.hpp>
#include <gl_sdl/sdl_window.hpp>

#include <math/random.hpp>

#include <common/algorithm.hpp>
#include <common/log.hpp>
#include <common/macros_class.hpp>
#include <common/macros_scope_exit.hpp>
#include <common/move.hpp>
#include <common/wall_clock.hpp>

#include <cstdlib>
#include <vector>

struct CameraMatrices;
class rng_t;

namespace opengl
{
struct draw_call_counter;
} // namespace opengl

namespace demo
{

struct TestWindow
{
  SDL_GL_System sdl_gl_system;
  SDLWindow     window;
};

Result<TestWindow, int>
make_default_window(log_t&, char const*, bool, int, int);

#define MAKE_DEMO_WINDOW_BOILERPLATE(LOGGER, TITLE, WIDTH, HEIGHT)                                 \
  auto const on_error = [](int const error_code) {                                                 \
    std::cerr << "Error creating demo window, error code: '" << error_code << "'\n";               \
    return EXIT_FAILURE;                                                                           \
  };                                                                                               \
  bool constexpr FULLSCREEN = false;                                                               \
  auto LOGGER_GLSL_PAIR =                                                                          \
      demo::make_default_window(LOGGER, TITLE, FULLSCREEN, WIDTH, HEIGHT).expect_fn(on_error)

enum ScreenSector
{
  LEFT_TOP = 0,
  RIGHT_TOP,
  LEFT_BOTTOM,
  RIGHT_BOTTOM,
  MAX
};

struct MouseCursorInfo
{
  ScreenSector        sector;
  MouseClickPositions click_positions;
};

inline auto
make_perspective_rect_gpuhandle(log_t& logger, RectFloat const& rect,
                                opengl::vertex_attribute const& va)
{
  auto buffer = RectBuilder{rect}.build();
  return OG::copy_rectangle(logger, va, buffer);
}

inline auto
make_perspective_rect(Viewport const& viewport, rng_t& rng)
{
  auto const gen = [&rng](auto const low, auto const high) {
    auto const low_f  = static_cast<float>(low);
    auto const high_f = static_cast<float>(high);
    return rng.gen(low_f, high_f);
  };

  auto const left  = gen(viewport.left(), viewport.right());
  auto const right = gen(left, viewport.right());

  auto const top    = gen(viewport.top(), viewport.bottom());
  auto const bottom = gen(top, viewport.bottom());

  return rect::create(left, top, right, bottom);
};

class CubeEntity
{
  Cube               cube_;
  Transform          tr_;
  opengl::draw_state di_;

public:
  MOVE_DEFAULT_ONLY(CubeEntity);

  CubeEntity(Cube&& cube, Transform&& tr, opengl::draw_state&& di)
      : cube_(MOVE(cube))
      , tr_(MOVE(tr))
      , di_(MOVE(di))
  {
  }

  bool                  selected = false;
  std::optional<color3> overlap_color;

  auto const& cube() const { return cube_; }
  auto&       cube() { return cube_; }

  auto const& transform() const { return tr_; }
  auto&       transform() { return tr_; }

  auto const& draw_info() const { return di_; }
};
using CubeEntities = std::vector<CubeEntity>;

void
select_cubes_under_user_drawn_rect(log_t&, RectFloat const&, CubeEntities&, ProjMatrix const&,
                                   ViewMatrix const&, Viewport const&);

void
draw_bbox(log_t&, CameraMatrices const&, opengl::shader_type&, Transform const&,
          opengl::draw_state const&, color3 const&, opengl::draw_call_counter&);

void
draw_bboxes(log_t&, CameraMatrices const&, CubeEntities&, opengl::shader_type&,
            opengl::draw_call_counter&);

opengl::shader_type
make_terrain_program(log_t&, NumLights const&);

opengl::shader_type
make_wireframe_program(log_t&);

Cube
make_cube(rng_t&, float, float);

CubeEntities
gen_cube_entities(log_t&, size_t, RectInt const&, opengl::shader_type const&, rng_t&, bool);

template <typename U, typename D, typename PE, typename SS>
int
main_loop(SDLWindow& window, U const& update_fn, D const& draw_fn, PE const& process_event_fn,
          SS const& per_frame_state)
{
  using namespace common;
  using namespace opengl;

  SDL_Event event;
  bool      quit = false;

  WallClock wclock;

  dt_t constexpr dt = 0.01f;
  while (!quit) {
    auto const frame_state = per_frame_state();

    while ((!quit) && (0 != SDL_PollEvent(&event))) {
      quit = gl_sdl::is_quit_action(event);
      if (!quit) {
        auto const delta_time = create_dt(wclock, dt);
        process_event_fn(event, delta_time, frame_state);
      }
    }

    update_fn(dt, frame_state);

    draw_call_counter ds;
    draw_fn(ds, frame_state);

    // Update window with OpenGL rendering
    SDL_GL_SwapWindow(window.window_ptr());
  }
  return EXIT_SUCCESS;
}

template <typename U, typename D, typename PE>
int
main_loop(SDLWindow& window, U const& update_fn, D const& draw_fn, PE const& process_event_fn)
{
  struct EmptyFrameState
  {
  };
  auto const make_empty_fs = []() { return EmptyFrameState{}; };
  return main_loop(window, update_fn, draw_fn, process_event_fn, make_empty_fs);
}

} // namespace demo
