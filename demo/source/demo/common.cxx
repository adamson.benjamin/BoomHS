#include "common.hpp"

#include <boomhs/collision.hpp>
#include <math/rectangle.hpp>
#include <math/space_conversions.hpp>

#include <opengl/bind.hpp>
#include <opengl/gpu.hpp>
#include <opengl/renderer.hpp>
#include <opengl/shader.hpp>
#include <opengl/uniform.hpp>

#include <common/os.hpp>

using namespace common;
using namespace gl_sdl;
using namespace opengl;

namespace
{
auto
make_bbox(log_t& logger, shader_type const& sp, Cube const& cr)
{
  auto const vertices = VertexFactory::build_cube(cr.min, cr.max);
  return OG::copy_cube_wireframe_gpu(logger, vertices, sp.va());
}

} // namespace

namespace demo
{
void
select_cubes_under_user_drawn_rect(log_t& LOGGER, RectFloat const& mouse_rect,
                                   CubeEntities& cube_ents, ProjMatrix const& proj,
                                   ViewMatrix const& view, Viewport const& vp)
{
  namespace sc       = math::space_conversions;
  auto const cxx3l   = [](auto const& p) { return glm::vec3{p.x, p.y, 0}; };
  auto const vp_rect = vp.rect_float();

  // Determine whether a cube projected onto the given plane and another rectangle overlap.
  auto const cube_mouserect_overlap = [&](auto const& cube_entity) {
    auto const& cube = cube_entity.cube();
    auto        tr   = cube_entity.transform();

    // Take the Cube in Object space, and create a rectangle from the x/z coordinates.
    auto xz = cube.xy_rect();
    {
      auto const model                     = tr.model_matrix();
      auto const convert_to_viewport_space = [&](auto const& point) {
        return sc::object_to_viewport(point, model, proj, view, vp_rect);
      };

      auto const lt = cxx3l(xz.left_top());
      auto const rb = cxx3l(xz.right_bottom());

      auto const lt_vp = convert_to_viewport_space(lt);
      auto const rb_vp = convert_to_viewport_space(rb);
      xz               = rect::from_points(lt_vp, rb_vp);
    }

    // Conver the mouse_rect to a 3d cube
    auto const mouse_cube_hw = mouse_rect.width() / 2;
    auto const mouse_cube_hh = mouse_rect.height() / 2;

    if (mouse_cube_hw > 30) {
      LOG_DEBUG("DEBUG LINE");
    }

    glm::vec3 const mouse_cube_min{-mouse_cube_hw, +mouse_cube_hh, +10000};
    glm::vec3 const mouse_cube_max{+mouse_cube_hw, -mouse_cube_hh, -10000};

    auto const      mouse_center = mouse_rect.center();
    glm::vec3 const mouse_center_3d{mouse_center, 0};

    Transform const mouse_tr{mouse_center_3d};
    Cube const      mouse_cube{mouse_cube_min, mouse_cube_max};
    OBB const       mouse_obb = OBB::from_cube_transform(mouse_cube, mouse_tr);

    OBB const  cube_obb    = OBB::from_cube_transform(cube, tr);
    bool const obbcollides = collision::overlap(mouse_obb, cube_obb);
    if (obbcollides) {
      LOG_ERROR("OBBCOLLIDES, DAwg");
    }
    return obbcollides;

    // auto const tr2d = transform::from_3d_to_2d(tr);
    // RectTransform const rect_tr{xz, tr2d};
    // return collision::overlap(mouse_rect, rect_tr, viewport, is_2d);
  };

  for (auto& ce : cube_ents) {
    ce.selected = cube_mouserect_overlap(ce);
  }
}

void
draw_bbox(log_t& logger, CameraMatrices const& cm, shader_type& sp, Transform const& tr,
          draw_state const& dinfo, color3 const& color, draw_call_counter& ds)
{
  auto const model_matrix = tr.model_matrix();

  BIND_UNTIL_END_OF_SCOPE(logger, sp);
  uniform::set(logger, sp, "u_wirecolor", color);

  BIND_UNTIL_END_OF_SCOPE(logger, dinfo);
  auto const camera_matrix = cm.proj * cm.view;
  OR::set_mvpmatrix(logger, camera_matrix, model_matrix, sp);
  OR::draw_drawinfo(logger, ds, GL_LINES, sp, dinfo);
}

void
draw_bboxes(log_t& logger, CameraMatrices const& cm, CubeEntities& cube_ents, shader_type& sp,
            draw_call_counter& ds)
{
  for (auto& ce : cube_ents) {
    auto const& tr             = ce.transform();
    auto const& dinfo          = ce.draw_info();
    bool const  mouse_selected = ce.selected;
    bool const  overlap        = ce.overlap_color.has_value();

    auto wire_color = overlap ? *ce.overlap_color : LOC3::GREEN;
    if (mouse_selected) {
      wire_color = LOC3::RED;
    }
    draw_bbox(logger, cm, sp, tr, dinfo, wire_color, ds);
  }
}

shader_type
make_wireframe_program(log_t& logger)
{
  std::vector<attribute_pointer> const apis{
      {attribute_pointer{0, GL_FLOAT, vertex_attribute_type::POSITION, 3}}};

  auto               va = opengl::make_vertex_attribute(apis);
  opengl::shader_cfg spc{"wireframe", "wireframe.vert", "wireframe.frag", MOVE(va)};
  NumLights constexpr NUM_LIGHTS{0, 0};
  return shader::from_files(logger, MOVE(spc), NUM_LIGHTS)
      .expect("Error loading wireframe shader program");
}

shader_type
make_terrain_program(log_t& logger, NumLights const& num_lights)
{
  std::vector<attribute_pointer> const apis{
      {attribute_pointer{0, GL_FLOAT, vertex_attribute_type::POSITION, 3},
       attribute_pointer{1, GL_FLOAT, vertex_attribute_type::NORMAL, 3},
       attribute_pointer{2, GL_FLOAT, vertex_attribute_type::UV, 2}}};

  auto va = opengl::make_vertex_attribute(apis);

  opengl::shader_cfg spc{"terrain", "terrain.vert", "terrain.frag", MOVE(va)};
  return shader::from_files(logger, MOVE(spc), num_lights)
      .expect("Error loading terrain shader program");
}

Cube
make_cube(rng_t& rng)
{
  float constexpr MIN = 0, MAX = 100;
  static_assert(MIN < MAX, "MIN must be strictly less than MAX");

  auto const gen = [&]() { return rng.gen(MIN + 1, MAX); };
  auto const x = gen(), y = gen(), z = gen();

  glm::vec3 const min = glm::vec3{-x, -y, -z};
  glm::vec3 const max = glm::vec3{+x, +y, +z};
  return Cube{min, max};
}

CubeEntities
gen_cube_entities(log_t& logger, size_t const num_cubes, RectInt const& view_size,
                  shader_type const& sp, rng_t& rng, bool const is_2d)
{
  auto const gen_tr = [&]() {
    auto const x = rng.gen(static_cast<float>(view_size.width()));
    auto const y = rng.gen(static_cast<float>(view_size.height()));
    return is_2d ? glm::vec3{x, y, 0} : glm::vec3{x, 0, y};
  };

  CubeEntities cube_ents;
  FOR(i, num_cubes)
  {
    auto cube = demo::make_cube(rng);
    auto tr   = gen_tr();
    auto di   = make_bbox(logger, sp, cube);
    cube_ents.emplace_back(MOVE(cube), MOVE(tr), MOVE(di));
  }
  return cube_ents;
}

Result<TestWindow, int>
make_default_window(log_t& LOGGER, char const* title, bool const fullscreen, int const width,
                    int const height)
{
  auto const on_error = [&LOGGER](auto const& error) {
    LOG_ERROR(error);
    return Err(EXIT_FAILURE);
  };

  auto glsdl_system = gl_sdl::init_sdl(LOGGER).expect_fn(on_error);

  WindowConfig wconfig;
  wconfig.width  = width;
  wconfig.height = height;
  wconfig.title  = title;
  wconfig.flags  = 0 | SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE;
  if (fullscreen) {
    wconfig.flags |= SDL_WINDOW_FULLSCREEN;
  }

  auto       window = gl_sdl::make_window(LOGGER, glsdl_system, wconfig).expect_fn(on_error);
  TestWindow tw{MOVE(glsdl_system), MOVE(window)};
  return OK_MOVE(tw);
}

} // namespace demo
