#pragma once
#pragma GCC system_header
#pragma clang system_header

#define GLEW_STATIC
#include <GL/glew.h>

#ifdef __APPLE__
// OpenGL system headers
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>

#else

// OpenGL system headers
#include <GL/gl.h>
#include <GL/glu.h>
#endif
