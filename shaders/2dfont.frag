in vec2 v_uv;

out vec4 fragment_color;

uniform sampler2D u_sampler;
uniform vec3 u_color;

const float width = 0.5;
const float edge = 0.1;

void main()
{
  float distance  = 1.0 - texture(u_sampler, v_uv).a;
  float alpha     = 1.0 - smoothstep(width, width + edge, distance);

  fragment_color = vec4(u_color, alpha);
}
