in vec2 v_uv;

uniform sampler2D u_sampler;

// The center (in screen coordinates) of the light source
uniform DirectionalLight u_dirlight[MAX_NUM_DIRLIGHTS];
uniform int              u_numdirlights;

// The width of the blur (the smaller it is the further each pixel is going to sample)
uniform float u_blurwidth;
uniform int u_numsamples;

out vec4 fragment_color;


// Compute the fragment's color
// sample the texture u_numsamples times
vec3
compute_dirlight_contibution(DirectionalLight dlight)
{
  // compute ray from pixel to light center
  vec2 sun_pos = dlight.screenspace_pos;
  vec2 ray = v_uv - sun_pos;

  vec3 color = vec3(0.0);

  for(int j = 0; j < u_numsamples; j++)
  {
    // sample the texture on the pixel-to-center ray getting closer to the center every iteration
    float scale = 1.0 + u_blurwidth * (float(j) / float(u_numsamples - 1));

    // summing all the samples togheter
    color += (texture(u_sampler, (ray * scale) + sun_pos).xyz) / float(u_numsamples);
  }
  return color;
}

void main()
{
  // output color
  vec3 color = vec3(0.0);

  for(int i = 0; i < u_numdirlights; i++)
  {
    color += compute_dirlight_contibution(u_dirlight[i]);
  }
  fragment_color = vec4(color, 1.0);
}
