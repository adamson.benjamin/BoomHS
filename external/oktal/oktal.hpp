/*
   Mathieu Stefani, 03 mai 2016

   This header provides a Result type that can be used to replace exceptions in code
   that has to handle error.
   Result<T, E> can be used to return and propagate an error to the caller. Result<T, E> is an
   algebraic data type that can either Ok(T) to represent success or Err(E) to represent an error.
*/

// NOTE: I have modified this heavily from the source:
//    https://github.com/oktal/result
//
// Specifically: Result's can now hold and give back move-only types.

#pragma once

#include <functional>
#include <iostream>
#include <string>
#include <type_traits>
#include <variant>

namespace types
{
template <typename T>
struct Ok
{
  constexpr Ok(const T& v)
      : val(v)
  {
  }
  constexpr Ok(T&& v)
      : val(std::move(v))
  {
  }

  T val;
};

template <>
struct Ok<void>
{
};

template <typename E>
struct Err
{
  constexpr Err(const E& v)
      : val(v)
  {
  }
  constexpr Err(E&& v)
      : val(std::move(v))
  {
  }

  E val;
};
} // namespace types

template <typename T, typename CleanT = typename std::decay<T>::type>
types::Ok<CleanT>
Ok(T&& val)
{
  return types::Ok<CleanT>(std::forward<T>(val));
}

inline types::Ok<void>
Ok()
{
  return types::Ok<void>{};
}

template <typename E, typename CleanE = typename std::decay<E>::type>
types::Err<CleanE>
Err(E&& val)
{
  return types::Err<CleanE>{std::forward<E>(val)};
}

template <typename T, typename E>
struct Result;

namespace details
{

template <typename T, typename E>
auto
get_t(Result<T, E>&& r)
{
  auto&& variant = r.storage();
  return std::get<types::Ok<T>>(std::move(variant)).val;
}

template <typename T, typename E>
auto const&
get_t(Result<T, E> const& r)
{
  auto const& variant = r.storage();
  return std::get<types::Ok<T>>(variant).val;
}

template <typename T, typename E>
auto
get_e(Result<T, E>&& r)
{
  auto&& variant = r.storage();
  return std::get<types::Err<E>>(std::move(variant)).val;
}

template <typename T, typename E>
auto const&
get_e(Result<T, E> const& r)
{
  auto const& variant = r.storage();
  return std::get<types::Err<E>>(variant).val;
}

template <typename...>
struct void_t
{
  typedef void type;
};

namespace impl
{
template <typename Func>
struct result_of;

template <typename Ret, typename Cls, typename... Args>
struct result_of<Ret (Cls::*)(Args...)> : public result_of<Ret(Args...)>
{
};

template <typename Ret, typename... Args>
struct result_of<Ret(Args...)>
{
  typedef Ret type;
};
} // namespace impl

template <typename Func>
struct result_of : public impl::result_of<decltype(&Func::operator())>
{
};

template <typename Ret, typename Cls, typename... Args>
struct result_of<Ret (Cls::*)(Args...) const>
{
  typedef Ret type;
};

template <typename Ret, typename... Args>
struct result_of<Ret (*)(Args...)>
{
  typedef Ret type;
};

template <typename R>
struct ResultOkType
{
  typedef typename std::decay<R>::type type;
};

template <typename T, typename E>
struct ResultOkType<Result<T, E>>
{
  typedef T type;
};

template <typename R>
struct ResultErrType
{
  typedef R type;
};

template <typename T, typename E>
struct ResultErrType<Result<T, E>>
{
  typedef typename std::remove_reference<E>::type type;
};

template <typename R>
struct IsResult : public std::false_type
{
};
template <typename T, typename E>
struct IsResult<Result<T, E>> : public std::true_type
{
};

namespace ok
{

namespace impl
{

template <typename T>
struct Map;

template <typename T>
struct Map;

template <typename Ret, typename Cls, typename Arg>
struct Map<Ret (Cls::*)(Arg) const>
{

  static_assert(!IsResult<Ret>::value,
                "Can not map a callback returning a Result, use or_else instead");

  template <typename T, typename E, typename Func>
  static Result<T, Ret> map(Result<T, E> const& result, Func func)
  {
    if (result.is_err()) {
      auto res = func(details::get_t(result));
      return types::Err<Ret>(res);
    }

    return types::Ok<T>{details::get_e(result)};
  }

  template <typename E, typename Func>
  static Result<void, Ret> map(Result<void, E> const& result, Func func)
  {
    if (result.is_err()) {
      auto res = func(details::get_e(result));
      return types::Err<Ret>(res);
    }

    return types::Ok<void>();
  }
};

} // namespace impl

template <typename Func>
struct Map : public impl::Map<decltype(&Func::operator())>
{
};

} // namespace ok

namespace err
{

namespace impl
{

template <typename T>
struct Map;

template <typename Ret, typename Cls, typename Arg>
struct Map<Ret (Cls::*)(Arg) const> : public Map<Ret(Arg)>
{
};

template <typename Ret, typename Cls, typename Arg>
struct Map<Ret (Cls::*)(Arg)> : public Map<Ret(Arg)>
{
};

// General implementation
template <typename Ret, typename Arg>
struct Map<Ret(Arg)>
{

  static_assert(!IsResult<Ret>::value,
                "Can not map a callback returning a Result, use andThen instead");

  template <typename T, typename E, typename Func>
  static Result<T, Ret> map(Result<T, E> const& result, Func func)
  {
    static_assert(std::is_same<E, Arg>::value || std::is_convertible<E, Arg>::value,
                  "Incompatible types detected");

    if (result.is_err()) {
      auto res = func(details::get_e(result));
      return types::Err<Ret>(res);
    }
    return types::Ok<T>{details::get_t(result)};
  }

  template <typename T, typename E, typename Func>
  static Result<T, Ret> map(Result<T, E>&& result, Func func)
  {
    static_assert(std::is_same<E, Arg>::value || std::is_convertible<E, Arg>::value,
                  "Incompatible types detected");

    if (result.is_err()) {
      auto res = func(details::get_e(std::move(result)));
      return types::Err<Ret>(res);
    }
    return types::Ok<T>{details::get_t(std::move(result))};
  }
};

} // namespace impl

template <typename Func>
struct Map : public impl::Map<decltype(&Func::operator())>
{
};

template <typename Ret, typename... Args>
struct Map<Ret (*)(Args...)> : public impl::Map<Ret(Args...)>
{
};

template <typename Ret, typename Cls, typename... Args>
struct Map<Ret (Cls::*)(Args...)> : public impl::Map<Ret(Args...)>
{
};

template <typename Ret, typename Cls, typename... Args>
struct Map<Ret (Cls::*)(Args...) const> : public impl::Map<Ret(Args...)>
{
};

template <typename Ret, typename... Args>
struct Map<std::function<Ret(Args...)>> : public impl::Map<Ret(Args...)>
{
};

} // namespace err

namespace And
{

namespace impl
{

template <typename Func>
struct Then;

template <typename Ret, typename... Args>
struct Then<Ret (*)(Args...)> : public Then<Ret(Args...)>
{
};

template <typename Ret, typename Cls, typename... Args>
struct Then<Ret (Cls::*)(Args...)> : public Then<Ret(Args...)>
{
};

template <typename Ret, typename Cls, typename... Args>
struct Then<Ret (Cls::*)(Args...) const> : public Then<Ret(Args...)>
{
};

template <typename Ret, typename Arg>
struct Then<Ret(Arg)>
{
  static_assert(std::is_same<Ret, void>::value,
                "then() should not return anything, use map() instead");

  template <typename T, typename E, typename Func>
  static Result<T, E> then(Result<T, E> const& result, Func func)
  {
    if (result.is_ok()) {
      return func(details::get_t(result));
    }
    return result;
  }
};

template <typename Ret>
struct Then<Ret(void)>
{
  static_assert(std::is_same<Ret, void>::value,
                "then() should not return anything, use map() instead");

  template <typename T, typename E, typename Func>
  static Result<T, E> then(Result<T, E> const& result, Func func)
  {
    static_assert(std::is_same<T, void>::value,
                  "Can not call a void-callback on a non-void Result");

    if (result.is_ok()) {
      func();
    }

    return result;
  }
};

} // namespace impl

template <typename Func>
struct Then : public impl::Then<decltype(&Func::operator())>
{
};

template <typename Ret, typename... Args>
struct Then<Ret (*)(Args...)> : public impl::Then<Ret(Args...)>
{
};

template <typename Ret, typename Cls, typename... Args>
struct Then<Ret (Cls::*)(Args...)> : public impl::Then<Ret(Args...)>
{
};

template <typename Ret, typename Cls, typename... Args>
struct Then<Ret (Cls::*)(Args...) const> : public impl::Then<Ret(Args...)>
{
};

} // namespace And

namespace Or
{

namespace impl
{

template <typename Func>
struct Else;

template <typename Ret, typename... Args>
struct Else<Ret (*)(Args...)> : public Else<Ret(Args...)>
{
};

template <typename Ret, typename Cls, typename... Args>
struct Else<Ret (Cls::*)(Args...)> : public Else<Ret(Args...)>
{
};

template <typename Ret, typename Cls, typename... Args>
struct Else<Ret (Cls::*)(Args...) const> : public Else<Ret(Args...)>
{
};

template <typename T, typename F, typename Arg>
struct Else<Result<T, F>(Arg)>
{

  template <typename E, typename Func>
  static Result<T, F> or_else(Result<T, E> const& result, Func func)
  {
    static_assert(std::is_same<E, Arg>::value || std::is_convertible<E, Arg>::value,
                  "Incompatible types detected");

    if (result.is_err()) {
      auto res = func(details::get_e(result));
      return res;
    }

    return types::Ok<T>(details::get_t(result));
  }

  template <typename E, typename Func>
  static Result<void, F> or_else(Result<void, E> const& result, Func func)
  {
    if (result.is_err()) {
      auto res = func(details::get_e(result));
      return res;
    }

    return types::Ok<void>();
  }
};

template <typename T, typename F>
struct Else<Result<T, F>(void)>
{

  template <typename E, typename Func>
  static Result<T, F> or_else(Result<T, E> const& result, Func func)
  {
    static_assert(std::is_same<T, void>::value,
                  "Can not call a void-callback on a non-void Result");

    if (result.is_err()) {
      return func();
    }

    return types::Ok<T>{details::get_t(result)};
  }

  template <typename E, typename Func>
  static Result<void, F> or_else(Result<void, E> const& result, Func func)
  {
    if (result.is_err()) {
      auto res = func();
      return res;
    }

    return types::Ok<void>();
  }
};

} // namespace impl

template <typename Func>
struct Else : public impl::Else<decltype(&Func::operator())>
{
};

template <typename Ret, typename... Args>
struct Else<Ret (*)(Args...)> : public impl::Else<Ret(Args...)>
{
};

template <typename Ret, typename Cls, typename... Args>
struct Else<Ret (Cls::*)(Args...)> : public impl::Else<Ret(Args...)>
{
};

template <typename Ret, typename Cls, typename... Args>
struct Else<Ret (Cls::*)(Args...) const> : public impl::Else<Ret(Args...)>
{
};

} // namespace Or

namespace Other
{

namespace impl
{

template <typename Func>
struct Wise;

template <typename Ret, typename... Args>
struct Wise<Ret (*)(Args...)> : public Wise<Ret(Args...)>
{
};

template <typename Ret, typename Cls, typename... Args>
struct Wise<Ret (Cls::*)(Args...)> : public Wise<Ret(Args...)>
{
};

template <typename Ret, typename Cls, typename... Args>
struct Wise<Ret (Cls::*)(Args...) const> : public Wise<Ret(Args...)>
{
};

template <typename Ret, typename Arg>
struct Wise<Ret(Arg)>
{

  template <typename T, typename E, typename Func>
  static Result<T, E> otherwise(Result<T, E> const& result, Func func)
  {
    static_assert(std::is_same<E, Arg>::value || std::is_convertible<E, Arg>::value,
                  "Incompatible types detected");

    static_assert(std::is_same<Ret, void>::value,
                  "callback should not return anything, use map_error() for that");

    if (result.is_err()) {
      func(details::get_e(result));
    }
    return result;
  }
};

} // namespace impl

template <typename Func>
struct Wise : public impl::Wise<decltype(&Func::operator())>
{
};

template <typename Ret, typename... Args>
struct Wise<Ret (*)(Args...)> : public impl::Wise<Ret(Args...)>
{
};

template <typename Ret, typename Cls, typename... Args>
struct Wise<Ret (Cls::*)(Args...)> : public impl::Wise<Ret(Args...)>
{
};

template <typename Ret, typename Cls, typename... Args>
struct Wise<Ret (Cls::*)(Args...) const> : public impl::Wise<Ret(Args...)>
{
};

} // namespace Other

template <typename T, typename E, typename Func,
          typename Ret = Result<
              typename details::ResultOkType<typename details::result_of<Func>::type>::type, E>>
Ret
map(Result<T, E> const& result, Func func)
{
  return ok::Map<Func>::map(result, func);
}

template <typename T, typename E, typename Func,
          typename Ret = Result<
              T, typename details::ResultErrType<typename details::result_of<Func>::type>::type>>
Ret
map_error(Result<T, E> const& result, Func func)
{
  return err::Map<Func>::map(result, func);
}

template <typename T, typename E, typename Func,
          typename Ret = Result<
              T, typename details::ResultErrType<typename details::result_of<Func>::type>::type>>
Ret
map_error(Result<T, E>&& result, Func func)
{
  return err::Map<Func>::map(std::move(result), func);
}

template <typename T, typename E, typename Func>
Result<T, E>
then(Result<T, E> const& result, Func func)
{
  return And::Then<Func>::then(result, func);
}

template <typename T, typename E, typename Func>
Result<T, E>
otherwise(Result<T, E> const& result, Func func)
{
  return Other::Wise<Func>::otherwise(result, func);
}

template <typename T, typename E, typename Func,
          typename Ret = Result<
              T, typename details::ResultErrType<typename details::result_of<Func>::type>::type>>
Ret
or_else(Result<T, E> const& result, Func func)
{
  return Or::Else<Func>::or_else(result, func);
}

struct ok_tag
{
};
struct err_tag
{
};

} // namespace details

namespace oktal_concept
{

template <typename T, typename = void>
struct EqualityComparable : std::false_type
{
};

template <typename T>
struct EqualityComparable<
    T, typename std::enable_if<true, typename details::void_t<decltype(
                                         std::declval<T>() == std::declval<T>())>::type>::type>
    : std::true_type
{
};

} // namespace oktal_concept

template <typename Tv, typename Ev>
class [[nodiscard]] Result
{
public:
  using T = typename std::decay<Tv>::type;
  using E = typename std::decay<Ev>::type;

private:
  static_assert(!std::is_same<E, void>::value, "void error type is not allowed");
  using storage_type = std::variant<types::Ok<T>, types::Err<E>>;

private:
  storage_type storage_;

public:
  Result(types::Ok<T> ok)
      : storage_(std::move(ok))
  {
  }

  Result(types::Err<E> err)
      : storage_(std::move(err))
  {
  }

  Result(Result && other)
      : storage_(std::move(other))
  {
    other.storage_ = std::monostate{};
  }

  Result(Result const& other)
      : storage_(other.storage_)
  {
  }

public:
  bool is_ok() const { return storage_.index() == 0; }
  bool is_err() const { return storage_.index() == 1; }

  auto&&      storage()&& { return std::move(storage_); }
  auto&       storage()& { return storage_; }
  auto const& storage() const& { return storage_; }

public:
#define UNWRAP_RETURN_TYPE typename std::enable_if<!std::is_same<U, void>::value, U>::type

  template <typename U = T>
  UNWRAP_RETURN_TYPE const& unwrap() const
  {
    if (is_ok()) {
      return details::get_t(*this);
    }

    std::fprintf(stderr, "Attempting to unwrap an error Result\n");
    std::terminate();
  }

  template <typename U = T>
  UNWRAP_RETURN_TYPE unwrap()
  {
    if (is_ok()) {
      return details::get_t(std::move(*this));
    }

    std::fprintf(stderr, "Attempting to unwrap an error Result\n");
    std::terminate();
  }
#undef UNWRAP_RETURN_TYPE

public:
  T expect(const char* str) const
  {
    if (is_err()) {
      std::fprintf(stderr, "%s\n", str);
      std::terminate();
    }
    return unwrap();
  }

  T expect(std::string const& str) const { return expect(str.c_str()); }

  T expect(const char* str)
  {
    if (!is_ok()) {
      std::fprintf(stderr, "%s\n", str);
      std::terminate();
    }
    return unwrap();
  }

  T expect(std::string const& str) { return expect(str.c_str()); }

  template <typename FN>
  T expect_fn(FN const& fn)
  {
    if (!is_ok()) {
      fn(details::get_e(*this));
      std::terminate();
    }
    return unwrap();
  }

public:
  template <typename Func,
            typename Ret = Result<
                typename details::ResultOkType<typename details::result_of<Func>::type>::type, E>>
  Ret map(Func func) const
  {
    return details::map(*this, func);
  }

  template <typename Func,
            typename Ret = Result<
                T, typename details::ResultErrType<typename details::result_of<Func>::type>::type>>
  Ret map_error(Func func) const
  {
    return details::map_error(*this, func);
  }

  template <typename Func,
            typename Ret = Result<
                T, typename details::ResultErrType<typename details::result_of<Func>::type>::type>>
  Ret map_error(Func func)
  {
    return details::map_error(std::move(*this), func);
  }

  template <typename Func>
  Result<T, E> then(Func func) const
  {
    return details::then(*this, func);
  }

  template <typename Func>
  Result<T, E> otherwise(Func func) const
  {
    return details::otherwise(*this, func);
  }

  template <typename Func,
            typename Ret = Result<
                T, typename details::ResultErrType<typename details::result_of<Func>::type>::type>>
  Ret or_else(Func func) const
  {
    return details::or_else(*this, func);
  }

  template <typename Func,
            typename Ret = Result<
                T, typename details::ResultErrType<typename details::result_of<Func>::type>::type>>
  Ret or_else(Func func)
  {
    return details::or_else(std::move(*this), func);
  }

  template <typename U = T>
  typename std::enable_if<!std::is_same<U, void>::value, U>::type unwrapOr(const U& defaultValue)
      const
  {
    if (is_ok()) {
      return details::get_t(*this);
    }
    return defaultValue;
  }

  auto unwrap_error() const
  {
    if (is_err()) {
      return details::get_e(*this);
    }

    std::fprintf(stderr, "Attempting to unwrap_error an ok Result\n");
    std::terminate();
  }

  auto unwrap_error()
  {
    if (is_err()) {
      return details::get_e(std::move(*this));
    }

    std::fprintf(stderr, "Attempting to unwrap_error an ok Result\n");
    std::terminate();
  }

  operator bool() const { return is_ok(); }
};

template <typename T, typename E>
bool
operator==(Result<T, E> const& lhs, Result<T, E> const& rhs)
{
  static_assert(oktal_concept ::EqualityComparable<T>::value,
                "T must be EqualityComparable for Result to be comparable");
  static_assert(oktal_concept ::EqualityComparable<E>::value,
                "E must be EqualityComparable for Result to be comparable");
  return lhs.storage() == rhs.storage();
}

template <typename T, typename E>
bool
operator==(Result<T, E> const& lhs, types::Ok<T> ok)
{
  static_assert(oktal_concept ::EqualityComparable<T>::value,
                "T must be EqualityComparable for Result to be comparable");

  return lhs.storage() == ok.val;
}

template <typename E>
bool
operator==(Result<void, E> const& lhs, types::Ok<void>)
{
  return lhs.is_ok();
}

template <typename T, typename E>
bool
operator==(Result<T, E> const& lhs, types::Err<E> err)
{
  static_assert(oktal_concept ::EqualityComparable<E>::value,
                "E must be EqualityComparable for Result to be comparable");
  if (!lhs.is_err())
    return false;

  return lhs.storage() == err.val;
}
