[_TOC_]

NOTE: Please upvote this [gitlab issue](https://gitlab.com/gitlab-org/gitlab-ce/issues/45388) to get automatic TOC generation prioritized.

# Developer Information

The project is written in modern C++(17), and currently uses the OpenGL API directly for rendering. The code is structured so that another backend can be swapped in someday without rewriting everything.

- The entities within the game are managed using an [ECS system](https://github.com/skypjack/entt).
- A full list of external dependencies can be seen from the [external](./external/)  directory.
- The codebase itself is completely cross-platform.
  - It has been successfully compiled/ran on Linux using [clang](http://clang.org/) and [gcc](https://gcc.gnu.org/).
  - Work is currently [on-going](https://gitlab.com/adamson.benjamin/BoomHS/tree/windows) to support Visual Studio 2017 on windows.

## Getting Started

### Linux (macOS is untested, but could/should work similarly)

The general procedure for a developer getting started for the first time is straightforward. Fetch the repository locally, invoke a script to install compilation dependencies, run cmake, run the program once compilation finishes.

```bash
git clone https://gitlab.com/adamson.benjamin/BoomHS.git
cd BoomHS
sudo scripts/install-dependencies.bash
scripts/bootstrap.bash
scripts/build-and-run.bash
```



EDIT: This next hint doesn't work right now.

gitlab issue: https://gitlab.com/adamson.benjamin/BoomHS/issues/16

~~NOTE: If you pipe yes into the install dependencies script, you can automate the pressing of Y, and just install all the dependencies (which will take some time, as it compiles both clang, gcc, and cmake from source)~~

```bash
##yes | sudo scripts/install-dependencies.bash
```



Explanation:

Clone the git repository locally. You will need to have to have [git](https://git-scm.com/) installed on your machine.

Install dependencies. A bash script is provided in the [scripts/](./scripts/) directory that will interactively install the dependencies locally necessary to compile the project.

Bootstrap the project (setup cmake database).
* Bootstrapping the project is handled through a script. This script takes care of enumerating all
  the source files in the project and passing them to cmake.
* The bootstrapping script supports some command line arguments.
  - Build System: The [-n] flag switches build systems from the default (Unix Makfiles) to the [Ninja](https://ninja-build.org/) build system (assumes installed locally).
  - Static Analysis: The [-a] flag enables static analysis information to be linked with the binary
    during compilation.
  - Debug/Release: The [-r] flag will instruct CMake to build a release binary (no debugging
    symbols). The default build type is Debug.
  - List of supported commands: The [-h] flag will display a list of all command line arguments
    supported by the script, and then immediately exit.

4. Build and run the program.

### Windows

![cone](./assets/cone.png?raw=true) `NOTE: Windows support is currently being worked on. This section of documentation is incomplete.`

1. Install [cmake (version 3.8 or greater)](https://cmake.org/) and add it your PATH environment variable [click here](https://www.computerhope.com/issues/ch000549.htm) for more information.
2. Install [Ninja (c++ build system)](https://ninja-build.org/).
3. Install [Visual Studio 2017](https://visualstudio.microsoft.com/free-developer-offers/) (the free version is sufficient).

### NOTES

- There are helpful scripts in the [scripts/](./scripts/) directory that do lots of helpful things (from code formatting to executing the debugger after setting up environment variables).
- Source-code formatting is done by invoking clang-format, use the [format-code](scripts/format-code.bash) format script inside the scripts/ directory.
- When adding a new source file to the project, you must run the [bootstrap](scripts/boostrap.bash) script again.

## Developer Guidelines

- Limit the code exposed through header files, try and put as much code as possible inside translation units. <u>If a function is never called outside of a translation unit, do not expose it in a header file</u>. This makes it easier to re-organize the code or change the project architecture as new development proceeds.
  - For testing purposes, consider separating code into smaller functions/objects so the absolute minimum interface is exposed via header.
  - Templates make this difficult. Use a nested namespace (ie: detail) namespaces to limit exposed interfaces. Detail namespaces are assumed to not be called outside of the header file where the interface is exposed.
  - Use the anonymous namespace inside a translation unit when possible, this separates the header implementation from the algorithm implementation.
- Use composition over inheritance, except when necessary to promote DRY. This will keep objects decoupled.
  - So far neither virtual inheritance or multiple inheritance have not been necessary in any manner (not including code in external libraries/dependencies). Keep it this way, it prevents a large class of possible bugs/confusion and makes debugging easier.
  - Sometimes a macro inside a translation unit is a better choice than inheriting from a base class.
- Limit heap usage.
  - Use the stack for your objects.
  - So far invoking raw operator "new" has not been necessary, this should be maintained moving forward unless a discussion results in it being considered necessary.
- Don't call OpenGL functions directly in the game logic, use the [opengl/](./include/opengl/) wrapper functions provided, or add one if needed. This will prevent mixing application logic and rendering logic, allowing easier porting to other graphics libraries in the future (DirectX, Vulkan, etc...)
