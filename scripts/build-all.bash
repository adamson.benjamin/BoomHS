#!/usr/bin/env bash
set -ex
source "scripts/common-build.bash"

cd ${BUILD}
time cmake --build .
cd ..

source "scripts/common-postbuild.bash"
