#!/usr/bin/env bash
source "scripts/common.bash"

# NOTE: I used to do tweaking to LD_LIBARARY_PATH here, but it's no longer needed since libstdc++
# is now linked statically. I'm leaving this file here in case I want to do something later, no
# harm in removing at any point.
