#!/usr/bin/env bash
set -ex
source "scripts/common-build.bash"

cd ${BUILD}
time (
  cmake --build . --target viewport_mouse_raycast_boxselection
  cmake --build . --target seperating_axis_theorem
  cmake --build . --target debug-membug
  cmake --build . --target terrain_viewer
  cmake --build . --target two-windows
  )
cd ..

source "scripts/common-postbuild.bash"
