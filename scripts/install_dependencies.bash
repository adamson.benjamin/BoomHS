#!/usr/bin/env bash
set -e
source "scripts/common.bash"

####################################################################################################
# TODO: Investigate removing the requirement to run the script as root.

####################################################################################################
# GLOBAL VARIABLES
CWD=$(pwd)

####################################################################################################

# Cleanup function that ensures all source files will be cleaned up no matter how the script exits.
function cleanup {
  print_yellow ""
  print_yellow "#################################################################################"
  print_yellow "Executing cleanup function, removing ./temp/ directory ..."

  if ! [ -n "$KEEP_BUILD_ARTIFACTS" ]
  then
    print_white "Cleaning up ./temp/ directory ..."
    cd $CWD
    rm  -rf ./temp/
    print_white "done cleaning up!"
  else
    print_cyan "Keeping ./temp/ directory"
  fi

  if [ -n "$SHUTDOWN_AFTER_INSTALL" ]
  then
    print_yellow "User specified specific compiler, shutting down after installing dependencies."
    print_yellow "Shutting down system now!"
    shutdown now
  fi

  print_yellow "cleanup finished!"
  print_white ""
  print_white "script closing. Goodbye!"
}

function ask_user_if_they_want_to_install_from_source() {
  local NAME=$1
  read -r -p "Do you want to install $NAME (trunk) from source? [y/N] " response
  case "$response" in
    ([yY][eE][sS] | [yY])
          return 0
          ;;
      (*)
          ;;
  esac
  return 1
}

# Determine whether software (with a minimum version) is already installed.
function found_software_already_installed() {
  local NAME=$1
  local MIN_REQUIRED=$2
  local CURRENT=$3

  if [ "" != "$CURRENT" ];
  then
    if version_gte $MIN_REQUIRED $CURRENT;
    then
      return 0
    fi
  fi

  return 1
}

function software_is_not_installed_and_user_wants_it_installed() {
  local NAME=$1
  local MIN_REQUIRED=$2
  local CURRENT=$3
  if [ -n "$FORCE_INSTALL_SOFTWARE_FROM_SOURCE" ];
  then
    if ask_user_if_they_want_to_install_from_source "$NAME";
    then
      return 0
    fi
  elif found_software_already_installed "$NAME" "$MIN_REQUIRED" "$CURRENT";
    then
    print_green "Found $NAME version $CURRENT already installed!\n"
  else
    print_yellow "Did not find $NAME with (minimum) required version ($MIN_REQUIRED) installed."
    if ask_user_if_they_want_to_install_from_source "$NAME";
    then
      return 0
    fi
  fi

  return 1
}

# Prompt the user if they want to install cmake from source, if we can't detect it on their system
# (using $PATH)
function install_cmake() {
  local NAME="cmake"
  local MIN_REQUIRED="3.15"

  if type "$NAME" > /dev/null 2>&1;
  then
    local CURRENT="$(cmake --version | grep version | cut -d ' ' -f3)"
  fi

  if software_is_not_installed_and_user_wants_it_installed $NAME $MIN_REQUIRED $CURRENT;
  then
    wget https://github.com/Kitware/CMake/releases/download/v3.15.5/cmake-3.15.5.tar.gz
    tar -xvzf cmake-3.15.5.tar.gz
    cd cmake-3.15.5

    # BUG work-around (force gcc-7, seems to work)
    export CC=$(which gcc-7)
    export CXX=$(which g++-7)

    ./configure
    make
    sudo make install
    cd ../

    # BUG work-around (reset gcc to whatever)
    export CC=$(which gcc)
    export CCX=$(which g++)
  fi
}

function install_gcc() {
  local GCC_STRING=""
  local MIN_VERSION_REQUIRED=""
  local MOST_UP_TO_DATE_VERSION=""
  find_gcc GCC_STRING MIN_VERSION_REQUIRED MOST_UP_TO_DATE_VERSION

  # If none of the installed versions of GCC work, then offer to install from source.
  if software_is_not_installed_and_user_wants_it_installed $GCC_STRING $MIN_VERSION_REQUIRED $MOST_UP_TO_DATE_VERSION;
  then
    svn checkout svn://gcc.gnu.org/svn/gcc/trunk gcc-source
    mkdir gcc-build
    cd gcc-source
    ./contrib/download_prerequisites
    cd ../
    cd gcc-build
    ../gcc-source/configure --disable-multilib
    make
    sudo make install
    cd ../
  fi
}

function install_boost() {
  # DOCS used
  # https://wiki.tiker.net/BoostInstallationHowto
  # https://boostorg.github.io/build/tutorial.html
  # http://www.linuxfromscratch.org/blfs/view/svn/general/boost.html
  if ask_user_if_they_want_to_install_from_source "libboost";
  then
    wget https://dl.bintray.com/boostorg/release/1.71.0/source/boost_1_71_0.tar.bz2
    tar -xvf boost_1_71_0.tar.bz2
    cd boost_1_71_0
    ./bootstrap.sh --prefix=/usr
    sudo ./b2 stage threading=multi link=shared variant=release install
  fi
}

function install_clang() {
  local CLANG_STRING=""
  local MOST_UP_TO_DATE_VERSION=""
  local MIN_VERSION_REQUIRED=""
  local IS_APPLE_CLANG=false

  find_clang CLANG_STRING MIN_VERSION_REQUIRED MOST_UP_TO_DATE_VERSION IS_APPLE_CLANG
  if $IS_APPLE_CLANG
  then
    print_yellow "Found Apple's Clang installed. Script currently not smart enough to install clang with Apple's clang installed, and Apple's clang cannot compile this project."
    return
  fi

  if software_is_not_installed_and_user_wants_it_installed $CLANG_STRING $MIN_VERSION_REQUIRED $MOST_UP_TO_DATE_VERSION;
  then
    git clone https://github.com/llvm/llvm-project.git
    cd llvm-project
    mkdir build
    cd build
    cmake -DCMAKE_BUILD_TYPE=Release -DLLVM_ENABLE_PROJECTS=clang -G "Unix Makefiles" ../llvm
    make
    sudo make install
  fi
}

function install_libsoil() {
  if ask_user_if_they_want_to_install_from_source "libsoil";
  then
    git clone https://github.com/kbranigan/Simple-OpenGL-Image-Library.git
    cd Simple-OpenGL-Image-Library
    make
    sudo make install
  fi
}

function install_libtrace() {
  if ask_user_if_they_want_to_install_from_source "libtrace";
  then
    git clone https://github.com/ianlancetaylor/libbacktrace.git
    cd libbacktrace
    ./configure
    make
    sudo make install
    sudo ldconfig
  fi
}

function install_software_for_osx() {
  print_white "Checking if Brew is installed ..."
  if ! type "brew" > /dev/null 2>&1;
    then
    if ask_user_if_they_want_to_install_from_source "brew";
    then
      /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
    fi
  fi

  echo "Installing required software/libraries ..."
  brew install ninja
  brew install cmake
  brew install glew
  print_green "Finished installing required software and libraries for osx!"
}

function install_software_for_linux() {
  # Install required softwares.
  sudo apt-get install ninja-build

  # Install basic libraries
  sudo apt-get install libc++-dev
  sudo apt-get install libalut-dev
  sudo apt-get install libglew-dev
  sudo apt-get install libdw-dev
  sudo apt-get install binutils-dev
  sudo apt-get install libsdl2-dev
  sudo apt-get install libsoil-dev

  # dev-version of standard libaries (for GDB and clang)
  # The following versions were in use on my dev pc, so I grabbed dev versions of them all
  # `dpkg --list | grep libstdc++`
  sudo apt-get install libc++-dev
  sudo apt-get install libdw-dev
  sudo apt-get install libgl-dev
  print_green "Finished installing software using apt-get!"
}

# TODO: windows detection, right now windows will run down the else above which won't work!
function install_software_from_package_managers() {
  if [[ "$OSTYPE" == "darwin"* ]]; then
    # Mac OSX
    print_white "Detected MAC OSX operating system, installing dependencies ..."
    install_software_for_osx
  elif [[ "$OSTYPE" == "msys" ]]; then
    # Windows OS
    print_blue "WINDOWS DETECTED"
    exit
  else
    # Linux OS
    print_cyan "Detected LINUX operating system, installing dependencies ..."
    install_software_for_linux
  fi
}

function load_commandline_args() {
  while getopts ":dfhst" opt; do
    case ${opt} in
      d )
        export PRINT_SCRIPT_DEBUG="Yes"
        ;;
      f )
        export FORCE_INSTALL_SOFTWARE_FROM_SOURCE="Yes"
        ;;
      s )
        export SHUTDOWN_AFTER_INSTALL="Yes"
        ;;
      t )
        export KEEP_BUILD_ARTIFACTS="Yes"
        ;;
      \h )
        print_white "Help options for installing dependencies script."
        print_yellow "[-d] Print extended script debugging information."
        print_yellow "[-f] Prompt user to install all software from source, ingoring current
        installed versions."
        print_yellow "[-s] Shutdown the system after running this script."
        print_yellow "[-t] Don't delete the ./temp/ directory (contains build artifacts for
        dependencies). You will need to delete (or move) the ./temp/ directory before subsequent
        executions may proceed."
        print_yellow "[-h] See this message."

        print_red "Please run again without the -h flag."
        return 0
        ;;
    esac
  done
  shift $((OPTIND -1))

  return 1
}

# Tries establishing an internet connection using the 'ping' tool.
# returns 0 on success
# return 1 on failure
function check_interwebs_connection() {
  local INTERWEBS_ADDRESS="www.google.com"
  print_yellow "Checking internet connectivity with a 1 second timeout ..."
  ping -c 1 $INTERWEBS_ADDRESS>>/dev/null

  if [ $? -ne  0 ];
  then
    print_yellow "Error: Unable to connect to internet. Will try again with a longer timeout!"
  fi

  if [ $? -ne  0 ]
  then
    ping -c 5 $INTERWEBS_ADDRESS>>/dev/null
    exit 1;
  fi
  return 0
}

# Returns 0 if the user is running as root, 1 otherwise.
function check_if_running_as_root_user() {
  if [[ $(id -u) -ne 0 ]];
  then
    # not root
    return 1
  fi
  # root
  return 0
}

####################################################################################################
####################################################################################################
# Logic starts here.
if check_if_running_as_root_user;
then
  print_red "Must not run as root." 
  exit 1
fi

####################################################################################################
if load_commandline_args $1;
then
  exit 1
fi

####################################################################################################
# Install a function that bash will call when the script quits, in any case (success or failure).
mkdir temp
cd temp
trap cleanup EXIT

print_yellow "Help options for this script can be viewed by passing the -h flag."
print_white "This script will install a number of libraries/software on your behalf."
print_white "Some applications/libraries are installed using the OS package manager. Some
applications/libraries need be to installed from source. In this case, the script first does it's
best to detect if the software is already installed on your system (by seeing if it can find the
software in your \$PATH environment variable. If the script does not detect the required software,
it will ask you if you would like to install the software from source.\n\n"

print_white "All source files are downloaded into a ./temp/ directory, which is automatically
cleaned up when the script exits. This behavior can be overriden, see the command line flags for
exactly how to do this (invoke this script with -h flag).\n\n"

read -n 1 -s -r -p "Press any key to continue. Ctrl+c to quit."
print_white ""

####################################################################################################
cat $CWD/scripts/figlet_install_dependencies.txt
print_white "\n"

print_white "Install Depenencies Script begining ..."
print_yellow "NOTE: Depending on your system, and which software you need to compile from source,
this could take quite some time."

print_white ".................."
print_white "...................................."
print_white "........................................................................"
print_white "........................................................."
print_white "...................................."
print_white ".................."
print_white ""

####################################################################################################
# If the user requested all script info to be printed, do it!
if [ -n "$PRINT_SCRIPT_DEBUG" ]
then
  set -x
fi

####################################################################################################
if ! check_interwebs_connection;
then
  print_red "Error: Unable to connect to internet (with 5 second timeout)."
  print_red "This script requires internet. Script will exit."
  exit 1
else
  print_green "Successfully pinged the internet!\n"
fi

####################################################################################################
print_cyan "Installing software using operating system's native package manager ..."
install_software_from_package_managers
print_green "Finished installing software using operating system's package manager!\n"

####################################################################################################
print_cyan "Checking for software that needs to be installed from source ..."

# We need cmake
# We do this before prompting for installing GCC due to a bug in gcc9
# https://stackoverflow.com/questions/52663287/glibcxx-3-4-26-not-found

if [ -n "$FORCE_INSTALL_SOFTWARE_FROM_SOURCE" ];
then
  print_green "'f' flag detected!\n"
  print_yellow "reminder: The -f flags prompts you to install all software from source, ingoring current installed versions."
fi

install_cmake
install_gcc
install_clang
install_libsoil
install_libtrace
install_boost

print_green "Finished installing software needing to be installed from source!"
print_green "All dependencies installed successfully!"
