#!/usr/bin/env bash
set -ex
source "scripts/common-build.bash"

cd ${BUILD}
time cmake --build . --target main
cd ..

source "scripts/common-postbuild.bash"
