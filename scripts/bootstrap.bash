#!/usr/bin/env bash
source "scripts/common.bash"

####################################################################################################
# GLOBAL VARIABLES
CWD=$(pwd)

DEBUG_OR_RELEASE="Debug"
STATIC_OR_SHARED="STATIC"
OPENGL_RUNTIME_CHECKS="Enabled"
####################################################################################################

# Load all (supported) user-arguments supported by the scripts.
function load_commandline_args() {
while getopts ":aceghlmorsvuz" opt; do
  case ${opt} in
      # Define all sanitizers here:
      a )
      export SANITIZER_FLAGS="-fsanitize=address"
        ;;
      l )
      export SANITIZER_FLAGS="-fsanitize=leak"
        ;;
      m )
      export SANITIZER_FLAGS="-fsanitize=memory"
        ;;
      u )
      export SANITIZER_FLAGS="-fsanitize=undefined"
        ;;

      # Define other options here:
      c )
        echo "User specified clang compiler ..."
        if ! select_clang_compiler;
        then
          exit 1
        fi
        ;;
      e )
        export LOG_LEVEL="Error"
        ;;
      g )
        echo "User specified gcc compiler ..."
        if ! select_gcc_compiler;
        then
          exit 1
        fi
        ;;
      o )
      export OPENGL_RUNTIME_CHECKS="Disabled"
        ;;
      r )
        export DEBUG_OR_RELEASE="Release"
        ;;
      s )
        export STATIC_OR_SHARED="SHARED"
        ;;
      v )
        export VERBOSE_COMPILER_OUTPUT_FLAG="-v"
        ;;
      z )
        export BUILD_GENERATOR="Unix Makefiles"
        ;;
      \h )
        echo "Help options for bootstrapping process."
        print_white "[-c] Force the Clang compiler toolchain. Error if unavailable."
        print_white "[-g] Force the GCC compiler toolchain. Error if unavailable"

        print_yellow "[-a] To enable Address Sanitizer. https://github.com/google/sanitizers/wiki/AddressSanitizer"
        print_yellow "[-m] To enable Leak (stand-alone) Sanitizer. https://clang.llvm.org/docs/LeakSanitizer.html"
        print_yellow "[-m] To enable Memory Sanitizer. https://clang.llvm.org/docs/MemorySanitizer.html"

        print_yellow "[-e] Set log level to Error (Debug only)"
        print_yellow "[-o] Disable OpenGL runtime checks (Debug only)"

        print_yellow "[-r] To switch from Debug to Release mode. (Debug is the default)"
        print_yellow "[-s] Force SHARED static libraries. (STATIC is the default)"
        print_yellow "[-v] Tell the compiler to use verbose mode when compiling the program."
        print_yellow "[-u] To enable Undefined-behavior Sanitizer. https://clang.llvm.org/docs/UndefinedBehaviorSanitizer.html"
        print_yellow "[-z] To to to use the default 'Make' build-system."

        print_yellow "[-h] See this help message."
        print_yellow "Please run again without the -h flag."
        return 0
        ;;
    esac
  done
  shift $((OPTIND -1))

  return 1
}

function select_gcc_compiler() {
  print_white "Attempting to find 'gcc' CXX compiler ..."

  local GCC_STRING_LOCAL=""
  local MIN_REQUIRED_LOCAL=""
  local GCC_VERSION_LOCAL=""
  find_gcc GCC_STRING_LOCAL MIN_REQUIRED_LOCAL GCC_VERSION_LOCAL

  if ! version_gte $MIN_REQUIRED_LOCAL $GCC_VERSION_LOCAL
  then
    print_yellow "Compiler ($GCC_STRING_LOCAL version: $GCC_VERSION_LOCAL found, gcc version $MIN_REQUIRED_LOCAL required) out of date."
    return 1;
  fi

  export COMPILER=$GCC_STRING_LOCAL
  export COMPILER_VERSION=$GCC_VERSION_LOCAL
  export BUILD_GENERATOR="Ninja"
  return 0;
}

function select_clang_compiler() {
  print_white "Attempting to find 'clang' CXX compiler ..."

  local CLANG_STRING_LOCAL=""
  local MIN_REQUIRED_LOCAL=""
  local CLANG_VERSION_LOCAL=""
  local IS_APPLE_CLANG=false

  find_clang CLANG_STRING_LOCAL MIN_REQUIRED_LOCAL CLANG_VERSION_LOCAL IS_APPLE_CLANG

  if $IS_APPLE_CLANG
  then
    print_yellow "Found Apple's Clang installed. Script currently doesn't know how to use Apple's clang. Not using clang."
    return 1;
  elif ! version_gte $MIN_REQUIRED_LOCAL $CLANG_VERSION_LOCAL
  then
    print_yellow "Compiler ($CLANG_STRING_LOCAL version: $CLANG_VERSION_LOCAL found, clang version $MIN_REQUIRED_LOCAL required) out of date."
    return 1;
  fi


  if [ "$OSTYPE" == "msys" ];
  then
    export BUILD_GENERATOR='Visual Studio 16 2019'
    export BUILD_TOOLCHAIN='-T ClangCL'
  elif [ "$OSTYPE" == "linux-gnu" ];
  then
    export BUILD_GENERATOR='Ninja'
    export BUILD_TOOLCHAIN=''
  fi

  export COMPILER=$CLANG_STRING_LOCAL
  export COMPILER_VERSION=$CLANG_VERSION_LOCAL
  return 0;
}

function find_available_compiler() {
  if [ -n "$COMPILER" ]
  then
    print_cyan "User specified specific compiler, skipping ..."
    return 0;
  fi

  if select_clang_compiler;
  then
    print_green "Success! $COMPILER $COMPILER_VERSION found!"
    return 0;
  fi

  if select_gcc_compiler;
  then
    print_green "Success! $COMPILER $COMPILER_VERSION found!"
    return 0;
  fi

  print_red "Error: Could not determine compatible compiler on your system."
  print_cyan "Did you try running install_dependencies.bash?"
  return 1;
}

####################################################################################################
# Logic starts here.
print_yellow "Help options for this script can be viewed by passing the -h flag."
print_white "This script will bootstrap your project for cmake automatically. Run this script if
you need cmake to pickup new source files. All build artifacts (the entire directory) will be
deleted."
print_yellow "After executing this script, the entire project will need to be recompiled."

cat $CWD/scripts/figlet_bootstrap.txt

if load_commandline_args $1;
then
  exit 1
fi

# Remove all build files from previous builds
full_clean

# Touch all source files (workaround to bug on windows where VS constantly will rebuild a file if
# it has a newer timestamp than current time, ie: if the windows current time was set wrong at some
# point in time, very big pain to diagnose)
source "scripts/touch-all-sourcefiles.bash"

# Create the new build directory.
print_white "Creating ${BUILD} directory."
mkdir -p ${BUILD}

# Ensure a compiler is available
print_white "Picking available compiler on system ..."
find_available_compiler

# Set Verbose Output Flag.
VERBOSE_ONOFF="ON"
if [ -z "$VERBOSE_COMPILER_OUTPUT_FLAG" ]; then
  VERBOSE_ONOFF="OFF"
fi

print_blue "BOOTSTRAP VARIABLES [BEGIN]"
print_cyan "Build Generator: $BUILD_GENERATOR"
print_cyan "Build Toolchain: $BUILD_TOOLCHAIN"
print_cyan "Compiler: $COMPILER"
print_cyan "DEBUG/RELEASE: $DEBUG_OR_RELEASE"
print_cyan "LOG_LEVEL: $LOG_LEVEL"
print_cyan "OPENGL RUNTIME CHECKS: $OPENGL_RUNTIME_CHECKS"
print_cyan "STATIC/SHARED LIBRARIES: $STATIC_OR_SHARED"
print_cyan "Verbose Compiler Ouput: (ON|OFF): $VERBOSE_ONOFF"
print_cyan "Sanitizer: $SANITIZER_FLAGS"
print_blue "BOOTSTRAP VARIABLES [END]"

export CC=$(which ${COMPILER})
export CXX=$(which ${COMPILER}++)

###################################################################################################
cd ${BUILD}

# Write the BUILD_SYSTEM to a file for reading later.
if [ "$BUILD_GENERATOR" = "Unix Makefiles" ];
then
  echo "make" >> ./BUILD_SYSTEM_GENERATOR
elif [ "$BUILD_GENERATOR" = "Ninja" ];
then
  echo "ninja" >> ./BUILD_SYSTEM_GENERATOR
fi

###################################################################################################
## Invoke cmake
mkdir bin
cmake -G "${BUILD_GENERATOR}" $BUILD_TOOLCHAIN --config {$DEBUG_OR_RELEASE} ..                     \
  -DCMAKE_BUILD_TYPE=${DEBUG_OR_RELEASE}                                                           \
  -DSTATIC_OR_SHARED=${STATIC_OR_SHARED}                                                           \
  -DSANITIZER_FLAGS=${SANITIZER_FLAGS}                                                             \
  -DVERBOSE_COMPILER_OUTPUT_FLAG=${VERBOSE_COMPILER_OUTPUT_FLAG}                                   \
  -DLOG_LEVEL=${LOG_LEVEL}                                                                         \
  -DOPENGL_RUNTIME_CHECKS=${OPENGL_RUNTIME_CHECKS}
cd ..
