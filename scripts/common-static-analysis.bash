#!/usr/bin/env bash
set -ex
source "scripts/common-build.bash"

# Allow core dumps
ulimit -c unlimited

# Address Sanitizer Runtime Flags
ASAN_FLAGS="verbosity=1"
ASAN_FLAGS="${ASAN_FLAGS}:check_initialization_order=1"
ASAN_FLAGS="${ASAN_FLAGS}:detect_invalid_pointer_pairs=10"
ASAN_FLAGS="${ASAN_FLAGS}:detect_leaks=1"
ASAN_FLAGS="${ASAN_FLAGS}:detect_container_overflow=1"
ASAN_FLAGS="${ASAN_FLAGS}:detect_odr_violation=2"
ASAN_FLAGS="${ASAN_FLAGS}:detect_stack_use_after_return=1"
ASAN_FLAGS="${ASAN_FLAGS}:halt_on_error=1"
ASAN_FLAGS="${ASAN_FLAGS}:strict_init_order=1"
ASAN_FLAGS="${ASAN_FLAGS}:strict_string_checks=1"

export ASAN_OPTIONS=${ASAN_FLAGS}
export LSAN_OPTIONS="suppressions=./config/lsan.supp"
export MSAN_OPTIONS=poison_in_dtor=1
