#!/usr/bin/env bash
set -ex
source "scripts/common-clangtools.bash"

cd ${BUILD}
$BUILD_SYSTEM_GENERATOR cppformat
cd ..
