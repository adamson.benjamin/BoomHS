#!/usr/bin/env bash
set -e

ROOT="$(pwd)"
BUILD="${ROOT}/build-system"
BINARY="${BUILD}/bin/main"
SOURCE="${ROOT}/source"

####################################################################################################
# Utility function for determining if a version is geater than or equal to a minimum version.
#
# Return values:
# 0 ==> true
# 1 ==> false
function version_gte() {
  local MIN_REQUIRED=$1
  local THE_VERSION=$2
  if [ "$(printf '%s\n' "$MIN_REQUIRED" "$THE_VERSION" | sort -V | head -n1)" = "$MIN_REQUIRED" ];
  then
    # THE_VERSION: Greater than or equal to MIN_REQUIRED
    return 0
  fi

  # THE_VERSION: Less-than MIN_REQUIRED
  return 1
}

####################################################################################################
# Find the supported version of GCC on your system.
#
# Parameters:
# $1 => Out parameter, name of gcc executable found (ie: gcc, gcc-8, gcc-9, etc...)
# $1 => Out parameter indicating the minimum version of GCC required.
# $3 => The out parameter this function will store the latest version of GCC found.
function find_gcc() {
  local GCC_NAME_LOCAL=""
  local MIN_VERSION_REQUIRED_LOCAL="8.0.0"
  local LATEST_VERSION_FOUND_LOCAL=""

  if type "gcc" > /dev/null 2>&1;
  then
    GCC_NAME_LOCAL="gcc"
    LATEST_VERSION_FOUND_LOCAL="$(gcc -dumpversion)"
  fi

  # If the version of GCC is too old, try looking for older versions specifically.
  if ! version_gte $MIN_VERSION_REQUIRED_LOCAL $LATEST_VERSION_FOUND_LOCAL;
  then
    if type "gcc-8" > /dev/null 2>&1;
    then
      GCC_NAME_LOCAL="gcc-8"
      LATEST_VERSION_FOUND_LOCAL="$(gcc-8 -dumpversion)"
    fi
  fi

  eval "$1=$GCC_NAME_LOCAL"
  eval "$2=$MIN_VERSION_REQUIRED_LOCAL"
  eval "$3=$LATEST_VERSION_FOUND_LOCAL"
}

####################################################################################################
# Find the supported version of clang on your system.
#
# Parameters:
# $1 => Out parameter, name of clang executable found (ie: clang, clang-8, clang-9, etc...)
# $1 => Out parameter indicating the minimum version of clang required.
# $3 => The out parameter this function will store the latest version of clang found.
# $4 => The out parameter this function will store indicating whether the function found Apple's clang.
function find_clang() {
  local CLANG_NAME_LOCAL="clang"
  local MIN_VERSION_REQUIRED_LOCAL="7.0.0"
  local LATEST_VERSION_FOUND_LOCAL=""
  local IS_APPLE_CLANG_LOCAL=false

  if type "clang" > /dev/null 2>&1;
  then
    if [[ $(clang --version | grep version) == *"Apple"* ]];
    then
      LATEST_VERSION_FOUND_LOCAL="$(clang --version | grep version | cut -d ' ' -f4)"
      IS_APPLE_CLANG_LOCAL=true
    else
      # Ugly hack because at the time of this writing clang is/was written to compatible with gcc
      # 4.2.1.. UGH FML ):
      # https://stackoverflow.com/a/13226244
      LATEST_VERSION_FOUND_LOCAL="$(clang --version | grep version | cut -d ' ' -f3)"
    fi
  fi

  eval "$1=$CLANG_NAME_LOCAL"
  eval "$2=$MIN_VERSION_REQUIRED_LOCAL"
  eval "$3=$LATEST_VERSION_FOUND_LOCAL"
  eval "$4=$IS_APPLE_CLANG_LOCAL"
}

####################################################################################################
# Delete the binary produced by building this project.
#
## Does not require the project to be rebootstrapped.
function clean() {
  rm -f ${BINARY}
  rm -rf ${BUILD}/CMakeFiles
}

# Delete all the build artifacts from the build directory.
#
## Project must be re-bootstrapped after this function is run.
function full_clean() {
  print_white "Removing old build directory ..."
  rm -rf ${BUILD}
}

####################################################################################################
# Functions for printing to the terminal using various colors.

# Helper function used internally to print to the terminal with a specific color.
function print_color() {
  tput setaf $1;
  printf "$2\n"
  tput sgr0;
}

function print_red() {
  print_color 1 "$1"
}

function print_green() {
  print_color 2 "$1"
}

function print_yellow() {
  print_color 3 "$1"
}

function print_white() {
  print_color 7 "$1"
}

function print_blue() {
  print_color 4 "$1"
}

function print_magenta() {
  print_color 5 "$1"
}

function print_cyan() {
  print_color 6 "$1"
}
