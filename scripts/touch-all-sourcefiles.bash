#!/usr/bin/env bash
source "scripts/common.bash"

print_white "Touching all source files ..."

cd source
find . -type f -exec touch {} +
cd ..

cd include
find . -type f -exec touch {} +
cd ..

cd external
find . -type f -exec touch {} +
cd ..

cd demo
find . -type f -exec touch {} +
cd ..

cd test
find . -type f -exec touch {} +
cd ..