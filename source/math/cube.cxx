#include "cube.hpp"
#include <boomhs/transform.hpp>

namespace
{
auto constexpr default_cube_vertices(glm::vec3 const& min, glm::vec3 const& max,
                                     glm::vec3 const& dim)
{
  auto const a = glm::vec3{min.x + 0.0f, min.y + 0.0f, min.z + 0.0f};   /* 0, 0, 0 */
  auto const b = glm::vec3{min.x + dim.x, min.y + 0.0f, min.z + 0.0f};  /* x, 0, 0 */
  auto const c = glm::vec3{min.x + 0.0f, min.y + 0.0f, min.z + dim.z};  /* 0, 0, x */
  auto const d = glm::vec3{min.x + dim.x, min.y + 0.0f, min.z + dim.z}; /* x, 0, x */

  auto const e = glm::vec3{max.x + 0.0f, max.y + dim.y, max.z + 0.0f};   /* 0, x, 0 */
  auto const f = glm::vec3{max.x + dim.x, max.y + dim.y, max.z + 0.0f};  /* x, x, 0 */
  auto const g = glm::vec3{max.x + 0.0f, max.y + dim.y, max.z + dim.z};  /* 0, x, x */
  auto const h = glm::vec3{max.x + dim.x, max.y + dim.y, max.z + dim.z}; /* x, x, x */

  return Cube::CubeVertices{{a, b, c, d, e, f, g, h}};
}

} // namespace

Cube::Cube(glm::vec3 const& minp, glm::vec3 const& maxp)
    : min(minp)
    , max(maxp)
{
}

glm::vec3
Cube::dimensions() const
{
  return math::cube::dimensions(min, max);
}

glm::vec3
Cube::scaled_dimensions(glm::vec3 const& scale) const
{
  auto const smin = scaled_min(scale);
  auto const smax = scaled_max(scale);
  return math::cube::dimensions(smin, smax);
}

glm::vec3
Cube::scaled_dimensions(Transform const& tr) const
{
  return scaled_dimensions(tr.scale);
}

glm::vec3
Cube::center() const
{
  return (min + max) / 2;
}

glm::vec3
Cube::half_widths() const
{
  return dimensions() / glm::vec3{2.0f};
}

glm::vec3
Cube::scaled_half_widths(glm::vec3 const& scale) const
{
  return scaled_dimensions(scale) / glm::vec3{2.0f};
}

glm::vec3
Cube::scaled_half_widths(Transform const& tr) const
{
  return scaled_half_widths(tr.scale);
}

glm::vec3
Cube::scaled_min(glm::vec3 const& scale) const
{
  auto const c  = center();
  auto const hw = half_widths();

  auto r = this->min;
  r.x    = c.x - (scale.x * hw.x);
  r.y    = c.y - (scale.y * hw.y);
  r.z    = c.z - (scale.z * hw.z);
  return r;
}

glm::vec3
Cube::scaled_min(Transform const& tr) const
{
  return scaled_min(tr.scale);
}

glm::vec3
Cube::scaled_max(glm::vec3 const& scale) const
{
  auto const c  = center();
  auto const hw = half_widths();

  auto r = this->max;
  r.x    = c.x + (scale.x * hw.x);
  r.y    = c.y + (scale.y * hw.y);
  r.z    = c.z + (scale.z * hw.z);
  return r;
}

glm::vec3
Cube::scaled_max(Transform const& tr) const
{
  return scaled_max(tr.scale);
}

Cube::CubeVertices
Cube::cube_vertices() const
{
  auto const dim = dimensions();
  return default_cube_vertices(min, max, dim);
}

Cube::CubeVertices
Cube::scaled_cube_vertices(glm::vec3 const& scale) const
{
  auto const smin = scaled_min(scale);
  auto const smax = scaled_max(scale);
  auto const sdim = scaled_dimensions(scale);
  return default_cube_vertices(smin, smax, sdim);
}

Cube::CubeVertices
Cube::scaled_cube_vertices(Transform const& tr) const
{
  return scaled_cube_vertices(tr.scale);
}

RectFloat
Cube::xz_rect() const
{
  return rect::create(min.x, min.z, max.x, max.z);
}

RectFloat
Cube::xy_rect() const
{
  return rect::create(min.x, min.y, max.x, max.y);
}

std::string
Cube::to_string() const
{
  return fmt::sprintf("min: %s max %s", glm::to_string(min), glm::to_string(max));
}

std::ostream&
operator<<(std::ostream& ostream, Cube const& cube)
{
  ostream << cube.to_string();
  return ostream;
}

namespace math::cube
{
glm::vec3
dimensions(glm::vec3 const& min, glm::vec3 const& max)
{
  auto const w = std::abs(max.x - min.x);
  auto const h = std::abs(max.y - min.y);
  auto const l = std::abs(max.z - min.z);
  return glm::vec3{w, h, l};
}

} // namespace math::cube
