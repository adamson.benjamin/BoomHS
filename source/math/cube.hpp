#pragma once
#include <extlibs/glm.hpp>
#include <math/rectangle_type.hpp>

#include <array>
#include <ostream>
#include <string>

struct Transform;

struct Cube
{
  glm::vec3 min, max;

public:
  // Construct a cube given two points (min and max).
  explicit Cube(glm::vec3 const&, glm::vec3 const&);

public:
  glm::vec3 dimensions() const;
  glm::vec3 center() const;
  glm::vec3 half_widths() const;

  // Compute the vertex-position of a cube's "min" in local-space.
  // Transform => Transform containing the "scaling" vec3.
  glm::vec3 scaled_min(Transform const&) const;
  glm::vec3 scaled_min(glm::vec3 const&) const;

  // Compute the vertex-position of a cube's "max" in local-space.
  // Transform => Transform containing the "scaling" vec3.
  glm::vec3 scaled_max(Transform const&) const;
  glm::vec3 scaled_max(glm::vec3 const&) const;

  // Compute the dimensions (length, width, height) for the cube with a scaling vector applied.
  // Transform => Transform containing the "scaling" vec3.
  glm::vec3 scaled_dimensions(Transform const&) const;
  glm::vec3 scaled_dimensions(glm::vec3 const&) const;

  // Compute the half-widths (dimensions() / 2) for the Cube..
  // Transform => Transform containing the "scaling" vec3.
  glm::vec3 scaled_half_widths(Transform const&) const;
  glm::vec3 scaled_half_widths(glm::vec3 const&) const;

  using CubeVertices = std::array<glm::vec3, 8>;

  // Compute the array of vertices for the cube in local space.
  CubeVertices cube_vertices() const;
  CubeVertices scaled_cube_vertices(Transform const&) const;
  CubeVertices scaled_cube_vertices(glm::vec3 const&) const;

  RectFloat xz_rect() const;
  RectFloat xy_rect() const;

  std::string to_string() const;
};

std::ostream&
operator<<(std::ostream&, Cube const&);

namespace math::cube
{
// Calculates the cube's dimensions, given a min/max point in 3D.
//
// Interpret the glm::vec3 returned like:
//   x == width
//   y == height
//   z == length
glm::vec3
dimensions(glm::vec3 const&, glm::vec3 const&);

} // namespace math::cube
