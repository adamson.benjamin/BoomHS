#include "frustum.hpp"

#include <cmath>

float
Frustum::bottom_float() const
{
  return static_cast<float>(bottom);
}

float
Frustum::top_float() const
{
  return static_cast<float>(top);
}

float
Frustum::left_float() const
{
  return static_cast<float>(left);
}

float
Frustum::right_float() const
{
  return static_cast<float>(right);
}

int
Frustum::height() const
{
  assert(bottom > top);
  return std::abs(bottom - top);
}

float
Frustum::height_float() const
{
  return static_cast<float>(height());
}

int
Frustum::width() const
{
  assert(right > left);
  return std::abs(right - left);
}

float
Frustum::width_float() const
{
  return static_cast<float>(width());
}

float
Frustum::depth() const
{
  assert(far > near);
  return std::abs(far - near);
}

int
Frustum::half_height() const
{
  return height() / 2;
}

int
Frustum::half_width() const
{
  return width() / 2;
}

float
Frustum::half_depth() const
{
  return depth() / 2;
}

int
Frustum::half_height_int() const
{
  return half_height();
}

int
Frustum::half_width_int() const
{
  return half_height();
}

int
Frustum::half_depth_int() const
{
  return half_height();
}

glm::vec2
Frustum::dimensions2d() const
{
  return glm::vec2{width(), height()};
}

glm::vec3
Frustum::dimensions3d() const
{
  return glm::vec3{width(), height(), depth()};
}

glm::vec3
Frustum::center() const
{
  return dimensions3d() / 2;
}

RectInt
Frustum::rect() const
{
  return rect::create(left, top, width(), height());
}

RectFloat
Frustum::rect_float() const
{
  return rect::to_float_rect(rect());
}

std::string
Frustum::to_string() const
{
  auto constexpr fmt = "left: %i, right: %i, bottom: %i, top: %i, near: %f, far: %f";
  return fmt::sprintf(fmt, left, right, bottom, top, near, far);
}

Frustum
Frustum::from_rect_and_nearfar(RectInt const& rect, float const near, float const far)
{
  return Frustum{rect.left(), rect.right(), rect.bottom(), rect.top(), near, far};
}

namespace math
{
float
compute_ar(Frustum const& fr)
{
  auto const w = fr.width_float();
  auto const h = fr.height_float();
  return w / h;
}
} // namespace math
