#include "algorithm.hpp"

namespace math
{

// Original source for algorithm.
// http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-17-quaternions/
glm::quat
rotation_between_vectors(glm::vec3 start, glm::vec3 dest)
{
  start = glm::normalize(start);
  dest  = glm::normalize(dest);

  auto const cos_theta = glm::dot(start, dest);
  glm::vec3  axis;
  if (cos_theta < (-1 + 0.001f)) {
    // special case when vectors in opposite directions:
    // there is no "ideal" rotation axis
    // So guess one; any will do as long as it's perpendicular to start
    axis = glm::cross(constants::Z_UNIT_VECTOR, start);
    if (glm::length2(axis) < 0.01f) {
      // bad luck, they were parallel, try again!
      axis = glm::cross(constants::X_UNIT_VECTOR, start);
    }

    axis = glm::normalize(axis);
    return glm::angleAxis(glm::radians(180.0f), axis);
  }
  axis = glm::cross(start, dest);

  auto const s    = std::sqrt((1.0f + cos_theta) * 2.0f);
  auto const invs = 1.0f / s;
  return glm::quat{s * 0.5f, axis.x * invs, axis.y * invs, axis.z * invs};
}

} // namespace math
