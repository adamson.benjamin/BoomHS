#include "matrices.hpp"

CameraMatrix
CameraMatrices::camera_matrix() const
{
  return proj * view;
}

namespace math
{
ModelMatrix
model_matrix_3d(glm::vec3 const& translation, glm::quat const& rotation, glm::vec3 const& scale)
{
  auto const tmatrix = glm::translate(glm::mat4{}, translation);
  auto const rmatrix = glm::toMat4(rotation);
  auto const smatrix = glm::scale(glm::mat4{}, scale);
  return tmatrix * rmatrix * smatrix;
}

ModelMatrix
model_matrix_2d(glm::vec2 const& t, glm::quat const& r, glm::vec2 const& s)
{
  glm::vec3 const tr{t.x, t.y, 0};
  glm::vec3 const sc{s.x, s.y, 0};
  return model_matrix_3d(tr, r, sc);
}

ModelViewProjMatrix
mvp_matrix(ModelMatrix const& model, ViewMatrix const& view, ProjMatrix const& proj)
{
  return proj * view * model;
}

glm::vec3
rotate_around(glm::vec3 const& point, glm::vec3 const& center, RotationMatrix const& rmat)
{
  auto const translate     = glm::translate(glm::mat4{}, center);
  auto const inv_translate = glm::translate(glm::mat4{}, -center);

  // The idea:
  // 1) Translate the object to the center
  // 2) Make the rotation
  // 3) Translate the object back to its original location
  auto const mm  = translate * rmat * inv_translate;
  auto const pos = mm * glm::vec4{point, 1.0f};
  return glm::vec3{pos.x, pos.y, pos.z};
}
} // namespace math
