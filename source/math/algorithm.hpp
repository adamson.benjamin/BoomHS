#pragma once
#include <math/constants.hpp>
#include <math/rectangle.hpp>

#include <common/algorithm.hpp>
#include <common/macro_util.hpp>

#include <extlibs/glm.hpp>

#include <array>
#include <cassert>
#include <cmath>
#include <limits>
#include <type_traits>

#undef near
#undef far

namespace math
{
template <typename... T>
inline void
negate(T&&... values)
{
  auto const negate_impl = [](auto& v) { v = -v; };
  (negate_impl(values), ...);
}

template <typename T>
inline auto
squared(T const& value)
{
  return value * value;
}

inline bool
opposite_signs(int const x, int const y)
{
  return ((x ^ y) < 0);
}

template <typename T>
T const&
lesser_of(T const& a, T const& b)
{
  return a < b ? a : b;
}

inline bool
float_compare(float const a, float const b)
{
  return std::fabs(a - b) < constants::EPSILONF;
}

inline bool
float_cmp(float const a, float const b)
{
  return float_compare(a, b);
}

inline bool
is_unitv(glm::vec3 const& v)
{
  return float_compare(glm::length(v), 1);
}

inline glm::vec3
lerp(glm::vec3 const& a, glm::vec3 const& b, float const f)
{
  return (a * (1.0f - f)) + (b * f);
}

inline bool
allnan(glm::vec3 const& v)
{
  return std::isnan(v.x) && std::isnan(v.y) && std::isnan(v.z);
}

inline bool
anynan(glm::vec3 const& v)
{
  return std::isnan(v.x) || std::isnan(v.y) || std::isnan(v.z);
}

template <typename T>
inline auto
pythag_distance(glm::tvec2<T> const& a, glm::tvec2<T> const& b)
{
  // pythagorean theorem
  return std::sqrt(squared(b.x - a.x) + squared(b.y - a.y));
}

// Normalizes "value" from the "from_range" to the "to_range"
template <typename T, typename P1, typename P2>
constexpr float
normalize(T const value, P1 const& from_range, P2 const& to_range)
{
  static_assert(std::is_integral<T>::value, "Input must be integral");
  auto const minimum    = from_range.first;
  auto const maximum    = from_range.second;
  auto const floor      = to_range.first;
  auto const ceil       = to_range.second;
  auto const normalized = ((ceil - floor) * (value - minimum)) / (maximum - minimum) + floor;
  return static_cast<float>(normalized);
}

inline float
angle_between_vectors(glm::vec3 const& a, glm::vec3 const& b, glm::vec3 const& origin)
{
  glm::vec3 const da = glm::normalize(a - origin);
  glm::vec3 const db = glm::normalize(b - origin);

  return std::acos(glm::dot(da, db));
}

// Original source for algorithm.
// http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-17-quaternions/
glm::quat
rotation_between_vectors(glm::vec3 start, glm::vec3 dest);

template <typename T1, typename T2>
inline constexpr void
assert_abs_lessthan_equal(T1 const& v, T2 const& max)
{
  constexpr bool IS_CONVERTIBLE = std::is_convertible_v<T1, T2>;
  static_assert(IS_CONVERTIBLE, "T1 and T2 must be implicitely convertible.");
  assert(std::abs(v) <= max);
}

template <typename T>
inline constexpr void
assert_abs_lessthan_equal(glm::tvec3<T> const& v, T const& max)
{
  assert_abs_lessthan_equal(v.x, max);
  assert_abs_lessthan_equal(v.y, max);
  assert_abs_lessthan_equal(v.z, max);
}

} // namespace math

namespace math
{
// Given two points, compute the "Absolute Value" rectangle.
//
// Essentially figures out which points are less-than the others and which points are greater than
// the others, and assigns then the values which will yield a rectangle.
//
// This function's purpose is to create a rectangle out of two points, with the top/left point
// always being <= to the bottom right point, even if the input points would naturally create a
// rectangle with the top/left being >= then the bottom/right
template <typename T>
auto
rect_abs_from_twopoints(glm::tvec2<T> const& a, glm::tvec2<T> const& b)
{
  auto const lx = math::lesser_of(a.x, b.x);
  auto const rx = common::other_of_two(lx, PAIR(a.x, b.x));

  auto const ty = math::lesser_of(a.y, b.y);
  auto const by = common::other_of_two(ty, PAIR(a.y, b.y));

  return rect::from_points(VEC2{lx, ty}, VEC2{rx, by});
}

} // namespace math
