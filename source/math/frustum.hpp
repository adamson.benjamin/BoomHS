#pragma once
#include <math/rectangle_type.hpp>

#include <extlibs/glm.hpp>

#undef near
#undef far

struct Frustum
{
  int   left, right, bottom, top;
  float near = 0.0f;
  float far  = 0.0f;

  float bottom_float() const;
  float top_float() const;
  float left_float() const;
  float right_float() const;

  int   height() const;
  int   half_height() const;
  float height_float() const;

  int   width() const;
  int   half_width() const;
  float width_float() const;

  float depth() const;
  float half_depth() const;

  int half_height_int() const;
  int half_width_int() const;
  int half_depth_int() const;

  glm::vec2 dimensions2d() const;
  glm::vec3 dimensions3d() const;
  glm::vec3 center() const;

  RectInt   rect() const;
  RectFloat rect_float() const;

  std::string to_string() const;

  static Frustum from_rect_and_nearfar(RectInt const&, float, float);
};

namespace math
{
// Compute the aspect ratio from the frustum.
float
compute_ar(Frustum const&);

} // namespace math
