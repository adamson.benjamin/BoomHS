#include "rectangle.hpp"

namespace
{

// Multiply all vertices of the rectangle (rv) with the vec2 (ivec2, vec2 with floats, etc...)
// to produce a rectangle with it's points multiplied with the vector.
template <typename VT, typename R>
auto constexpr multiply_rect_vector(R const& rect, VT const& vt)
{
  auto const l = rect.left() * vt.x;
  auto const t = rect.top() * vt.y;

  auto const w = rect.width() * vt.x;
  auto const h = rect.height() * vt.y;

  return rect::create(l, t, w, h);
}

template <typename VT, typename T, typename R>
auto constexpr multiply_rect_scalar(R const& rect, T const v)
{
  return multiply_rect_vector(rect, VT{v, v});
}

template <typename R>
auto constexpr multiply_rect_scalar(R const& rect, float const v)
{
  return multiply_rect_scalar<glm::vec2, float>(rect, v);
}

template <typename R>
auto constexpr multiply_rect_scalar(R const& rect, int const v)
{
  return multiply_rect_scalar<glm::ivec2, int>(rect, v);
}

template <typename R, typename VT>
auto constexpr divide_rect_vector(R const& rect, VT const& vt)
{
  auto const l = rect.left() / vt.x;
  auto const t = rect.top() / vt.y;

  auto const w = rect.width() / vt.x;
  auto const h = rect.height() / vt.y;

  return rect::create(l, t, w, h);
}

} // namespace

RectFloat operator*(RectFloat const& rect, glm::vec2 const& vec)
{
  return multiply_rect_vector(rect, vec);
}

RectFloat operator*(glm::vec2 const& vec, RectFloat const& rect)
{
  return multiply_rect_vector(rect, vec);
}

RectFloat operator*(RectFloat const& rect, float const v) { return multiply_rect_scalar(rect, v); }

RectInt operator*(RectInt const& rect, glm::ivec2 const& vec)
{
  return multiply_rect_vector(rect, vec);
}

RectInt operator*(glm::ivec2 const& vec, RectInt const& rect)
{
  return multiply_rect_vector(rect, vec);
}

RectInt operator*(RectInt const& rect, int const v) { return multiply_rect_scalar(rect, v); }

RectFloat
operator/(RectFloat const& rect, glm::vec2 const& vec)
{
  return divide_rect_vector(rect, vec);
}

RectInt
operator/(RectInt const& rect, glm::ivec2 const& vec)
{
  return divide_rect_vector(rect, vec);
}
