#include "spherical.hpp"
#include <math/algorithm.hpp>
#include <math/constants.hpp>

#include <extlibs/fmt.hpp>

std::string
SphericalCoordinates::to_string() const
{
  return fmt::sprintf("{radius %f, theta %f, phi %f}", radius, theta, phi);
}

std::ostream&
operator<<(std::ostream& stream, SphericalCoordinates const& sc)
{
  stream << sc.to_string();
  return stream;
}

namespace math
{
glm::vec3
to_cartesian(SphericalCoordinates const& coords)
{
  float const radius = coords.radius;
  float const theta  = coords.theta;
  float const phi    = coords.phi;

  float const sin_phi = std::sin(phi);
  float const x       = radius * sin_phi * std::sin(theta);
  float const y       = radius * std::cos(phi);
  float const z       = radius * sin_phi * std::cos(theta);

  // Convert spherical coordinates into Cartesian coordinates
  // float const x = sin(phi) * cos(theta) * radius;
  // float const y = sin(phi) * sin(theta) * radius;
  // float const z = cos(phi) * radius;
  return glm::vec3{x, y, z};
}

SphericalCoordinates
to_spherical(glm::vec3 cartesian)
{
  if (math::float_cmp(cartesian.x, 0.0f)) {
    cartesian.x = math::constants::EPSILONF;
  }
  float const &x = cartesian.x, y = cartesian.y, z = cartesian.z;
  float const  x2 = x * x;
  float const  y2 = y * y;
  float const  z2 = z * z;

  float const radius = std::sqrt(x2 + y2 + z2);
  float       theta  = std::atan(y / x);
  if (cartesian.x < 0) {
    theta += math::constants::PI;
  }
  float const phi = std::atan((x2 + y2) / z);
  return SphericalCoordinates{radius, theta, phi};
}

} // namespace math
