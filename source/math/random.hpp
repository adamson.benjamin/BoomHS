#pragma once
#include <common/algorithm.hpp>
#include <common/forward.hpp>
#include <common/macros_class.hpp>
#include <common/type_traits.hpp>

#include <extlibs/glm.hpp>

#include <cassert>
#include <chrono>
#include <random>
#include <string>

class rng_t
{
  uint64_t const  seed_;
  std::mt19937_64 gen_;

public:
  template <typename T>
  auto constexpr gen(T const low, T const high)
  {
    if constexpr (std::is_integral<T>::value) {
      std::uniform_int_distribution<T> distr{low, high};
      return distr(gen_);
    }
    else if constexpr (std::is_floating_point<T>::value) {
      std::uniform_real_distribution<T> distr{low, high};
      return distr(gen_);
    }
    else {
      static_assert(always_false<T>);
    }
  }

  template <typename T>
  auto constexpr gen(T const high)
  {
    auto const min = T{0};
    assert(high >= min);
    return gen(min, high);
  }

  // Generate an array of integers within the range [low, high].
  template <size_t N, typename T>
  auto gen_array_range(T const low, T const high)
  {
    // Construct a function to pass to make_array_from that captures the "this" pointer of the rng_t
    // instance. This will allow the function 'fn' to be callable without passing the "this"
    // pointer explicitely to make_array_from.
    auto const fn = [&](auto&&... args) { return gen(FORWARD(args)); };
    return common::make_array_from<N>(fn, low, high);
  }

public:
  NO_MOVE_OR_COPY(rng_t);
  explicit rng_t();

public:
  // single-value generators

  // Generate a value within the range [false, true].
  bool gen_bool();

  // Generate a value within the range [-1.0f, 1.0f].
  float gen_negative1to1();

  // Generate a value within the range [0.0f, 1.0f].
  float gen_0to1();

  // Generate a value within the range [low, high].
  float gen_position(float, float);
  float gen_position(glm::vec2 const&);

  // Generate a value within the range [low, high].
  glm::vec3 gen_3dposition(float, float);

  // Generate a value within the range [low, high].
  glm::vec3 gen_3dposition(glm::vec3 const&, glm::vec3 const&);

  // Generate a 3d position between [(0, 0, 0), (MAX_XZ, MAX_Y, MAX_XZ)]
  glm::vec3 gen_3dposition_above_ground();

  // Generate an integer within the range [low, high].
  int    gen_int_range(int const low, int const high);
  size_t gen_size_t_range(size_t const low, size_t const high);

  // Generate a value with in the range [0, length].
  std::string gen_str(size_t);
  std::string gen_str(size_t, size_t);

public:
  // Array generators

  // Generate an array of values within the range [low, high].
  template <size_t N>
  auto gen_array_int(int const low, int const high)
  {
    return gen_array_range<N>(low, high);
  }

  // Generate an array of values within the range [low, high].
  template <size_t N>
  auto gen_array_float(float const low, float const high)
  {
    return gen_array_range<N>(low, high);
  }

  // Generate an array of values within the range [low, high].
  template <size_t N>
  auto gen_array_size_t(size_t const low, size_t const high)
  {
    return gen_array_range<N>(low, high);
  }
};
