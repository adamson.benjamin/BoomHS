#pragma once
#include <boomhs/polygon.hpp>
#include <boomhs/transform.hpp>

#include <common/macro_util.hpp>

// Implementation of various algorithms dealing with 2-dimensional rectangles.
//
// NOTE:
// Implementation of these algorithms is left as non-member functions on a central rectangle "Type"
// is intentional to keep algorithms de-coupled from vertex-type, to allow re-use of these
// algorithms among various Rectangle types.
namespace rectangle
{

/// Compute various information from/about rectangles.
///
/// NOTE: Most of these functions tend to have a member function defined.

// Compute the vertex (object-space).
template <typename R>
auto constexpr left_top(R const& rv) -> typename R::vertex_type
{
  return {rv.left(), rv.top()};
}

// Compute the vertex (object-space).
template <typename R>
auto constexpr left_bottom(R const& rv) -> typename R::vertex_type
{
  return {rv.left(), rv.bottom()};
}

// Compute the vertex (object-space).
template <typename R>
auto constexpr right_top(R const& rv) -> typename R::vertex_type
{
  return {rv.right(), rv.top()};
}

// Compute the vertex (object-space).
template <typename R>
auto constexpr right_bottom(R const& rv) -> typename R::vertex_type
{
  return {rv.right(), rv.bottom()};
}

// Function to map a vertex index to a vertex point.
//
// NOTE: This function is the single source of vertex-ordering for rectangles, other functions use
// (or should) use this function so vertex-index to vertex-point ordering remains consistent across
// the project.
template <typename R>
auto constexpr rect_vertex_index_to_point(R const& rv, size_t const index)
{
  // Sanity check, ensure that a valid index has been passed in.
  assert(index <= R::num_vertices);

  // Map each index to a vertex.
  switch (index) {
  case 0:
    return left_top(rv);
  case 1:
    return left_bottom(rv);
  case 2:
    return right_bottom(rv);
  case 3:
    return right_top(rv);
  default:
    break;
  }
  /* INVALID to index this far into a rectangle. Rectangle only has 4 points. */
  std::exit(EXIT_FAILURE);
}

// Yield the vertex points (counter-clockwise order) in object-space.
template <typename R>
auto constexpr vertex_points(R const& rv)
{
  // Collect the points into an array.
  auto const p0 = rect_vertex_index_to_point(rv, 0);
  auto const p1 = rect_vertex_index_to_point(rv, 1);
  auto const p2 = rect_vertex_index_to_point(rv, 2);
  auto const p3 = rect_vertex_index_to_point(rv, 3);

  using ArrayType = typename R::array_type;
  return ArrayType{p0, p1, p2, p3};
}

// Move the rectangle by (x, y)
template <typename R>
void
move(R& rv, typename R::value_type const& x, typename R::value_type const& y)
{
  rv.left() += x;
  rv.top() += y;
}

template <typename R, typename V>
void
move(R const& rv, V const& v)
{
  move(rv, v.x, v.y);
}

template <typename R>
auto constexpr size(R const& rv) -> typename R::vertex_type
{
  auto const w = rv.width();
  auto const h = rv.height();
  return {w, h};
}

template <typename R>
auto constexpr half_size(R const& rv)
{
  return size(rv) / 2;
}

template <typename R>
auto constexpr half_width(R const& rv)
{
  return rv.width() / 2;
}

template <typename R>
auto constexpr half_height(R const& rv)
{
  return rv.height() / 2;
}

template <typename R>
auto constexpr center(R const& rv)
{
  auto const lt = left_top(rv);
  auto const hs = half_size(rv);
  return lt + hs;
}

template <typename R>
auto constexpr center_left(R const& rv) -> typename R::vertex_type
{
  auto const l = rv.left();
  auto const y = rv.center().y;
  return {l, y};
}

template <typename R>
auto constexpr center_right(R const& rv) -> typename R::vertex_type
{
  auto const r = rv.right();
  auto const y = rv.center().y;
  return {r, y};
}

template <typename R>
auto constexpr center_top(R const& rv) -> typename R::vertex_type
{
  auto const x = rv.center().x;
  auto const t = rv.top();
  return {x, t};
}

template <typename R>
auto constexpr center_bottom(R const& rv) -> typename R::vertex_type
{
  auto const x = rv.center().x;
  auto const b = rv.bottom();
  return {x, b};
}

// Compute a pair containing:
//
// (first) element:
//   The Rectangle's center.
//
// (second) element:
//   The rectangle's half-rect.
template <typename R>
auto
center_and_scaled_half_size(R const& rv, Transform2D const& tr)
{
  auto const& s    = tr.scale;
  auto const  hs   = half_size(rv);
  auto const  s_hs = glm::vec2{s.x * hs.x, s.y * hs.y};

  auto const c = center(rv);
  return PAIR(c, s_hs);
}

// Compute the left-top vertex of the rectangle, after the provided Transform's scaling has
// been applied.
template <typename R>
auto constexpr left_top_scaled(R const& rv, Transform2D const& tr)
{
  auto const [c, shs] = center_and_scaled_half_size(rv, tr);

  typename R::vertex_type p;
  p.x = c.x - shs.x;
  p.y = c.y - shs.y;
  return p;
}

// Compute the right-top vertex of the rectangle, after the provided Transform's sautocaling has
// been applied.
template <typename R>
auto constexpr right_top_scaled(R const& rv, Transform2D const& tr)
{
  auto const [c, shs] = center_and_scaled_half_size(rv, tr);

  typename R::vertex_type p;
  p.x = c.x + shs.x;
  p.y = c.y - shs.y;
  return p;
}

// Compute the left-bottom vertex of the rectangle, after the provided Transform's scaling has
// been applied.
template <typename R>
auto constexpr left_bottom_scaled(R const& rv, Transform2D const& tr)
{
  auto const [c, shs] = center_and_scaled_half_size(rv, tr);

  typename R::vertex_type p;
  p.x = c.x - shs.x;
  p.y = c.y + shs.y;
  return p;
}

// Compute the right-bottom vertex of the rectangle, after the provided Transform's scaling has
// been applied.
template <typename R>
auto constexpr right_bottom_scaled(R const& rv, Transform2D const& tr)
{
  auto const [c, shs] = center_and_scaled_half_size(rv, tr);

  typename R::vertex_type p;
  p.x = c.x + shs.x;
  p.y = c.y + shs.y;
  return p;
}

} // namespace rectangle
