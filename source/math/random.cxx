#include "random.hpp"

namespace
{
auto
make_seed()
{
  auto          seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
  std::seed_seq seeder{uint32_t(seed), uint32_t(seed >> 32)};
  ++seed;

  int out;
  seeder.generate(&out, &out + 1);
  return static_cast<uint64_t>(out);
}

} // namespace

rng_t::rng_t()
    : seed_(make_seed())
    , gen_(seed_)
{
}

float
rng_t::gen_negative1to1()
{
  return gen(-1.0f, 1.0f);
}

float
rng_t::gen_0to1()
{
  return gen(0.0f, 1.0f);
}

bool
rng_t::gen_bool()
{
  auto const v = gen(0, 1);
  return v > 0 ? true : false;
}

float
rng_t::gen_position(float const low, float const high)
{
  return gen(low, high);
}

float
rng_t::gen_position(glm::vec2 const& from)
{
  return gen_position(from.x, from.y);
}

glm::vec3
rng_t::gen_3dposition(glm::vec3 const& lower, glm::vec3 const& upper)
{
  auto const x = gen_position({lower[0], upper[0]});
  auto const y = gen_position({lower[1], upper[1]});
  auto const z = gen_position({lower[2], upper[2]});
  return glm::vec3{x, y, z};
}

glm::vec3
rng_t::gen_3dposition(float const low, float const high)
{
  auto const low_vec3  = glm::vec3{low};
  auto const high_vec3 = glm::vec3{high};
  return gen_3dposition(low_vec3, high_vec3);
}

glm::vec3
rng_t::gen_3dposition_above_ground()
{
  auto constexpr MAX_XZ = 10;
  auto constexpr MAX_Y  = 5;

  auto const LOWER = glm::vec3{0.0f, 0.1f, 0.0f};
  auto const UPPER = glm::vec3{MAX_XZ, MAX_Y, MAX_XZ};
  return gen_3dposition(LOWER, UPPER);
}

// Generate an integer within the range [low, high].
int
rng_t::gen_int_range(int const low, int const high)
{
  return gen(low, high);
}
size_t
rng_t::gen_size_t_range(size_t const low, size_t const high)
{
  return gen(low, high);
}

std::string
rng_t::gen_str(size_t const len)
{
  static constexpr char alphabet[] = "0123456789"
                                     "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                     "abcdefghijklmnopqrstuvwxyz";
  static constexpr auto ALPHABET_LEN = std::char_traits<char>::length(alphabet);

  std::string str;
  {
    std::uniform_int_distribution<size_t> distr{0, ALPHABET_LEN - 1};
    auto const                            gen = [&]() { return alphabet[distr(gen_)]; };
    std::generate_n(std::back_inserter(str), len, gen);
  }
  return str;
}

std::string
rng_t::gen_str(size_t const min_len, size_t const max_len)
{
  auto const len = gen_size_t_range(min_len, max_len);
  return gen_str(len);
}
