#pragma once
#include <boomhs/transform.hpp>
#include <common/algorithm.hpp>
#include <math/rectangle_algorithms.hpp>

#include <array>
#include <extlibs/fmt.hpp>
#include <extlibs/glm.hpp>

// Define/implement various 2dimensional rectangle types.
//
// Algorithm's are seperated from the actual rectangle types to allow reuse.
//
// User's may directly call the functions in rectangle if they wish, but the functionality
// has been re-exported as member functions on Rectangle objects.

// Template for defining 2D Rectangle types with a polymorphic Vertex type V.
//
// This class template maps the functions from rectangle_algorithm's namespace to member
// functions on a Rectangle type.
//
// This seperation of algorithm and object is intentional. The details of how the algorithm's
// are implemented is seperated from the "object-ness" of the rectangle itself.
template <typename V>
class RectT
{
public:
  static auto constexpr num_vertices = 4;
  using vertex_type                  = V;
  using value_type                   = typename V::value_type;
  using array_type                   = PolygonVertices<V, num_vertices>;

private:
  value_type left_ = 0, top_ = 0;
  value_type width_ = 0, height_ = 0;

public:
  RectT() = default;
  explicit constexpr RectT(value_type const l, value_type const t, value_type const w,
                           value_type const h)
      : left_(l)
      , top_(t)
      , width_(w)
      , height_(h)
  {
  }

public:
  void move(value_type const& x, value_type const& y) { rectangle::move(*this, x, y); }
  void move(V const& v) { rectangle::move(*this, v); }

  constexpr auto& left() { return left_; }
  constexpr auto& top() { return top_; }
  constexpr auto& width() { return width_; }
  constexpr auto& height() { return height_; }

  auto constexpr left() const { return left_; }
  auto constexpr top() const { return top_; }
  auto constexpr width() const { return width_; }
  auto constexpr height() const { return height_; }

  auto constexpr right() const { return left() + width(); }
  auto constexpr bottom() const { return top() + height(); }

  auto constexpr left_top() const { return rectangle::left_top(*this); }
  auto constexpr left_bottom() const { return rectangle::left_bottom(*this); }
  auto constexpr right_top() const { return rectangle::right_top(*this); }
  auto constexpr right_bottom() const { return rectangle::right_bottom(*this); }

  auto constexpr size() const { return rectangle::size(*this); }
  auto constexpr size_vec4() const { return glm::vec4{left_top(), right_bottom()}; }

  auto constexpr half_size() const { return rectangle::half_size(*this); }
  auto constexpr half_width() const { return rectangle::half_width(*this); }
  auto constexpr half_height() const { return rectangle::half_height(*this); }

  auto constexpr center() const { return rectangle::center(*this); }
  auto constexpr center_left() const { return rectangle::center_left(*this); }
  auto constexpr center_right() const { return rectangle::center_right(*this); }

  auto constexpr center_top() const { return rectangle::center_top(*this); }
  auto constexpr center_bottom() const { return rectangle::center_bottom(*this); }

  void shrink(value_type const& v)
  {
    left() += v;
    top() += v;
    width() -= v;
    height() -= v;
  }

  void grow(value_type const& v)
  {
    left() -= v;
    top() -= v;
    width() += v;
    height() += v;
  }

  V constexpr left_top_scaled(Transform2D const& tr) const
  {
    return rectangle::left_top_scaled(*this, tr);
  }
  V constexpr right_top_scaled(Transform2D const& tr) const
  {
    return rectangle::right_top_scaled(*this, tr);
  }
  V constexpr left_bottom_scaled(Transform2D const& tr) const
  {
    return rectangle::left_bottom_scaled(*this, tr);
  }
  V constexpr right_bottom_scaled(Transform2D const& tr) const
  {
    return rectangle::right_bottom_scaled(*this, tr);
  }

  // Map the vertex index to the vertex point.
  auto constexpr operator[](size_t const i) const
  {
    return rectangle::rect_vertex_index_to_point(*this, i);
  }

  constexpr auto& operator+=(value_type const& v)
  {
    left() += v;
    top() += v;
    width() += v;
    height() += v;
    return *this;
  }

  constexpr auto& operator-=(value_type const& v)
  {
    left() -= v;
    top() -= v;
    width() -= v;
    height() -= v;
    return *this;
  }

  constexpr auto& operator*=(value_type const& v)
  {
    left() *= v;
    top() *= v;
    width() *= v;
    height() *= v;
    return *this;
  }

  constexpr auto& operator/=(value_type const& v)
  {
    left() /= v;
    top() /= v;
    width() /= v;
    height() /= v;
    return *this;
  }

  // Yield the vertex points in object-space (counter-clockwise order).
  array_type constexpr points() const { return rectangle::vertex_points(*this); }

  std::string to_string() const
  {
    return fmt::format("[{} , {} , {}, {}] (w: {}, h: {})", left(), top(), right(), bottom(),
                       width(), height());
  }
};

template <typename V>
constexpr auto
operator+(RectT<V> const& rect, typename RectT<V>::value_type const d)
{
  auto const left   = rect.left() + d;
  auto const top    = rect.top() + d;
  auto const right  = rect.right() + d;
  auto const bottom = rect.bottom() + d;
  return RectT<V>{left, top, right, bottom};
}

template <typename V>
constexpr auto
operator-(RectT<V> const& rect, typename RectT<V>::value_type const d)
{
  auto const left   = rect.left() - d;
  auto const top    = rect.top() - d;
  auto const right  = rect.right() - d;
  auto const bottom = rect.bottom() - d;
  return RectT<V>{left, top, right, bottom};
}

template <typename V>
constexpr auto
operator/(RectT<V> const& rect, typename RectT<V>::value_type const d)
{
  auto const left   = rect.left() / d;
  auto const top    = rect.top() / d;
  auto const right  = rect.right() / d;
  auto const bottom = rect.bottom() / d;
  return RectT<V>{left, top, right, bottom};
}

template <typename V>
constexpr auto operator*(RectT<V> const& rect, typename RectT<V>::value_type const d)
{
  auto const left   = rect.left() * d;
  auto const top    = rect.top() * d;
  auto const right  = rect.right() * d;
  auto const bottom = rect.bottom() * d;
  return RectT<V>{left, top, right, bottom};
}

// Define Rectangle types from primitives.
using RectFloat = RectT<glm::vec2>;
using RectInt   = RectT<glm::ivec2>;

namespace rect
{
auto constexpr create(float const l, float const t, float const w, float const h)
{
  return RectFloat{l, t, w, h};
}

auto constexpr create(float const v) { return create(v, v, v, v); }

auto constexpr create(glm::vec2 const& pos, float const w, float const h)
{
  return create(pos.x, pos.y, w, h);
}

auto constexpr create(std::array<float, 4> const& p)
{
  return create(p[0], p[1], p[2], p[3]);
}

auto constexpr create(float const x, float const y, glm::vec2 const& size)
{
  return create(x, y, size.x, size.y);
}

inline auto /*constexpr*/
from_points(glm::vec2 const& p0, glm::vec2 const& p1)
{
  auto const w = std::abs(p1.x - p0.x);
  auto const h = std::abs(p1.y - p0.y);
  return create(p0.x, p0.y, w, h);
}

auto constexpr create(int const x, int const y, int const w, int const h)
{
  return RectInt{x, y, w, h};
}

auto constexpr create(glm::ivec2 const& p0, int const w, int const h)
{
  return create(p0.x, p0.y, w, h);
}

auto constexpr create(int const v) { return create(v, v, v, v); }

} // namespace rect

namespace rect
{


template <typename A, typename B, typename C, typename D>
auto constexpr create_float(A const l, B const t, C const w, D const h)
{
  auto const left   = static_cast<float>(l);
  auto const top    = static_cast<float>(t);
  auto const width  = static_cast<float>(w);
  auto const height = static_cast<float>(h);
  return create(left, top, width, height);
}

template <typename T>
auto constexpr create_float(std::array<T, 4> const& arr)
{
  return create_float(arr[0], arr[1], arr[2], arr[3]);
}

auto constexpr to_float_rect(RectInt const& rect)
{
  auto const left   = static_cast<float>(rect.left());
  auto const top    = static_cast<float>(rect.top());
  auto const width  = static_cast<float>(rect.width());
  auto const height = static_cast<float>(rect.height());

  return create(left, top, width, height);
}

} // namespace rect
