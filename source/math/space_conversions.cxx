#include "space_conversions.hpp"
#include <math/algorithm.hpp>

namespace
{
auto constexpr DEFAULT_W = 1;
} // namespace

namespace math::space_conversions
{

glm::vec4
object_to_world(glm::vec4 const& p, ModelMatrix const& mm)
{
  return mm * p;
}

glm::vec3
object_to_world(glm::vec3 const& p, ModelMatrix const& mm)
{
  auto const v4 = glm::vec4{p, DEFAULT_W};

  return object_to_world(v4, mm);
}

glm::vec4
world_to_object(glm::vec4 const& p, ModelMatrix const& mm)
{
  auto const imm = glm::inverse(mm);
  return imm * p;
}

glm::vec4
world_to_object(glm::vec3 const& p, ModelMatrix const& mm)
{
  auto const v4 = glm::vec4{p, DEFAULT_W};
  return world_to_object(v4, mm);
}

glm::vec4
world_to_eye(glm::vec4 const& p, ViewMatrix const& view)
{
  return view * p;
}

glm::vec3
world_to_eye(glm::vec3 const& p, ViewMatrix const& view)
{
  auto const v4 = glm::vec4{p, DEFAULT_W};
  return world_to_eye(v4, view);
}

glm::vec4
eye_to_clip(glm::vec4 const& p, ProjMatrix const& proj)
{
  return proj * p;
}

glm::vec3
eye_to_clip(glm::vec3 const& p, ProjMatrix const& proj)
{
  auto const v4 = glm::vec4{p, DEFAULT_W};
  return eye_to_clip(v4, proj);
}

glm::vec3
clip_to_ndc(glm::vec4 const& p)
{
  auto const ndc4 = p / p.w;
  return glm::vec3{ndc4.x, ndc4.y, ndc4.z};
}

glm::vec3
clip_to_ndc(glm::vec3 const& p)
{
  glm::vec4 const clip4{p, DEFAULT_W};

  return clip_to_ndc(clip4);
}

glm::ivec2
screen_to_viewport(glm::ivec2 const& point, glm::ivec2 const& origin)
{
  return point - origin;
}

glm::ivec2
screen_to_viewport(glm::vec2 const& point, glm::ivec2 const& origin)
{
  glm::ivec2 const point_i{point};
  return screen_to_viewport(point_i, origin);
}

glm::vec4
clip_to_eye(glm::vec4 const& clip, ProjMatrix const& pm)
{
  auto const      inv_proj   = glm::inverse(pm);
  glm::vec4 const eye_coords = inv_proj * clip;

  auto constexpr W = 0;
  return glm::vec4{eye_coords.x, eye_coords.y, eye_coords.z, W};
}

glm::vec3
eye_to_world(glm::vec4 const& eye, ViewMatrix const& vm)
{
  auto const      inv_view = glm::inverse(vm);
  glm::vec4 const wpos     = inv_view * eye;
  return glm::vec3{wpos.x, wpos.y, wpos.z};
}

glm::vec2
screen_to_ndc(glm::vec2 const& scoords, glm::vec4 const& viewport)
{
  auto const& right  = viewport.z;
  auto const& bottom = viewport.w;
  float const x      = ((2.0f * scoords.x) / right) - 1.0f;
  float const y      = ((2.0f * scoords.y) / bottom) - 1.0f;

  assert_abs_lessthan_equal(x, 1.0f);
  assert_abs_lessthan_equal(y, 1.0f);
  return glm::vec2{x, -y};
}

glm::vec2
screen_to_ndc(glm::vec2 const& scoords, RectFloat const& viewport)
{
  auto const vec4 = viewport.size_vec4();
  return screen_to_ndc(scoords, vec4);
}

glm::vec2
ndc_to_screen(glm::vec3 const& ndc, RectFloat const& viewport)
{
  auto const origin    = viewport.left_top();
  auto const view_rect = viewport.size();
  return glm::vec2{(((ndc.x + 1) / 2) * view_rect.x) + origin.x,
                   ((((1 - ndc.y) / 2) * view_rect.y) + origin.y)};
}

glm::vec4
ndc_to_clip(glm::vec2 const& ndc, float const z)
{
  auto const W = 1.0f;
  return glm::vec4{ndc.x, ndc.y, z, W};
}

glm::vec3
screen_to_world(glm::vec3 const& scoords, ProjMatrix const& proj, ViewMatrix const& view,
                RectFloat const& viewport)
{
  // ss => screenspace
  glm::vec2 const ss2d = glm::vec2{scoords.x, scoords.y};
  auto const&     z    = scoords.z;

  glm::vec2 const ndc   = screen_to_ndc(ss2d, viewport);
  glm::vec4 const clip  = ndc_to_clip(ndc, z);
  glm::vec4 const eye   = clip_to_eye(clip, proj);
  glm::vec3 const world = eye_to_world(eye, view);

  /*
  {
    auto const vp_vec4 = viewport.size_vec4();
    auto const TEST = glm::unProject(scoords, proj, view, vp_vec4);
    assert(float_cmp(TEST.x, world.x));
    assert(float_cmp(TEST.y, world.y));
    assert(float_cmp(TEST.z, world.z));
  }
  */
  return world;
}

glm::vec3
screen_to_world(glm::vec2 const& scoords, float const z, ProjMatrix const& proj,
                ViewMatrix const& view, RectFloat const& viewport)
{
  // ss => screenspace
  glm::vec3 const ss3d{scoords.x, scoords.y, z};
  return screen_to_world(ss3d, proj, view, viewport);
}

glm::vec3
screen_to_world(glm::vec2 const& scoords, float const z, ProjMatrix const& proj,
                ViewMatrix const& view, glm::vec4 const& viewport)
{
  auto const& v         = viewport;
  auto const  view_rect = rect::create(v.x, v.y, v.z, v.w);
  return screen_to_world(scoords, z, proj, view, view_rect);
}

glm::vec4
object_to_eye(glm::vec4 const& p, ViewMatrix const& view, ModelMatrix const& model)
{
  auto const wp = object_to_world(p, model);
  return world_to_eye(wp, view);
}

glm::vec4
object_to_eye(glm::vec3 const& p, ViewMatrix const& view, ModelMatrix const& model)
{
  auto const v4 = glm::vec4{p, DEFAULT_W};
  return object_to_eye(v4, view, model);
}

glm::vec3
object_to_ndc(glm::vec4 const& p, ModelMatrix const& model, ProjMatrix const& proj,
              ViewMatrix const& view)
{
  auto const eye  = object_to_eye(p, view, model);
  auto const clip = eye_to_clip(eye, proj);

  return clip_to_ndc(clip);
}

glm::vec3
object_to_ndc(glm::vec3 const& p, ModelMatrix const& model, ProjMatrix const& proj,
              ViewMatrix const& view)
{
  glm::vec4 const p4{p, DEFAULT_W};

  return object_to_ndc(p4, model, proj, view);
}

glm::vec2
object_to_screen(glm::vec4 const& p, ModelMatrix const& /* model*/, ProjMatrix const& proj,
                 ViewMatrix const& view, RectFloat const& viewport)
{
  // TODO: This function is a hack it's implemementation first steps not being what I expect I
  // should try and understand what's going on ASAP.
  auto const mvp  = proj * view;
  auto const clip = mvp * p;
  auto const ndc  = clip_to_ndc(clip);
  return ndc_to_screen(ndc, viewport);
}

glm::vec2
object_to_screen(glm::vec3 const& p, ModelMatrix const& model, ProjMatrix const& proj,
                 ViewMatrix const& view, RectFloat const& viewport)
{
  glm::vec4 const p4{p, DEFAULT_W};

  return object_to_screen(p4, model, proj, view, viewport);
}

glm::vec2
object_to_viewport(glm::vec4 const& p, ModelMatrix const& model, ProjMatrix const& proj,
                   ViewMatrix const& view, RectFloat const& viewport)
{
  auto const ss = object_to_screen(p, model, proj, view, viewport);
  return screen_to_viewport(ss, viewport.left_top());
}

glm::vec2
object_to_viewport(glm::vec3 const& p, ModelMatrix const& model, ProjMatrix const& proj,
                   ViewMatrix const& view, RectFloat const& viewport)
{
  glm::vec4 const p4{p, DEFAULT_W};
  return object_to_viewport(p4, model, proj, view, viewport);
}

} // namespace math::space_conversions
