#pragma once
#include <extlibs/glm.hpp>
#include <math/matrices.hpp>
#include <math/rectangle_type.hpp>

namespace math::space_conversions
{
// from:
//   object space
// to:
//    world space
glm::vec4
object_to_world(glm::vec4 const&, ModelMatrix const&);

glm::vec3
object_to_world(glm::vec3 const&, ModelMatrix const&);

// from:
//    world space
// to:
//    object space
glm::vec4
world_to_object(glm::vec4 const&, ModelMatrix const&);

glm::vec4
world_to_object(glm::vec3 const&, ModelMatrix const&);

// from:
//    world space
// to:
//    eye space
glm::vec4
world_to_eye(glm::vec4 const&, ViewMatrix const&);

glm::vec3
world_to_eye(glm::vec3 const&, ViewMatrix const&);

// from:
//   eye space
// to:
//    clip space
glm::vec4
eye_to_clip(glm::vec4 const&, ProjMatrix const&);

glm::vec3
eye_to_clip(glm::vec3 const&, ProjMatrix const&);

// from:
//   clip space
// to:
//    ndc space
glm::vec3
clip_to_ndc(glm::vec4 const&);

glm::vec3
clip_to_ndc(glm::vec3 const&);

// Convert a point in screen/window space to the viewport's space.
//
// "ViewportSpace" defined as an offset from the screen's origin (0, 0) being the top-left, a
// simple subtraction is required.
glm::ivec2
screen_to_viewport(glm::ivec2 const&, glm::ivec2 const&);

glm::ivec2
screen_to_viewport(glm::vec2 const&, glm::ivec2 const&);

glm::vec4
clip_to_eye(glm::vec4 const&, ProjMatrix const&);

glm::vec3
eye_to_world(glm::vec4 const&, ViewMatrix const&);

// from:
//    ndc
// to:
//    screen/window
glm::vec2
screen_to_ndc(glm::vec2 const&, glm::vec4 const&);

glm::vec2
screen_to_ndc(glm::vec2 const&, RectFloat const&);

//
// Algorithm modified from:
// http://www.songho.ca/opengl/gl_transform.html
glm::vec2
ndc_to_screen(glm::vec3 const&, RectFloat const&);

// from:
//    ndc
// to:
//    clip
glm::vec4
ndc_to_clip(glm::vec2 const&, float);

// from:
//    screen/window
// to:
//    world
glm::vec3
screen_to_world(glm::vec3 const&, ProjMatrix const& proj, ViewMatrix const&, RectFloat const&);

glm::vec3
screen_to_world(glm::vec2 const&, float, ProjMatrix const&, ViewMatrix const&, RectFloat const&);

glm::vec3
screen_to_world(glm::vec2 const&, float, ProjMatrix const&, ViewMatrix const&, glm::vec4 const&);

// from:
//   object space
// to:
//    eye space
glm::vec4
object_to_eye(glm::vec4 const&, ViewMatrix const&, ModelMatrix const&);

glm::vec4
object_to_eye(glm::vec3 const&, ViewMatrix const&, ModelMatrix const&);

// from:
//   object space
// to:
//    ndc space
glm::vec3
object_to_ndc(glm::vec4 const&, ModelMatrix const&, ProjMatrix const&, ViewMatrix const&);

glm::vec3
object_to_ndc(glm::vec3 const&, ModelMatrix const&, ProjMatrix const&, ViewMatrix const&);

// from:
//   object space
// to:
//    screen/window space
glm::vec2
object_to_screen(glm::vec4 const&, ModelMatrix const&, ProjMatrix const&, ViewMatrix const&,
                 RectFloat const&);

glm::vec2
object_to_screen(glm::vec3 const&, ModelMatrix const&, ProjMatrix const&, ViewMatrix const&,
                 RectFloat const&);

// from:
//   object space
// to:
//    viewport space
glm::vec2
object_to_viewport(glm::vec4 const&, ModelMatrix const&, ProjMatrix const&, ViewMatrix const&,
                   RectFloat const&);

glm::vec2
object_to_viewport(glm::vec3 const&, ModelMatrix const&, ProjMatrix const&, ViewMatrix const&,
                   RectFloat const&);

} // namespace math::space_conversions
