#pragma once
#include <extlibs/glm.hpp>

using CameraMatrix        = glm::mat4;
using ModelMatrix         = glm::mat4;
using ProjMatrix          = glm::mat4;
using ViewMatrix          = glm::mat4;
using ModelViewMatrix     = glm::mat4;
using ModelViewProjMatrix = glm::mat4;

using TranslationMatrix = glm::mat4;
using ScaleMatrix       = glm::mat4;
using RotationMatrix    = glm::mat4;

struct CameraMatrices
{
  ProjMatrix proj;
  ViewMatrix view;

  CameraMatrix camera_matrix() const;
};

namespace math
{
glm::vec3
rotate_around(glm::vec3 const&, glm::vec3 const&, RotationMatrix const&);

ModelMatrix
model_matrix_3d(glm::vec3 const&, glm::quat const&, glm::vec3 const&);

ModelMatrix
model_matrix_2d(glm::vec2 const&, glm::quat const&, glm::vec2 const&);

ModelViewProjMatrix
mvp_matrix(ModelMatrix const&, ViewMatrix const&, ProjMatrix const&);

} // namespace math
