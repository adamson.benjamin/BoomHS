#pragma once
#include <math/frustum.hpp>
#include <math/viewport.hpp>

#include <common/macros_class.hpp>
#include <common/macros_container.hpp>
#include <common/move_only_type.hpp>
#include <common/result.hpp>

#include <cassert>
#include <extlibs/sdl.hpp>
#include <memory>
#include <string>
#include <type_traits>

struct GameState;
struct log_t;

struct RenderDistances
{
  float near = 0.001f;
  float far  = 5000.0f;
};

class OpenglState
{
  SDL_GLContext context_;

public:
  explicit OpenglState(SDL_GLContext);
  ~OpenglState();

  // MOVE-CONSTRUCTIBLE ONLY
  NO_COPY(OpenglState);
  NO_MOVE_ASSIGN(OpenglState);
  OpenglState(OpenglState&&);

  SDL_GLContext context_ptr() { return context_; }
  SDL_GLContext context_ptr() const { return context_; }
};

using WindowType = SDL_Window;
using WindowPtr  = std::unique_ptr<WindowType, decltype(&SDL_DestroyWindow)>;
using WindowID   = Uint32;

// struct SizeChangeHandler
//{
//  using callback = void (*)(log_t&, Frustum const&, void*);
//
//  callback fn   = nullptr;
//  void*    data = nullptr;
//};

class SDLWindow
{
  log_t&      LOGGER;
  WindowPtr   window_;
  OpenglState ostate_;
  bool        focused_    = false;
  bool        mouse_over_ = false;

public:
  SDLWindow(log_t&, WindowPtr&&, SDL_GLContext);

  // MOVE-CONSTRUCTIBLE ONLY
  NO_COPY(SDLWindow);
  NO_MOVE_ASSIGN(SDLWindow);
  SDLWindow(SDLWindow&&);

  // fields
  RenderDistances render_distances;

  // returns the rectange on the screen the window is occupying
  RectInt view_rect() const;

  // return the rect of the window
  RectInt view_size() const;

  // Allow getting the window's SDL pointer
  WindowType* window_ptr() const { return window_.get(); }

  auto const& opengl() const { return ostate_; }
  auto&       opengl() { return ostate_; }

  void make_current();

  auto id() const { return SDL_GetWindowID(window_ptr()); }

  bool is_focused() const { return focused_; }
  bool is_mouse_over() const { return mouse_over_; }
  bool is_hidden() const;
  bool is_shown() const;
  bool is_fullscreen() const;

  void set_focus(bool const v) { focused_ = v; }
  void set_mouse_over(bool const v) { mouse_over_ = v; }

  void swap_window_buffer();
  void hide_window();

  Frustum  view_frustum() const;
  Viewport viewport() const;
};

using WindowTitle  = char const*;
using WindowHeight = int;
using WindowWidth  = int;
using WindowPosX   = int;
using WindowPosY   = int;

using WindowFlags = Uint32;

struct WindowConfig
{
  WindowTitle  title = "UNTITLED WINDOW";
  WindowWidth  width;
  WindowHeight height;

  WindowFlags flags = 0;

  WindowPosX xpos = SDL_WINDOWPOS_UNDEFINED;
  WindowPosY ypos = SDL_WINDOWPOS_UNDEFINED;
};

class WindowManager
{
  std::vector<SDLWindow> windows_;

public:
  NO_MOVE_OR_COPY(WindowManager);
  DEFINE_VECTOR_LIKE_WRAPPER_FNS(windows_)
  WindowManager() = default;

  void add(SDLWindow&&);
  bool check_window_events(log_t&, GameState&, SDL_Event&);
};
