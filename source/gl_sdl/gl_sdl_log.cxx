﻿#include "gl_sdl_log.hpp"

#include <common/breakpoint.hpp>
#include <common/log.hpp>
#include <extlibs/fmt.hpp>

#include <extlibs/sdl.hpp>
#include <iostream>
#include <string>

namespace gl_error
{

std::optional<GlErrors>
retrieve()
{
  if (nullptr == SDL_GL_GetCurrentContext()) {
    // HISTORY NOTE: cannot call glGetError() function until the global instance is created... this
    // was a fun bug to track down when I ported to OSX. (┛ಠ_ಠ)┛彡┻━┻
    return std::nullopt;
  }
  std::vector<std::string> gl;
  for (GLenum error_code = glGetError();; error_code = glGetError()) {
    if (error_code == GL_NO_ERROR) {
      break;
    }
    std::string e = reinterpret_cast<char const*>(gluErrorString(error_code));
    gl.emplace_back(MOVE(e));
  }
  if (gl.empty()) {
    return std::nullopt;
  }
  return GlErrors{MOVE(gl)};
}

void
clear()
{
  while (glGetError() != GL_NO_ERROR) {
  }
  SDL_ClearError();
}

std::string
get_shader_log(GLuint const shader)
{
  // return retrieve(shader, glGetShaderInfoLog);

  GLchar info_log[512] = {'0'};
  ::glGetShaderInfoLog(shader, 512, nullptr, info_log);
  // std::cerr << "shader raw info_log: '" << info_log << "'\n";
  return info_log;
}

std::string
get_program_log(GLuint const program)
{
  GLchar info_log[512] = {'0'};
  ::glGetProgramInfoLog(program, 512, nullptr, info_log);
  // std::cerr << "program raw info_log: '" << info_log << "'\n";

  return info_log;
}

GLenum
log_any_gl_general_errors(log_t& LOGGER, char const* fn_name, char const* filename, int const line)
{
  GLenum const   err_code    = glGetError();
  GLubyte const* glu_err_str = gluErrorString(err_code);

  if (GL_NO_ERROR != err_code) {
    LOG_ERROR("GL error detected on file: %s:%d:, OpenGL function: '%s', raw error code: "
              "'%d', gluErrorString(%d): '%s'",
              filename, line, fn_name, err_code, err_code, glu_err_str);
  }
  return err_code;
}

bool
log_any_gl_program_errors(log_t& LOGGER, GLuint const program, char const* filename, int const line)
{
  auto const plog      = get_program_log(program);
  bool const is_errors = !plog.empty();

  if (is_errors) {
    LOG_ERROR("GL program error detected from file %s:%i, program: %u, error: '%s'", filename, line,
              program, plog);
  }
  return is_errors;
}

bool
log_any_gl_shader_errors(log_t& LOGGER, GLuint const shader, char const* filename, int const line)
{
  auto const slog      = get_shader_log(shader);
  bool const is_errors = !slog.empty();

  if (is_errors) {
    LOG_ERROR("GL shader error detected from file %s:%i, shader: %u, error: '%s'", filename, line,
              shader, slog);
  }
  return is_errors;
}

} // namespace gl_error

std::ostream&
operator<<(std::ostream& stream, GlErrors const& errors)
{
  stream << "{";
  for (auto const& e : errors.values) {
    stream << e << "\n";
  }
  stream << "}";
  return stream;
}

namespace sdl_error
{

std::optional<SdlErrors>
retrieve()
{
  auto sdl = SDL_GetError();
  assert(nullptr != sdl);
  if (0 == ::strlen(sdl)) {
    return std::nullopt;
  }
  return SdlErrors{sdl};
}

void
clear()
{
  SDL_ClearError();
}

} // namespace sdl_error

std::ostream&
operator<<(std::ostream& stream, SdlErrors const& errors)
{
  stream << "{" << errors.value << "}";
  return stream;
}

namespace gfx_log
{

void
abort_if_any_errors(log_t& LOGGER)
{
  auto const write_errors = [&LOGGER](char const* prefix, auto const& error) {
    LOG_ERROR("%s detected: %s\n", prefix, error);
    // common::cpu_breakpoint();
    // std::exit(EXIT_FAILURE);
  };
  auto const gl_errors = gl_error::retrieve();
  if (gl_errors) {
    write_errors("OpenGL Errors", *gl_errors);
  }
  auto const sdl_errors = sdl_error::retrieve();
  if (sdl_errors) {
    write_errors("SDL Errors", *sdl_errors);
  }
}

} // namespace gfx_log
