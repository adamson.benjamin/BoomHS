#pragma once
#include <common/algorithm.hpp>
#include <common/log.hpp>

#include <extlibs/glew.hpp>

#include <cassert>
#include <optional>
#include <string>
#include <vector>

#ifdef DEBUG_BUILD
#define GL_CHECK(stmt)                                                                             \
  [&]() {                                                                                          \
    stmt;                                                                                          \
    LOG_ANY_GL_GENERAL_ERRORS(LOGGER, #stmt);                                                      \
  }()

#define GL_CHECK_V(stmt)                                                                           \
  [&]() {                                                                                          \
    auto glcheck_rv = stmt;                                                                        \
    LOG_ANY_GL_GENERAL_ERRORS(LOGGER, #stmt);                                                      \
    return glcheck_rv;                                                                             \
  }()

#else
#define GL_CHECK(stmt) stmt
#define GL_CHECK_V(stmt) stmt
#endif

struct GlErrors
{
  std::vector<std::string> values;
};
std::ostream&
operator<<(std::ostream&, GlErrors const&);

namespace gl_error
{
std::optional<GlErrors>
retrieve();

void
clear();

std::string
get_shader_log(GLuint const);

std::string
get_program_log(GLuint const);

GLenum
log_any_gl_general_errors(log_t&, char const*, char const*, int);

inline GLenum
log_any_gl_general_errors(log_t* pLOGGER, char const* func, char const* file, int const line)
{
  assert(pLOGGER);
  return log_any_gl_general_errors(*pLOGGER, func, file, line);
}

// Returns true if there are errors, false if there are NO errors.
bool
log_any_gl_program_errors(log_t&, GLuint, char const*, int);

// Returns true if there are errors, false if there are NO errors.
bool
log_any_gl_shader_errors(log_t&, GLuint, char const*, int);

} // namespace gl_error

struct SdlErrors
{
  std::string value;
};
std::ostream&
operator<<(std::ostream&, SdlErrors const&);

namespace sdl_error
{
std::optional<SdlErrors>
retrieve();

void
clear();

static constexpr char const* NONE = "none";
} // namespace sdl_error

namespace gfx_log
{
void
abort_if_any_errors(log_t&);

} // namespace gfx_log

////////////////////////////////////////////////////////////////////////////////////////////////////
// TODO: Move this macro somewhere into common/
// https://stackoverflow.com/questions/8487986/file-macro-shows-full-path
#undef __FILENAME__
#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)

#define LOG_ANY_GL_GENERAL_ERRORS(LOGGER, fn_name)                                                 \
  ::gl_error::log_any_gl_general_errors(LOGGER, fn_name, __FILENAME__, __LINE__)

#define LOG_ANY_GL_GENERAL_ERRORS_AND_ABORT(LOGGER, fn_name)                                       \
  do {                                                                                             \
    auto const err_code = LOG_ANY_GL_GENERAL_ERRORS(LOGGER, fn_name);                              \
    if (GL_NO_ERROR != err_code) {                                                                 \
      LOG_ERROR("OpenGL error detected, aborting program now.");                                   \
      /*std::exit(EXIT_FAILURE); */                                                                \
    }                                                                                              \
  } while (0)

#define LOG_ANY_GL_PROGRAM_ERRORS(program)                                                         \
  ::gl_error::log_any_gl_program_errors(LOGGER, program, __FILENAME__, __LINE__);

#define LOG_ANY_GL_PROGRAM_ERRORS_AND_ABORT(LOGGER, program)                                       \
  do {                                                                                             \
    bool const is_errors = LOG_ANY_GL_PROGRAM_ERRORS(LOGGER, program);                             \
    if (is_errors) {                                                                               \
      LOG_ERROR("OpenGL program error detected, aborting program now.");                           \
      /*std::exit(EXIT_FAILURE); */                                                                \
    }                                                                                              \
  } while (0)

#define LOG_ANY_GL_SHADER_ERRORS(LOGGER, shader)                                                   \
  ::gl_error::log_any_gl_shader_errors(LOGGER, shader, __FILENAME__, __LINE__);

#define LOG_ANY_GL_SHADER_ERRORS_AND_ABORT(LOGGER, shader)                                         \
  do {                                                                                             \
    bool const is_errors = LOG_ANY_GL_SHADER_ERRORS(LOGGER, shader);                               \
    if (is_errors) {                                                                               \
      LOG_ERROR("OpenGL shader error detected, aborting program now.");                            \
      /*std::exit(EXIT_FAILURE); */                                                                \
    }                                                                                              \
  } while (0)
////////////////////////////////////////////////////////////////////////////////////////////////////////
