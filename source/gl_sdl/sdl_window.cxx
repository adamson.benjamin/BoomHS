#include "sdl_window.hpp"
#include <gl_sdl/gl_sdl_log.hpp>
#include <gl_sdl/global.hpp>

#include <opengl/renderer.hpp>

#include <boomhs/screen.hpp>
#include <boomhs/state.hpp>

#include <common/algorithm.hpp>
#include <common/log.hpp>
#include <common/macros_class.hpp>
#include <common/move.hpp>
#include <common/result.hpp>

#include <math/viewport.hpp>

#include <extlibs/fmt.hpp>
#include <extlibs/sdl.hpp>

#include <cassert>

using namespace gl_sdl;

namespace
{
bool
check_flag(SDL_WindowFlags const f, WindowType* pw)
{
  auto const flags = SDL_GetWindowFlags(pw);
  return f & flags;
}

void
on_window_size_change(log_t& LOGGER, GameState& gs, SDLWindow& window)
{
  auto& sstack = gs.sstack;
  assert(!sstack.empty());
  auto& top = sstack.top();

  auto const fr = window.view_frustum();
  {
    auto const vp = Viewport::from_frustum(fr);
    opengl::render::set_viewport_and_scissor(vp, fr.height());
  }
  if (top.resize_fn) {
    window.make_current();
    top.resize_fn(LOGGER, gs, top, fr).expect("resize failed");
  }
}

bool
process_window_events(log_t& LOGGER, GameState& gs, SDLWindow& window, SDL_Event& event)
{
  auto const wid = window.id();
  if (wid == event.window.windowID) {
    auto const we = event.window.event;
    switch (we) {
    case SDL_WINDOWEVENT_CLOSE:
      window.hide_window();
      break;
    case SDL_WINDOWEVENT_RESIZED:
    case SDL_WINDOWEVENT_SIZE_CHANGED:
      on_window_size_change(LOGGER, gs, window);
      break;
    case SDL_WINDOWEVENT_ENTER:
      window.set_mouse_over(true);
      break;
    case SDL_WINDOWEVENT_LEAVE:
      window.set_mouse_over(false);
      break;
    case SDL_WINDOWEVENT_FOCUS_GAINED:
      window.set_focus(true);
      break;
    case SDL_WINDOWEVENT_FOCUS_LOST:
      window.set_focus(false);
      break;
    }
  }
  return gl_sdl::is_quit_action(event);
}

} // namespace

OpenglState::OpenglState(SDL_GLContext ctx)
    : context_(ctx)
{
}

OpenglState::OpenglState(OpenglState&& other)
    : context_(other.context_)
{
  other.context_ = nullptr;
}

OpenglState::~OpenglState()
{
  if (nullptr != context_) {
    SDL_GL_DeleteContext(context_);
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// SDLWindow
SDLWindow::SDLWindow(log_t& logger, WindowPtr&& w, SDL_GLContext c)
    : LOGGER(logger)
    , window_(MOVE(w))
    , ostate_(c)
{
}

SDLWindow::SDLWindow(SDLWindow&& other)
    : LOGGER(other.LOGGER)
    , window_(MOVE(other.window_))
    , ostate_(MOVE(other.ostate_))
{
  other.window_ = nullptr;
}

void
SDLWindow::make_current()
{
  auto const mc_r = SDL_GL_MakeCurrent(window_.get(), opengl().context_ptr());
  gfx_log::abort_if_any_errors(LOGGER);
  if (0 != mc_r) {
    LOG_ERROR("Error making window current. SDL Error: {}\n", SDL_GetError());
    std::abort();
  }
}

RectInt
SDLWindow::view_rect() const
{
  assert(nullptr != window_.get());

  int x, y;
  SDL_GetWindowPosition(window_.get(), &x, &y);

  int w, h;
  SDL_GetWindowSize(window_.get(), &w, &h);

  return rect::create(x, y, w, h);
}

RectInt
SDLWindow::view_size() const
{
  assert(nullptr != window_.get());

  int w, h;
  SDL_GetWindowSize(window_.get(), &w, &h);
  return rect::create(0, 0, w, h);
}

bool
SDLWindow::is_hidden() const
{
  return check_flag(SDL_WINDOW_HIDDEN, window_ptr());
}

bool
SDLWindow::is_shown() const
{
  return check_flag(SDL_WINDOW_SHOWN, window_ptr());
}

bool
SDLWindow::is_fullscreen() const
{
  return check_flag(SDL_WINDOW_FULLSCREEN, window_ptr());
}

void
SDLWindow::hide_window()
{
  SDL_HideWindow(window_ptr());
}

Frustum
SDLWindow::view_frustum() const
{
  auto const  rect = view_size();
  auto const& rd   = render_distances;
  return Frustum::from_rect_and_nearfar(rect, rd.near, rd.far);
}

Viewport
SDLWindow::viewport() const
{
  return Viewport::from_frustum(this->view_frustum());
}

void
SDLWindow::swap_window_buffer()
{
  SDL_GL_SwapWindow(window_ptr());
}

void
WindowManager::add(SDLWindow&& w)
{
  windows_.emplace_back(MOVE(w));
}

bool
WindowManager::check_window_events(log_t& LOGGER, GameState& gs, SDL_Event& event)
{
  bool all_hidden = true;
  bool quit       = false;
  for (auto& w : windows_) {
    if (quit)
      break;

    if (event.type == SDL_WINDOWEVENT) {
      quit = process_window_events(LOGGER, gs, w, event);
    }
    if (w.is_shown() || w.is_fullscreen()) {
      all_hidden = false;
    }
  }
  quit |= gl_sdl::is_quit_action(event);
  return quit || all_hidden;
}
