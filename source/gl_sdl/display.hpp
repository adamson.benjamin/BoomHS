#pragma once
#include <common/macros_class.hpp>
#include <common/macros_container.hpp>

#include <extlibs/sdl.hpp>
#include <vector>

struct VideoDisplay
{
  std::vector<SDL_DisplayMode> modes;
  DEFINE_VECTOR_LIKE_WRAPPER_FNS(modes)
};

struct DisplayInformation
{
  std::vector<VideoDisplay> displays;
  DEFINE_VECTOR_LIKE_WRAPPER_FNS(displays)
};
