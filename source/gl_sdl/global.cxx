#include "global.hpp"
#include <gl_sdl/gl_sdl_log.hpp>
#include <gl_sdl/sdl_window.hpp>

#include <math/space_conversions.hpp>
#include <opengl/renderer.hpp>

#include <common/FOR.hpp>

#include <extlibs/glew.hpp>

using namespace gl_sdl;

namespace
{

void
check_errors(log_t& LOGGER)
{
  gfx_log::abort_if_any_errors(LOGGER);
}

[[nodiscard]] Result<common::none_t, std::string>
set_attribute(log_t& LOGGER, SDL_GLattr const attr, int const value)
{
  int const set_r = SDL_GL_SetAttribute(attr, value);
  check_errors(LOGGER);
  if (0 != set_r) {
    auto const fmt = fmt::format("Setting attribute '{}' failed, error is '{}'\n",
                                 std::to_string(attr), SDL_GetError());
    return Err(fmt);
  }
  return OK_NONE;
}

void
init_glew(log_t& LOGGER)
{
  // glewExperimental = GL_TRUE;
  // ErrorLog::abort_if_any_errors(LOGGER);

  auto const glew_status = glewInit();
  check_errors(LOGGER);

  if (GLEW_OK != glew_status) {
    LOG_ERROR("GLEW could not initialize! GLEW error: {}\n", glewGetErrorString(glew_status));
    std::exit(EXIT_FAILURE);
  }
}

} // namespace

namespace gl_sdl::detail
{

} // namespace gl_sdl::detail

void
destroy_sdl_gl_state(gl_sdl::detail::SDL_GL_System_State&)
{
  assert(::SDL_WasInit(SDL_INIT_EVERYTHING));
  ::SDL_Quit();
}

namespace gl_sdl
{

Result<SDL_GL_System, std::string>
init_sdl(log_t& LOGGER)
{
  LOG_INFO("Recording SDL Library information ...");
  {
    SDL_version compiled;
    SDL_VERSION(&compiled)
    LOG_INFO("We compiled against SDL version %d.%d.%d ...\n", compiled.major, compiled.minor,
             compiled.patch);
  }
  {
    SDL_version linked;
    ::SDL_GetVersion(&linked);
    LOG_INFO("But we are linking against SDL version %d.%d.%d.\n", linked.major, linked.minor,
             linked.patch);
  }

  LOG_INFO("Initializing SDL ...");
  if (::SDL_Init(SDL_INIT_EVERYTHING) < 0) {
    // Display error message
    auto const error = fmt::format("SDL could not initialize! SDL_Error: {}\n", SDL_GetError());
    return Err(error);
  }

  // Allow both SDL windows to share the same opengl context
  TRY(set_attribute(LOGGER, SDL_GL_SHARE_WITH_CURRENT_CONTEXT, 1));

  SDL_GL_System state;
  LOG_INFO("Initializing SDL successful!");
  return OK_MOVE(state);
}

Result<SDLWindow, std::string>
make_window(log_t& LOGGER, SDL_GL_System const&, WindowConfig const& wconfig)
{
  auto const  height = wconfig.height;
  auto const  width  = wconfig.width;
  auto const  flags  = wconfig.flags;
  char const* title  = wconfig.title;

  auto const set_a = [&LOGGER](auto const attr, auto const v) {
    return set_attribute(LOGGER, attr, v);
  };

  // (from the docs) The requested attributes should be set before creating an
  // OpenGL window
  // Use OpenGL 3.1 core
  TRY(set_a(SDL_GL_CONTEXT_MAJOR_VERSION, 3));
  TRY(set_a(SDL_GL_CONTEXT_MINOR_VERSION, 1));

  TRY(set_a(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG));
  TRY(set_a(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE));

  // Turn on double buffering with a 24bit Z buffer.
  // You may need to change this to 16 or 32 for your system
  TRY(set_a(SDL_GL_DOUBLEBUFFER, 1));
  TRY(set_a(SDL_GL_DEPTH_SIZE, 24));
  TRY(set_a(SDL_GL_STENCIL_SIZE, 8));

  // First, create the SDL window.
  auto const xpos = wconfig.xpos;
  auto const ypos = wconfig.ypos;
  auto       raw  = ::SDL_CreateWindow(title, xpos, ypos, width, height, flags);
  check_errors(LOGGER);
  if (nullptr == raw) {
    auto constexpr FMT_STR = "SDL could not initialize! SDL_Error: '%s'";
    auto const error       = fmt::format(FMT_STR, SDL_GetError());
    return Err(error);
  }
  WindowPtr wptr{raw, &SDL_DestroyWindow};

  // Second, create the graphics context.
  auto gl_context = ::SDL_GL_CreateContext(wptr.get());
  check_errors(LOGGER);
  if (nullptr == gl_context) {
    // Display error message
    auto constexpr FMT_STR = "OpenGL context could not be created! SDL Error: '%s'";
    auto const error       = fmt::format(FMT_STR, SDL_GetError());
    return Err(error);
  }

  init_glew(LOGGER);
  check_errors(LOGGER);

  // Make the window the current one
  auto const mc_r = ::SDL_GL_MakeCurrent(wptr.get(), gl_context);
  check_errors(LOGGER);
  if (0 != mc_r) {
    auto const fmt = fmt::format("Error making window current. SDL Error: {}\n", SDL_GetError());
    return Err(fmt);
  }

  // set window initially to top-left of screen.
  ::SDL_SetWindowPosition(wptr.get(), 0, 0);
  check_errors(LOGGER);

  // Initially relative mode is not active. This allows the mouse cursor to be seen in the
  // main-menu.
  ::SDL_SetRelativeMouseMode(SDL_FALSE);

  // Without this hint, it seems (on at-least linux) that a bug in SDL2 won't allow proper
  // implementation of an FPS view.
  //
  // https://bugzilla.libsdl.org/show_bug.cgi?id=2150
  // https://bugzilla.libsdl.org/show_bug.cgi?id=2954
  //
  // workaround:
  // https://bugzilla.libsdl.org/show_bug.cgi?id=2150
  ::SDL_SetHintWithPriority(SDL_HINT_MOUSE_RELATIVE_MODE_WARP, "1", SDL_HINT_OVERRIDE);

  // NOTE: must happen AFTER SDL_GL_MakeCurrent call occurs.
  SDLWindow window{LOGGER, MOVE(wptr), gl_context};

  check_errors(LOGGER);
  OR::init(LOGGER, window);
  return OK_MOVE(window);
}

RectFloat
make_mouse_click_rect(glm::ivec2 const& initial_ss, glm::ivec2 const& now_ss,
                      glm::ivec2 const& vp_origin)
{
  namespace sc = math::space_conversions;

  // screen pos -> viewport pos
  auto const resize = sc::screen_to_viewport(initial_ss, vp_origin);
  auto const now_vp = sc::screen_to_viewport(now_ss, vp_origin);

  // Create a rectangle using the two points, making sure that it works out if the user clicks
  // and drags up or down, left or right.
  return math::rect_abs_from_twopoints(resize, now_vp);
}

glm::ivec2
mouse_coords()
{
  int x, y;
  ::SDL_GetMouseState(&x, &y);
  return glm::ivec2{x, y};
}

bool
is_quit_action(SDL_Event& event)
{
  bool const event_type_keydown = event.type == SDL_KEYDOWN;
  if (event_type_keydown) {
    SDL_Keycode const key_pressed = event.key.keysym.sym;
    switch (key_pressed) {
    case SDLK_F10:
      // case SDLK_ESCAPE:
      return true;
    }
  }
  return event.type == SDL_QUIT;
}

Result<DisplayInformation, std::string>
get_all_display_modes()
{
  DisplayInformation di;

  SDL_DisplayMode current;
  FOR(i, SDL_GetNumVideoDisplays())
  {
    int const num_modes = SDL_GetNumDisplayModes(i);
    if (num_modes < 1) {
      auto const fmt = "Error retreving number display modes for video display '%i', error: '%s'";
      return ERR_SPRINTF(fmt, i, SDL_GetError());
    }

    VideoDisplay vd;
    FOR(m, num_modes)
    {
      int const result = SDL_GetDisplayMode(i, m, &current);
      if (0 != result) {
        return ERR_SPRINTF("Error retrieving display mode '%i': error '%s'", m, SDL_GetError());
      }
      vd.emplace_back(current);
    }
    di.emplace_back(MOVE(vd));
  }
  return OK_MOVE(di);
}

glm::ivec2
current_display_mode(log_t& LOGGER)
{
  SDL_DisplayMode dmode;
  if (0 != SDL_GetCurrentDisplayMode(0, &dmode)) {
    LOG_ERROR("Error SETTING display mode info for 0th display.");
    std::exit(EXIT_FAILURE);
  }
  return glm::ivec2{dmode.w, dmode.h};
}

} // namespace gl_sdl
