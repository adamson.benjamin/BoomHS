#pragma once
#include <gl_sdl/display.hpp>

#include <math/rectangle_type.hpp>

#include <common/macros_class.hpp>
#include <common/move_only_type.hpp>
#include <common/result.hpp>

#include <extlibs/glm.hpp>
#include <extlibs/sdl.hpp>

#include <string>

struct log_t;
struct WindowConfig;
class SDLWindow;

namespace gl_sdl::detail
{
class SDL_GL_System_State
{
public:
  MOVE_DEFAULT_ONLY(SDL_GL_System_State);
  SDL_GL_System_State() = default;
};

} // namespace gl_sdl::detail

void
destroy_sdl_gl_state(gl_sdl::detail::SDL_GL_System_State&);

using SDL_GL_System = move_only_type<gl_sdl::detail::SDL_GL_System_State, destroy_sdl_gl_state>;

namespace gl_sdl
{
Result<SDL_GL_System, std::string>
init_sdl(log_t&);

// Factory function for creating the RectFloat with points where the user last clicked, and the
// mouse is currently.
//
// initial => mouse position (in screen space) where the user clicked.
// now     => mouse position (in screen space) where the mouse is currently (think
// click-and-drag).
// viewport_orgin => Origin of the viewport (0, 0) being top-left of the screen.
RectFloat
make_mouse_click_rect(glm::ivec2 const&, glm::ivec2 const&, glm::ivec2 const&);

Result<SDLWindow, std::string>
make_window(log_t&, SDL_GL_System const&, WindowConfig const&);

glm::ivec2
mouse_coords();

bool
is_quit_action(SDL_Event&);

glm::ivec2
current_display_mode(log_t&);

Result<DisplayInformation, std::string>
get_all_display_modes();
} // namespace gl_sdl
