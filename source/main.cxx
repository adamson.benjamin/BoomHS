#include <boomhs/boomhs.hpp>
#include <boomhs/camera.hpp>
#include <boomhs/controller.hpp>
#include <boomhs/delta_time.hpp>
#include <boomhs/engine.hpp>
#include <boomhs/entity.hpp>
#include <boomhs/io_sdl.hpp>
#include <boomhs/main_menu.hpp>
#include <boomhs/scene_renderer.hpp>
#include <boomhs/stacktrace.hpp>
#include <boomhs/water.hpp>

#include <common/breakpoint.hpp>
#include <common/log.hpp>
#include <common/time.hpp>
#include <common/timer.hpp>

#include <gl_sdl/global.hpp>
#include <gl_sdl/sdl_window.hpp>

#include <math/matrices.hpp>

#include <opengl/bind.hpp>
#include <opengl/renderer.hpp>

#include <extlibs/date.hpp>
#include <extlibs/openal.hpp>

#include <algorithm>
#include <csignal>
#include <filesystem>
#include <fstream>

using namespace math;
using namespace common;
using namespace opengl;
using namespace gl_sdl;
using namespace common;

namespace fs = std::filesystem;

namespace
{
[[noreturn]] void
atexit_fn()
{
  auto LOGGER = log_factory::make_stderr();
  LOG_ERROR("ATEXIT fn handler invoked");
  std::exit(EXIT_FAILURE);
}

[[noreturn]] void
signal_fn(int const signum)
{
  auto LOGGER = log_factory::make_stderr();

  char const* strname =
#ifdef _WIN64
      "(NOTE: mapping signal IT -> NAME not implemented on windows platform)";
#else
      ::strsignal(signum);
#endif
  LOG_ERROR("Handling signal num:'%i':'%s'", signum, strname);

  stacktrace::dump_to_file(LOGGER);
  LOG_ERROR("Finished generating stacktrace.");

  common::cpu_breakpoint();
  std::exit(EXIT_FAILURE);
}

template <typename FN>
void
draw_window(SDLWindow& w, FN const& fn)
{
  if (!w.is_shown()) {
    return;
  }
  w.make_current();
  fn(w);
  ::SDL_GL_SwapWindow(w.window_ptr());
}

void
draw_gui_texture_window(log_t& logger, SDLWindow& w, Camera const& camera, GameState& gs,
                        texture_h& ti, draw_call_counter& ds)
{
  auto const fr = w.view_frustum();
  auto       cm = camera::gui_matrices(camera, fr);

  auto& ldata = gs.ldata;
  auto& sp    = shader2d::texture2d(ldata.registry);

  BIND_UNTIL_END_OF_SCOPE(logger, sp);
  BIND_UNTIL_END_OF_SCOPE(logger, ti);

  auto const rect = rect::create_float(0, 0, fr.width(), fr.height());
  auto const uvs  = rect::create_float(0, 0, 1, 1);
  opengl::render::draw_2dtextured_rect(logger, cm.proj, rect, uvs, sp, ti, ds);
}

void
update_mousebuttons(SDL_MouseButtonEvent& event, Mouse& mouse)
{
  auto const get_mp = [&mouse]() { return glm::vec2{mouse.coords().x, mouse.coords().y}; };

  auto&         cp  = mouse.click_position;
  uint8_t const btn = event.button;
  if (btn == SDL_BUTTON_MIDDLE) {
    cp.middle = get_mp();
  }
  else if ((SDL_BUTTON_LEFT == btn) || (SDL_BUTTON_RIGHT == btn)) {
    cp.left_right = get_mp();
  }
}

} // namespace

namespace
{
class BoomHSEventHandler
{
  SDLWindow&     window_;
  WindowID const wid_;

public:
  NO_MOVE_OR_COPY(BoomHSEventHandler);
  explicit BoomHSEventHandler(SDLWindow& w)
      : window_(w)
      , wid_(window_.id())
  {
  }

  void process_event(log_t& LOGGER, SDL_Event& event, GameState& gs, Devices& devs,
                     SDLWindow& window, Camera& camera, DeltaTime const& dt)
  {
    auto const fr  = window.view_frustum();
    auto const ke  = [&]() { return KeyEvent{gs, devs, camera, fr, event}; };
    auto const mbe = [&]() { return MouseButtonEvent{gs, devs, camera, fr, event.button}; };
    auto const mme = [&]() { return MouseMotionEvent{gs, devs, camera, event.motion}; };
    auto const mwe = [&]() { return MouseWheelEvent{gs, camera, event.wheel}; };

    auto const handle_event = [&](auto& screen) {
      auto const type = event.type;
      switch (type) {
      case SDL_KEYDOWN:
        screen.on_key_down(LOGGER, screen, ke(), dt);
        break;
      case SDL_KEYUP:
        screen.on_key_up(LOGGER, screen, ke(), dt);
        break;

      case SDL_MOUSEBUTTONDOWN:
        update_mousebuttons(event.button, devs.mouse);
        screen.on_mouse_button_down(LOGGER, screen, mbe(), dt);
        break;
      case SDL_MOUSEBUTTONUP:
        screen.on_mouse_button_up(LOGGER, screen, mbe(), dt);
        break;

      case SDL_MOUSEMOTION:
        screen.on_mouse_motion(LOGGER, screen, mme(), dt);
        break;
      case SDL_MOUSEWHEEL:
        update_mousebuttons(event.button, devs.mouse);
        screen.on_mouse_wheel(LOGGER, screen, mwe(), dt);
        break;
      }
    };
    {
      auto& sstack = gs.sstack;
      assert(!sstack.empty());
      auto& screen = sstack.top();

      switch (screen.type) {
      case ScreenType::InGame:
        handle_event(static_cast<InGameScreen&>(screen));
        break;

      case ScreenType::MainMenu:
      case ScreenType::Options:
        handle_event(static_cast<MainMenuScreen&>(screen));
        break;
      }
    }
  }

  auto wid() const { return wid_; }
};

void
loop_events(log_t& LOGGER, GameState& gs, Devices& devs, WindowManager& wm, Camera& camera,
            DeltaTime const& dt)
{
  auto&              w0 = wm[0];
  BoomHSEventHandler eh{w0};
  bool&              quit = gs.quit;

  SDL_Event event;
  while ((!quit) && (0 != SDL_PollEvent(&event))) {
    if (eh.wid() != event.window.windowID) {
      continue;
    }

    eh.process_event(LOGGER, event, gs, devs, w0, camera, dt);
    quit |= wm.check_window_events(LOGGER, gs, event);
  }
}

void
draw_windows(log_t& LOGGER, GameState& gs, WindowManager& wm, Camera& camera, draw_call_counter& ds,
             DeltaTime const& dt)
{
  draw_window(wm[0], [&](SDLWindow& w) {
    auto const   fr = w.view_frustum();
    auto const   cm = camera::compute_matrices(camera, fr, camera.mode, camera.position());
    render_state rs{cm, fr, ds};

    boomhs::draw_everything(LOGGER, gs, camera, fr, rs, dt);
  });

  draw_window(wm[1], [&](SDLWindow& w) {
    OR::clear_screen(LOC4::WHITE);

    if (!gs.graphics.enable_sunshafts) {
      auto& ldata             = gs.ldata;
      auto& srs               = ldata.srenders;
      auto& sunshaft_renderer = srs.sunshaft;
      auto& ti                = sunshaft_renderer.texture_info();
      draw_gui_texture_window(LOGGER, w, camera, gs, ti, ds);
    }
  });

  draw_window(wm[2], [&](SDLWindow& w) {
    OR::clear_screen(LOC4::WHITE);

    if (GraphicsMode::Advanced == gs.graphics.mode) {
      auto& ldata = gs.ldata;
      auto& srs   = ldata.srenders;
      auto& wr    = srs.water.advanced;
      auto& ti    = wr.reflection_ti();
      draw_gui_texture_window(LOGGER, w, camera, gs, ti, ds);
    }
  });
  draw_window(wm[3], [&](SDLWindow& w) {
    OR::clear_screen(LOC4::WHITE);

    if (GraphicsMode::Advanced == gs.graphics.mode) {
      auto& ldata = gs.ldata;
      auto& srs   = ldata.srenders;
      auto& wr    = srs.water.advanced;
      auto& ti    = wr.refraction_ti();
      draw_gui_texture_window(LOGGER, w, camera, gs, ti, ds);
    }
  });
}

void
timed_game_loop(log_t& LOGGER, Engine& engine, WindowManager& wm, GameState& gs, Camera& camera)
{
  using namespace std::chrono;

  auto& wc   = gs.wclock;
  auto& devs = engine.devices;

  float constexpr MIN_DT   = 0.01f;
  [[maybe_unused]] float t = 0;
  float accumulator        = 0;
  auto  current_time       = steady_clock::now();

  auto const game_loop = [&](auto const& delta_time) {
    auto& w0 = wm[0];
    boomhs::game_loop(LOGGER, w0, gs, devs, camera, delta_time);
  };

  {
    // Run game_loop atleast once, if we don't do this then draw() gets called the very first loop
    auto const delta_time = create_dt(wc, MIN_DT);
    game_loop(delta_time);
  }

  while (!gs.quit) {
    auto const new_time   = steady_clock::now();
    auto       frame_time = duration_cast<duration<float>>(new_time - current_time).count();
    if (frame_time > 0.25f) {
      frame_time = 0.25f;
    }
    current_time = new_time;
    accumulator += frame_time;

    int num_gamesloops = 0;
    while (accumulator >= MIN_DT) {
      auto const delta_time = create_dt(wc, MIN_DT);
      loop_events(LOGGER, gs, devs, wm, camera, delta_time);
      game_loop(delta_time);
      t += MIN_DT;
      accumulator -= MIN_DT;
      ++num_gamesloops;
    }
    {
      draw_call_counter draw_state;
      draw_state.draw_wireframes = gs.uibuffers_classic.debug.wireframe_override;

      auto const delta_time = create_dt(wc, MIN_DT);
      draw_windows(LOGGER, gs, wm, camera, draw_state, delta_time);
    }
    LOG_TRACE("%i game loops per render", num_gamesloops);
  }
}

Result<common::none_t, std::string>
start(log_t& LOGGER, Engine& engine, WindowManager& wm)
{
  // camera-look at origin
  // cameraspace "up" is === "up" in worldspace.
  auto const PERS_FORWARD = -constants::Z_UNIT_VECTOR;
  auto constexpr PERS_UP  = constants::Y_UNIT_VECTOR;
  WorldOrientation const wo_3dperspective{PERS_FORWARD, PERS_UP};

  auto const ORTHO_FORWARD = -constants::Y_UNIT_VECTOR;
  auto constexpr ORTHO_UP  = constants::Z_UNIT_VECTOR;
  WorldOrientation const wo_2dorthographic{ORTHO_FORWARD, ORTHO_UP};
  auto camera = camera::make_default(CameraMode::ThirdPerson, wo_3dperspective, wo_2dorthographic);
  camera.ortho.flip_rightv = true;

  auto& w0 = wm[0];
  w0.make_current();

  EngineState      es{engine.wclock};
  GraphicsSettings gmode;

  auto const fr = w0.view_frustum();
  auto       gs = TRY(
      boomhs::create_gamestate(LOGGER, es, camera, engine.registries, wo_3dperspective, gmode, fr));

  engine.devices.display_info = TRY(gl_sdl::get_all_display_modes());

  // Start game in a timed loop
  timed_game_loop(LOGGER, engine, wm, gs, camera);

  // Game has finished
  LOG_TRACE("game loop finished.");
  return OK_NONE;
}

Result<log_t, const char*>
make_logger(char const* title)
{
  auto time_str = date::format("%F %T", std::chrono::system_clock::now());
  std::replace(time_str.begin(), time_str.end(), ':', '-');

  auto const logpath  = fs::current_path() / "build-system" / "bin" / "logs" / "";
  auto const log_name = fmt::sprintf("%s%s-%s.txt", logpath.string(), time_str, title);

  auto const log_level = []() {
#ifdef LOG_LEVEL
    return spdlog::level::err;
#else
    return spdlog::level::info;
#endif
  }();

  return Ok(log_factory::make_default(log_name.c_str(), log_level));
}

} // namespace

int
main(int, char**)
{
  assert(SIG_ERR != std::signal(SIGSEGV, &signal_fn));
  assert(SIG_ERR != std::signal(SIGABRT, &signal_fn));
  assert(0 == std::atexit(atexit_fn));
  assert(0 == std::at_quick_exit(atexit_fn));

  constexpr char const* TITLE = "BoomHS";

  auto logger_result = make_logger(TITLE);
  if (!logger_result) {
    std::cerr << logger_result.unwrap_error();
    return EXIT_FAILURE;
  }
  auto LOGGER = logger_result.unwrap();
  stacktrace::check_on_disk(LOGGER);

  auto const on_error = [&](auto const& error) {
    LOG_ERROR(error);
    return EXIT_FAILURE;
  };

  LOG_DEBUG("Initializing OpenGL context and SDL window.");
  auto glsdl_system = gl_sdl::init_sdl(LOGGER).expect_fn(on_error);
  auto controller   = SDLControllers::find_attached_controllers(LOGGER).expect_fn(on_error);
  auto display_info = gl_sdl::get_all_display_modes().expect_fn(on_error);

  WindowConfig wconfig;
  wconfig.title  = TITLE;
  wconfig.width  = 1024;
  wconfig.height = 768;

  wconfig.flags = SDL_WINDOW_RESIZABLE | SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN;

  auto w0 = gl_sdl::make_window(LOGGER, glsdl_system, wconfig).expect_fn(on_error);

  wconfig.width  = 800;
  wconfig.height = 600;

  wconfig.xpos = SDL_WINDOWPOS_CENTERED;
  wconfig.ypos = SDL_WINDOWPOS_CENTERED;

  wconfig.title = "SUNSHINE BABY";
  auto w1       = gl_sdl::make_window(LOGGER, glsdl_system, wconfig).expect_fn(on_error);
  w1.hide_window();

  wconfig.title = "REFLECTION";
  auto w2       = gl_sdl::make_window(LOGGER, glsdl_system, wconfig).expect_fn(on_error);
  w2.hide_window();

  wconfig.title = "REFRACTION";
  auto w3       = gl_sdl::make_window(LOGGER, glsdl_system, wconfig).expect_fn(on_error);
  w3.hide_window();

  LOG_INFO("Initializing OpenAL");
  auto alstate = audio::init(LOGGER).expect_fn(on_error);
  ON_SCOPE_EXIT([&]() { audio::shutdown(LOGGER, alstate); });
  LOG_INFO("Initializing OpenAL -- SUCCESS");

  //////////////////////////////////////////////////////////////////////////////////////////////////
  // Construct the engine
  Engine        engine{WallClock{}, MOVE(controller), MOVE(display_info), *alstate.pdevice};
  WindowManager wm;
  wm.add(MOVE(w0));
  wm.add(MOVE(w1));
  wm.add(MOVE(w2));
  wm.add(MOVE(w3));

  LOG_INFO("Starting game loop");
  start(LOGGER, engine, wm).expect_fn(on_error);

  LOG_INFO("Game loop finished successfully! Ending program now.");
  return EXIT_SUCCESS;
}
