#include "boomhs.hpp"
#include <boomhs/audio.hpp>
#include <boomhs/billboard.hpp>
#include <boomhs/bounding_object.hpp>
#include <boomhs/camera.hpp>
#include <boomhs/camera_algorithm.hpp>
#include <boomhs/chat_history.hpp>
#include <boomhs/collision.hpp>
#include <boomhs/components.hpp>
#include <boomhs/controller.hpp>
#include <boomhs/delta_time.hpp>
#include <boomhs/engine.hpp>
#include <boomhs/entity.hpp>
#include <boomhs/game_config.hpp>
#include <boomhs/heightmap.hpp>
#include <boomhs/io_sdl.hpp>
#include <boomhs/item.hpp>
#include <boomhs/item_factory.hpp>
#include <boomhs/level_loader.hpp>
#include <boomhs/main_menu.hpp>
#include <boomhs/mouse.hpp>
#include <boomhs/movement.hpp>
#include <boomhs/npc.hpp>
#include <boomhs/ortho_renderer.hpp>
#include <boomhs/perspective_renderer.hpp>
#include <boomhs/player.hpp>
#include <boomhs/scene_renderer.hpp>
#include <boomhs/screen.hpp>
#include <boomhs/skybox.hpp>
#include <boomhs/start_area_generator.hpp>
#include <boomhs/terrain.hpp>
#include <boomhs/tree.hpp>
#include <boomhs/ui_chatwindow.hpp>
#include <boomhs/vertex_factory.hpp>
#include <boomhs/vertex_interleave.hpp>
#include <boomhs/water.hpp>

#include <math/random.hpp>
#include <math/raycast.hpp>

#include <gl_sdl/sdl_window.hpp>

#include <opengl/bind.hpp>
#include <opengl/gpu.hpp>
#include <opengl/texture.hpp>
#include <opengl/uniform.hpp>

#include <common/log.hpp>
#include <common/result.hpp>

#include <extlibs/fastnoise.hpp>
#include <extlibs/sdl.hpp>

#include <cassert>
#include <chrono>
#include <cstdlib>
#include <ctime>
#include <iomanip>
#include <sstream>
#include <string>

namespace
{
void
add_mesh(log_t& LOGGER, ObjStore& obj_store, eid_t const eid, enttreg_t& reg)
{
  auto& sc = reg.get<ShaderComponent>(eid);
  auto& va = shader3d::ref(reg, sc.value).va();

  auto const& mesh_name = reg.get<MeshRenderable>(eid).name;
  auto const& obj       = obj_store.get(LOGGER, mesh_name);
  {
    auto handle = OG::copy_gpu(LOGGER, va, obj);
    reg.emplace<opengl::draw_state>(eid, MOVE(handle));
  }
  auto const  posbuffer = obj.positions();
  auto const& min       = posbuffer.min();
  auto const& max       = posbuffer.max();
  AABoundingBox::add_to_entity(LOGGER, reg, eid, min, max);
}

void
add_cube(log_t& LOGGER, eid_t const eid, enttreg_t& reg)
{
  auto const& cr = reg.get<CubeRenderable>(eid);
  auto&       sc = reg.get<ShaderComponent>(eid);
  auto&       va = shader3d::ref(reg, sc.value).va();

  auto const vertices = VertexFactory::build_cube(cr.min, cr.max);
  {
    auto handle = OG::copy_cube_gpu(LOGGER, vertices, va);
    reg.emplace<opengl::draw_state>(eid, MOVE(handle));
  }
  AABoundingBox::add_to_entity(LOGGER, reg, eid, cr.min, cr.max);
}

bool
player_in_water(log_t& LOGGER, enttreg_t& reg)
{
  auto const& player      = find_player(reg);
  auto const& player_bbox = player.bounding_box().cube;
  auto const& player_tr   = player.transform();

  auto const eids = find_all_entities_with_component<WaterInfo, Transform, AABoundingBox>(reg);
  for (auto const eid : eids) {
    auto const& water_bbox = reg.get<AABoundingBox>(eid).cube;
    auto const& water_info = reg.get<WaterInfo>(eid);

    auto water_tr    = reg.get<Transform>(eid);
    water_tr.scale.x = water_info.dimensions.x;
    water_tr.scale.z = water_info.dimensions.y;

    CubeTransform const& player_ct{player_bbox, player_tr};
    CubeTransform const& water_ct{water_bbox, water_tr};
    if (collision::overlap_axis_aligned(LOGGER, player_ct, water_ct)) {
      return true;
    }
  }
  return false;
}

void
update_playaudio(log_t& LOGGER, GameState& gs, enttreg_t& reg)
{
  auto& audio = gs.was;
  audio.set_volume(gs.uibuffers_classic.audio.ambient);

  if (player_in_water(LOGGER, reg)) {
    audio.play_inwater_sound();
  }
  else {
    audio.stop_inwater_sound();
  }
}

void
set_heights_ontop_terrain(log_t& LOGGER, TerrainGrid& terrain, enttreg_t& reg, eid_t const eid)
{
  auto&       transform = reg.get<Transform>(eid);
  auto const& bbox      = reg.get<AABoundingBox>(eid).cube;
  auto&       tr        = transform.translation;
  float const height    = terrain.get_height(LOGGER, tr.x, tr.z);

  // update original transform
  tr.y = bbox.half_widths().y + height;
}

void
update_npcpositions(log_t& LOGGER, enttreg_t& reg, TerrainGrid& terrain)
{
  auto const update = [&](auto const eid) {
    auto& npcdata = reg.get<NPCData>(eid);
    auto& npc_hp  = npcdata.health;
    if (NPC::is_dead(npc_hp)) {
      return;
    }
    set_heights_ontop_terrain(LOGGER, terrain, reg, eid);
  };
  for (auto const eid : reg.view<NPCData, Transform, AABoundingBox>()) {
    update(eid);
  }
}

void
update_nearbytargets(NearbyTargets& nbt, enttreg_t& reg)
{
  auto const& player = find_player(reg);

  auto const         enemies = find_enemies(reg);
  std::vector<eid_t> eids;
  for (auto const eid : enemies) {
    if (reg.get<IsRenderable>(eid).hidden) {
      continue;
    }
    eids.emplace_back(eid);
  }

  auto const& ppos    = player.transform().translation;
  auto const  sort_fn = [&](auto const& aeid, auto const& beid) {
    auto const& apos = reg.get<Transform>(aeid).translation;
    auto const& bpos = reg.get<Transform>(beid).translation;
    return glm::distance(ppos, apos) < glm::distance(ppos, bpos);
  };
  std::sort(eids.begin(), eids.end(), sort_fn);

  auto const selected_o = nbt.selected();
  nbt.clear();
  for (eid_t const eid : eids) {
    nbt.add_target(eid);
  }

  if (selected_o) {
    nbt.set_selected(*selected_o);
  }
}

void
update_orbital_bodies(UiDebugState const& ui_debug, ViewMatrix const& vm, ProjMatrix const& pm,
                      enttreg_t& reg, DeltaTime const& dt)
{
  // Must re-calculate ldata and reg, possibly changed since call to move_between()
  auto const update_orbitals = [&](auto const eid) {
    auto& transform = reg.get<Transform>(eid);
    auto& orbital   = reg.get<OrbitalBody>(eid);
    auto& pos       = transform.translation;

    auto constexpr SLOWDOWN_FACTOR = 50.0f;
    auto const  time               = dt() * dt.since_start_millis<float>() / SLOWDOWN_FACTOR;
    float const cos_time           = std::cos(time + orbital.offset);
    float const sin_time           = std::sin(time + orbital.offset);

    pos.x = orbital.radius.x * cos_time;
    pos.y = orbital.radius.y * sin_time;
    pos.z = orbital.radius.z * sin_time;

    if (reg.try_get<DirectionalLight>(eid)) {
      auto const mvp  = (pm * vm) * transform.model_matrix();
      auto const clip = mvp * glm::vec4{pos, 1.0f};
      auto const ndc  = glm::vec3{clip.x, clip.y, clip.z} / clip.w;

      auto const wx = ((ndc.x + 1.0f) / 2.0f); // + 256.0;
      auto const wy = ((ndc.y + 1.0f) / 2.0f); // + 192.0;

      auto& dlight = reg.get<DirectionalLight>(eid);
      {
        auto const orbital_to_origin_direction = glm::normalize(-pos);
        dlight.direction                       = orbital_to_origin_direction;
      }
      {
        glm::vec2 const wpos{wx, wy};
        dlight.screenspace_pos = wpos;
      }
    }
  };

  auto const eids = find_orbital_bodies(reg);
  if (ui_debug.update_orbital_bodies) {
    for (auto const eid : eids) {
      update_orbitals(eid);
    }
  }
}

inline auto
find_torches(enttreg_t& reg)
{
  eid_array torches;
  auto      view = reg.view<Torch>();
  for (auto const eid : view) {
    assert(reg.try_get<Transform>(eid));
    torches.emplace_back(eid);
  }
  return torches;
}

void
update_torchflicker(enttreg_t& reg, rng_t& rng, DeltaTime const& dt)
{
  auto const update_torch = [&](auto const eid) {
    auto&      pointlight = reg.get<PointLight>(eid);
    auto&      light      = pointlight.light;
    auto const v          = std::sin(dt() * dt.since_start_millis<float>() * math::constants::PI);

    auto& flicker  = reg.get<LightFlicker>(eid);
    light.diffuse  = color::lerp(flicker.colors[0], flicker.colors[1], v);
    light.specular = light.diffuse;

    auto& item            = reg.get<Item>(eid);
    auto& torch_transform = reg.get<Transform>(eid);
    if (item.is_pickedup) {
      // Player try_get picked up the torch, make it follow player around
      auto const& player     = find_player(reg);
      auto const& player_pos = player.world_position();

      torch_transform.translation = player_pos;

      // Move the light above the player's head
      torch_transform.translation.y = 1.0f;
    }

    // auto const torch_pos = torch_transform.translation;
    // auto&      attenuation = pointlight.attenuation;

    auto const attenuate = [&rng](float& value, float const gen_range, float const base_value) {
      value += rng.gen(-gen_range, gen_range);

      auto const clamp = gen_range * 2.0f;
      value            = glm::clamp(value, base_value - clamp, base_value + clamp);
    };

    // static float constexpr CONSTANT = 0.1f;
    // attenuate(attenuation.constant, CONSTANT, torch.default_attenuation.constant);

    // static float constexpr LINEAR = 0.015f;
    // attenuate(attenuation.linear, LINEAR, torch.default_attenuation.linear);

    // static float constexpr QUADRATIC = LINEAR * LINEAR;
    // attenuate(attenuation.quadratic, QUADRATIC, torch.default_attenuation.quadratic);

    static float constexpr SPEED_DELTA = 0.24f;
    attenuate(flicker.current_speed, SPEED_DELTA, flicker.base_speed);
  };
  auto const torches = find_torches(reg);
  for (auto const eid : torches) {
    update_torch(eid);
  }
}

void
update_visible_entities(LevelData&, enttreg_t& reg)
{
  // auto& ldata        = ldata;
  // auto& terrain_grid = ldata.terrain;

  for (auto const eid : reg.view<NPCData>()) {
    auto& isr  = reg.get<IsRenderable>(eid);
    isr.hidden = false; // terrain_grid.is_visible(reg);
  }
}

bool
is_target_selected_and_alive(enttreg_t& reg, NearbyTargets const& nbt)
{
  auto const target = nbt.selected();
  if (!target) {
    return false; // target not selected
  }
  auto const target_eid = *target;
  auto&      npcdata    = reg.get<NPCData>(target_eid);
  auto&      target_hp  = npcdata.health;
  return !NPC::is_dead(target_hp);
}

void
update_boomhs(log_t& LOGGER, SDLWindow& window, GameState& gs, Devices& ds, Camera const& camera,
              DeltaTime const& dt)
{
  auto& ldata  = gs.ldata;
  auto& reg    = ldata.registry;
  auto& skybox = ldata.skybox;
  auto& ttable = ldata.ttable;
  auto& nbt    = ldata.nearby_targets;

  // Update the world
  update_playaudio(LOGGER, gs, reg);
  {
    auto const fr          = window.view_frustum();
    auto const view_matrix = camera.view_matrix(fr);
    auto const proj_matrix = camera.proj_matrix(fr);

    auto const& ui_debug = gs.uibuffers_classic.debug;
    update_orbital_bodies(ui_debug, view_matrix, proj_matrix, reg, dt);
  }
  skybox.update(dt);

  update_visible_entities(ldata, reg);
  update_torchflicker(reg, gs.rng, dt);

  // Update these as a chunk, so they stay in the correct order.
  {
    auto& tgrid = ldata.tgrid;
    update_npcpositions(LOGGER, reg, tgrid);
  }
  update_nearbytargets(nbt, reg);

  // LOG_ERROR("ortho cam pos: %s, player pos: %s",
  // glm::to_string(camera.ortho.position),
  // glm::to_string(player.transform().translation));

  // auto& terrain = ldata.terrain;
  // for (auto const eid : reg.view<Transform, MeshRenderable>()) {
  // set_heights_ontop_terrain(LOGGER, terrain, reg, eid);
  //}

  auto& player = find_player(reg);
  player.update(LOGGER, gs, dt);

  bool const previously_alive = is_target_selected_and_alive(reg, nbt);
  if (previously_alive) {
    auto const target = nbt.selected();
    if (target) {
      auto const target_eid               = *target;
      auto&      npcdata                  = reg.get<NPCData>(target_eid);
      auto&      target_hp                = npcdata.health;
      bool const target_dead_after_attack = NPC::is_dead(target_hp);
      bool const dead_from_attack         = previously_alive && target_dead_after_attack;

      auto const add_worlditem_at_targets_location = [&](eid_t const item_eid) {
        auto& item_tr = reg.get<Transform>(item_eid);

        auto const& target_pos = reg.get<Transform>(target_eid).translation;
        item_tr.translation    = target_pos;
        item_tr.rotate_degrees(90, EulerAxis::X);
        auto const& item_name = reg.get<Name>(item_eid).value;
        LOG_ERROR("ADDING item %s AT xyz: %s", item_name, glm::to_string(item_tr.translation));

        auto& obj_store = ldata.objs;
        add_mesh(LOGGER, obj_store, item_eid, reg);
      };
      if (dead_from_attack) {
        auto const book_eid = ItemFactory::create_book(reg, ttable);
        add_worlditem_at_targets_location(book_eid);

        auto const spear_eid = ItemFactory::create_spear(reg, ttable);
        add_worlditem_at_targets_location(spear_eid);
      }
    }
  }
  for (auto& textbox : gs.sstack.top().uistate.textboxes) {
    textbox->update(LOGGER, ds.mouse);
  }
}

void
update_everything(log_t& LOGGER, SDLWindow& window, GameState& gs, Devices& ds,
                  Camera const& camera, DeltaTime const& dt)
{
  auto& ss = gs.sstack;
  assert(!ss.empty());
  ss.top().update_fn(LOGGER, window, gs, ds, camera, dt);
} // namespace

Result<NOTHING, std::string>
copy_assets_gpu(log_t& LOGGER, enttreg_t& reg, ObjStore& obj_store)
{
  // copy CUBES to GPU
  reg.view<ShaderComponent, CubeRenderable>().each(
      [&](eid_t const eid, auto&&...) { add_cube(LOGGER, eid, reg); });

  // copy MESHES to GPU
  reg.view<ShaderComponent, MeshRenderable>().each(
      [&](eid_t const eid, auto&&...) { add_mesh(LOGGER, obj_store, eid, reg); });

  // copy billboarded textures to GPU
  reg.view<ShaderComponent, BillboardRenderable, TextureRenderable>().each(
      [&](eid_t const eid, auto& sc, auto&, auto& texture) {
        auto& va = shader2d::ref(reg, sc.value).va();
        auto* ti = texture.texture_info;
        assert(ti);

        auto const v        = VertexFactory::build_default();
        auto const uv       = uv_factory::build_rectangle(texture.uvs);
        auto const vertices = vertex_interleave(v, uv);
        auto       handle   = opengl::gpu::copy_rectangle(LOGGER, va, vertices);

        reg.emplace<opengl::draw_state>(eid, MOVE(handle));
      });

  // Update the tree's to match their initial values.
  reg.view<ShaderComponent, MeshRenderable, TreeComponent>().each(
      [&](auto entity, ShaderComponent& sc, MeshRenderable&, TreeComponent&) {
        // TODO: This is weird, why not go through the Mesh& above??
        // auto& name = reg.get<MeshRenderable>(entity).name;

        auto& va = shader3d::ref(reg, sc.value).va();
        // auto const     flags = opengl::BufferFlags::from_va(va);
        // ObjQuery const query{name, flags};
        // auto&          obj = obj_store.get(LOGGER, name);

        // TODO: This is weird, why not go through the Tree& above??
        auto&       tc    = reg.get<TreeComponent>(entity);
        auto const& dinfo = reg.get<opengl::draw_state>(entity);
        Tree::update_colors(LOGGER, va, dinfo, tc);
      });

  return OK_NONE;
}

void
add_orbitalbodies_and_water(log_t& LOGGER, LevelData& ldata, GraphicsMode const gmode)
{
  auto& reg = ldata.registry;
  for (auto const eid : reg.view<OrbitalBody>()) {
    auto constexpr MIN = glm::vec3{-1.0f};
    auto constexpr MAX = glm::vec3{-1.0f};
    AABoundingBox::add_to_entity(LOGGER, reg, eid, MIN, MAX);
  }
  for (auto const eid : reg.view<WaterInfo>()) {
    {
      opengl::buffer_flags const flags{true, false, false, true};

      auto&      wi           = reg.get<WaterInfo>(eid);
      auto const dimensions   = wi.dimensions;
      auto const num_vertexes = wi.num_vertexes;
      auto const data         = WaterFactory::generate_water_data(LOGGER, dimensions, num_vertexes);
      auto const buffer       = opengl::vertex_buffer::create_interleaved(LOGGER, data, flags);

      auto& sp     = graphics_mode_to_water_shader(reg, gmode);
      auto  handle = opengl::gpu::copy_gpu(LOGGER, sp.va(), buffer);
      reg.emplace<opengl::draw_state>(eid, MOVE(handle));

      wi.eid = eid;
    }
  }
}

std::string
floornumber_to_levelfilename(int const floor_number)
{
  return "area" + std::to_string(floor_number) + ".toml";
}

void
draw_game(log_t& LOGGER, GameState& gs, Camera const& camera, Frustum const& fr,
          opengl::render_state& rs, DeltaTime const& dt)
{
  auto& srs = gs.ldata.srenders;

  auto const mode = camera.mode;
  if (CameraMode::FPS == mode || CameraMode::ThirdPerson == mode) {
    auto const vp = Viewport::from_frustum(fr);
    opengl::render::set_viewport_and_scissor(vp, fr.height());

    perspective::draw_scene(LOGGER, gs, rs, camera, srs, dt);
  }
  else if (CameraMode::Ortho == mode) {
    ortho::draw_scene(LOGGER, gs, rs, camera, srs, dt);
  }
  else {
    std::exit(EXIT_FAILURE);
  }
}

Result<NOTHING, std::string>
resize_fn(log_t& LOGGER, GameState& gs, Screen& screen, Frustum const& fr)
{
  {
    auto&      rs = gs.ldata.srenders;
    auto const vp = Viewport::from_frustum(fr);
    rs.resize(LOGGER, vp);
  }
  for (auto& textbox : screen.uistate.textboxes) {
    TRY(textbox->resize(LOGGER, fr));
  }
  return OK_NONE;
}

Result<GameState, std::string>
init_gs(log_t& LOGGER, EngineState& es, LevelData&& ldata, WaterAudioSystem&& water_audio,
        GraphicsSettings const gsets, Frustum const& fr)
{
  GameState gs{es.quit, es.wclock, es.rng, MOVE(ldata), MOVE(water_audio), gsets};
  {
    ScreenVector screens;
    screens.emplace_back(std::make_unique<InGameScreen>());
    {
      auto mm_screens = TRY(main_menu::create_list_screens(LOGGER, gs, fr));
      common::concat_vector(screens, MOVE(mm_screens));
    }
    gs.sstack = ScreenStack{MOVE(screens)};
  }
  {
    auto& s     = gs.sstack.find_or_abort(ScreenType::InGame);
    s.update_fn = ::update_boomhs;
    s.draw_fn   = ::draw_game;
    s.resize_fn = ::resize_fn;
    gs.sstack.push(ScreenType::MainMenu);
  }
  ldata.registry.set<MovementState>();
  return OK_MOVE(gs);
}

} // namespace

namespace
{
enum class MouseButton
{
  LEFT,
  RIGHT
};

namespace
{
void
fps_mousemove(Camera& camera, Player& player, float const xrel, float const yrel,
              DeltaTime const& dt)
{
  camera::rotate_radians(camera, xrel, yrel, dt);
  player.world_object().rotate_to_match_camera_rotation(camera);

  player.transform().rotate_degrees(180.0f, EulerAxis::Y);
}

void
thirdperson_mousemove(Player& player, Camera& camera, Mouse const& ms, float const xrel,
                      float const yrel, DeltaTime const& dt)
{
  if (ms.left_pressed()) {
    camera::rotate_radians(camera, xrel, yrel, dt);
  }
  if (ms.right_pressed()) {
    auto constexpr ROTATE_ANGLE = 1.0;
    float angle                 = xrel > 0 ? ROTATE_ANGLE : -ROTATE_ANGLE;
    angle *= dt();

    auto const degrees = glm::degrees(angle);
    player.world_object().rotate_degrees(degrees, EulerAxis::Y);
  }
}

using EntityDistances = std::vector<std::pair<eid_t, float>>;

bool
ray_intersects_cube_entity(log_t& LOGGER, eid_t const eid, Ray const& ray, Transform const& tr,
                           Cube const& cube, EntityDistances& distances)
{
  float      distance   = 0.0f;
  bool const intersects = collision::intersects(LOGGER, ray, tr, cube, distance);
  if (intersects) {
    distances.emplace_back(PAIR(eid, distance));
  }
  return intersects;
}

void
select_mouse_under_cursor(log_t& LOGGER, Camera const& camera, enttreg_t& reg, Devices& devs,
                          Frustum const& fr, MouseButton const mb)
{
  auto const      coords = devs.mouse.coords();
  glm::vec2 const mouse_pos{coords.x, coords.y};

  auto const proj_matrix = camera.proj_matrix(fr);
  auto const view_matrix = camera.view_matrix(fr);
  auto const view_rect   = Viewport::from_frustum(fr);

  glm::vec3 const ray_start = /*(CameraMode::Ortho == fstate.camera_mode) */
                              //? CameraORTHO::EYE_FORWARD
      /*:*/ camera.position();
  glm::vec3 const ray_dir =
      Raycast::calculate_ray_into_screen(mouse_pos, proj_matrix, view_matrix, view_rect);
  Ray const ray{ray_start, ray_dir};

  EntityDistances distances;
  for (auto const eid : find_all_entities_with_component<Selectable>(reg)) {
    auto const& cube = reg.get<AABoundingBox>(eid).cube;
    auto const& tr   = reg.get<Transform>(eid);
    auto&       sel  = reg.get<Selectable>(eid);

    bool const intersects = ray_intersects_cube_entity(LOGGER, eid, ray, tr, cube, distances);
    if (intersects) {
      // LOG_INFO("\n\n\n\n\n\n\nIntersects something\n\n\n\n\n\n\n");
      // LOG_INFO("mouse pos: %s", glm::to_string(mouse_pos));
      // LOG_INFO("ray_start %s, ray_dir %s", glm::to_string(ray_start),
      // glm::to_string(ray_dir));
    }
    sel.selected = intersects;
  }
  bool const something_selected = !distances.empty();
  if (something_selected) {
    auto const cmp = [](auto const& l, auto const& r) { return l.second < r.second; };
    std::sort(distances.begin(), distances.end(), cmp);
    auto const& pair = mb == MouseButton::LEFT ? distances.front() : distances.back();

    auto const eid  = pair.first;
    auto const name = reg.try_get<Name>(eid) ? reg.get<Name>(eid).value : "Unnamed";
  }
}

} // namespace

} // namespace

Result<GameState, std::string>
create_gs(log_t& LOGGER, EngineState& es, entt_registries& registries, WorldOrientation const& wo,
          GraphicsSettings const gsettings, Frustum const& fr)
{
  int constexpr FLOOR_NUMBER = 0;
  auto const vp              = Viewport::from_frustum(fr);

  auto& reg            = registries[FLOOR_NUMBER];
  auto  level_name     = floornumber_to_levelfilename(FLOOR_NUMBER);
  auto  level_assets   = TRY(load_level(LOGGER, reg, es.rng, level_name));
  auto& ttable         = level_assets.ttable;
  auto& material_table = level_assets.mtable;

  {
    // TODO: somewhere that makes more sense
    auto& sp = shader2d::texture2d(reg);
    BIND_UNTIL_END_OF_SCOPE(LOGGER, sp);
    opengl::uniform::set(LOGGER, sp, "u_sampler", 0);
  }

  constexpr char const* HEIGHTMAP_NAME = "Area0-HM";
  auto const            heightmap = TRY(heightmap::load_fromtable(LOGGER, ttable, HEIGHTMAP_NAME));

  auto gendata =
      TRY(start_area::gen_level(LOGGER, reg, es.rng, ttable, material_table, heightmap, wo));

  LevelData ldata{LOGGER, MOVE(level_assets), MOVE(gendata.tgrid), reg, vp};
  TRY(copy_assets_gpu(LOGGER, reg, ldata.objs));
  add_orbitalbodies_and_water(LOGGER, ldata, gsettings.mode);

  // auto& terrain = ldata.terrain;
  // for (auto const eid : reg.view<Transform, AABoundingBox, MeshRenderable>()) {
  // set_heights_ontop_terrain(LOGGER, terrain, reg, eid);
  //}
  auto const weid        = reg.create();
  auto&      abuf        = reg.emplace<audio_buffer>(weid, LOGGER);
  auto&      asrc        = reg.emplace<audio_source>(weid, LOGGER);
  auto       water_audio = TRY(audio::create_water_audio(LOGGER, abuf, asrc));
  return init_gs(LOGGER, es, MOVE(ldata), MOVE(water_audio), gsettings, fr);
}

namespace boomhs
{
Result<GameState, std::string>
create_gamestate(log_t& LOGGER, EngineState& es, Camera& camera, entt_registries& registries,
                 WorldOrientation const& wo, GraphicsSettings const gsettings, Frustum const& fr)
{
  GameState gs = TRY(create_gs(LOGGER, es, registries, wo, gsettings, fr));

  auto& ldata  = gs.ldata;
  auto& reg    = ldata.registry;
  auto& player = find_player(reg);
  camera.set_target(player.head_world_object());
  {
    auto& ftable = gs.ldata.ftable;
    auto* pft    = ftable.lookup("purisa");
    assert(pft);
    auto& font = *pft;

    auto& tboxes = gs.sstack.find_or_abort(ScreenType::InGame).uistate.textboxes;
    tboxes.emplace_back(std::make_unique<ChatWindow>(LOGGER, font, fr));
    {
      auto const resize_fn = [](Frustum const& fr) {
        auto const l = fr.width_float() / 10.0f;
        auto const t = fr.height_float() / 3.25f;
        auto const w = fr.width_float() - (2 * l);
        auto const h = fr.height_float() - (2 * t);
        return rect::create_float(l, t, w, h);
      };
      tboxes.emplace_back(std::make_unique<DialogTextBox>(LOGGER, resize_fn, font, fr));
    }
  }
  {
    auto const add_channel = [&](ChatWindow& cw, ChannelId const id, char const* name,
                                 color4 const& color) {
      Channel c{id, name, color};
      cw.history().add_channel(c);
    };

    auto& tboxes = gs.sstack.find_or_abort(ScreenType::InGame).uistate.textboxes;
    for (auto& tbox : tboxes) {
      CONTINUE_IF(TextBoxType::ChatWindow != tbox->type())
      auto& cw = static_cast<ChatWindow&>(*tbox);

      add_channel(cw, 0, "General", LOC4::WHITE);
      add_channel(cw, 1, "Group", LOC4::LIGHT_BLUE);
      add_channel(cw, 2, "Guild", LOC4::LIGHT_GREEN);
      add_channel(cw, 3, "Whisper", LOC4::MEDIUM_PURPLE);
      add_channel(cw, 4, "Area", LOC4::INDIAN_RED);
    }
  }
  return OK_MOVE(gs);
}

void
game_loop(log_t& LOGGER, SDLWindow& window, GameState& gs, Devices& devs, Camera& camera,
          DeltaTime const& dt)
{
  {
    bool const fps_mode      = camera.mode == CameraMode::FPS;
    auto const relative_mode = fps_mode ? SDL_TRUE : SDL_FALSE;
    assert(0 == SDL_SetRelativeMouseMode(relative_mode));
  }
  auto const fr = window.view_frustum();
  IO_SDL::read_devices(LOGGER, SDLReadDevicesArgs{gs, devs, camera, fr}, dt);
  ::SDL_SetCursor(devs.cursors.active());
  update_everything(LOGGER, window, gs, devs, camera, dt);
}

void
draw_everything(log_t& LOGGER, GameState& gs, Camera& camera, Frustum const& fr,
                opengl::render_state& rs, DeltaTime const& dt)
{
  auto& ss = gs.sstack;
  assert(!ss.empty());
  auto& s = ss.top();

  assert(s.draw_fn);
  s.draw_fn(LOGGER, gs, camera, fr, rs, dt);
}

} // namespace boomhs

void
InGameScreen::on_key_down(log_t& LOGGER, InGameScreen&, KeyEvent&& ke, DeltaTime const& dt)
{
  auto& gs    = ke.game_state;
  auto& ldata = gs.ldata;

  auto& nbt    = ldata.nearby_targets;
  auto& reg    = ldata.registry;
  auto& player = find_player(reg);
  {
    auto const okd = [&](auto& tbox) { tbox.on_key_down(LOGGER, tbox, MOVE(ke), dt); };
    for (auto& tbox : gs.sstack.top().uistate.textboxes) {
      switch (tbox->type()) {
      case TextBoxType::Dialog:
        okd(static_cast<DialogTextBox&>(*tbox));
        break;
      case TextBoxType::ChatWindow:
        okd(static_cast<ChatWindow&>(*tbox));
        break;
      }
    }
  }

  switch (ke.event.key.keysym.sym) {
  case SDLK_F10:
    gs.quit = true;
    return;

  case SDLK_ESCAPE: {
    if (player.is_attacking()) {
      player.toggle_attacking();
    }
    else if (nbt.selected()) {
      nbt.clear();
    }
    else {
      auto& stack = gs.sstack;
      stack.pop_all();
      stack.push(ScreenType::MainMenu);
      gs.was.stop_inwater_sound();
    }
  } break;

  case SDLK_e:
    if (nbt.selected()) {
      LOG_INFO("e selected");
    }
    break;

  case SDLK_f:
    player.try_pickup_nearby_item(LOGGER, reg);
    break;
  case SDLK_q:
    break;

  case SDLK_t:
    ke.camera.next_mode();
    break;

  case SDLK_TAB: {
    uint8_t const*       keystate = SDL_GetKeyboardState(nullptr);
    CycleDirection const dir =
        keystate[SDL_SCANCODE_LSHIFT] ? CycleDirection::Backward : CycleDirection::Forward;
    nbt.cycle(dir, dt);
  } break;
  case SDLK_BACKQUOTE: {
    auto& inventory = player.inventory;
    inventory.toggle_open();
  } break;
  case SDLK_SPACE: {
    auto const selected_opt = nbt.selected();

    // Toggle the state trackerwhether or not the player is attacking
    // AND
    // If the player try_get an entity selected, try and attack it.
    if (selected_opt) {
      eid_t const target_eid = *selected_opt;
      auto&       target     = reg.get<NPCData>(target_eid);
      if (NPC::is_dead(target.health)) {
        LOG_INFO("TARGET IS DEAD");
        break;
      }
      else {
        player.toggle_attacking();
      }
    }
    else {
      assert(!player.is_attacking());
    }
  } break;
  // scaling
  case SDLK_KP_PLUS:
  case SDLK_o:
    break;
  case SDLK_KP_MINUS:
    break;
  }
}

void
InGameScreen::on_key_up(log_t&, InGameScreen&, KeyEvent&&, DeltaTime const&)
{
}

void
InGameScreen::on_mouse_button_down(log_t& LOGGER, InGameScreen&, MouseButtonEvent&& mbe,
                                   DeltaTime const&)
{
  auto& gs     = mbe.game_state;
  auto& ds     = mbe.devices;
  auto& camera = mbe.camera;
  // auto const& event  = mbe.event;

  auto& ms = ds.mouse;

  auto&      ldata = gs.ldata;
  auto&      reg   = ldata.registry;
  auto const mode  = camera.mode;
  if (mode != CameraMode::FPS && ms.either_pressed()) {
    auto const& fr = mbe.frustum;
    if (!gs.uibuffers_classic.debug.lock_debugselected) {
      if (ms.left_pressed()) {
        LOG_INFO("selecting mouse...");
        select_mouse_under_cursor(LOGGER, camera, reg, ds, fr, MouseButton::LEFT);
      }
      else if (ms.right_pressed()) {
        select_mouse_under_cursor(LOGGER, camera, reg, ds, fr, MouseButton::RIGHT);
      }
    }
  }
  if (mode == CameraMode::FPS || mode == CameraMode::ThirdPerson) {
    if (ms.middle_pressed()) {
      LOG_INFO("toggling mouse up/down (pitch) lock");
      camera.toggle_rotation_lock();
    }
  }
  else if (mode == CameraMode::Ortho) {
    if (ms.middle_pressed()) {
      ds.cursors.set_active(SDL_SYSTEM_CURSOR_HAND);
    }
  }
}

void
InGameScreen::on_mouse_button_up(log_t&, InGameScreen&, MouseButtonEvent&& mbe, DeltaTime const&)
{
  auto& ds     = mbe.devices;
  auto& camera = mbe.camera;

  auto const mode = camera.mode;
  if (mode == CameraMode::Ortho) {
    ds.cursors.set_active(SDL_SYSTEM_CURSOR_ARROW);
  }
}

void
InGameScreen::on_mouse_motion(log_t&, InGameScreen&, MouseMotionEvent&& mme, DeltaTime const& dt)
{
  auto&       ds     = mme.devices;
  auto&       camera = mme.camera;
  auto const& motion = mme.motion;
  auto&       player = find_player(mme.game_state.ldata.registry);

  auto const& ms = ds.mouse;

  // convert from int to floating-point value
  float xrel = static_cast<float>(motion.xrel), yrel = static_cast<float>(motion.yrel);

  bool const  is_fps  = camera.is_firstperson();
  auto const& cs      = is_fps ? camera.fps.cs : camera.arcball.cs;
  auto const  dt_mult = dt(); // * dt.since_start_seconds();

  xrel *= cs.sensitivity.x * dt_mult;
  yrel *= cs.sensitivity.y * dt_mult;
  if (is_fps) {
    fps_mousemove(camera, player, xrel, yrel, dt);
  }
  else if (camera.is_thirdperson()) {
    thirdperson_mousemove(player, camera, ms, xrel, yrel, dt);
  }
  else {
  }
}

void
InGameScreen::on_mouse_wheel(log_t& LOGGER, InGameScreen&, MouseWheelEvent&& mwe,
                             DeltaTime const& dt)
{
  auto& camera = mwe.camera;
  auto& wheel  = mwe.wheel;
  LOG_TRACE("mouse wheel event detected.");

  auto& arcball = camera.arcball;
  auto& ortho   = camera.ortho;

  float constexpr ZOOM_FACTOR = 0.2f;
  if (wheel.y > 0) {
    camera::zoom_out(arcball, ZOOM_FACTOR, dt);
    camera::zoom_in(ortho, glm::vec2{1.0f}, dt);
  }
  else {
    camera::zoom_in(arcball, ZOOM_FACTOR, dt);
    camera::zoom_in(ortho, glm::vec2{1.0f}, dt);
  }
}
