#include "material.hpp"
#include <boomhs/color.hpp>

////////////////////////////////////////////////////////////////////////////////////////////////////
// Material
Material::Material(glm::vec3 const& amb, glm::vec3 const& diff, glm::vec3 const& spec,
                   float const shiny)
    : ambient(amb)
    , diffuse(diff)
    , specular(spec)
    , shininess(shiny)
{
}

Material::Material(color3 const& amb, color3 const& diff, color3 const& spec, float const shiny)
    : Material(color::vec3(amb), color::vec3(diff), color::vec3(spec), shiny)
{
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// MaterialTable
void
MaterialTable::add(NameMaterial&& nm)
{
  data_.emplace_back(MOVE(nm));
}

#define FIND_IMPL(MATERIAL_NAME, BEGIN, END)                                                       \
  auto const cmp = [&MATERIAL_NAME](NameMaterial const& nm) { return nm.name == MATERIAL_NAME; };  \
                                                                                                   \
  auto const it = std::find_if(BEGIN, END, cmp);                                                   \
  assert(it != END);                                                                               \
  return it->material

Material&
MaterialTable::find(std::string_view const& material_name)
{
  FIND_IMPL(material_name, begin(), end());
}

Material const&
MaterialTable::find(std::string_view const& material_name) const
{
  FIND_IMPL(material_name, cbegin(), cend());
}
#undef FIND_IMPL

eid_array
find_materials(enttreg_t& registry)
{
  return find_all_entities_with_component<Material>(registry);
}
