#include "start_area_generator.hpp"
#include <boomhs/game_config.hpp>
#include <boomhs/heightmap.hpp>
#include <boomhs/item_factory.hpp>
#include <boomhs/leveldata.hpp>
#include <boomhs/lighting.hpp>
#include <boomhs/npc.hpp>
#include <boomhs/player.hpp>
#include <boomhs/terrain.hpp>
#include <boomhs/water.hpp>

#include <math/random.hpp>

#include <opengl/texture.hpp>

#include <common/FOR.hpp>
#include <common/os.hpp>

#include <algorithm>

using namespace math::constants;
using namespace opengl;

static auto constexpr MIN_MONSTERS_PER_FLOOR = 30;
static auto constexpr MAX_MONSTERS_PER_FLOOR = 40;

namespace
{

void
place_item_on_ground(log_t& LOGGER, TerrainGrid& terrain, Transform& transform,
                     glm::vec2 const& pos)
{
  float const x = pos.x, z = pos.y;
  auto const  y         = terrain.get_height(LOGGER, x, z);
  transform.translation = glm::vec3{x, y, z};
}

PlaceTorchResult
place_torch(log_t& LOGGER, TerrainGrid& terrain, enttreg_t& reg, texture_storage& ttable,
            glm::vec2 const& pos)
{
  auto const eid = TRY(ItemFactory::create_torch(LOGGER, reg, ttable));

  auto& transform = reg.get<Transform>(eid);
  place_item_on_ground(LOGGER, terrain, transform, pos);

  return Ok(eid);
}

void
place_monsters(log_t& LOGGER, TerrainGrid const& terrain, enttreg_t& reg, rng_t& rng)
{
  auto const num_monsters = rng.gen_int_range(MIN_MONSTERS_PER_FLOOR, MAX_MONSTERS_PER_FLOOR);
  FOR(i, num_monsters) { NPC::create_random(LOGGER, terrain, reg, rng); }
}

void
place_player(log_t& LOGGER, TerrainGrid const& terrain, MaterialTable const& material_table,
             enttreg_t& reg, WorldOrientation const& wo)
{
  auto const eid = reg.create();

  auto head = PlayerHead::create(LOGGER, reg, wo);

  auto& player = reg.emplace<Player>(eid, eid, reg, wo, MOVE(head));
  player.level = 14;
  player.name  = "BEN";
  player.speed = 4;

  reg.emplace<FollowTransform>(eid, head.eid());

  auto& transform = player.transform();
  {
    float const x = 8, z = 8;
    auto const  y         = terrain.get_height(LOGGER, x, z);
    transform.translation = glm::vec3{x, y, z};
  }

  reg.emplace<IsRenderable>(eid);
  reg.emplace<ShaderComponent>(eid, "3d_pos_normal_color");
  reg.emplace<MeshRenderable>(eid, "at");

  reg.emplace<color4>(eid, LOC4::WHITE);

  reg.emplace<Material>(eid) = material_table.find("player");

  FOR(i, 10)
  {
    FOR(j, 10)
    {
      auto const eidwall = reg.create();
      {
        auto& tr = reg.emplace<Transform>(eidwall).translation;
        tr.x = static_cast<float>(i);
        tr.z = static_cast<float>(j);
      }
      reg.emplace<JunkEntityFromFILE>(eidwall);
      reg.emplace<IsRenderable>(eidwall);
      reg.emplace<MeshRenderable>(eidwall, "hashtag");
      reg.emplace<color4>(eidwall, LOC4::WHITE);
      reg.emplace<Material>(eidwall) = material_table.find("player");
      reg.emplace<ShaderComponent>(eidwall, "3d_wall");
    }
  }
}

void
place_waters(log_t& LOGGER, enttreg_t& reg, texture_storage& ttable, rng_t& rng)
{
  auto const place = [&](glm::vec2 const& pos, unsigned int count) {
    auto const eid = reg.create();

    auto& wi     = WaterFactory::make_default(LOGGER, ttable, eid, reg);
    wi.mix_color = color::random4(rng);

    auto& tr         = reg.get<Transform>(eid);
    tr.translation.x = pos.x;
    tr.translation.z = pos.y;

    auto name = "WaterInfo #" + std::to_string(count);
    reg.emplace<Name>(eid, MOVE(name));
  };

  unsigned int count = 0;
  FOR(i, 4)
  {
    FOR(j, 4)
    {
      auto const pos = glm::vec2{i * 25, j * 25};
      place(pos, count);
      ++count;
    }
  }
}

} // namespace

namespace start_area
{

GenLevelResult
gen_level(log_t& LOGGER, enttreg_t& reg, rng_t& rng, texture_storage& ttable,
          MaterialTable const& material_table, Heightmap const& heightmap,
          WorldOrientation const& world_orientation)
{
  LOG_TRACE("Generating Starting Area");

  LOG_TRACE("Generating Terrain");
  TerrainConfig const tc;
  auto&               sp = shader3d::ref(reg, tc.shader_name);

  TerrainGridConfig const tgc{1, 1};
  auto                    terrain = terrain::generate_grid(LOGGER, tgc, tc, heightmap, sp);

  LOG_TRACE("Placing Torch");
  TRY(place_torch(LOGGER, terrain, reg, ttable, glm::vec2{2, 2}));
  TRY(place_torch(LOGGER, terrain, reg, ttable, glm::vec2{3, 3}));
  TRY(place_torch(LOGGER, terrain, reg, ttable, glm::vec2{4, 4}));

  LOG_TRACE("Placing Player");
  place_player(LOGGER, terrain, material_table, reg, world_orientation);

  LOG_TRACE("placing monsters ...\n");
  place_monsters(LOGGER, terrain, reg, rng);

  LOG_TRACE("placing water ...\n");
  place_waters(LOGGER, reg, ttable, rng);

  LOG_TRACE("finished generating starting area!");
  return Ok(LevelGeneratedData{MOVE(terrain)});
}

} // namespace start_area
