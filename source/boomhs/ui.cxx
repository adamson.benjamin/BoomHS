#include "ui.hpp"
#include <boomhs/collision.hpp>
#include <boomhs/engine.hpp>
#include <boomhs/mouse.hpp>
#include <boomhs/state.hpp>
#include <boomhs/ui_controls.hpp>

#include <opengl/bind.hpp>
#include <opengl/renderer.hpp>
#include <opengl/shader.hpp>
#include <opengl/texture.hpp>

using namespace opengl;

namespace
{
void
draw_button(log_t& LOGGER, RectFloat const& rect, color3 const& color,
            LetterRectCollection const& lrc, shader_type& sp, texture_h& ti, render_state& rs)
{
  auto const& pm = rs.cm.proj;
  render::draw_2dgui_sentence(LOGGER, rect, lrc, color, sp, ti, pm, rs.count);
}

void
mouse_over_button(log_t& /*LOGGER*/, Button& btn, GameState& gs, Mouse const& /*ms*/,
                  bool const mouse_is_over)
{
  bool const mouse_was_over = btn.mouse_is_over;

  btn.mouse_is_over = mouse_is_over;
  if (mouse_was_over && !btn.mouse_is_over) {
    btn.on_mouseexit(gs, btn);
  }
  else if (btn.mouse_is_over) {
    if (btn.on_mouseover) {
      btn.on_mouseover(gs, btn);
    }
  }
}

void
mouse_click_button(log_t&, Button& btn, GameState& gs)
{
  if (btn.mouse_is_over && btn.on_left_click) {
    btn.on_left_click(gs, btn);
  }
}

} // namespace

namespace ui
{
void
draw(log_t& LOGGER, TextButton const& btn, shader_type& sp, texture_h& ti, render_state& rs)
{
  auto const& color = btn.text_color;
  ::draw_button(LOGGER, btn.trect, color, btn.lrects, sp, ti, rs);
}

void
draw(log_t& LOGGER, enttreg_t& reg, CheckboxTextButton const& btn, texture_storage& ttable,
     shader_type& sp, texture_h& ti, render_state& rs)
{
  ::draw_button(LOGGER, btn.trect, btn.text_color, btn.lrects, sp, ti, rs);

  auto& tix = *ttable.find("checkbox_on");
  BIND_UNTIL_END_OF_SCOPE(LOGGER, tix);

  auto& spx = shader2d::texture2d(reg);
  BIND_UNTIL_END_OF_SCOPE(LOGGER, spx);

  auto const  uvs = rect::create_float(0, 0, 1, 1);
  auto const& pm  = rs.cm.proj;

  if (btn.pressed) {
    render::draw_2dtextured_rect(LOGGER, pm, btn.cbox, uvs, spx, tix, rs.count);
  }
}

void
resize(CheckboxTextButton& btn, RectFloat const& br)
{
  btn.cbox = btn.trect;

  auto const cbox_width = br.width() / 12.0f;
  btn.cbox.width()      = cbox_width;

  auto const space_between = cbox_width / 5.0f;
  btn.trect.move(cbox_width + space_between, 0.0f);
}

void
on_mouse_over(log_t& LOGGER, TextButton& btn, GameState& gs, Mouse const& ms)
{
  bool const mouse_is_over = ui::mouse_is_over(ms, btn);
  ::mouse_over_button(LOGGER, btn, gs, ms, mouse_is_over);
}

void
on_mouse_over(log_t& LOGGER, CheckboxTextButton& btn, GameState& gs, Mouse const& ms)
{
  bool const mouse_is_over = ui::mouse_is_over(ms, btn);
  ::mouse_over_button(LOGGER, btn, gs, ms, mouse_is_over);
}

void
on_mouse_click(log_t& LOGGER, TextButton& btn, GameState& gs)
{
  ::mouse_click_button(LOGGER, btn, gs);
}

void
on_mouse_click(log_t& LOGGER, CheckboxTextButton& btn, GameState& gs)
{
  ::mouse_click_button(LOGGER, btn, gs);
  if (btn.mouse_is_over) {
    btn.pressed ^= true;
  }
}

bool
mouse_is_over(Mouse const& ms, TextButton const& button)
{
  return collision::intersects(ms.coords(), button.trect);
}

bool
mouse_is_over(Mouse const& ms, CheckboxTextButton const& button)
{
  auto const c         = ms.coords();
  bool const over_tbox = collision::intersects(c, button.trect);
  bool const over_cbox = collision::intersects(c, button.cbox);
  return over_tbox | over_cbox;
}

} // namespace ui
