#pragma once
#include <boomhs/components.hpp>
#include <boomhs/entity.hpp>
#include <boomhs/fog.hpp>
#include <boomhs/material.hpp>
#include <boomhs/nearby_targets.hpp>
#include <boomhs/obj_store.hpp>
#include <boomhs/scene_renderer.hpp>
#include <boomhs/skybox.hpp>
#include <boomhs/terrain.hpp>
#include <boomhs/wind.hpp>

#include <opengl/draw_info.hpp>
#include <opengl/font_texture.hpp>

#include <common/macros_class.hpp>
#include <vector>

struct log_t;
class Viewport;

struct LevelGeneratedData
{
  TerrainGrid tgrid;
};

struct LevelAssets
{
  GlobalLight     ambient;
  Fog             fog;
  MaterialTable   mtable;
  AttenuationList attens;

  ObjStore                objs;
  opengl::texture_storage ttable;
  opengl::font_storage    ftable;
  // MOVE_CONSTRUCTIBLE_ONLY(LevelAssets);
};

class LevelData : public LevelAssets
{
public:
  MOVE_CONSTRUCTIBLE_ONLY(LevelData);
  LevelData(log_t&, LevelAssets&&, TerrainGrid&&, enttreg_t&, Viewport const&);

  // public fields
  enttreg_t& registry;

  Skybox      skybox;
  TerrainGrid tgrid;
  Wind        wind;

  // nearby targets user can select
  NearbyTargets nearby_targets;

  // this should be last field, as it uses "this" in it's construction.
  StaticRenderers srenders;

  auto dimensions() const { return tgrid.max_xz(); }
};
using LevelDatas = std::vector<LevelData>;
