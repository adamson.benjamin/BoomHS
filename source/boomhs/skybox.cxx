#include "skybox.hpp"
#include <boomhs/delta_time.hpp>

#include <opengl/shader.hpp>
#include <opengl/texture.hpp>

#include <cassert>

static constexpr float SKYBOX_SCALE_SIZE = 1000.0f;

///////////////////////////////////////////////////////////////////////////////////////////////////
// Skybox
Skybox::Skybox()
    : speed_(10.0f)
{
  transform_.scale = glm::vec3{SKYBOX_SCALE_SIZE};
}

void
Skybox::update(DeltaTime const& dt)
{
  transform_.rotate_degrees(speed_ * dt(), EulerAxis::Y);
}
