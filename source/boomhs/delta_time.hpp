#pragma once
#include <boomhs/delta_time.hpp>
#include <common/macros_class.hpp>
#include <common/wall_clock.hpp>

#include <chrono>

using dt_t = float;

class DeltaTime
{
  dt_t const       dt_;
  duration_t const since_start_;

public:
  NO_MOVE_OR_COPY(DeltaTime);
  explicit DeltaTime(dt_t const dt, duration_t const sstart)
      : dt_(dt)
      , since_start_(sstart)
  {
  }

  auto operator()() const { return dt_; }
  auto since_start() const { return since_start_; }

  template <typename T = long>
  T since_start_millis() const
  {
    auto const ss    = since_start();
    auto const count = std::chrono::duration_cast<std::chrono::milliseconds>(ss).count();
    return static_cast<T>(count);
  }

  auto since_start_seconds() const
  {
    auto const ss = since_start();
    return std::chrono::duration_cast<std::chrono::seconds>(ss).count();
  }
};

inline DeltaTime
create_dt(WallClock const& wc, dt_t const dt)
{
  auto const sbegin = wc.since_beginning();
  return DeltaTime{dt, sbegin};
}
