#pragma once
#include <common/macros_class.hpp>
#include <common/macros_container.hpp>
#include <common/move.hpp>

#include <extlibs/entt.hpp>
#include <extlibs/glm.hpp>

#include <limits>
#include <vector>

using eid_t                     = entt::registry::entity_type;
static auto constexpr eid_t_max = std::numeric_limits<eid_t>::max();

using enttreg_t       = entt::registry;
using entt_registries = std::vector<enttreg_t>;
using eid_array       = std::vector<eid_t>;

template <typename... C>
auto
find_all_entities_with_component(enttreg_t& registry)
{
  eid_array  result;
  auto const view = registry.view<C...>();
  for (auto const e : view) {
    result.emplace_back(e);
  }
  return result;
}

eid_array
all_nearby_entities(glm::vec3 const&, float, enttreg_t&);
