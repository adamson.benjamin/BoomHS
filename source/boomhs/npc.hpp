#pragma once
#include <boomhs/components.hpp>
#include <boomhs/entity.hpp>
#include <common/macros_class.hpp>

struct log_t;
class rng_t;
class TerrainGrid;

enum class Alignment
{
  EVIL = 0,
  NEUTRAL,
  GOOD,
  NOT_SET
};

char const*
alignment_to_string(Alignment const);

struct NPCData
{
  char const*  name      = "NOT SET. ERROR";
  HealthPoints health    = {-1, -1};
  int          level     = -1;
  Alignment    alignment = Alignment::NOT_SET;

  bool within_attack_range() const;
};

using NpcLevel = int;
using NpcName  = char const*;
using MeshName = char const*;

class NPC
{
  NPC() = delete;

public:
  // Loads a new NPC into the enttreg_t.
  static void create(enttreg_t&, NpcName, MeshName, NpcLevel, glm::vec3 const&);

  static void create_random(log_t&, TerrainGrid const&, enttreg_t&, rng_t&);

  static bool is_dead(HealthPoints const&);
  static bool within_attack_range(glm::vec3 const&, glm::vec3 const&);
};

inline auto
find_enemies(enttreg_t& registry)
{
  using namespace opengl;
  return find_all_entities_with_component<NPCData>(registry);
}
