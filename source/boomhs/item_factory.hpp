#pragma once
#include <boomhs/entity.hpp>
#include <common/result.hpp>

namespace opengl
{
class texture_storage;
} // namespace opengl

struct log_t;
class rng_t;

using PlaceTorchResult = Result<eid_t, std::string>;

struct ItemFactory
{
  ItemFactory() = delete;

  static eid_t create_empty(enttreg_t&, opengl::texture_storage&);

  static eid_t            create_book(enttreg_t&, opengl::texture_storage&);
  static eid_t            create_spear(enttreg_t&, opengl::texture_storage&);
  static PlaceTorchResult create_torch(log_t&, enttreg_t&, opengl::texture_storage&);
};
