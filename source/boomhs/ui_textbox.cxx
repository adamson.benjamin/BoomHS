#include "ui_textbox.hpp"
#include <boomhs/collision.hpp>
#include <boomhs/event.hpp>
#include <boomhs/font.hpp>
#include <boomhs/mouse.hpp>
#include <boomhs/state.hpp>

#include <opengl/font_texture.hpp>

#include <cassert>
#include <gl_sdl/global.hpp>

using namespace opengl;

namespace
{
auto
compute_bumper_height(RectFloat const& box)
{
  return box.height() / 16.0f;
}

auto
compute_bumper_width(RectFloat const& box)
{
  return box.width() / 20.0f;
}

auto
compute_scroll_left(RectFloat const& box)
{
  return box.right() - compute_bumper_width(box);
}

auto
compute_scrollbar_bumper_rectangles(RectFloat const& box)
{
  auto const left  = compute_scroll_left(box);
  auto const top   = box.top();
  auto const width = box.right() - left;

  auto const bumper_height = compute_bumper_height(box);

  auto const top_rect = rect::create(left, top, width, bumper_height);

  auto const bottom      = box.bottom();
  auto       bottom_rect = rect::create(left, bottom - bumper_height, width, bumper_height);
  return ScrollBarBumpers{top_rect, bottom_rect};
}

auto
compute_scroll_area_rect(RectFloat const& box)
{
  auto const bumpers = compute_scrollbar_bumper_rectangles(box);

  auto const left   = compute_scroll_left(box);
  auto const top    = box.top() + bumpers.top.height();
  auto const width  = box.right() - left;
  auto const height = box.height() - (bumpers.top.height() + bumpers.bottom.height());
  return rect::create(left, top, width, height);
}

// Result<NOTHING, std::string>
// compute_msg_info(FontInfo const& finfo, glm::ivec2 const& dinfo, RectFloat const& rect,
// TextBoxMessage& msg)
//{
// auto const& fsize = finfo.font_size;
// auto const& text  = msg.contents;
// msg.letter_rects  = TRY(font::compute_letter_rectangles_bounded(finfo, rect, text, fsize,
// dinfo)); return OK_NONE;
//}

} // namespace

void
ScrollBar::try_scroll(ScrollDirection const dir, float const amount)
{
  switch (dir) {
  case ScrollDirection::DOWN:
    if ((current + amount) <= max) {
      current += amount;
    }
    else {
      current = max;
    }
    break;
  case ScrollDirection::UP:
    if ((current - amount) > 0) {
      current -= amount;
    }
    else {
      current = 0;
    }
    break;
  }
}

auto
make_textbox(log_t& LOGGER, RectFloat const& rect, font_and_texture const& ft)
{
  auto const dinfo = gl_sdl::current_display_mode(LOGGER);
  glm::vec2 constexpr FONT_SIZE{2};

  glm::ivec2 const img_size{ft.texture.width(), ft.texture.height()};
  glm::vec2 const  ratio{rect.width(), rect.height()};
  return font::create_fontinfo(ft.ffile, ratio, FONT_SIZE, dinfo, img_size).expect("FUCK");
}

TextBox::TextBox(log_t& logger, TextBoxType const tbtype, ComputeSizeFN resize_fn,
                 font_and_texture const& ft, Frustum const& fr)
    : type_(tbtype)
    , finfo_(make_textbox(logger, resize_fn(fr), ft))
    , resize_fn_(resize_fn)
{
}

RectFloat
TextBox::overlay_rect() const
{
  return rect_;
}

RectFloat
TextBox::text_rect() const
{
  auto r = rect_;
  r.width() -= compute_bumper_width(overlay_rect());
  return r;
}

void
TextBox::update(log_t&, Mouse const& ms)
{
  auto& sb = scrollbar_;

  auto const coords       = ms.coords();
  bool const left_pressed = ms.left_pressed();
  if (left_pressed && sb.visible) {
    if (collision::intersects(coords, sb.scroll_area)) {
      auto const sc_area_lt = sb.scroll_area.left_top();

      auto const coords_f          = glm::vec2{ms.coords()};
      auto const rel_to_parent_pos = coords_f - sc_area_lt;

      auto const spos = (rel_to_parent_pos.y / sb.scroll_area.height()) * sb.max;
      sb.current      = spos;
    }
    else if (collision::intersects(coords, sb.bumpers.top)) {
      sb.try_scroll(ScrollDirection::UP, sb.scroll_percentage());
    }
    else if (collision::intersects(coords, sb.bumpers.bottom)) {
      sb.try_scroll(ScrollDirection::DOWN, sb.scroll_percentage());
    }
  }
}

Result<NOTHING, std::string>
TextBox::resize(log_t& /*LOGGER*/, Frustum const& fr)
{
  rect()                 = resize_fn_(fr);
  scrollbar_.scroll_area = compute_scroll_area_rect(overlay_rect());
  scrollbar_.slider_rect = scrollbar_.scroll_area;
  return OK_NONE;
}

TextBoxMessage&
TextBoxMessages::insert_back(TextBoxMessage&& msg)
{
  return data_.emplace_back(MOVE(msg));
}

TextBoxMessage&
TextBoxMessages::insert_back(std::string&& text)
{
  return insert_back(TextBoxMessage{MOVE(text)});
}

TextBoxMessage&
TextBoxMessages::insert_front(std::string&& text)
{
  auto const b   = std::begin(data_);
  auto       msg = TextBoxMessage{MOVE(text)};
  data_.emplace(b, MOVE(msg));
  return data_.front();
}

DialogTextBox::DialogTextBox(log_t& logger, ComputeSizeFN const resize_fn,
                             font_and_texture const& ft, Frustum const& fr)
    : TextBox(logger, TextBoxType::Dialog, resize_fn, ft, fr)
{
}

Result<NOTHING, std::string>
DialogTextBox::push_message_back(log_t& LOGGER, std::string&& text)
{
  auto const dinfo = gl_sdl::current_display_mode(LOGGER);
  auto&      msg   = history_.insert_back(MOVE(text));
  return textbox::compute_new_message(LOGGER, dinfo, *this, msg);
}

Result<NOTHING, std::string>
DialogTextBox::push_message_front(log_t& LOGGER, std::string&& text)
{
  auto const dinfo = gl_sdl::current_display_mode(LOGGER);
  auto&      msg   = history_.insert_front(MOVE(text));
  return textbox::compute_new_message(LOGGER, dinfo, *this, msg);
}

void
DialogTextBox::textbox_on_key_down(log_t& LOGGER, DialogTextBox& tbox, KeyEvent&& ke,
                                   DeltaTime const&)
{
  auto& sb = tbox.scrollbar();

  switch (ke.event.key.keysym.sym) {
  case SDLK_UP:
    sb.try_scroll(ScrollDirection::UP);
    break;
  case SDLK_DOWN:
    sb.try_scroll(ScrollDirection::DOWN);
    break;

  case SDLK_c: {
    tbox.push_message_back(LOGGER,
                           "HELLO "
                           "MOTO1111111111111111111111111111111111111111111111111111111111111111111"
                           "11111111111111111111111111111111111111111111111111111111111111111")
        .expect("PUSHING TEST MESSAGES FAILED");
  } break;

  case SDLK_PAGEDOWN:
    sb.try_scroll(ScrollDirection::DOWN, sb.scroll_percentage());
    break;

  case SDLK_PAGEUP:
    sb.try_scroll(ScrollDirection::UP, sb.scroll_percentage());
    break;
  }
}

Result<NOTHING, std::string>
DialogTextBox::resize(log_t& LOGGER, Frustum const& fr)
{
  TRY(TextBox::resize(LOGGER, fr));
  auto const dinfo = gl_sdl::current_display_mode(LOGGER);
  return textbox::recompute_message_sizes(LOGGER, *this, dinfo);
}

void
DialogTextBox::update(log_t& LOGGER, Mouse const& mouse)
{
  textbox::recompute_scroll_area(*this);
  scrollbar_.visible = textbox::should_show(*this);
  TextBox::update(LOGGER, mouse);
}

namespace textbox
{
void
recompute_scroll_area(TextBox& tbox, int const lines_required_for, int const lines_could_fit_inside)
{
  auto const& overlay = tbox.overlay_rect();

  auto& sb   = tbox.scrollbar();
  sb.bumpers = compute_scrollbar_bumper_rectangles(overlay);
  sb.max     = static_cast<float>(std::max(0, lines_required_for - lines_could_fit_inside));

  auto const bumper_height = sb.bumpers.top.height() + sb.bumpers.bottom.height();
  auto const rw_height     = overlay.height() - bumper_height;

  auto const th                  = static_cast<float>(lines_required_for);
  auto const rh                  = static_cast<float>(lines_could_fit_inside);
  auto const ratio               = (th / rh);
  auto const height_divide_ratio = rw_height / ratio;

  auto& sbr = sb.slider_rect;
  {
    auto& sbh = sbr.height();
    sbh       = rw_height - (rw_height - height_divide_ratio);
    {
      auto const minimum_scroll_height = compute_bumper_height(overlay) / 1.5f;
      sbh                              = std::max(sbh, minimum_scroll_height);
    }
    auto& sbt = sb.slider_rect.top();
    sbt       = ((sb.current * (rw_height - sbh)) / std::max(1.0f, sb.max));
  }
  auto const offset = overlay.top() + sb.bumpers.top.height();
  sbr.move(0, offset);
}

} // namespace textbox
