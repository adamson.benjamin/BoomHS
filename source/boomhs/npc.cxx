#include "npc.hpp"
#include <boomhs/components.hpp>
#include <boomhs/entity.hpp>
#include <boomhs/material.hpp>
#include <boomhs/terrain.hpp>

#include <math/random.hpp>

namespace
{

glm::vec3
generate_npc_position(log_t& logger, TerrainGrid const& terrain_grid, enttreg_t& registry,
                      rng_t& rng)
{
  auto const& dimensions = terrain_grid.max_xz();
  auto const  width      = dimensions.x;
  auto const  length     = dimensions.y;
  assert(width > 0 && length > 0);
  float x, z;
  while (true) {
    x = rng.gen(width - 1);
    z = rng.gen(length - 1);

    float const y = terrain_grid.get_height(logger, x, z);

    glm::vec3 const pos{x, y, z};
    static auto constexpr MAX_DISTANCE = 2.0f;
    auto const nearby                  = all_nearby_entities(pos, MAX_DISTANCE, registry);
    if (!nearby.empty()) {
      continue;
    }
    return pos;
  }
}

} // namespace

char const*
alignment_to_string(Alignment const al)
{
#define CASE(ATTRIBUTE, ATTRIBUTE_S)                                                               \
  case Alignment::ATTRIBUTE:                                                                       \
    return ATTRIBUTE_S

  switch (al) {
    CASE(EVIL, "EVIL");
    CASE(NEUTRAL, "NEUTRAL");
    CASE(GOOD, "GOOD");
    CASE(NOT_SET, "NOT_SET");
  }
#undef CASE
  return nullptr;
}

void
NPC::create(enttreg_t& registry, NpcName name, MeshName mesh, NpcLevel const level,
            glm::vec3 const& pos)
{
  auto eid = registry.create();
  registry.emplace<color4>(eid, LOC4::NO_ALPHA);
  registry.emplace<Name>(eid, name);
  registry.emplace<IsRenderable>(eid);

  // TODO: look this up in the material table
  registry.emplace<Material>(eid);

  // Enemies get a mesh
  registry.emplace<MeshRenderable>(eid, mesh);

  // shader
  registry.emplace<ShaderComponent>(eid, "3d_pos_normal_color");

  // transform
  auto& transform       = registry.emplace<Transform>(eid);
  transform.translation = pos;

  // npc TAG
  auto& npcdata = registry.emplace<NPCData>(eid);
  npcdata.name  = name;

  auto& hp   = npcdata.health;
  hp.current = 10;
  hp.max     = hp.current;

  npcdata.level     = level;
  npcdata.alignment = Alignment::EVIL;
}

void
NPC::create_random(log_t& logger, TerrainGrid const& terrain_grid, enttreg_t& registry, rng_t& rng)
{
  auto const make_monster = [&](NpcName name, MeshName mesh) {
    auto const pos = generate_npc_position(logger, terrain_grid, registry, rng);

    int const level = rng.gen_int_range(1, 20);
    NPC::create(registry, name, mesh, level, pos);
  };
  if (rng.gen_bool()) {
    make_monster("Orc Widow", "O");
  }
  else {
    make_monster("Toucan", "T");
  }
}

bool
NPC::is_dead(HealthPoints const& hp)
{
  assert(hp.max > 0);
  return hp.current <= 0;
}

bool
NPC::within_attack_range(glm::vec3 const& a, glm::vec3 const& b)
{
  return glm::distance(a, b) < 2;
}
