#include "ui_chatwindow.hpp"
#include <boomhs/event.hpp>
#include <boomhs/state.hpp>

#include <gl_sdl/global.hpp>

#include <math/frustum.hpp>

namespace
{
glm::vec2
compute_overlay_position(Frustum const& fr)
{
  float const space_on_lhs = fr.width_float() / 60.0f;
  float const left         = fr.left_float() + space_on_lhs;

  // float constexpr SPACE_BENEATH = 30.0f;
  float const top = 10.0f; // fr.bottom - compute_size(fr).y - SPACE_BENEATH;
  return glm::vec2{left, top};
}

auto
compute_overlay_size(Frustum const& fr)
{
  auto const width  = fr.width_float() / 4.0f;
  auto const height = fr.height_float() / 4.0f;
  return rect::create(compute_overlay_position(fr), width, height);
}

} // namespace

ChatWindow::ChatWindow(log_t& logger, opengl::font_and_texture const& ft, Frustum const& fr)
    : TextBox(logger, TextBoxType::ChatWindow, compute_overlay_size, ft, fr)
{
}

Result<NOTHING, std::string>
ChatWindow::push_message_back(log_t& LOGGER, ChannelId const cid, std::string&& text)
{
  auto const dinfo = gl_sdl::current_display_mode(LOGGER);
  auto&      msg   = history_.insert_back(cid, MOVE(text));
  return textbox::compute_new_message(LOGGER, dinfo, *this, msg);
}

Result<NOTHING, std::string>
ChatWindow::push_message_front(log_t& LOGGER, ChannelId const cid, std::string&& text)
{
  auto const dinfo = gl_sdl::current_display_mode(LOGGER);
  auto&      msg   = history_.insert_front(cid, MOVE(text));
  return textbox::compute_new_message(LOGGER, dinfo, *this, msg);
}

Result<NOTHING, std::string>
ChatWindow::push_random_message(log_t& LOGGER, rng_t& rng)
{
  auto const name = rng.gen_str(4, 8);
  auto const text = rng.gen_str(10, 300);
  auto       msg  = fmt::sprintf("%s: %s", name, text);

  auto const numc = history_.num_channels();
  if (numc < 1) {
    return Err(std::string{"No channel to push message"});
  }

  auto const cid = rng.gen<uint32_t>(0, (numc - 1));
  return push_message_back(LOGGER, cid, MOVE(msg));
}

Result<NOTHING, std::string>
ChatWindow::push_test_messages(log_t& LOGGER)
{
  std::array constexpr CHAT_MESSAGES = {
      PAIR(0, "Welcome to the server 'Turnshroom Habitat'"),
      PAIR(0, "Wizz: Hey"),
      PAIR(0, "Thorny: Yo"),
      PAIR(0, "Mufk: SUp man"),
      PAIR(2, "Kazaghual: anyone w2b this axe I just found?"),
      PAIR(2, "PizzaMan: Yo I'm here to deliver this pizza, I'll just leave it over here by the "
              "dragon ok?"),
      PAIR(3, "Moo: grass plz"),
      PAIR(4, "Aladin: STFU Jafar"),
      PAIR(2, "Rocky: JKSLFJS"),
      PAIR(1, "You took 31 damage."),
      PAIR(1, "You've given 25 damage."),
      PAIR(1, "You took 61 damage."),
      PAIR(1, "You've given 20 damage."),
      PAIR(2, R"(A gender chalks in the vintage coke. When will the murder pocket a wanted symptom?
              My
              attitude observes any nuisance into the laughing constant. Every candidate
              offers the railway under the beforehand molecule. The rescue buys his wrath
              underneath the above garble.
              The truth collars the bass into a lower heel. A squashed machinery kisses the
              abandon. Across its horse swims a sheep. Any umbrella damage rants over a sniff.

              How can a theorem chalk the frustrating fraud? Should the world wash an
              incomprehensible curriculum?)"),

      PAIR(1, R"(The cap ducks inside the freedom. The mum hammers the apathy above our preserved
              ozone. Will the peanut nose a review species? His vocabulary beams near the virgin.

              The short supporter blames the hack fudge. The waffle exacts the bankrupt within an
              infantile attitude.)"),
      PAIR(2,
           "A flesh hazards the sneaking tooth. An analyst steams before an instinct! The muscle "
           "expands within each brother! Why can't the indefinite garbage harden? The feasible "
           "cider moans in the forest."),
      PAIR(1, "Opposite the initiative scratches an inane plant. Why won't the late school "
              "experiment with a crown? The sneak papers a go dinner without a straw. How can an "
              "eating guy camp?"
              "Around the convinced verdict waffles a scratching shed. The "
              "inhabitant escapes before whatever outcry."),
      PAIR(1, "Do you believe in an afterlife?"),
      PAIR(2, "What is the first thing you do in the morning?"),
      PAIR(4, "PIG COW LLAMA COOKIE CRISP! "),
  };

  for (auto const& msg : CHAT_MESSAGES) {
    auto const cid = static_cast<ChannelId>(msg.first);
    TRY(push_message_back(LOGGER, cid, msg.second));
  }
  return push_message_back(LOGGER, 2, "System: QED");
}

void
ChatWindow::on_key_down(log_t& LOGGER, ChatWindow& tbox, KeyEvent&& ke, DeltaTime const&)
{
  auto& gs = ke.game_state;
  auto& sb = tbox.scrollbar();

  switch (ke.event.key.keysym.sym) {
  case SDLK_UP:
    sb.try_scroll(ScrollDirection::UP);
    break;
  case SDLK_DOWN:
    sb.try_scroll(ScrollDirection::DOWN);
    break;

  case SDLK_c: {
    tbox.push_message_back(LOGGER, 3, "HELLO MOTO").expect("PUSHING TEST MESSAGES FAILED");
  } break;

  case SDLK_x: {
    tbox.push_test_messages(LOGGER).expect("PUSHING TEST MESSAGES FAILED");
  } break;

  case SDLK_z: {
    tbox.push_random_message(LOGGER, gs.rng).expect("PUSHING RANDOM MESSAGE FAILED");
  } break;

  case SDLK_PAGEDOWN:
    sb.try_scroll(ScrollDirection::DOWN, sb.scroll_percentage());
    break;

  case SDLK_PAGEUP:
    sb.try_scroll(ScrollDirection::UP, sb.scroll_percentage());
    break;
  }
}

void
ChatWindow::update(log_t& LOGGER, Mouse const& mouse)
{
  textbox::recompute_scroll_area(*this);
  scrollbar_.visible = textbox::should_show(*this);
  TextBox::update(LOGGER, mouse);
}
