#pragma once
#include <boomhs/chat_history.hpp>
#include <boomhs/ui_textbox.hpp>

#include <common/non_const.hpp>
#include <common/result.hpp>

struct Frustum;
class Mouse;
class rng_t;

class ChatWindow : public TextBox
{
  ChatHistory history_;

public:
  ChatWindow(log_t&, opengl::font_and_texture const&, Frustum const&);

public:
  ChatHistory const& history() const { return history_; }
  NON_CONST(history)

public:
  using MessageType = typename ChatHistory::MessageType;

public:
  Result<NOTHING, std::string> push_random_message(log_t&, rng_t&);
  Result<NOTHING, std::string> push_test_messages(log_t&);

  Result<NOTHING, std::string> push_message_back(log_t&, ChannelId, std::string&&);
  Result<NOTHING, std::string> push_message_front(log_t&, ChannelId, std::string&&);

public:
  virtual void update(log_t&, Mouse const&) override;

public:
  static void on_key_down(log_t&, ChatWindow&, KeyEvent&&, DeltaTime const&);
};
