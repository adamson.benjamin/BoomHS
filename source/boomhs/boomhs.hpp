#pragma once
#include <boomhs/delta_time.hpp>
#include <boomhs/entity.hpp>
#include <boomhs/game_config.hpp>
#include <boomhs/screen.hpp>
#include <boomhs/state.hpp>

#include <common/result.hpp>
#include <string>

struct log_t;
struct Camera;
class DeltaTime;
struct Devices;
struct EngineState;
struct Frustum;
class MouseState;
class SDLWindow;
struct WorldOrientation;

namespace opengl
{
struct DrawState;
struct render_state;
} // namespace opengl

namespace boomhs
{
Result<GameState, std::string>
create_gamestate(log_t&, EngineState&, Camera&, entt_registries&, WorldOrientation const&,
                 GraphicsSettings, Frustum const&);

void
game_loop(log_t&, SDLWindow&, GameState&, Devices&, Camera&, DeltaTime const&);

void
draw_everything(log_t&, GameState&, Camera&, Frustum const&, opengl::render_state&,
                DeltaTime const&);
} // namespace boomhs

class InGameScreen final : public Screen
{
public:
  InGameScreen()
      : Screen(ScreenType::InGame)
  {
  }

public:
  void on_key_down(log_t&, InGameScreen&, KeyEvent&&, DeltaTime const&);
  void on_key_up(log_t&, InGameScreen&, KeyEvent&&, DeltaTime const&);

  void on_mouse_button_down(log_t&, InGameScreen&, MouseButtonEvent&&, DeltaTime const&);
  void on_mouse_button_up(log_t&, InGameScreen&, MouseButtonEvent&&, DeltaTime const&);

  void on_mouse_motion(log_t&, InGameScreen&, MouseMotionEvent&&, DeltaTime const&);
  void on_mouse_wheel(log_t&, InGameScreen&, MouseWheelEvent&&, DeltaTime const&);
};
