#include "font.hpp"
#include <common/algorithm.hpp>
#include <common/overload.hpp>

#include <algorithm>
#include <cassert>

using namespace font;

namespace
{
auto
compute_letter_uvs(CharInfo const& cinfo, std::array<int, 4> padding, glm::ivec2 const& img_size)
{
  auto const img_width  = static_cast<float>(img_size.x);
  auto const img_height = static_cast<float>(img_size.y);

  float const uvx = static_cast<float>(cinfo.xpos + padding[0]) / img_width;
  float const uvy = static_cast<float>(cinfo.ypos + padding[1]) / img_height;

  float const uvw = static_cast<float>(cinfo.width + padding[2]) / img_width;
  float const uvh = static_cast<float>(cinfo.height + padding[3]) / img_height;
  return rect::create(uvx, uvy, uvw, uvh);
}

auto
compute_letter_wh_rect(glm::vec2 const& ratio, CharInfo const& cinfo, glm::vec2 const& font_size,
                       glm::ivec2 const& dinfo)
{
  auto const char_width  = static_cast<float>(cinfo.width);
  auto const char_height = static_cast<float>(cinfo.height);

  auto const xoffset = static_cast<float>(cinfo.xoffset);
  auto const yoffset = static_cast<float>(cinfo.yoffset);

  float const width_ratio  = ratio.x / static_cast<float>(dinfo.x);
  float const height_ratio = ratio.y / static_cast<float>(dinfo.y);
  float const quad_width   = (char_width + xoffset) * font_size.x * width_ratio;
  float const quad_height  = (char_height + yoffset) * font_size.y * height_ratio;

  float const xoff = xoffset * width_ratio;
  float const yoff = yoffset * height_ratio;

  return rect::create(xoff, yoff, quad_width, quad_height);
}

auto
compute_letter_rectangles(CharInfo const& cinfo, glm::vec2 const& ratio, glm::ivec2 const& img_size,
                          glm::vec2 const& tsize, glm::ivec2 const& dinfo,
                          std::array<int, 4> const& uv_padding)
{
  auto const uv_rect     = compute_letter_uvs(cinfo, uv_padding, img_size);
  auto const letter_rect = compute_letter_wh_rect(ratio, cinfo, tsize, dinfo);
  return LetterRects{letter_rect, uv_rect};
}

Result<LetterRectCollection, std::string>
compute_letter_infos(FontFile const& ffile, glm::vec2 const& ratio, glm::vec2 const& font_size,
                     glm::ivec2 const& dinfo, glm::ivec2 const& img_size)
{
  LetterRectCollection infos;
  auto const&          uv_padding = ffile.padding;
  for (auto const& cinfo : ffile.char_infos) {
    auto lrects = compute_letter_rectangles(cinfo, ratio, img_size, font_size, dinfo, uv_padding);
    infos.emplace_back(MOVE(lrects));
  }
  return OK_MOVE(infos);
}

Result<CharLookupResult, std::string>
lookup_character(char const c, FontFile const& ff)
{
  if (c == '\n') {
    return Ok(CharLookupResult{NullTerminatorCharInfo{}});
  }

  auto const& cinfos = ff.char_infos;
  auto const  cmp    = [&c](CharInfo const& it) { return it.c == c; };
  auto const  it     = std::find_if(cinfos.cbegin(), cinfos.end(), cmp);
  if (cinfos.cend() == it) {
    return ERR_SPRINTF("Could not find character '%c' in font file '%s'", c, ff.name);
  }
  return Ok(CharLookupResult{MOVE(*it)});
}

// KerningPair const*
// lookup_kerning(char const a, char const b, FontFile const& ff)
//{
//  auto const cmp = [&](KerningPair const& it) { return it.c0 == a && it.c1 == b; };
//
//  auto const& kp = ff.kerning_pairs;
//  auto const  it = std::find_if(kp.cbegin(), kp.cend(), cmp);
//  return it == kp.cend() ? nullptr : &*it;
//}

struct ComputeWordResult
{
  size_t               num_chars = 0;
  LetterRectCollection item;
};

template <typename IT>
Result<ComputeWordResult, std::string>
compute_next_word(RectFloat const& rect, FontInfo const& finfo, glm::vec2 const& font_size,
                  glm::ivec2 const& dinfo, CheckWrapAround const /*check_wraparound*/,
                  IT const& cbegin, IT const& cend)
{
  glm::vec2 const ratio{rect.width(), rect.height()};
  auto const&     img_size = finfo.texture_size;

  auto const& ffile      = finfo.ffile;
  auto const& uv_padding = ffile.padding;

  LetterRectCollection lrcollection;
  using RT               = Result<CharInfo, std::string>;
  auto const visit_cinfo = [&](CharInfo const& cinfo) -> RT {
    auto linfo = compute_letter_rectangles(cinfo, ratio, img_size, font_size, dinfo, uv_padding);
    lrcollection.emplace_back(MOVE(linfo));
    return Ok(cinfo);
  };
  auto const visit_nullterm = [](NullTerminatorCharInfo const&) -> RT {
    return ERR_SPRINTF("Found null terminator while processing");
  };
  size_t num_chars = 0;
  for (auto it = cbegin; it < cend; ++it, ++num_chars) {
    auto const& letter = *it;
    auto const  clr    = TRY(lookup_character(letter, ffile));
    TRY(std::visit(overload{visit_cinfo, visit_nullterm}, clr));
  }
  return Ok(ComputeWordResult{num_chars, MOVE(lrcollection)});
}

struct ComputeLineResult
{
  size_t               num_chars = 0;
  LetterRectCollection items;
};

template <typename IT>
Result<ComputeLineResult, std::string>
compute_next_line(RectFloat const& init, FontInfo const& finfo, glm::vec2 const& font_size,
                  glm::ivec2 const& dinfo, CheckWrapAround const cwa, IT const& cbegin,
                  IT const& cend)
{
  ComputeLineResult line_result;
  for (auto it = cbegin; it < cend;) {
    assert(it < cend);
    auto word_infor = compute_next_word(init, finfo, font_size, dinfo, cwa, it, cend);
    auto word_info  = TRY(word_infor);
    {
      auto const num_letters_in_word = word_info.num_chars;
      line_result.num_chars += num_letters_in_word;
      std::advance(it, num_letters_in_word);
    }
    for (auto&& lr : word_info.item) {
      line_result.items.emplace_back(MOVE(lr));
    }
  }
  return OK_MOVE(line_result);
}

// Expands the "outrect" parameter to be big enough to encompass the text rectangle collection.
// Grows in width up to the bounds, then grows height to accomodate extra vertical space required.
void
grow_rect_to_minimal_area_for_text_within_bounded_area(LetterRectCollection const& lrc,
                                                       RectFloat const& bounds, RectFloat& outrect)
{
  float       w = std::max(outrect.width(), 0.0f);
  float const h = compute_largest_height(lrc);
  for (auto const& letter : lrc) {
    auto const& lr = letter.rect;
    w += lr.width();
    if (w > bounds.width()) {
      outrect.height() += h;
      outrect.width() = bounds.width();
      w               = 0.0f;
    }
  }
  outrect.height() = std::max(h, outrect.height());
  outrect.width()  = std::max(w, outrect.width());
}

} // namespace

namespace font
{
float
compute_largest_height(LetterRectCollection const& lrc)
{
  auto h = 0.0f;
  for (auto const& letter : lrc) {
    h = std::max(h, letter.rect.height());
  }
  return h;
}

unsigned int
compute_howmany_lines_required_for(LetterRectCollection const& lrc, RectFloat const& box)
{
  auto num_lines = 1u;
  auto w         = 0.0f;
  for (auto const& letter : lrc) {
    auto const lw = letter.rect.width();
    if (w + lw > box.width()) {
      ++num_lines;
      w = 0.0f;
    }
    w += lw;
  }
  return num_lines;
}

RectFloat
compute_minimal_rect(LetterRectCollection const& lrc, RectFloat const& box)
{
  auto result = rect::create(box.left_top(), 0.0f, 0.0f);
  grow_rect_to_minimal_area_for_text_within_bounded_area(lrc, box, result);
  return result;
}

Result<FontInfo, std::string>
create_fontinfo(FontFile const& ffile, glm::vec2 const& ratio, glm::vec2 const& font_size,
                glm::ivec2 const& dinfo, glm::ivec2 const& img_size)
{
  auto const result = TRY(lookup_character(' ', ffile));

  using RT               = Result<FontInfo, std::string>;
  auto const visit_cinfo = [&](CharInfo const& cinfo) -> RT {
    auto const letter_rect = compute_letter_wh_rect(ratio, cinfo, font_size, dinfo);
    auto const line_height = letter_rect.height();

    auto infos = TRY(compute_letter_infos(ffile, ratio, font_size, dinfo, img_size));
    return Ok(FontInfo{ffile, img_size, font_size, line_height, MOVE(infos)});
  };

  constexpr char const* NULL_TERM_ERR = "Could not lookup whitespace character.";
  return std::visit(
      overload{
          [&](CharInfo const& ci) -> RT { return visit_cinfo(ci); },
          [&](NullTerminatorCharInfo const&) -> RT { return ERR_SPRINTF(NULL_TERM_ERR); },
      },
      result);
}

Result<LetterRectCollection, std::string>
compute_letter_rectangles(FontInfo const& finfo, RectFloat const& rect, std::string const& words,
                          glm::vec2 const& font_size, glm::ivec2 const& dinfo,
                          CheckWrapAround const check_wraparound, CenteredFlag const is_centered)
{
  auto const cbegin = std::cbegin(words);
  auto const cend   = std::cend(words);

  LetterRectCollection sitems;
  for (auto it = cbegin; it < cend;) {
    auto nl_result   = compute_next_line(rect, finfo, font_size, dinfo, check_wraparound, it, cend);
    auto line_result = TRY(nl_result);
    for (auto&& lr : line_result.items) {
      sitems.emplace_back(MOVE(lr));
    }
    std::advance(it, line_result.num_chars);
  }
  if (is_centered) {
    auto const center_rect = [&](RectFloat& r) {
      auto const sentence_rect = font::compute_minimal_rect(sitems, rect);
      r.left() -= sentence_rect.width() / 2.0f;
    };
    for (auto& li : sitems) {
      center_rect(li.rect);
    }
  }
  return OK_MOVE(sitems);
}

Result<LetterRectCollection, std::string>
compute_letter_rectangles_bounded(FontInfo const& finfo, RectFloat const& rect,
                                  std::string const& words, glm::vec2 const& font_size,
                                  glm::ivec2 const& dinfo)
{
  CheckWrapAround constexpr CHECK_WRAPAROUND = true;
  CenteredFlag constexpr CENTER_TEXT         = false;
  return compute_letter_rectangles(finfo, rect, words, font_size, dinfo, CHECK_WRAPAROUND,
                                   CENTER_TEXT);
}

} // namespace font
