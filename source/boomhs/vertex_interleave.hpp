#pragma once
#include <boomhs/shape.hpp>
#include <boomhs/vertex_factory.hpp>

// Interleave the vertices and the UVs of the Rectangles together.
inline RectangleUvVertices
vertex_interleave(VertexFactory::RectangleVertices const& v, RectangleUvs const& uv)
{
  // clang-format off
  return common::concat_array(
    v.zero(), uv.zero(),
    v.one(),  uv.one(),
    v.two(),  uv.two(),

    v.three(), uv.two(),
    v.four(),  uv.three(),
    v.five(),  uv.zero()
  );
  // clang-format on
}
