#pragma once
#include <boomhs/transform.hpp>
#include <common/macros_class.hpp>

class DeltaTime;

class Skybox
{
  Transform transform_;
  float     speed_;

public:
  Skybox();
  MOVE_CONSTRUCTIBLE_ONLY(Skybox);
  void update(DeltaTime const&);

  auto const& transform() const { return transform_; }
  auto&       transform() { return transform_; }
};
