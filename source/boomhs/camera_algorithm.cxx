#include "camera_algorithm.hpp"

#include <math/constants.hpp>
#include <math/frustum.hpp>
#include <math/viewport.hpp>

namespace
{
auto
compute_ortho_zoom(glm::vec2 const& amount, DeltaTime const& dt)
{
  return amount * dt();
}

} // namespace

namespace camera::ortho
{

glm::mat4
compute_pm(Frustum const& f, ScreenSize const& ss, CameraPosition const& position,
           glm::ivec2 const& zoom)
{
  auto const view_width  = static_cast<float>(ss.width);
  auto const view_height = static_cast<float>(ss.height);

  auto const zoom_x = static_cast<float>(zoom.x);
  auto const zoom_y = static_cast<float>(zoom.y);

  auto const f_left = static_cast<float>(f.left);
  auto const f_top  = static_cast<float>(f.top);

  auto const left  = 0.0f + f_left + zoom_x - position.x;
  auto const right = left + view_width - zoom_x - position.x;

  auto const top    = 0.0f + f_top + zoom_y + position.z;
  auto const bottom = top + view_height - zoom_y + position.z;

  auto constexpr NEAR = 1.0f;
  auto constexpr FAR  = -1.0f;
  return glm::ortho(left, right, bottom, top, NEAR, FAR);
}

glm::mat4
compute_vm(CameraPosition const& pos, CameraCenter const& center, CameraUp const& up,
           bool const flip_rightv)
{
  // Calculate the view model matrix.
  auto vm = glm::lookAtRH(pos, center, up);

  // Flip the "right" vector computed by lookAt() so the X-Axis points "right" on the user's screen.
  //
  // See implementation of glm::lookAtRH() for more details.
  if (flip_rightv) {
    auto& sx = vm[0][0];
    auto& sy = vm[1][0];
    auto& sz = vm[2][0];
    math::negate(sx, sy, sz);
  }

  return vm;
}

} // namespace camera::ortho

namespace camera
{

glm::vec3
eye_forward(CameraFPS const& c)
{
  // TODO: This doesn't work anymore after upgrading GLM version.
  return c.orientation.forward; //  *camera::transform(c).rotation;
}

glm::vec3
eye_up(CameraFPS const& c)
{
  return c.orientation.up;
}

glm::vec3
eye_forward(CameraORTHO const& c)
{
  return c.orientation.forward;
}

glm::vec3
eye_up(CameraORTHO const& c)
{
  return c.orientation.up;
}

glm::vec3
eye_forward(CameraArcball const& c)
{
  return glm::normalize(c.position() - c.target_position());
}

glm::vec3
eye_up(CameraArcball const& c)
{
  return c.up_inverted ? -c.world_up : c.world_up;
}

glm::vec3
eye_forward(Camera const& c)
{
  switch (c.mode) {
  case CameraMode::FPS:
    return camera::eye_forward(c.fps);
  case CameraMode::Ortho:
  case CameraMode::Fullscreen_2DUI:
    return camera::eye_forward(c.ortho);
  case CameraMode::ThirdPerson:
    return camera::eye_forward(c.arcball);
  case CameraMode::FREE_FLOATING:
  case CameraMode::MAX:
    break;
  }
  std::exit(EXIT_FAILURE);
}

glm::vec3
eye_up(Camera const& c)
{
  switch (c.mode) {
  case CameraMode::FPS:
    return camera::eye_up(c.fps);

  case CameraMode::Ortho:
  case CameraMode::Fullscreen_2DUI:
    return camera::eye_up(c.ortho);

  case CameraMode::ThirdPerson:
    return camera::eye_up(c.arcball);

  case CameraMode::FREE_FLOATING:
  case CameraMode::MAX:
    break;
  }
  std::exit(EXIT_FAILURE);
}

glm::vec3
eye_backward(Camera const& c)
{
  return -eye_forward(c);
}

glm::vec3
eye_right(Camera const& c)
{
  auto const f = eye_forward(c);
  auto const u = eye_up(c);
  return glm::normalize(glm::cross(f, u));
}

glm::vec3
eye_left(Camera const& c)
{
  return -eye_right(c);
}

CameraMatrices
calc_cm(CameraArcball const& c, ViewSettings const& vp, Frustum const& fr, glm::vec3 const& pos)
{
  auto const ar  = math::compute_ar(fr);
  auto const fov = vp.field_of_view;
  auto const pm  = glm::perspective(fov, ar, fr.near, fr.far);

  auto const& target_pos = camera::transform(c).translation;
  auto const  vm         = glm::lookAt(pos, target_pos, camera::eye_up(c));

  return CameraMatrices{pm, vm};
}

CameraMatrices
calc_cm(CameraFPS const& c, ViewSettings const& vs, Frustum const& fr, glm::vec3 const& eye_fwd)
{
  auto const ar  = math::compute_ar(fr);
  auto const fov = vs.field_of_view;
  auto const pm  = glm::perspective(fov, ar, fr.near, fr.far);

  auto const pos = camera::transform(c).translation;
  auto const vm  = glm::lookAt(pos, pos + eye_fwd, camera::eye_up(c));
  return CameraMatrices{pm, vm};
}

CameraMatrices
calc_cm(CameraORTHO const& c, Frustum const& f, ScreenSize const& ss)
{
  auto const& zoom_amount = c.zoom;

  auto const pos = c.position;
  auto const pm  = ortho::compute_pm(f, ss, pos, zoom_amount);

  auto const  target = pos + camera::eye_forward(c);
  auto const& up     = camera::eye_up(c);

  auto const vm = ortho::compute_vm(pos, target, up, c.flip_rightv);

  return CameraMatrices{pm, vm};
}

CameraMatrices
calc_cm(Camera const& c, ViewSettings const& vs, Frustum const& frustum, ScreenSize const& ss,
        glm::vec3 const& pos)
{
  switch (c.mode) {
  case CameraMode::FPS:
    return calc_cm(c.fps, vs, frustum, pos);
  case CameraMode::ThirdPerson:
    return calc_cm(c.arcball, vs, frustum, pos);
  case CameraMode::Ortho:
  case CameraMode::Fullscreen_2DUI:
    return calc_cm(c.ortho, frustum, ss);
  case CameraMode::FREE_FLOATING:
  case CameraMode::MAX:
    break;
  }
  std::exit(EXIT_FAILURE);
}

CameraFPS&
rotate_radians(CameraFPS& c, float const dx, float const dy, DeltaTime const&)
{
  c.xrot += dx;
  auto const& cs = c.cs;
  {
    float const newy_rot     = c.yrot + dy;
    bool const  within_angle = std::abs(newy_rot) < glm::radians(65.0f);

    if (cs.rotation_lock || within_angle) {
      c.yrot = newy_rot;
    }
  }

  auto const& orient = c.orientation;
  auto const  xaxis  = cs.flip_x ? orient.up : -orient.up;
  auto const  yaxis  = cs.flip_y ? orient.right() : -orient.right();

  // combine existing rotation with new rotation.
  // Y-axis first
  glm::quat a                   = glm::angleAxis(c.yrot, yaxis) * glm::angleAxis(dy, yaxis);
  glm::quat b                   = glm::angleAxis(c.xrot, xaxis) * glm::angleAxis(dx, xaxis);
  camera::transform(c).rotation = a * b;
  return c;
}

CameraArcball&
rotate_radians(CameraArcball& c, float dx, float dy, DeltaTime const&)
{
  using namespace math::constants;

  auto const& cs      = c.cs;
  auto&       scoords = c.scoords;
  {
    auto& theta = scoords.theta;
    theta       = (camera::eye_up(c).y > 0.0f) ? (theta - dx) : (theta + dx);
    if (theta > TWO_PI) {
      theta -= TWO_PI;
    }
    else if (theta < -TWO_PI) {
      theta += TWO_PI;
    }
  }

  auto&       phi     = scoords.phi;
  float const new_phi = cs.flip_y ? (phi + dy) : (phi - dy);

  bool const in_region0     = new_phi > 0 && new_phi < (PI / 2.0f);
  bool const in_region1     = new_phi < -(PI / 2.0f);
  bool const top_hemisphere = in_region0 || in_region1;
  if (!cs.rotation_lock || top_hemisphere) {
    phi = new_phi;
  }

  // Keep phi within -2PI to +2PI for easy 'up' comparison
  if (phi > TWO_PI) {
    phi -= TWO_PI;
  }
  else if (phi < -TWO_PI) {
    phi += TWO_PI;
  }

  // If phi in range (0, PI) or (-PI to -2PI), make 'up' be positive Y, otherwise make it negative Y
  if ((phi > 0 && phi < PI) || (phi < -PI && phi > -TWO_PI)) {
    c.up_inverted = false;
  }
  else {
    c.up_inverted = true;
  }
  return c;
}

Camera&
rotate_radians(Camera& c, float dx, float dy, DeltaTime const& dt)
{
  dx *= dt();
  dy *= dt();

  switch (c.mode) {
  case CameraMode::FPS:
    camera::rotate_radians(c.fps, dx, dy, dt);
    break;
  case CameraMode::Ortho:
  case CameraMode::Fullscreen_2DUI:
    break;
  case CameraMode::ThirdPerson:
    camera::rotate_radians(c.arcball, dx, dy, dt);
    break;
  case CameraMode::FREE_FLOATING:
  case CameraMode::MAX:
    std::exit(EXIT_FAILURE);
  }
  return c;
}

void
scroll(CameraORTHO& c, glm::vec2 const& sv)
{
  c.position += glm::vec3{sv.x, 0, sv.y};
}

void
zoom_out(CameraORTHO& c, glm::vec2 const& amount, DeltaTime const& dt)
{
  c.zoom += compute_ortho_zoom(+amount, dt);
}

void
zoom_in(CameraORTHO& c, glm::vec2 const& amount, DeltaTime const& dt)
{
  c.zoom += compute_ortho_zoom(-amount, dt);
}

void
zoom(CameraArcball& c, float const amount, DeltaTime const& dt)
{
  float constexpr MIN_RADIUS            = 0.01f;
  float constexpr ZOOM_SPEED_MULTIPLIER = 1000.0;

  float const delta      = (amount * dt() * ZOOM_SPEED_MULTIPLIER);
  float const new_radius = c.scoords.radius + delta;

  c.scoords.radius = (new_radius >= MIN_RADIUS) ? new_radius : MIN_RADIUS;
}

void
zoom_out(CameraArcball& c, float const amount, DeltaTime const& dt)
{
  zoom(c, -amount, dt);
}

void
zoom_in(CameraArcball& c, float const amount, DeltaTime const& dt)
{
  zoom(c, amount, dt);
}

#define ZOOM_INOUT_IMPL(CAMERA, METHOD)                                                            \
  switch (CAMERA.mode) {                                                                           \
  case CameraMode::FPS:                                                                            \
  case CameraMode::Fullscreen_2DUI:                                                                \
    break;                                                                                         \
                                                                                                   \
  case CameraMode::Ortho:                                                                          \
    METHOD(CAMERA.ortho, glm::vec2{v}, dt);                                                        \
    break;                                                                                         \
                                                                                                   \
  case CameraMode::ThirdPerson:                                                                    \
    METHOD(CAMERA.arcball, v, dt);                                                                 \
    break;                                                                                         \
  case CameraMode::FREE_FLOATING:                                                                  \
  case CameraMode::MAX:                                                                            \
    std::exit(EXIT_FAILURE);                                                                       \
  }

void
zoom_out(Camera& c, float const v, DeltaTime const& dt)
{
  ZOOM_INOUT_IMPL(c, zoom_out)
}

void
zoom_in(Camera& c, float const v, DeltaTime const& dt)
{
  ZOOM_INOUT_IMPL(c, zoom_in)
}

#undef ZOOM_INOUT_IMPL

} // namespace camera
