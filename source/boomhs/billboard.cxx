#include "billboard.hpp"
#include <boomhs/entity.hpp>
#include <boomhs/transform.hpp>

namespace
{

void
spherical_data(float* data)
{
  // Column 0:
  data[0] = 1.0f;
  data[1] = 0.0f;
  data[2] = 0.0f;

  // Column 1:
  data[4 + 0] = 0.0f;
  data[4 + 1] = 1.0f;
  data[4 + 2] = 0.0f;

  // Column 2:
  data[8 + 0] = 0.0f;
  data[8 + 1] = 0.0f;
  data[8 + 2] = 1.0f;
}

void
cylindrical_data(float* data)
{
  // Column 0:
  data[0] = 1.0f;
  data[1] = 0.0f;
  data[2] = 0.0f;

  // Column 2:
  data[8 + 0] = 0.0f;
  data[8 + 1] = 0.0f;
  data[8 + 2] = 1.0f;
}

} // namespace

namespace billboard
{

BillboardType
from_string(std::string_view const& str)
{
  if ("spherical" == str) {
    return BillboardType::Spherical;
  }
  else if ("cylindrical" == str) {
    return BillboardType::Cylindrical;
  }
  else {
    std::exit(EXIT_FAILURE);
  }
}

glm::mat4
compute_viewmodel(Transform const& transform, ViewMatrix const& vm, BillboardType const bb_type)
{
  auto view_model = vm * transform.model_matrix();

  // Reset the rotation values in order to achieve a billboard effect.
  //
  // http://www.geeks3d.com/20140807/billboarding-vertex-shader-glsl/
  float* data = glm::value_ptr(view_model);
  switch (bb_type) {
  case BillboardType::Spherical:
    spherical_data(data);
    break;
  case BillboardType::Cylindrical:
    cylindrical_data(data);
    break;
  case BillboardType::INVALID:
    std::exit(EXIT_FAILURE);
  }

  auto const& s = transform.scale;
  data[0]       = s.x;
  data[5]       = s.y;
  data[10]      = s.z;

  return view_model;
}

eid_array
find_all(enttreg_t& registry)
{
  eid_array bboards;
  auto      view = registry.view<BillboardRenderable>();
  for (auto const eid : view) {
    bboards.emplace_back(eid);
  }
  return bboards;
}

} // namespace billboard
