#pragma once
#include <boomhs/entity.hpp>
#include <boomhs/leveldata.hpp>
#include <common/result.hpp>

namespace opengl
{
class texture_storage;
} // namespace opengl

struct log_t;
class Heightmap;
struct WorldOrientation;

namespace start_area
{

using GenLevelResult = Result<LevelGeneratedData, std::string>;

GenLevelResult
gen_level(log_t&, enttreg_t&, rng_t&, opengl::texture_storage&, MaterialTable const&,
          Heightmap const&, WorldOrientation const&);

} // namespace start_area
