#include "engine.hpp"
#include <boomhs/controller.hpp>
#include <common/move.hpp>

#include <extlibs/openal.hpp>

////////////////////////////////////////////////////////////////////////////////////////////////////
// EngineState
EngineState::EngineState(WallClock& wc)
    : wclock(wc)
{
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// Engine
Engine::Engine(WallClock&& wc, SDLControllers&& c, DisplayInformation&& di, ALCdevice& opal)
    : wclock(MOVE(wc))
    , devices(Devices{MOVE(c), MOVE(di)})
    , openal(opal)
{
  registries.resize(1);
}
