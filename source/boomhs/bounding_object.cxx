#include "bounding_object.hpp"
#include <boomhs/components.hpp>
#include <boomhs/entity.hpp>
#include <boomhs/leveldata.hpp>
#include <boomhs/vertex_factory.hpp>

#include <opengl/gpu.hpp>
#include <opengl/shader.hpp>

using namespace opengl;

////////////////////////////////////////////////////////////////////////////////////////////////////
// AABoundingBox
AABoundingBox::AABoundingBox(glm::vec3 const& minp, glm::vec3 const& maxp, draw_state&& dinfo)
    : cube(Cube{minp, maxp})
    , draw_info(MOVE(dinfo))
{
}

AABoundingBox&
AABoundingBox::add_to_entity(log_t& logger, enttreg_t& reg, eid_t const eid, glm::vec3 const& min,
                             glm::vec3 const& max)
{
  auto& va = shader3d::wireframe(reg).va();

  auto const cv    = VertexFactory::build_cube(min, max);
  auto       dinfo = OG::copy_cube_wireframe_gpu(logger, cv, va);
  auto&      bbox  = reg.emplace<AABoundingBox>(eid, min, max, MOVE(dinfo));

  reg.emplace<Selectable>(eid);
  return bbox;
}
