#pragma once
#include <array>
#include <common/macros_class.hpp>
#include <extlibs/glm.hpp>
#include <extlibs/sdl.hpp>

class CursorManager
{
  static auto constexpr CURSOR_INDEX_BEGIN = SDL_SYSTEM_CURSOR_ARROW;
  static auto constexpr CURSOR_INDEX_END   = SDL_NUM_SYSTEM_CURSORS;

  static_assert(CURSOR_INDEX_END > CURSOR_INDEX_BEGIN, "CursorEndIndex Must Be > CursorBeginIndex");
  static auto constexpr CURSOR_COUNT = CURSOR_INDEX_END - CURSOR_INDEX_BEGIN - 1;

  std::array<SDL_Cursor*, CURSOR_INDEX_END - CURSOR_INDEX_BEGIN> cursors;
  SDL_SystemCursor                                               active_ = CURSOR_INDEX_BEGIN;

  bool contains(SDL_SystemCursor) const;

public:
  NO_MOVE_OR_COPY(CursorManager);

  void        set_active(SDL_SystemCursor);
  SDL_Cursor* active() const;

  CursorManager();
  ~CursorManager();
};

// While the user has "pressed" a mouse button down and held it, this value holds the origin of
// where the user began holding down the mouse button.
//
// This field is useful for drawing things relative to where a user clicks and holds down/drags
// the mouse.
//
// ie: Unit selection from a top-down perspective.
struct MouseClickPositions
{
  glm::ivec2 left_right;
  glm::ivec2 middle;
};

class Mouse
{
  static auto mask() { return SDL_GetMouseState(nullptr, nullptr); }

public:
  static glm::ivec2 coords();
  static bool       left_pressed();
  static bool       right_pressed();
  static bool       middle_pressed();

  static bool both_pressed();
  static bool either_pressed();

  static glm::ivec2 rel_pos();
  static auto       xrel() { return rel_pos().x; }
  static auto       yrel() { return rel_pos().y; }

public:
  glm::ivec2          rel_since_last_left_click();
  MouseClickPositions click_position;
};