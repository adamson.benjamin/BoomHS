#include "obj.hpp"
#include <boomhs/color.hpp>
#include <boomhs/components.hpp>

#include <common/FOR.hpp>
#include <common/algorithm.hpp>
#include <common/log.hpp>

#include <cassert>

using namespace opengl;

namespace
{
auto constexpr index_cast(int const i) { return static_cast<size_t>(i); }

LoadStatus
load_positions(tinyobj::index_t const& index, tinyobj::attrib_t const& attrib,
               std::vector<float>* pvertices)
{
  auto const pos_index = 3 * index.vertex_index;
  if (pos_index < 0) {
    return LoadStatus::MISSING_POSITION_ATTRIBUTES;
  }
  auto const x = attrib.vertices[index_cast(pos_index + 0)];
  auto const y = attrib.vertices[index_cast(pos_index + 1)];
  auto const z = attrib.vertices[index_cast(pos_index + 2)];

  auto& vertices = *pvertices;
  vertices.push_back(x);
  vertices.push_back(y);
  vertices.push_back(z);
  return LoadStatus::SUCCESS;
}

LoadStatus
load_normals(tinyobj::index_t const& index, tinyobj::attrib_t const& attrib,
             std::vector<float>* pvertices)
{
  auto const ni = 3 * index.normal_index;
  if (ni >= 0) {
    auto const xn = attrib.normals[index_cast(ni + 0)];
    auto const yn = attrib.normals[index_cast(ni + 1)];
    auto const zn = attrib.normals[index_cast(ni + 2)];

    auto& vertices = *pvertices;
    vertices.emplace_back(-xn);
    vertices.emplace_back(-yn);
    vertices.emplace_back(-zn);
  }
  else {
    return LoadStatus::MISSING_NORMAL_ATTRIBUTES;
  }
  return LoadStatus::SUCCESS;
}

LoadStatus
load_uvs(tinyobj::index_t const& index, tinyobj::attrib_t const& attrib,
         std::vector<float>* pvertices)
{
  auto const ti = 2 * index.texcoord_index;
  if (ti >= 0) {
    auto const u = attrib.texcoords[index_cast(ti + 0)];
    auto const v = 1.0f - attrib.texcoords[index_cast(ti + 1)];

    auto& vertices = *pvertices;
    vertices.emplace_back(u);
    vertices.emplace_back(v);
  }
  else {
    return LoadStatus::MISSING_UV_ATTRIBUTES;
  }
  return LoadStatus::SUCCESS;
}

LoadStatus
load_colors(color4 const& c, std::vector<float>* pvertices)
{
  auto& vertices = *pvertices;
  vertices.push_back(color::red(c));
  vertices.push_back(color::green(c));
  vertices.push_back(color::blue(c));
  vertices.push_back(color::alpha(c));
  return LoadStatus::SUCCESS;
}

} // namespace

////////////////////////////////////////////////////////////////////////////////////////////////////
// PositionsBuffer
PositionsBuffer::PositionsBuffer(ObjVertices&& v)
    : vertices(MOVE(v))
{
}

glm::vec3
PositionsBuffer::min() const
{
  glm::vec3 r;

  size_t i = 0;
  r.x      = vertices[i++];
  r.y      = vertices[i++];
  r.z      = vertices[i++];

  while (i < vertices.size()) {
    r.x = std::min(r.x, vertices[i++]);
    r.y = std::min(r.y, vertices[i++]);
    r.z = std::min(r.z, vertices[i++]);
    assert(i <= vertices.size());
  }

  return r;
}

glm::vec3
PositionsBuffer::max() const
{
  glm::vec3 r;

  size_t i = 0;
  r.x      = vertices[i++];
  r.y      = vertices[i++];
  r.z      = vertices[i++];

  while (i < vertices.size()) {
    r.x = std::max(r.x, vertices[i++]);
    r.y = std::max(r.y, vertices[i++]);
    r.z = std::max(r.z, vertices[i++]);

    assert(i <= vertices.size());
  }

  return r;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// ObjData
ObjData
ObjData::clone() const
{
  return *this;
}

PositionsBuffer
ObjData::positions() const
{
  auto copy = vertices;
  return PositionsBuffer{MOVE(copy)};
}

std::string
ObjData::to_string() const
{
  return fmt::sprintf(
      "{num_vertexes: %u, num vertices: %u, num colors: %u, num uvs: %u, num indices: %u}",
      num_vertexes, vertices.size(), colors.size(), normals.size(), uvs.size(), indices.size());
}

// LoadStatus
///////////////////////////////////////////////////////////////////////////////////////////////////

std::string
loadstatus_to_string(LoadStatus const ls)
{
//
// TODO: derive second argument from first somehow?
#define CASE(ATTRIBUTE, ATTRIBUTE_S)                                                               \
  case LoadStatus::ATTRIBUTE:                                                                      \
    return ATTRIBUTE_S

  // clang-format off
  switch (ls) {
		CASE(FILE_NOT_FOUND,              "FILE_NOT_FOUND");
    CASE(MISSING_POSITION_ATTRIBUTES, "MISSING_POSITION_ATTRIBUTES");
    CASE(MISSING_COLOR_ATTRIBUTES,    "MISSING_COLOR_ATTRIBUTES");
    CASE(MISSING_NORMAL_ATTRIBUTES,   "MISSING_NORMAL_ATTRIBUTES");
    CASE(MISSING_UV_ATTRIBUTES,       "MISSING_UV_ATTRIBUTES");

    CASE(TINYOBJ_ERROR,               "TINYOBJ_ERROR");
    CASE(SUCCESS,                     "SUCCESS");
  }
  // clang-format on
#undef CASE

  // terminal error
  std::exit(EXIT_FAILURE);
}

std::ostream&
operator<<(std::ostream& stream, LoadStatus const& ls)
{
  stream << loadstatus_to_string(ls);
  return stream;
}

LoadResult
load_objfile(log_t& LOGGER, std::filesystem::path const& path)
{
  LOG_INFO("Loading objfile: %s", path.string());

  tinyobj::attrib_t attrib;
  std::string       err, warn;

  ObjData objdata;
  auto&   materials = objdata.materials;
  auto&   shapes    = objdata.shapes;

  auto const obj_path = path.string() + ".obj";
  auto const mtl_path = path.parent_path().string() + "/";

  if (!tinyobj::LoadObj(&attrib, &shapes, &materials, &err, &warn, obj_path.c_str(), mtl_path.c_str())) {
    LOG_ERROR("error loading obj, msg: %s", err);
    LOG_WARN("associated warning: %s", warn);
    return Err(LoadStatus::FILE_NOT_FOUND);
  }
  if (!warn.empty()) {
    LOG_WARN("obj file=%s warning: %s", obj_path, warn);
  }

  assert(!objdata.materials.empty());

  // TODO: for now only loading one mesh exactly
  assert(1 == shapes.size());

  // confirm vertices are triangulated
  assert(0 == (attrib.vertices.size() % 3));
  assert(0 == (attrib.normals.size() % 3));
  assert(0 == (attrib.texcoords.size() % 2));

  auto& indices = objdata.indices;

  auto const vd3 = attrib.vertices.size() / 3;
  {
    assert(vd3 <= std::numeric_limits<unsigned int>::max());
    objdata.num_vertexes = static_cast<unsigned int>(vd3);
  }
  /*
  LOG_ERROR("vertice count %u", num_vertexes);
  LOG_ERROR("normal count %u", attrib.normals.rect());
  LOG_ERROR("texcoords count %u", attrib.texcoords.rect());
  LOG_ERROR("color count %u", attrib.colors.rect());
  */

  auto const get_facecolor = [&materials](auto const& shape, auto const f) {
    // per-face material
    int const face_materialid = shape.mesh.material_ids[f];
    assert(face_materialid >= 0);
    auto const  fmid    = static_cast<size_t>(face_materialid);
    auto const& diffuse = materials[fmid].diffuse;
    return color4{diffuse[0], diffuse[1], diffuse[2], 1.0f};
  };

  size_t     index_offset           = 0;
  auto const load_vertex_attributes = [&](auto const& shape, auto const& face) -> LoadStatus {
    auto const face_color = get_facecolor(shape, face);

    auto const fv = shape.mesh.num_face_vertices[face];
    // Loop over vertices in the face.
    FOR(vi, fv)
    {
      // access to vertex
      tinyobj::index_t const index = shape.mesh.indices[index_offset + vi];

#define LOAD_ATTR(...)                                                                             \
  ({                                                                                               \
    auto const load_status = __VA_ARGS__;                                                          \
    if (load_status != LoadStatus::SUCCESS) {                                                      \
      return load_status;                                                                          \
    }                                                                                              \
  })

      LOAD_ATTR(load_positions(index, attrib, &objdata.vertices));
      LOAD_ATTR(load_normals(index, attrib, &objdata.normals));
      LOAD_ATTR(load_uvs(index, attrib, &objdata.uvs));
      LOAD_ATTR(load_colors(face_color, &objdata.colors));

#undef LOAD_ATTR
      indices.push_back(static_cast<uint32_t>(indices.size())); // 0, 1, 2, ...
    }
    index_offset += fv;
    return LoadStatus::SUCCESS;
  };

  objdata.foreach_face(load_vertex_attributes);
  LOG_DEBUG("num vertices: %u", objdata.num_vertexes);
  LOG_DEBUG("vertices.size(): %u", objdata.vertices.size());
  LOG_DEBUG("colors.size(): %u", objdata.colors.size());
  LOG_DEBUG("normals.size(): %u", objdata.normals.size());
  LOG_DEBUG("uvs.size(): %u", objdata.uvs.size());
  LOG_DEBUG("num indices: %u", objdata.indices.size());
  return OK_MOVE(objdata);
}
