#pragma once
#include <boomhs/entity.hpp>
#include <math/matrices.hpp>

#include <string_view>

struct Transform;

enum class BillboardType
{
  Spherical = 0,
  Cylindrical,
  INVALID
};

struct BillboardRenderable
{
  BillboardType value = BillboardType::INVALID;
};

namespace billboard
{
BillboardType
from_string(std::string_view const&);

// Compute the viewmodel for a transform that appears as billboard.
glm::mat4
compute_viewmodel(Transform const&, ViewMatrix const&, BillboardType);

eid_array
find_all(enttreg_t&);

} // namespace billboard
