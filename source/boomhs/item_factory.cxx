#include "item_factory.hpp"
#include <boomhs/components.hpp>
#include <boomhs/item.hpp>
#include <boomhs/lighting.hpp>
#include <boomhs/material.hpp>

#include <opengl/shader.hpp>
#include <opengl/texture.hpp>

using namespace common;
using namespace opengl;

namespace
{
eid_t
create_item(enttreg_t& reg, texture_storage& ttable, char const* entity_name, char const* ui_name,
            char const* mesh_name, char const* texture, char const* sn)
{
  auto eid = ItemFactory::create_empty(reg, ttable);

  reg.get<Name>(eid).value = entity_name;

  reg.emplace<IsRenderable>(eid);
  reg.emplace<MeshRenderable>(eid, mesh_name);

  auto& tr        = reg.emplace<TextureRenderable>(eid);
  auto  texture_o = ttable.find(texture);
  assert(texture_o);
  tr.texture_info = &*texture_o;

  reg.emplace<ShaderComponent>(eid, sn);

  auto& item    = reg.get<Item>(eid);
  item.ui_tinfo = &*ttable.find(ui_name);
  assert(item.ui_tinfo);

  return eid;
}

} // namespace

eid_t
ItemFactory::create_empty(enttreg_t& reg, texture_storage& ttable)
{
  auto const eid = reg.create();
  reg.emplace<Name>(eid, "Empty Item");

  Item& item       = reg.emplace<Item>(eid);
  item.is_pickedup = false;
  item.ui_tinfo    = &*ttable.find("RedX");
  assert(item.ui_tinfo);

  item.name    = "RedX";
  item.tooltip = "This is some kind of item";

  reg.emplace<Transform>(eid);
  return eid;
}

eid_t
ItemFactory::create_book(enttreg_t& reg, texture_storage& ttable)
{
  auto const eid = create_item(reg, ttable, "Book EID", "BookUI", "B", "container", "3dtexture");
  reg.emplace<Book>(eid);
  reg.emplace<Material>(eid);

  auto& item   = reg.get<Item>(eid);
  item.name    = "Book";
  item.tooltip = "This is a book";

  return eid;
}

eid_t
ItemFactory::create_spear(enttreg_t& reg, opengl::texture_storage& ttable)
{
  auto const eid =
      create_item(reg, ttable, "Spear EID", "SpearUI", "hashtag", "container", "3dtexture");
  reg.emplace<Weapon>(eid);
  reg.emplace<Material>(eid);

  auto& item   = reg.get<Item>(eid);
  item.name    = "Spear";
  item.tooltip = "A mighty slaying spear!";

  return eid;
}

PlaceTorchResult
ItemFactory::create_torch(log_t& LOGGER, enttreg_t& reg, texture_storage& ttable)
{
  auto eid = create_item(reg, ttable, "Torch EID", "TorchUI", "star", "Lava", "torch");
  reg.emplace<Torch>(eid);

  auto& item   = reg.get<Item>(eid);
  item.name    = "Torch";
  item.tooltip = "This is a torch";

  auto& pointlight         = reg.emplace<PointLight>(eid);
  pointlight.light.diffuse = LOC3::YELLOW;

  LOG_INFO("Adding torch");
  TRY(shader3d::increment_num_pointlights(LOGGER, reg));

  auto& flicker         = reg.emplace<LightFlicker>(eid);
  flicker.base_speed    = 1.0f;
  flicker.current_speed = flicker.base_speed;

  flicker.colors[0] = LOC3::RED;
  flicker.colors[1] = LOC3::YELLOW;

  auto& att     = pointlight.attenuation;
  att.constant  = 1.0f;
  att.linear    = 0.93f;
  att.quadratic = 0.46f;
  return Ok(eid);
}
