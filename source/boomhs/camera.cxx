#include "camera.hpp"
#include <boomhs/camera_algorithm.hpp>
#include <boomhs/delta_time.hpp>
#include <boomhs/mouse.hpp>
#include <boomhs/world_object.hpp>

#include <gl_sdl/sdl_window.hpp>

#include <math/space_conversions.hpp>
#include <math/spherical.hpp>
#include <math/viewport.hpp>

namespace camera
{

CameraMatrices
compute_matrices(Camera const& c, Frustum const& fr, CameraMode const mode, glm::vec3 const& pos)
{
  CameraMatrices cm;

  auto const& vs = c.view_settings;

  switch (mode) {
  case CameraMode::Ortho: {
    ScreenSize constexpr DEFAULT_ORTHO_VIEWSIZE{128, 128};
    cm = camera::calc_cm(c.ortho, fr, DEFAULT_ORTHO_VIEWSIZE);
  } break;
  case CameraMode::Fullscreen_2DUI: {
    auto const vp = Viewport::from_frustum(fr);
    cm            = camera::calc_cm(c.ortho, fr, vp.size());
  } break;
  case CameraMode::FPS:
    cm = camera::calc_cm(c.fps, vs, fr, camera::eye_forward(c));
    break;
  case CameraMode::ThirdPerson:
    cm = camera::calc_cm(c.arcball, vs, fr, pos);
    break;
  case CameraMode::FREE_FLOATING:
  case CameraMode::MAX:
    std::exit(EXIT_FAILURE);
  }

  return cm;
}

Camera
make_default(CameraMode const mode, WorldOrientation const& pers_wo,
             WorldOrientation const& ortho_wo)
{
  auto constexpr FOV = glm::radians(110.0f);
  ViewSettings vs{FOV};

  CameraArcball arcball(pers_wo.up);
  CameraFPS     fps(pers_wo);

  // It's weird using this direction unit vector as a position, but this is intentionaly.
  auto constexpr ORTHO_CAMERA_INIT_POSITION = math::constants::Y_UNIT_VECTOR;
  CameraORTHO ortho(ortho_wo, ORTHO_CAMERA_INIT_POSITION);

  Camera camera{mode, MOVE(vs), MOVE(arcball), MOVE(fps), MOVE(ortho)};

  SphericalCoordinates sc;
  sc.radius              = 3.8f;
  sc.theta               = glm::radians(-0.229f);
  sc.phi                 = glm::radians(38.2735f);
  camera.arcball.scoords = sc;
  return camera;
}

CameraMatrices
gui_matrices(Camera const& camera, Frustum const& fr)
{
  return compute_matrices(camera, fr, CameraMode::Fullscreen_2DUI, camera.position());
}

} // namespace camera

////////////////////////////////////////////////////////////////////////////////////////////////////
// CameraTarget
Transform&
CameraTarget::get()
{
  // https://stackoverflow.com/a/47369227/562174
  return const_cast<Transform&>(std::as_const(*this).get());
}

Transform const&
CameraTarget::get() const
{
  validate();
  return *t_;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// CameraModes
std::vector<std::string>
CameraModes::string_list()
{
  std::vector<std::string> result;
  for (auto const& it : CAMERA_MODES) {
    result.emplace_back(it.second);
  }
  return result;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// CameraTarget
void
CameraTarget::validate() const
{
  assert(nullptr != t_);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// CameraArcball
CameraArcball::CameraArcball(glm::vec3 const& up)
    : scoords(0.0f, 0.0f, 0.0f)
    , up_inverted(false)
    , world_up(up)
{
}

glm::vec3
CameraArcball::local_position() const
{
  return math::to_cartesian(scoords);
}

glm::vec3
CameraArcball::position() const
{
  return target_position() + local_position();
}

glm::vec3
CameraArcball::target_position() const
{
  return target.get().translation;
}

void
CameraArcball::set_target(Transform& t)
{
  target.set(t);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// CameraFPS
CameraFPS::CameraFPS(WorldOrientation const& wo)
    : orientation(wo)
{
  cs.flip_x = true;
  cs.flip_y = true;
}

glm::vec3
CameraFPS::position() const
{
  return camera::transform(*this).translation;
}

void
CameraFPS::set_target(Transform& t)
{
  target.set(t);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// CameraORTHO
CameraORTHO::CameraORTHO(WorldOrientation const& wo, glm::vec3 const& pos)
    : orientation(wo)
    , zoom(VEC2{0})
    , position(pos)
{
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// Camera
Camera::Camera(CameraMode const cmode, ViewSettings&& vp, CameraArcball&& acam, CameraFPS&& fcam,
               CameraORTHO&& ocam)
    : view_settings(MOVE(vp))
    , mode(cmode)
    , arcball(MOVE(acam))
    , fps(MOVE(fcam))
    , ortho(MOVE(ocam))
{
}

Transform const&
Camera::target() const
{
  switch (mode) {
  case CameraMode::FPS:
    return fps.target.get();
  case CameraMode::ThirdPerson:
    return arcball.target.get();

  case CameraMode::Ortho:
  case CameraMode::Fullscreen_2DUI:
  case CameraMode::FREE_FLOATING:
  case CameraMode::MAX:
    break;
  }

  std::exit(EXIT_FAILURE);
}

Transform&
Camera::target()
{
  // https://stackoverflow.com/a/47369227/562174
  return const_cast<Transform&>(std::as_const(*this).target());
}

void
Camera::set_mode(CameraMode const m)
{
  mode = m;
}

void
Camera::next_mode()
{
  auto const       cast = [](auto const v) { return static_cast<int>(v); };
  CameraMode const m    = static_cast<CameraMode>(cast(mode) + cast(1));
  if (CameraMode::MAX == m || CameraMode::Fullscreen_2DUI == m) {
    set_mode(static_cast<CameraMode>(0));
  }
  else {
    assert(m < CameraMode::FREE_FLOATING);
    set_mode(m);
  }
}

void
Camera::toggle_rotation_lock()
{
  switch (mode) {
  case CameraMode::FPS:
    fps.cs.rotation_lock ^= true;
    break;
  case CameraMode::Ortho:
  case CameraMode::Fullscreen_2DUI:
    break;
  case CameraMode::ThirdPerson:
    arcball.cs.rotation_lock ^= true;
    break;
  case CameraMode::FREE_FLOATING:
  case CameraMode::MAX:
    std::exit(EXIT_FAILURE);
  }
}

void
Camera::set_target(Transform& t)
{
  fps.set_target(t);
  arcball.set_target(t);
}

glm::vec3
Camera::position() const
{
  switch (mode) {
  case CameraMode::FPS:
    return fps.position();

  case CameraMode::Ortho:
  case CameraMode::Fullscreen_2DUI:
    return ortho.position;

  case CameraMode::ThirdPerson:
    return arcball.position();

  case CameraMode::FREE_FLOATING:
  case CameraMode::MAX:
    break;
  }
  std::exit(EXIT_FAILURE);
}

CameraMatrices
Camera::matrices(Frustum const& fr) const
{
  return camera::compute_matrices(*this, fr, mode, position());
}

ProjMatrix
Camera::proj_matrix(Frustum const& fr) const
{
  auto const cm = matrices(fr);
  return cm.proj;
}

ViewMatrix
Camera::view_matrix(Frustum const& fr) const
{
  auto const cm = matrices(fr);
  return cm.view;
}
