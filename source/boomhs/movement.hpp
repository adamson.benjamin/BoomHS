#pragma once
#include <extlibs/glm.hpp>

struct MovementState
{
  glm::vec3 forward, backward, left, right;
  glm::vec3 mouse_forward;
};