#pragma once
#include <boomhs/color.hpp>
#include <boomhs/entity.hpp>
#include <common/macros_class.hpp>

#include <extlibs/glm.hpp>

#include <string>
#include <string_view>
#include <vector>

struct Material
{
  glm::vec3 ambient   = glm::vec3{1.0f};
  glm::vec3 diffuse   = glm::vec3{1.0f};
  glm::vec3 specular  = glm::vec3{1.0f};
  float     shininess = 1.0f;

  Material() = default;
  MOVE_DEFAULT(Material);
  COPY_DEFAULT(Material);

  Material(glm::vec3 const&, glm::vec3 const&, glm::vec3 const&, float);

  //
  // The alpha value for the colors is truncated.
  Material(color3 const&, color3 const&, color3 const&, float const);
};

struct NameMaterial
{
  std::string const name;
  Material          material;
};

struct MaterialTable
{
  std::vector<NameMaterial> data_;

public:
  MaterialTable() = default;
  MOVE_CONSTRUCTIBLE_ONLY(MaterialTable);
  INDEX_OPERATOR_FNS(data_)
  BEGIN_END_FORWARD_FNS(data_)

  void add(NameMaterial&&);

  Material&       find(std::string_view const&);
  Material const& find(std::string_view const&) const;

  auto size() const { return data_.size(); }
  auto empty() const { return data_.empty(); }
};

////////////////////////////////////////////////////////////////////////////////////////////////////
//
eid_array
find_materials(enttreg_t&);
