#include "font_loader.hpp"
#include <common/log.hpp>

#include <extlibs/boost_algorithm.hpp>

#include <charconv>

using namespace common;

using LineWords = std::vector<std::string>;

namespace
{

auto
parse_int(log_t& LOGGER, std::string const& str, char const* field_name)
{
  int        result;
  auto const pr = std::from_chars(str.data(), str.data() + str.size(), result);
  if (pr.ec != std::errc{}) {
    LOG_ERROR("erro parsing '%s'", field_name);
    std::abort();
  }
  return result;
}

auto
parse_charinfo(log_t& logger, LineWords const& words)
{
  CharInfo ci;
  ci.c       = parse_int(logger, words[0], "char");
  ci.xpos    = parse_int(logger, words[1], "xpos");
  ci.ypos    = parse_int(logger, words[2], "ypos");
  ci.width   = parse_int(logger, words[3], "width");
  ci.height  = parse_int(logger, words[4], "height");
  ci.xoffset = parse_int(logger, words[5], "xoffset");
  ci.yoffset = parse_int(logger, words[6], "yoffset");
  ci.orig_w  = parse_int(logger, words[7], "origw");
  ci.orig_h  = parse_int(logger, words[8], "origh");
  return ci;
}

auto
parse_kerningvalues(log_t& logger, LineWords const& words)
{
  KerningPair kp;
  kp.c0 = parse_int(logger, words[0], "kp c0");
  kp.c1 = parse_int(logger, words[1], "kp c1");

  // Since GCC/CLANG haven't implemented from_chars() floating-point support at this time, fall
  // back to strtod()
  kp.value = ::strtof(words[2].c_str(), nullptr);
  return kp;
}

void
parse_line(log_t& LOGGER, std::string const& line, size_t const line_number, FontFile& ffile)
{
  LineWords words;
  boost::split(words, line, boost::is_any_of("\t "), boost::token_compress_on);
  if (9 == words.size()) {
    auto const ci = parse_charinfo(LOGGER, words);
    ffile.char_infos.emplace_back(ci);
  }
  else if (3 == words.size()) {
    auto kp = parse_kerningvalues(LOGGER, words);
    ffile.kerning_pairs.emplace_back(kp);
  }
  else if (2 == words.size()) {
    if ("kerning pairs:" != line) {
      LOG_ERROR("Unexpected line contents found: '%s'", line);
      std::abort();
    }
  }
  else {
    LOG_ERROR("Unexpected number of words '%i' found on line '%i' in file", words.size(),
              line_number);
    std::abort();
  }
}

} // namespace

namespace font
{

Result<FontFile, std::string>
load_font_file(log_t& logger, std::string const& contents)
{
  FontFile                 ffile;
  std::vector<std::string> lines;
  boost::split(lines, contents, boost::is_any_of("\n"));

  auto constexpr MIN_LINES = 2u;
  assert(lines.size() > MIN_LINES);
  for (auto i = MIN_LINES; i < lines.size(); ++i) {
    parse_line(logger, lines[i], i, ffile);
  }

  return OK_MOVE(ffile);
}

} // namespace font
