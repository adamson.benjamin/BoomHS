#pragma once
#include <boomhs/ui.hpp>

#include <common/non_const.hpp>
#include <common/result.hpp>

#include <memory>
#include <stack>
#include <string>
#include <vector>

struct Screen;
using ScreenVector = std::vector<std::unique_ptr<Screen>>;

struct log_t;
struct Camera;
struct Devices;
struct Frustum;
struct GameState;
class SDLWindow;

namespace opengl
{
struct render_state;
} // namespace opengl

enum class ScreenType
{
  InGame,
  MainMenu,
  Options
};

struct Screen
{
  ScreenType const type;
  UiState          uistate = {};

public:
  Screen(ScreenType const st)
      : type(st)
  {
  }

public:
  void (*draw_fn)(log_t&, GameState&, Camera const&, Frustum const&, opengl::render_state&,
                  DeltaTime const&) = nullptr;

  void (*update_fn)(log_t&, SDLWindow&, GameState&, Devices&, Camera const&,
                    DeltaTime const&) = nullptr;

  Result<NOTHING, std::string> (*resize_fn)(log_t&, GameState&, Screen&, Frustum const&) = nullptr;
};

class ScreenStack
{
  ScreenVector           storage_;
  std::stack<ScreenType> stack_;

public:
  ScreenStack() = default;
  ScreenStack(ScreenVector&&);

  bool    empty() const;
  Screen& push(ScreenType);

  void pop();
  void pop_all();

  Screen const* find(ScreenType) const;
  NON_CONST(find)

  Screen const& top() const;
  NON_CONST(top)

  Screen const& find_or_abort(ScreenType) const;
  NON_CONST(find_or_abort)

  size_t size() const;
};
