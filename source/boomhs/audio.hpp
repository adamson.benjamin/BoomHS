#pragma once
#include <common/macros_class.hpp>
#include <common/move_only_type.hpp>
#include <common/result.hpp>

#include <extlibs/glm.hpp>
#include <extlibs/openal.hpp>

#include <optional>
#include <stdio.h>
#include <string>
#include <vector>

struct log_t;

struct audio_buffer
{
  log_t* LOGGER;
  ALuint handle;

public:
  MOVE_DEFAULT_ONLY(audio_buffer);
  audio_buffer(log_t&);
  ~audio_buffer();
};

struct audio_source
{
  log_t* LOGGER;
  ALuint handle;

public:
  audio_source(log_t&);
  ~audio_source();

public:
  void set_looping(bool);
  void set_volume(float);
  void set_pitch(float);

  void set_position(float, float, float);
  void set_position(glm::vec3 const&);

  void set_velocity(glm::vec3 const&);
  void set_velocity(float, float, float);

  void pause();
  void play();
  void stop();

  bool is_playing() const;
};

class audio_object
{
  log_t*        LOGGER_;
  audio_source* source_;
  audio_buffer* buffer_;

public:
  MOVE_DEFAULT_ONLY(audio_object);
  audio_object(log_t&, audio_buffer&, audio_source&);

  auto const& LOGGER() const { return *LOGGER_; }
  auto const& source() const { return *source_; }
  auto const& buffer() const { return *buffer_; }

  auto& LOGGER() { return *LOGGER_; }
  auto& source() { return *source_; }
  auto& buffer() { return *buffer_; }
};

void
destroy_audio_object(audio_object&);

using audio_object_ar = move_only_type<audio_object, destroy_audio_object>;

class WaterAudioSystem
{
  log_t&          LOGGER;
  audio_object_ar ao_;

public:
  WaterAudioSystem(log_t&, audio_buffer&, audio_source&);

public:
  auto& buffer() { return ao_->buffer(); }
  auto& source() { return ao_->source(); }

  void play_inwater_sound();
  void stop_inwater_sound();

  bool is_playing_watersound() const;
  void set_volume(float);
};

struct OpenALState
{
  ALCdevice*  pdevice;
  ALCcontext* pcontext;
};

namespace audio
{
Result<OpenALState, char const*>
init(log_t&);

void
shutdown(log_t&, OpenALState&);

Result<WaterAudioSystem, std::string>
create_water_audio(log_t&, audio_buffer&, audio_source&);

} // namespace audio
