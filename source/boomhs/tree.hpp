#pragma once
#include <boomhs/components.hpp>
#include <boomhs/entity.hpp>

#include <opengl/draw_info.hpp>

#include <extlibs/glm.hpp>

#include <array>
#include <utility>

class ObjData;
class ObjStore;
struct log_t;

namespace opengl
{
class draw_state;
} // namespace opengl

enum class Treecolor4Type
{
  Trunk = 0,
  Stem,
  Leaf
};

class TreeComponent
{
  ObjData* pobj_;

  size_t num_trunks_ = 0;
  size_t num_stems_  = 0;
  size_t num_leaves_ = 0;

  color4 const& color_from_index(size_t, size_t, size_t) const;

public:
  static constexpr int                MAX_NUMBER_TREE_COLORS = 4;
  color4Array<MAX_NUMBER_TREE_COLORS> colors;

  ObjData&       obj();
  ObjData const& obj() const;

  void add_color(Treecolor4Type, color4 const&);

  color4 const& trunk_color(size_t) const;
  color4 const& stem_color(size_t) const;
  color4 const& leaf_color(size_t) const;

  auto constexpr num_trunks() const { return num_trunks_; }
  auto constexpr num_stems() const { return num_stems_; }
  auto constexpr num_leaves() const { return num_leaves_; }

  TreeComponent(ObjData&);
  COPYMOVE_DEFAULT(TreeComponent);
};

class Tree
{
  Tree() = delete;

public:
  static void
  update_colors(log_t&, opengl::vertex_attribute const&, opengl::draw_state const&, TreeComponent&);
};
