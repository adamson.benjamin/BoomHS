#pragma once
#include <boomhs/controller.hpp>
#include <boomhs/entity.hpp>
#include <boomhs/mouse.hpp>

#include <gl_sdl/display.hpp>

#include <math/random.hpp>

#include <common/macros_class.hpp>
#include <common/wall_clock.hpp>

#include <extlibs/openal.hpp>

struct EngineState
{
  bool       quit = false;
  WallClock& wclock;
  rng_t      rng;

  // Constructors
  NO_COPY_OR_MOVE(EngineState);
  EngineState(WallClock&);
};

struct Devices
{
  SDLControllers     controllers;
  DisplayInformation display_info;

  Mouse         mouse   = {};
  CursorManager cursors = {};
};

struct Engine
{
  WallClock wclock;
  Devices   devices = {};

  ALCdevice&      openal;
  entt_registries registries = {};

  // ctor
  explicit Engine(WallClock&&, SDLControllers&&, DisplayInformation&&, ALCdevice&);

  // We mark this as no-move/copy so the registries data never moves, allowing the rest of the
  // program to store references into the data owned by registries.
  NO_COPY_OR_MOVE(Engine);
};
