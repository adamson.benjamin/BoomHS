#include "player.hpp"

#include <boomhs/bounding_object.hpp>
#include <boomhs/components.hpp>
#include <boomhs/delta_time.hpp>
#include <boomhs/entity.hpp>
#include <boomhs/inventory.hpp>
#include <boomhs/item.hpp>
#include <boomhs/item_factory.hpp>
#include <boomhs/leveldata.hpp>
#include <boomhs/movement.hpp>
#include <boomhs/nearby_targets.hpp>
#include <boomhs/npc.hpp>
#include <boomhs/state.hpp>
#include <boomhs/terrain.hpp>

#include <common/macros_scope_exit.hpp>
#include <common/time.hpp>
#include <optional>

using namespace std::chrono;

namespace
{

void
kill_entity(enttreg_t& reg, eid_t const entity_eid)
{
  auto const& bbox = reg.get<AABoundingBox>(entity_eid).cube;
  auto const  hw   = bbox.half_widths();

  // Move the entity half it's bounding box, so it is directly on the ground, or whatever it was
  // standing on.
  auto& entity_transform = reg.get<Transform>(entity_eid);
  auto& entity_tr        = entity_transform.translation;
  entity_tr.y -= hw.y;

  // TODO: Get the normal vector for the terrain at the (x, z) point and rotate the npc to look
  // properly aligned on slanted terrain.
  entity_transform.rotate_degrees(90.0f, EulerAxis::X);
}

void
try_attack_selected_target(log_t& LOGGER, enttreg_t& reg, Player& player, eid_t const target_eid)
{
  auto&      ptransform = player.transform();
  auto const playerpos  = ptransform.translation;

  auto&      npc_transform = reg.get<Transform>(target_eid);
  auto const npcpos        = npc_transform.translation;

  auto& npcdata   = reg.get<NPCData>(target_eid);
  auto& target_hp = npcdata.health;

  bool const already_dead = NPC::is_dead(target_hp);
  if (already_dead) {
    LOG_ERROR("TARGET DEAD");
    return;
  }

  bool const within_attack_range = NPC::within_attack_range(npcpos, playerpos);
  if (within_attack_range) {
    LOG_ERROR("DAMAGING TARGET");
    target_hp.current -= player.damage;
  }
  else {
    LOG_ERROR("TARGET NOT WITHIN ATTACK RANGE");
  }
}

void
move_worldobject(UiDebugState const& ui_debug, WorldObject& wo, glm::vec3 const& move_vec,
                 float const speed, TerrainGrid const& terrain, DeltaTime const& dt)
{
  auto const max_pos = terrain.max_xz();
  auto const max_x   = max_pos.x;
  auto const max_z   = max_pos.y;

  glm::vec3 const delta         = move_vec * dt() * speed;
  glm::vec3 const newpos        = wo.world_position() + delta;
  auto const      out_of_bounds = terrain.out_of_bounds(newpos.x, newpos.z);

  if (out_of_bounds && !ui_debug.mariolike_edges) {
    // If the world object *would* be out of bounds, return early (don't move the WO).
    return;
  }

  auto const flip_sides = [](auto const val, auto const min, auto const max) {
    assert(min < (max - 1.0f));
    auto value = val < min ? max : min;
    return value >= max ? (value - 1.0f) : value;
  };

  if (out_of_bounds.x) {
    auto const new_x = flip_sides(newpos.x, 0.0f, max_x);
    wo.move_to(new_x, 0.0, newpos.z);
  }
  else if (out_of_bounds.z) {
    auto const new_z = flip_sides(newpos.z, 0.0f, max_z);
    wo.move_to(newpos.x, 0.0, new_z);
  }
  else {
    wo.move(delta);
  }
}

void
update_position(log_t& LOGGER, GameState& gs, Player& player, DeltaTime const& dt)
{
  auto&       ldata    = gs.ldata;
  auto&       tgrid    = ldata.tgrid;
  auto const& movement = gs.ldata.registry.ctx<MovementState>();

  // Move the player forward along it's movement direction
  auto move_dir = movement.forward + movement.backward + movement.left + movement.right +
                  movement.mouse_forward;

  if (move_dir != math::constants::ZERO) {
    move_dir = glm::normalize(move_dir);
  }

  {
    auto& wo = player.world_object();
    move_worldobject(gs.uibuffers_classic.debug, wo, move_dir, player.speed, tgrid, dt);
  }

  // Lookup the player height from the terrain at the player's X, Z world-coordinates.
  auto&       player_pos    = player.transform().translation;
  float const player_height = tgrid.get_height(LOGGER, player_pos.x, player_pos.z);
  auto const& player_bbox   = player.bounding_box().cube;
  player_pos.y              = player_height + player_bbox.half_widths().y;
}

} // namespace

////////////////////////////////////////////////////////////////////////////////////////////////////
// PlayerHead
PlayerHead::PlayerHead(enttreg_t& reg, eid_t const eid, WorldOrientation const& wo)
    : reg_(&reg)
    , eid_(eid)
    , world_object(eid, reg, wo)
{
}

void
PlayerHead::update(DeltaTime const&)
{
  auto const player_eid = find_player_eid(*reg_);

  auto const& player_bbox = reg_->get<AABoundingBox>(player_eid).cube;
  auto const& head_bbox   = reg_->get<AABoundingBox>(eid_).cube;

  auto const& player_tr = reg_->get<Transform>(player_eid);
  auto&       head_tr   = reg_->get<Transform>(eid_);

  auto const player_half_height = player_bbox.scaled_half_widths(player_tr).y;
  auto const head_half_height   = head_bbox.scaled_half_widths(head_tr).y;

  head_tr.translation = player_tr.translation;
  head_tr.translation.y += (player_half_height - head_half_height);
}

PlayerHead
PlayerHead::create(log_t& LOGGER, enttreg_t& reg, WorldOrientation const& world_orientation)
{
  // construct the head
  auto const eid = reg.create();
  reg.emplace<IsRenderable>(eid);
  reg.emplace<Name>(eid, "PlayerHead");

  AABoundingBox::add_to_entity(LOGGER, reg, eid, -mc::ONE, mc::ONE);

  PlayerHead ph{reg, eid, world_orientation};
  auto&      tr = ph.world_object.transform();
  tr.scale      = glm::vec3{0.1f};
  return ph;
}

static duration_t constexpr HOW_OFTEN_GCD_RESETS = seconds_duration{1};

////////////////////////////////////////////////////////////////////////////////////////////////////
// Player
Player::Player(eid_t const eid, enttreg_t& r, WorldOrientation const& world_orientation,
               PlayerHead&& head)
    : eid_(eid)
    , reg_(&r)
    , wo_(eid, r, world_orientation)
    , head_(MOVE(head))
{
}

void
Player::pickup_entity(log_t& LOGGER, eid_t const eid, enttreg_t& reg)
{
  assert(inventory.add_item(eid));
  auto& item       = reg.get<Item>(eid);
  item.is_pickedup = true;

  reg.get<IsRenderable>(eid).hidden = true;

  if (reg.try_get<PointLight>(eid)) {
    shader3d::decrement_num_pointlights(LOGGER, reg).expect("decrement_num_pointlights");
  }

  if (reg.try_get<Torch>(eid)) {
    LOG_INFO("You have picked up a torch.");
  }
  else {
    LOG_INFO("You have picked up an item.");
  }

  // Add ourselves to this list of the item's previous owners.
  item.add_owner(this->name);
}

void
Player::drop_entity(log_t& LOGGER, eid_t const eid, enttreg_t& reg)
{
  auto& player = find_player(reg);

  auto& item       = reg.get<Item>(eid);
  item.is_pickedup = false;

  // Move the dropped item to the player's position
  auto const& player_pos = player.world_object().world_position();

  auto& transform       = reg.get<Transform>(eid);
  transform.translation = player_pos;

  if (reg.try_get<Torch>(eid)) {
    LOG_INFO("You have droppped a torch.");
  }

  if (reg.try_get<PointLight>(eid)) {
    shader3d::increment_num_pointlights(LOGGER, reg).expect("increment_num_pointlights");
  }

  reg.get<IsRenderable>(eid).hidden = false;
}

void
Player::update(log_t& LOGGER, GameState& gs, DeltaTime const& dt)
{
  auto& ldata = gs.ldata;
  auto& reg   = ldata.registry;
  auto& nbt   = ldata.nearby_targets;

  update_position(LOGGER, gs, *this, dt);
  head_.update(dt);

  // If no target is selected, no more work to do.
  auto const target_opt = nbt.selected();
  if (!target_opt) {
    return;
  }

  bool const gcd_ready          = gcd_.is_expired();
  auto const reset_gcd_if_ready = [&]() {
    if (gcd_ready) {
      gcd_.set_duration(HOW_OFTEN_GCD_RESETS);
    }
  };
  ON_SCOPE_EXIT(reset_gcd_if_ready);

  // Assumed nearby-target selected
  auto const  target_eid = *target_opt;
  auto const& target     = reg.get<NPCData>(target_eid);

  if (is_attacking() && NPC::is_dead(target.health)) {
    toggle_attacking();
    LOG_ERROR("NOT ATTACKING CAUSE TARGET DEAD");
    return;
  }

  if (is_attacking() && gcd_ready) {
    LOG_ERROR("GCD_READY IS_ATTACKING: %i, GCD_READY: %i", is_attacking(), gcd_ready);

    auto& npcdata   = reg.get<NPCData>(target_eid);
    auto& target_hp = npcdata.health;

    bool const already_dead = NPC::is_dead(target_hp);
    try_attack_selected_target(LOGGER, reg, *this, target_eid);

    bool const target_dead_after_attack = NPC::is_dead(target_hp);
    bool const dead_from_attack         = !already_dead && target_dead_after_attack;

    if (dead_from_attack) {
      kill_entity(reg, target_eid);
      LOG_ERROR("KILLING TARGET");
    }
    else if (already_dead) {
      LOG_ERROR("TARGET ALREADY DEAD");
    }
  }
}

void
Player::toggle_attacking()
{
  is_attacking_ ^= true;
  if (is_attacking()) {
    gcd_.set_duration(HOW_OFTEN_GCD_RESETS);
  }
}

void
Player::try_pickup_nearby_item(log_t& LOGGER, enttreg_t& reg)
{
  auto const& player_pos = transform().translation;

  static constexpr auto MINIMUM_DISTANCE_TO_PICKUP = 1.0f;
  auto const            items                      = find_items(reg);
  for (eid_t const eid : items) {
    Item& item = reg.get<Item>(eid);
    if (item.is_pickedup) {
      LOG_INFO("item already picked up.\n");
      continue;
    }

    auto&       item_transform = reg.get<Transform>(eid);
    auto const& item_pos       = item_transform.translation;
    auto const  distance       = glm::distance(item_pos, player_pos);

    if (distance > MINIMUM_DISTANCE_TO_PICKUP) {
      LOG_INFO("There is nothing nearby to pickup.");
      continue;
    }

    pickup_entity(LOGGER, eid, reg);
  }
}

glm::vec3
Player::world_position() const
{
  return transform().translation;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
eid_t
find_player_eid(enttreg_t& reg)
{
  // for now assume only 1 entity has the Player tag
  assert(1 == reg.view<Player>().size());

  std::optional<eid_t> peid;
  for (auto const eid : reg.view<Player>()) {
    // This assert ensures this loop only runs once.
    assert(!peid);
    peid = eid;
  }
  assert(peid);
  return *peid;
}

Player&
find_player(enttreg_t& reg)
{
  auto const peid = find_player_eid(reg);
  return reg.get<Player>(peid);
}
