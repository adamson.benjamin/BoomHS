#pragma once
#include <extlibs/sdl.hpp>

struct log_t;
struct Camera;
class DeltaTime;
struct Devices;
struct Frustum;
struct GameState;
class SDLControllers;

struct KeyEvent;
struct MouseButtonEvent;
struct MouseMotionEvent;
struct MouseWheelEvent;

struct MouseAndKeyboardArgs
{
  GameState&     game_state;
  Devices&       devices;
  Camera&        camera;
  Frustum const& frustum;
};

struct ControllerArgs
{
  GameState&            game_state;
  SDLControllers const& controllers;
  Camera&               camera;
};

struct SDLReadDevicesArgs
{
  GameState&     game_state;
  Devices&       devices;
  Camera&        camera;
  Frustum const& frustum;
};

struct SDLEventProcessArgs
{
  SDL_Event&     event;
  GameState&     game_state;
  Devices&       devices;
  Camera&        camera;
  Frustum const& frustum;
};

struct IO_SDL
{
  static void read_devices(log_t&, SDLReadDevicesArgs&&, DeltaTime const&);
};
