#pragma once
#include <boomhs/color.hpp>
#include <boomhs/entity.hpp>
#include <boomhs/lighting.hpp>

#include <cassert>
#include <string>

struct Cube;

namespace opengl
{
class texture_h;
} // namespace opengl

// Attached to other entities to keep two entities the same relative distance from one another over
// time.
//
// This component is used to automatically update an entity (passed into the constructor).
struct FollowTransform
{
  eid_t     target_eid;
  glm::vec3 target_offset;

  explicit FollowTransform(eid_t);
};

struct HealthPoints
{
  int current, max;
};

struct WidthHeightLength
{
  float const width;
  float const height;
  float const length;
};

struct OrbitalBody
{
  glm::vec3 radius;
  float     offset = 0.0f;

  explicit OrbitalBody(glm::vec3 const&, float);
};

struct SunGlow final : public color3
{
};

struct Name
{
  std::string value;

  explicit Name(std::string_view const& v)
      : value(std::string{v})
  {
  }

  bool empty() const { return value.empty(); }
};

inline bool
operator==(Name const& a, Name const& b)
{
  return a.value == b.value;
}

struct ShaderComponent
{
  std::string value;

  explicit ShaderComponent(std::string_view const& v)
      : value(std::string{v})
  {
  }
};

struct Selectable
{
  bool selected = false;
};

// Dictates whether an Entity will be considered for rendering or not.
//
// A Entity with this component attached will be considered for rendering by the EntityRenderer.
//
// If the field "hidden" is set to true, the Entity will not be considered for rendering any
// further.
struct IsRenderable
{
  bool hidden = false;

  IsRenderable() = default;
  explicit IsRenderable(bool const h)
      : hidden(h)
  {
  }
};

struct Torch
{
  Attenuation default_attenuation{1.0f, 0.93f, 0.46f};
};

struct LightFlicker
{
  float base_speed    = 0.0f;
  float current_speed = 0.0f;

  std::array<color3, 2> colors = {{LOC3::NO_ALPHA, LOC3::NO_ALPHA}};
};

struct JunkEntityFromFILE
{
};

struct CubeRenderable
{
  glm::vec3 min, max;
};

struct MeshRenderable
{
  std::string name;

  explicit MeshRenderable(std::string_view const& v)
      : name(std::string{v})
  {
  }
};

struct TextureRenderable
{
  opengl::texture_h* texture_info = nullptr;
  RectFloat          uvs;
};

inline auto
find_orbital_bodies(enttreg_t& registry)
{
  return find_all_entities_with_component<OrbitalBody>(registry);
}

// combined components
struct CubeTransform
{
  Cube const&      cube;
  Transform const& transform;
};

struct RectTransform
{
  RectFloat const&   rect;
  Transform2D const& transform;
};
