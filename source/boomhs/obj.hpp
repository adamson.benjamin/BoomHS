#pragma once
#include <common/FOR.hpp>
#include <common/macros_class.hpp>
#include <common/result.hpp>

#include <extlibs/glew.hpp>
#include <extlibs/glm.hpp>
#include <extlibs/tinyobj.hpp>

#include <filesystem>
#include <ostream>
#include <string>

struct log_t;
using ObjVertices = std::vector<float>;
using ObjIndices  = std::vector<uint32_t>;

struct PositionsBuffer
{
  ObjVertices vertices;

  PositionsBuffer(ObjVertices&&);

  glm::vec3 min() const;
  glm::vec3 max() const;
};

class ObjData
{
  COPY_DEFAULT(ObjData);

public:
  unsigned int num_vertexes;
  ObjVertices  vertices;
  ObjVertices  colors;
  ObjVertices  normals;
  ObjVertices  uvs;
  ObjIndices   indices;

  std::vector<tinyobj::shape_t>    shapes;
  std::vector<tinyobj::material_t> materials;

  ObjData() = default;
  MOVE_DEFAULT(ObjData);

  ObjData clone() const;

  // Returns all position values as a contiguos array following the pattern:
  // [x, y, z], [x, y, z], etc...
  PositionsBuffer positions() const;

  std::string to_string() const;

#define FOREACH_FACE_IMPL(fn)                                                                      \
  FOR(s, shapes.size())                                                                            \
  {                                                                                                \
    auto const& shape = shapes[s];                                                                 \
    auto const& mesh  = shape.mesh;                                                                \
    FOR(f, mesh.num_face_vertices.size()) { fn(shape, f); }                                        \
  }

  template <typename FN>
  void foreach_face(FN const& fn)
  {
    FOREACH_FACE_IMPL(fn)
  }

  template <typename FN>
  void foreach_face(FN const& fn) const
  {
    FOREACH_FACE_IMPL(fn)
  }
#undef FOREACH_FACE_IMPL
};

enum class LoadStatus
{
  FILE_NOT_FOUND = 0,
  MISSING_POSITION_ATTRIBUTES,
  MISSING_COLOR_ATTRIBUTES,
  MISSING_NORMAL_ATTRIBUTES,
  MISSING_UV_ATTRIBUTES,

  TINYOBJ_ERROR,

  SUCCESS
};

std::string
loadstatus_to_string(LoadStatus const ls);

std::ostream&
operator<<(std::ostream&, LoadStatus const&);

using LoadResult = Result<ObjData, LoadStatus>;

LoadResult
load_objfile(log_t&, std::filesystem::path const&);
