#include "mouse.hpp"
#include <common/algorithm.hpp>
#include <gl_sdl/global.hpp>

////////////////////////////////////////////////////////////////////////////////////////////////////
// CursorManager
CursorManager::CursorManager()
{
  for (size_t i = CURSOR_INDEX_BEGIN; i < CURSOR_INDEX_END; ++i) {
    auto const ci = static_cast<SDL_SystemCursor>(i);
    cursors[i]    = SDL_CreateSystemCursor(ci);
    assert(nullptr != cursors[i]);
  }
}

CursorManager::~CursorManager()
{
  std::for_each(cursors.cbegin(), cursors.cend(), &SDL_FreeCursor);
}

bool
CursorManager::contains(SDL_SystemCursor const id) const
{
  return id >= CURSOR_INDEX_BEGIN && id < CURSOR_INDEX_END;
}

void
CursorManager::set_active(SDL_SystemCursor const id)
{
  active_ = id;

  assert(contains(active_));
  SDL_SetCursor(cursors[active_]);
}

SDL_Cursor*
CursorManager::active() const
{
  return cursors[active_];
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// Mouse
glm::ivec2
Mouse::coords()
{
  return gl_sdl::mouse_coords();
}

bool
Mouse::left_pressed()
{
  return mask() & SDL_BUTTON(SDL_BUTTON_LEFT);
}

bool
Mouse::right_pressed()
{
  return mask() & SDL_BUTTON(SDL_BUTTON_RIGHT);
}

bool
Mouse::middle_pressed()
{
  return mask() & SDL_BUTTON(SDL_BUTTON_MIDDLE);
}

glm::ivec2
Mouse::rel_pos()
{
  glm::ivec2 pos;
  SDL_GetRelativeMouseState(&pos.x, &pos.y);
  return pos;
}

glm::ivec2
Mouse::rel_since_last_left_click()
{
  return coords() - click_position.left_right;
}

bool
Mouse::both_pressed()
{
  return left_pressed() && right_pressed();
}

bool
Mouse::either_pressed()
{
  return left_pressed() || right_pressed();
}
