#include "transform.hpp"

#include <math/constants.hpp>
#include <math/matrices.hpp>
#include <math/space_conversions.hpp>

////////////////////////////////////////////////////////////////////////////////////////////////////
// Transform2D
Transform2D::Transform2D(glm::vec2 const& pos)
    : translation(pos)
    , scale(math::constants::ONE)
{
}

Transform2D::Transform2D(float const x, float const y)
    : Transform2D(glm::vec2{x, y})
{
}

Transform2D::Transform2D()
    : Transform2D(math::constants::ZERO)
{
}

glm::mat4
Transform2D::model_matrix() const
{
  auto const& t = translation;
  auto const& r = rotation;
  auto const& s = scale;
  return math::model_matrix_2d(t, r, s);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// Transform
Transform::Transform(glm::vec3 const& tr)
    : translation(tr)
    , scale(math::constants::ONE)
{
}
Transform::Transform(float const x, float const y, float const z)
    : Transform(glm::vec3{x, y, z})
{
}

Transform::Transform()
    : Transform(math::constants::ZERO)
{
}

glm::mat4
Transform::model_matrix() const
{
  auto const& t = translation;
  auto const& r = rotation;
  auto const& s = scale;
  return math::model_matrix_3d(t, r, s);
}

namespace transform
{

Transform2D
from_3d_to_2d(Transform const& tr3d)
{
  Transform2D tr2d;
  tr2d.rotation = tr3d.rotation;
  {
    auto const& scale = tr3d.scale;
    tr2d.scale        = glm::vec2{scale.x, scale.y};
  }
  {
    auto const& pos_3d = tr3d.translation;
    auto&       pos_2d = tr2d.translation;

    pos_2d.x = pos_3d.x;
    pos_2d.y = pos_3d.y;
  }
  return tr2d;
}

} // namespace transform
