#pragma once
#include <boomhs/bounding_object.hpp>
#include <boomhs/entity.hpp>
#include <boomhs/inventory.hpp>
#include <boomhs/world_object.hpp>

#include <common/timer.hpp>

#include <extlibs/glm.hpp>

#include <optional>
#include <string>

namespace opengl
{
class texture_storage;
} // namespace opengl

class DeltaTime;
struct GameState;
class LevelData;
struct log_t;

class PlayerHead
{
  enttreg_t* reg_;
  eid_t      eid_;

  friend class Player;

public:
  explicit PlayerHead(enttreg_t&, eid_t, WorldOrientation const&);
  void update(DeltaTime const&);

  // fields
  WorldObject world_object;
  auto        eid() const { return eid_; }

  static PlayerHead create(log_t&, enttreg_t&, WorldOrientation const&);
};

class Player
{
  eid_t      eid_;
  enttreg_t* reg_;

  WorldObject   wo_;
  common::Timer gcd_;
  PlayerHead    head_;

  bool is_attacking_ = false;

public:
  NO_COPY(Player);
  MOVE_DEFAULT(Player);
  explicit Player(eid_t, enttreg_t&, WorldOrientation const&, PlayerHead&&);

  Inventory    inventory;
  HealthPoints hp{44, 50};
  int          level = -1;
  std::string  name;

  int   damage = 1;
  float speed;

  void pickup_entity(log_t&, eid_t, enttreg_t&);
  void drop_entity(log_t&, eid_t, enttreg_t&);

  bool is_attacking() const { return is_attacking_; }
  void toggle_attacking();
  void try_pickup_nearby_item(log_t&, enttreg_t&);

  void update(log_t&, GameState&, DeltaTime const&);

  auto const& transform() const { return reg_->get<Transform>(eid_); }
  Transform&  transform() { return reg_->get<Transform>(eid_); }

  auto const& bounding_box() const { return reg_->get<AABoundingBox>(eid_); }

  glm::vec3 world_position() const;

  WorldObject&       head_world_object() { return head_.world_object; }
  WorldObject const& head_world_object() const { return head_.world_object; }

  WorldObject&       world_object() { return wo_; }
  WorldObject const& world_object() const { return wo_; }
};

eid_t
find_player_eid(enttreg_t&);

Player&
find_player(enttreg_t&);
