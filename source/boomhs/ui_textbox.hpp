#pragma once
#include <boomhs/event.hpp>
#include <boomhs/font.hpp>

#include <math/algorithm.hpp>
#include <math/rectangle.hpp>

#include <common/macros_class.hpp>
#include <common/macros_container.hpp>
#include <common/non_const.hpp>
#include <common/result.hpp>

#include <string>

struct log_t;
class ChatHistory;
struct Frustum;
class Mouse;

namespace opengl
{
struct font_and_texture;
} // namespace opengl

enum class ScrollDirection
{
  UP = 0,
  DOWN
};

struct ScrollBarBumpers final
{
  RectFloat top;
  RectFloat bottom;
};

struct ScrollBar final
{
  bool visible = false;

  RectFloat        scroll_area;
  ScrollBarBumpers bumpers;
  RectFloat        slider_rect;

  float current = 0;
  float max     = 0;

  auto scroll_percentage() const { return max / 10.0f; }
  void try_scroll(ScrollDirection, float = 1);
};

using MessageContents = std::string;
struct TextBoxMessage
{
  MessageContents      contents;
  LetterRectCollection letter_rects = {};
};

enum class TextBoxType
{
  Dialog,
  ChatWindow
};

class TextBoxMessages
{
  std::vector<TextBoxMessage> data_;

private:
  DEFINE_VECTOR_LIKE_WRAPPER_FNS(data_)

public:
  TextBoxMessages() = default;
  MOVE_CONSTRUCTIBLE_ONLY(TextBoxMessages);

public:
  TextBoxMessage& insert_back(TextBoxMessage&&);
  TextBoxMessage& insert_back(std::string&&);

  TextBoxMessage& insert_front(std::string&&);

  std::vector<TextBoxMessage> const& all_messages() const { return data_; }
  NON_CONST(all_messages)
};

using ComputeSizeFN = RectFloat (*)(Frustum const&);
class TextBox
{
  TextBoxType const type_;
  FontInfo const    finfo_;

protected:
  RectFloat rect_;
  ScrollBar scrollbar_;

  ComputeSizeFN resize_fn_ = nullptr;

public:
  TextBox(log_t&, TextBoxType, ComputeSizeFN, opengl::font_and_texture const&, Frustum const&);
  virtual ~TextBox() = default;

public:
  auto const& font_info() const { return finfo_; }
  auto&       rect() { return rect_; }
  auto&       scrollbar() { return scrollbar_; }
  auto const& scrollbar() const { return scrollbar_; }

  RectFloat overlay_rect() const;
  RectFloat text_rect() const;
  auto      type() const { return type_; }

public:
  virtual Result<NOTHING, std::string> resize(log_t&, Frustum const&);
  virtual void                         update(log_t&, Mouse const&);
};

class DialogTextBox : public TextBox
{
  TextBoxMessages history_;

private:
  static void textbox_on_key_down(log_t&, DialogTextBox&, KeyEvent&&, DeltaTime const&);

public:
  DialogTextBox(log_t&, ComputeSizeFN, opengl::font_and_texture const&, Frustum const&);

public:
  Result<NOTHING, std::string> push_message_back(log_t&, std::string&&);
  Result<NOTHING, std::string> push_message_front(log_t&, std::string&&);

public:
  TextBoxMessages const& history() const { return history_; }
  NON_CONST(history)

public:
  virtual Result<NOTHING, std::string> resize(log_t&, Frustum const&) override;
  virtual void                         update(log_t&, Mouse const&) override;

public:
  OnKeyDownFN<DialogTextBox> on_key_down = textbox_on_key_down;
};

namespace textbox
{
// TODO: detail
template <typename T>
auto
compute_largest_height(T const& lom)
{
  auto largest = 0.0f;
  for (auto const& msg : lom) {
    auto const h = font::compute_largest_height(msg.letter_rects);
    largest      = std::max(largest, h);
  }
  return largest;
}

// TODO: detail
template <typename T>
auto
compute_howmany_lines_could_fit_inside(T const& lom, RectFloat const& textbox)
{
  auto const line_height = compute_largest_height(lom);
  if (math::float_cmp(0.0f, line_height)) {
    return 0;
  }

  assert(line_height >= 0.0f);
  auto const div       = textbox.height() / line_height;
  auto const num_lines = math::float_cmp(div, 0) ? 0 : div - 1;
  return static_cast<int>(num_lines);
}

// TODO: detail
template <typename T>
auto
compute_howmany_lines_required_for(T const& lom, RectFloat const& textbox)
{
  auto accum = 0;
  for (auto const& msg : lom) {
    accum += font::compute_howmany_lines_required_for(msg.letter_rects, textbox);
  }
  return accum;
}

void
recompute_scroll_area(TextBox&, int, int);

// TODO: detail
template <typename T>
void
recompute_scroll_area(T& tbox)
{
  auto const& lom = tbox.history().all_messages();

  auto const trect              = tbox.text_rect();
  auto       num_lines_required = compute_howmany_lines_required_for(lom, trect);
  auto const num_lines_inside   = compute_howmany_lines_could_fit_inside(lom, trect);
  return recompute_scroll_area(tbox, num_lines_required, num_lines_inside);
}

// TODO: detail
inline Result<NOTHING, std::string>
compute_msg_info(FontInfo const& finfo, glm::ivec2 const& dinfo, RectFloat const& rect,
                 TextBoxMessage& msg)
{
  auto const& fsize = finfo.font_size;
  auto const& text  = msg.contents;
  msg.letter_rects  = TRY(font::compute_letter_rectangles_bounded(finfo, rect, text, fsize, dinfo));
  return OK_NONE;
}

// TODO: detail
template <typename T, typename M>
Result<NOTHING, std::string>
compute_new_message(log_t& /*LOGGER*/, glm::ivec2 const& dinfo, T& tbox, M& msg)
{
  {
    auto const& finfo    = tbox.font_info();
    auto const  textrect = tbox.text_rect();
    TRY(textbox::compute_msg_info(finfo, dinfo, textrect, msg));
  }

  auto&      sb             = tbox.scrollbar();
  bool const current_at_max = math::float_cmp(sb.current, sb.max);
  recompute_scroll_area(tbox);
  if (current_at_max) {
    sb.current = sb.max;
  }
  return OK_NONE;
}

// TODO: detail
template <typename T>
Result<NOTHING, std::string>
recompute_message_sizes(log_t& /*LOGGER*/, T& tbox, glm::ivec2 const& dinfo)
{
  auto const  text_rect = tbox.text_rect();
  auto const& finfo     = tbox.font_info();
  for (auto& msg : tbox.history().all_messages()) {
    TRY(textbox::compute_msg_info(finfo, dinfo, text_rect, msg));
  }
  return OK_NONE;
}

// TODO: detail
template <typename T>
bool
should_show(T const& tbox)
{
  auto const& history = tbox.history();
  auto const& lom     = history.all_messages();
  auto const& cwrect  = tbox.text_rect();

  auto const actual   = textbox::compute_howmany_lines_required_for(lom, cwrect);
  auto const possible = textbox::compute_howmany_lines_could_fit_inside(lom, cwrect);
  return actual > possible;
}

} // namespace textbox
