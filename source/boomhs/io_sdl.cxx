#include "io_sdl.hpp"
#include <boomhs/camera.hpp>
#include <boomhs/camera_algorithm.hpp>
#include <boomhs/collision.hpp>
#include <boomhs/controller.hpp>
#include <boomhs/engine.hpp>
#include <boomhs/movement.hpp>
#include <boomhs/npc.hpp>
#include <boomhs/player.hpp>
#include <boomhs/state.hpp>
#include <boomhs/world_object.hpp>

#include <gl_sdl/sdl_window.hpp>

#include <math/constants.hpp>

#include <common/log.hpp>

#include <extlibs/sdl.hpp>
#include <iostream>

namespace io_internal
{
void
process_mouse_state(log_t&, MouseAndKeyboardArgs&& mk, DeltaTime const& dt)
{
  auto& gs     = mk.game_state;
  auto& ds     = mk.devices;
  auto& camera = mk.camera;
  auto& player = find_player(gs.ldata.registry).world_object();
  auto& ms_now = ds.mouse;

  auto const mode     = camera.mode;
  auto&      movement = gs.ldata.registry.ctx<MovementState>();
  if (mode == CameraMode::FPS || mode == CameraMode::ThirdPerson) {
    if (ms_now.both_pressed()) {
      if (mode == CameraMode::ThirdPerson) {
        player.rotate_to_match_camera_rotation(camera);
      }
      movement.mouse_forward = player.eye_forward();
    }
    else {
      movement.mouse_forward = mc::ZERO;
    }
  }
  else if (mode == CameraMode::Ortho) {
    if (ms_now.middle_pressed()) {
      auto const click_pos = glm::vec2{ms_now.click_position.left_right};

      auto const coords_now = glm::vec2{ms_now.coords()};
      auto const distance   = static_cast<float>(math::pythag_distance(click_pos, coords_now));

      auto const& fr = mk.frustum;
      auto const  dx = (coords_now - click_pos).x / fr.width_float();
      auto const  dy = (coords_now - click_pos).y / fr.height_float();

      auto constexpr SCROLL_SPEED = 15.0f;
      auto const multiplier       = SCROLL_SPEED * distance * dt();
      camera::scroll(camera.ortho, glm::vec2{dx, dy} * multiplier);
    }
  }
}

void
process_keyboard_state(log_t&, MouseAndKeyboardArgs&& mk, DeltaTime const&)
{
  auto& gs     = mk.game_state;
  auto& player = find_player(gs.ldata.registry).world_object();

  // continual keypress responses procesed here
  uint8_t const* keystate = SDL_GetKeyboardState(nullptr);
  assert(keystate);

  auto& movement    = gs.ldata.registry.ctx<MovementState>();
  movement.forward  = keystate[SDL_SCANCODE_W] ? player.eye_forward() : mc::ZERO;
  movement.backward = keystate[SDL_SCANCODE_S] ? player.eye_backward() : mc::ZERO;

  movement.left  = keystate[SDL_SCANCODE_A] ? player.eye_left() : mc::ZERO;
  movement.right = keystate[SDL_SCANCODE_D] ? player.eye_right() : mc::ZERO;
}

void
process_controller_state(log_t& LOGGER, ControllerArgs&& ca, DeltaTime const& dt)
{
  auto&       gs          = ca.game_state;
  auto const& controllers = ca.controllers;

  if (controllers.empty()) {
    return;
  }

  auto& ldata    = gs.ldata;
  auto& registry = ldata.registry;

  auto& c = controllers.first();

  SDL_Joystick* joystick = c.joystick;
  assert(joystick);

  // auto const read_axis = [&c](auto const axis) {
  // return SDL_GameControllerGetAxis(c.controller.get(), axis);
  //};

  // https://wiki.libsdl.org/SDL_GameControllerGetAxis
  //
  // using 32bit ints to be sure no overflow (maybe unnecessary?)
  int32_t constexpr AXIS_MIN = -32768;
  int32_t constexpr AXIS_MAX = 32767;

  auto constexpr THRESHOLD  = 0.4f;
  auto const less_threshold = [&](auto const& v) { return v <= 0 && (v <= AXIS_MIN * THRESHOLD); };
  auto const greater_threshold = [](auto const& v) {
    return v >= 0 && (v >= AXIS_MAX * THRESHOLD);
  };

  auto&      camera      = ca.camera;
  auto&      player      = find_player(registry);
  auto&      player_wo   = player.world_object();
  auto const axis_left_x = c.axis_left_x();
  auto&      movement    = registry.ctx<MovementState>();

  if (less_threshold(axis_left_x)) {
    movement.left = player_wo.eye_left();
    player_wo.rotate_to_match_camera_rotation(camera);
  }
  else {
    movement.left = mc::ZERO;
  }
  if (greater_threshold(axis_left_x)) {
    movement.right = player_wo.eye_right();
    player_wo.rotate_to_match_camera_rotation(camera);
  }
  else {
    movement.right = mc::ZERO;
  }
  auto const axis_left_y = c.axis_left_y();
  if (less_threshold(axis_left_y)) {
    movement.forward = player_wo.eye_forward();
    player_wo.rotate_to_match_camera_rotation(camera);
  }
  else {
    movement.forward = mc::ZERO;
  }
  if (greater_threshold(axis_left_y)) {
    movement.backward = player_wo.eye_backward();
    player_wo.rotate_to_match_camera_rotation(camera);
  }
  else {
    movement.backward = mc::ZERO;
  }
  {
    auto const axis_right_x = c.axis_right_x();
    if (less_threshold(axis_right_x)) {
      LOG_DEBUG("LT");
      camera::rotate_radians(camera, axis_right_x, 0.0, dt);
      player_wo.rotate_to_match_camera_rotation(camera);
    }
    if (greater_threshold(axis_right_x)) {
      LOG_DEBUG("GT");
      camera::rotate_radians(camera, axis_right_x, 0.0, dt);
      player_wo.rotate_to_match_camera_rotation(camera);
    }
  }
  {
    auto const right_axis_y = c.axis_right_y();
    if (less_threshold(right_axis_y)) {
      camera::rotate_radians(camera, 0.0, right_axis_y, dt);
    }
    if (greater_threshold(right_axis_y)) {
      camera::rotate_radians(camera, 0.0, right_axis_y, dt);
    }
  }
  if (c.button_a()) {
    LOG_DEBUG("BUTTON A\n");
    player.try_pickup_nearby_item(LOGGER, registry);
  }
  if (c.button_b()) {
    LOG_DEBUG("BUTTON B\n");
  }
  if (c.button_x()) {
    LOG_DEBUG("BUTTON X\n");
  }
  if (c.button_y()) {
    LOG_DEBUG("BUTTON Y\n");
  }

  if (c.button_back()) {
    LOG_DEBUG("BUTTON BACK\n");
  }
  if (c.button_guide()) {
    LOG_DEBUG("BUTTON GUIDE\n");
  }
  if (c.button_start()) {
    LOG_DEBUG("BUTTON START\n");
  }

  // joystick buttons
  if (c.button_left_joystick()) {
    LOG_DEBUG("BUTTON LEFT JOYSTICK\n");
  }
  if (c.button_right_joystick()) {
    LOG_DEBUG("BUTTON RIGHT JOYSTICK\n");
  }

  // shoulder buttons
  if (c.button_left_shoulder()) {
    LOG_DEBUG("BUTTON LEFT SHOULDER\n");
  }
  if (c.button_right_shoulder()) {
    LOG_DEBUG("BUTTON RIGHT SHOULDER\n");
  }

  // trigger buttons
  if (c.button_left_trigger()) {
    LOG_DEBUG("BUTTON LEFT TRIGGER\n");
  }
  if (c.button_right_trigger()) {
    LOG_DEBUG("BUTTON RIGHT TRIGGER\n");
  }

  // d-pad buttons
  if (c.button_dpad_down()) {
    LOG_DEBUG("BUTTON DPAD DOWN\n");
  }
  if (c.button_dpad_up()) {
    LOG_DEBUG("BUTTON DPAD UP\n");
  }

  auto& nbt = ldata.nearby_targets;
  if (c.button_dpad_left()) {
    LOG_DEBUG("BUTTON DPAD LEFT\n");
    nbt.cycle_backward(dt);
  }
  if (c.button_dpad_right()) {
    LOG_DEBUG("BUTTON DPAD RIGHT\n");
    nbt.cycle_forward(dt);
  }
}

} // namespace io_internal

void
IO_SDL::read_devices(log_t& LOGGER, SDLReadDevicesArgs&& rda, DeltaTime const& dt)
{
  auto&       gs     = rda.game_state;
  auto&       ds     = rda.devices;
  auto&       camera = rda.camera;
  auto const& fr     = rda.frustum;

  {
    auto const maka = [&]() { return MouseAndKeyboardArgs{gs, ds, camera, fr}; };
    io_internal::process_keyboard_state(LOGGER, maka(), dt);
    io_internal::process_mouse_state(LOGGER, maka(), dt);
  }

  if (!gs.uibuffers_classic.debug.disable_controller_input) {
    // TODO: using controller and keyboard input at the same time does not work.
    // reason: The controller when it's stick's aren't activated, every frame, set's the same
    // variables to the keyboard controller would use to 0, effectively nullifying any input the
    // keyboard can do.
    //
    // Idea: We could use separate vector's for tracking the controller input, if we want to allow
    // both at the same time (why?).
    auto const& controllers = ds.controllers;
    io_internal::process_controller_state(LOGGER, ControllerArgs{gs, controllers, camera}, dt);
  }
}
