#include "stacktrace.hpp"

#include <common/breakpoint.hpp>
#include <common/log.hpp>
#include <extlibs/boost_stacktrace.hpp>

#include <fstream>
#include <filesystem>

namespace fs = std::filesystem;
namespace
{
constexpr auto BACKTRACE_FILE = "./backtrace.dump";
} // namespace

namespace stacktrace
{
void
dump_to_file(log_t& LOGGER)
{
  auto const st = boost::stacktrace::stacktrace();
  for (auto const& frame : st) {
    LOG_ERROR("%p %s:%lu '%s'", frame.address(), frame.source_file(), frame.source_line(),
              frame.name());
  }

  boost::stacktrace::safe_dump_to(BACKTRACE_FILE);

  using perms = fs::perms;
  std::error_code ec;
  fs::permissions(BACKTRACE_FILE, perms::owner_read | perms::owner_write, ec);
  assert(!ec);

  common::cpu_breakpoint();
}

void
check_on_disk(log_t& LOGGER)
{
  if (!fs::exists(BACKTRACE_FILE)) {
    LOG_INFO("No backtrace file found.");
    return;
  }
  std::ifstream ifs{BACKTRACE_FILE};

  boost::stacktrace::stacktrace const st = boost::stacktrace::stacktrace::from_dump(ifs);
  LOG_INFO("Previous run crashed:\n%s", st);

  LOG_INFO("DELETING BT FILE: %s ...", BACKTRACE_FILE);
  {
    std::error_code ec;
    fs::remove(BACKTRACE_FILE, ec);
    assert(!ec);
  }
  LOG_INFO("FINISHED DELETING FILE!");
  std::exit(EXIT_FAILURE);
}
} // namespace stacktrace
