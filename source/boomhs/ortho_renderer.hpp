#pragma once

namespace opengl
{
struct DrawState;
struct render_state;
} // namespace opengl

struct Camera;
class DeltaTime;
struct GameState;
struct log_t;
struct StaticRenderers;

namespace ortho
{
void
draw_scene(log_t&, GameState&, opengl::render_state&, Camera const&, StaticRenderers&,
           DeltaTime const&);

} // namespace ortho
