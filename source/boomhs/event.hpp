#pragma once
#include <extlibs/sdl.hpp>

struct log_t;
struct Camera;
class DeltaTime;
struct GameState;
struct Devices;
struct Frustum;

struct MouseButtonEvent
{
  GameState&            game_state;
  Devices&              devices;
  Camera&               camera;
  Frustum const&        frustum;
  SDL_MouseButtonEvent& event;
};

struct MouseMotionEvent
{
  GameState&                  game_state;
  Devices&                    devices;
  Camera&                     camera;
  SDL_MouseMotionEvent const& motion;
};

struct MouseWheelEvent
{
  GameState&                 game_state;
  Camera&                    camera;
  SDL_MouseWheelEvent const& wheel;
};

struct KeyEvent
{
  GameState&       game_state;
  Devices&         devices;
  Camera&          camera;
  Frustum const&   fr;
  SDL_Event const& event;
};

namespace event_handler
{
template <typename... Args>
void
noop(Args&&...)
{
}

} // namespace event_handler

template <typename T>
using OnKeyDownFN = void (*)(log_t&, T&, KeyEvent&&, DeltaTime const&);

template <typename T>
using OnKeyUpFN = OnKeyDownFN<T>;

template <typename T>
using OnMouseButtonDownFN = void (*)(log_t&, T&, MouseButtonEvent&&, DeltaTime const&);

template <typename T>
using OnMouseButtonUpFN = OnMouseButtonDownFN<T>;

template <typename T>
using OnMouseMotionFN = void (*)(log_t&, T&, MouseMotionEvent&&, DeltaTime const&);

template <typename T>
using OnMouseWheelFN = void (*)(log_t&, T&, MouseWheelEvent&&, DeltaTime const&);