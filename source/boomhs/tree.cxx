#include "tree.hpp"
#include <boomhs/components.hpp>
#include <boomhs/entity.hpp>
#include <boomhs/obj_store.hpp>

#include <opengl/buffer.hpp>
#include <opengl/gpu.hpp>
#include <opengl/shader.hpp>

#include <common/FOR.hpp>

using namespace opengl;

namespace
{
template <typename FN>
std::vector<float>
generate_tree_colors(log_t&, ObjData const& objdata, FN const& face_to_colormap)
{
  std::vector<float> colors;
  auto const         update_branchcolors = [&](auto const& shape, size_t const face) {
    int const face_materialid = shape.mesh.material_ids[face];
    assert(face_materialid >= 0);
    auto const fmid = static_cast<size_t>(face_materialid);
    assert(fmid < face_to_colormap.size());
    auto const& face_color = *face_to_colormap[fmid];
    auto const  fv         = shape.mesh.num_face_vertices[face];
    FOR(vi, fv)
    {
      colors.emplace_back(color::red(face_color));
      colors.emplace_back(color::green(face_color));
      colors.emplace_back(color::blue(face_color));
      colors.emplace_back(color::alpha(face_color));
    }
  };
  objdata.foreach_face(update_branchcolors);
  assert((objdata.colors.size() % 4) == 0);
  assert(objdata.colors.size() == colors.size());
  return colors;
}

std::vector<float>
generate_tree_colors(log_t& logger, TreeComponent const& tree)
{
  std::vector<color4 const*> face_to_colormap;

  FOR(i, tree.num_leaves()) { face_to_colormap.emplace_back(&tree.leaf_color(i)); }
  FOR(i, tree.num_stems()) { face_to_colormap.emplace_back(&tree.stem_color(i)); }
  FOR(i, tree.num_trunks()) { face_to_colormap.emplace_back(&tree.trunk_color(i)); }
  return generate_tree_colors(logger, tree.obj(), face_to_colormap);
}

constexpr size_t
leaves_offset(TreeComponent const&)
{
  // Leaves are stored at the beginning
  return 0;
}

constexpr auto
stem_offset(TreeComponent const& tc)
{
  // Stems come after the leaves.
  return tc.num_leaves();
}

auto
trunk_offset(TreeComponent const& tc)
{
  // Trunks come after the leaves and stems.
  return tc.num_leaves() + tc.num_stems();
}

} // namespace

TreeComponent::TreeComponent(ObjData& data)
    : pobj_{&data}
{
}

ObjData&
TreeComponent::obj()
{
  assert(nullptr != pobj_);
  return *pobj_;
}

ObjData const&
TreeComponent::obj() const
{
  assert(nullptr != pobj_);
  return *pobj_;
}

void
TreeComponent::add_color(Treecolor4Type const type, color4 const& color)
{
  {
    auto const num_colors = num_trunks_ + num_stems_ + num_leaves_;
    assert((num_colors + 1) <= TreeComponent::MAX_NUMBER_TREE_COLORS);
  }
  size_t offset = 0;
  switch (type) {
  case Treecolor4Type::Trunk:
    offset = trunk_offset(*this) + num_trunks_;
    ++num_trunks_;
    break;
  case Treecolor4Type::Stem:
    offset = stem_offset(*this) + num_stems_;
    ++num_stems_;
    break;
  case Treecolor4Type::Leaf:
    offset = leaves_offset(*this) + num_leaves_;
    ++num_leaves_;
    break;
  }
  assert(offset < colors.size());
  colors[offset] = color;
}

color4 const&
TreeComponent::color_from_index(size_t const index, size_t const internal_max,
                                size_t const offset) const
{
  assert(index < internal_max);
  return colors[offset + index];
}

color4 const&
TreeComponent::trunk_color(size_t const index) const
{
  return color_from_index(index, num_trunks(), trunk_offset(*this));
}

color4 const&
TreeComponent::stem_color(size_t const index) const
{
  return color_from_index(index, num_stems(), stem_offset(*this));
}

color4 const&
TreeComponent::leaf_color(size_t const index) const
{
  return color_from_index(index, num_leaves(), leaves_offset(*this));
}

void
Tree::update_colors(log_t& logger, vertex_attribute const& va, draw_state const& dinfo,
                    TreeComponent& tree)
{
  auto& objdata  = tree.obj();
  objdata.colors = generate_tree_colors(logger, tree);
  gpu::overwrite_vertex_buffer(logger, va, dinfo, objdata);
}
