#pragma once
#include <boomhs/entity.hpp>
#include <boomhs/item.hpp>

#include <array>
#include <optional>

namespace opengl
{
class texture_storage;
} // namespace opengl

class InventorySlot
{
  std::optional<eid_t> eid_ = std::nullopt;

  void access_assert() const;

public:
  InventorySlot() = default;

  bool occupied() const;

  Item&       item(enttreg_t&);
  Item const& item(enttreg_t&) const;

  eid_t eid() const;

  char const* name(enttreg_t&) const;

  // Resets the slot's state to empty.
  void reset();

  // Sets the slot to the item.
  void set(eid_t);
};

class Inventory
{
  size_t static constexpr MAX_ITEMS = 40;
  std::array<InventorySlot, MAX_ITEMS> slots_;
  bool                                 open_ = false;

public:
  Inventory()     = default;
  using ItemIndex = size_t;

  // Whether or not the game considers the inventory "open" by the player.
  bool is_open() const;
  void toggle_open();

  // Yields references to the slots containing the Item components.
  InventorySlot& slot(ItemIndex);

  InventorySlot const& slot(ItemIndex) const;

  // Adds an item to the next available slot, returns false if no slots are available.
  //
  // Returns true in all other cases.
  bool add_item(eid_t);

  // Sets the item at the given index.
  void set_item(ItemIndex, eid_t);

  // Remove the item from the slot
  void remove_item(ItemIndex);
};
