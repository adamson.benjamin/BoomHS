#include "main_menu.hpp"

#include <boomhs/collision.hpp>
#include <boomhs/color.hpp>
#include <boomhs/engine.hpp>
#include <boomhs/font.hpp>
#include <boomhs/io_sdl.hpp>
#include <boomhs/state.hpp>
#include <boomhs/ui.hpp>

#include <opengl/bind.hpp>
#include <opengl/font_texture.hpp>
#include <opengl/renderer.hpp>
#include <opengl/texture.hpp>

#include <gl_sdl/global.hpp>
#include <gl_sdl/sdl_window.hpp>

#include <math/matrices.hpp>
#include <math/viewport.hpp>

#include <common/log.hpp>
#include <common/macro_util.hpp>
#include <common/macros_container.hpp>
#include <common/move.hpp>

#include <extlibs/glm.hpp>

#include <algorithm>

using namespace common;
using namespace opengl;

namespace
{
auto
compute_pos(RectFloat const& bounding_rect)
{
  auto const br_left   = bounding_rect.left();
  auto const br_width  = bounding_rect.width();
  auto const br_height = bounding_rect.height();

  auto const space_on_lhs = br_width / 40.0f;
  auto const left         = static_cast<float>(br_left) + space_on_lhs;
  auto const top          = br_height / 40.0f;
  return glm::vec2{left, top};
}

auto
compute_space_between(RectFloat const& bounding_rect)
{
  return bounding_rect.height() / 40.0f;
}

bool
is_mouse_over_any(Screen& screen)
{
  bool is_over_any = false;
  for (auto const& btn : screen.uistate.text_buttons) {
    is_over_any |= btn.mouse_is_over;
    RETURN_VALUE_IF(is_over_any)
  }
  for (auto const& btn : screen.uistate.checkboxes) {
    is_over_any |= btn.mouse_is_over;
    RETURN_VALUE_IF(is_over_any)
  }
  return is_over_any;
}

void
update_timer(MainMenuScreen& screen)
{
  bool const is_over_any = is_mouse_over_any(screen);

  auto& btimer = screen.blink_timer;
  if (is_over_any) {
    if (!btimer.is_enabled()) {
      btimer.set_state_a();
      btimer.turn_on();
    }
    else if (btimer.is_expired()) {
      btimer.toggle();
      btimer.reset();
    }
  }
  else {
    btimer.set_state_b();
    btimer.turn_off();
  }
}

} // namespace

namespace
{
struct BC
{
  char const* text;
  ButtonType  type;
  void (*on_left_click)(GameState&, Button&);
};

Button&
add_tb(TextButton&& btn, Screen& screen)
{
  return screen.uistate.text_buttons.emplace_back(MOVE(btn));
}

Button&
add_tb(CheckboxTextButton&& btn, Screen& screen)
{
  return screen.uistate.checkboxes.emplace_back(MOVE(btn));
}

template <typename T>
Button&
make_tb(MainMenuScreen& screen, BC const& bc)
{
  T btn;
  btn.text      = bc.text;
  btn.text_size = glm::vec2{4};

  btn.on_mouseover = [](GameState&, Button& button) {
    static_cast<T&>(button).text_color = LOC3::DARK_ORANGE;
  };
  btn.on_mouseexit = [](GameState&, Button& button) {
    static_cast<T&>(button).text_color = LOC3::ANTIQUE_WHITE;
  };
  return add_tb(MOVE(btn), screen);
}
} // namespace

void
MainMenuScreen::draw(log_t& LOGGER, GameState& gs, opengl::render_state& rs)
{
  auto& ldata  = gs.ldata;
  auto& ftable = ldata.ftable;
  auto& reg    = ldata.registry;

  render::clear_screen(LOC4::BLACK);

  auto* pft = ftable.lookup(finfo.ffile.name);
  assert(pft);
  auto& font = *pft;
  auto& ti   = font.texture;
  auto& sp   = shader2d::font(reg);

  BIND_UNTIL_END_OF_SCOPE(LOGGER, ti);
  BIND_UNTIL_END_OF_SCOPE(LOGGER, sp);
  ENABLE_ALPHA_BLENDING_UNTIL_SCOPE_EXIT();

  for (auto const& btn : uistate.text_buttons) {
    ui::draw(LOGGER, btn, sp, ti, rs);
  }
  for (auto const& btn : uistate.checkboxes) {
    ui::draw(LOGGER, reg, btn, ldata.ttable, sp, ti, rs);
  }
}

void
MainMenuScreen::update(log_t& LOGGER, GameState& gs, Mouse const& ms)
{
  auto const update_btn = [&](auto& button) {
    ui::on_mouse_over(LOGGER, button, gs, ms);
    if (blink_timer.state_a()) {
      button.text_color = LOC3::LEMON_CHION;
    }
  };

  for (auto& btn : uistate.text_buttons) {
    update_btn(btn);
  }
  for (auto& btn : uistate.checkboxes) {
    update_btn(btn);
  }

  update_timer(*this);
}

namespace mm_internal
{
void
on_key_down(log_t&, MainMenuScreen&, KeyEvent&& event, DeltaTime const&)
{
  auto const key_pressed = event.event.key.keysym.sym;

  auto& gs    = event.game_state;
  auto& stack = gs.sstack;
  switch (key_pressed) {
  case SDLK_F10:
    gs.quit = true;
    return;

  case SDLK_ESCAPE: {
    if (stack.size() > 1) {
      stack.pop();
    }
  } break;
  }
}

void
on_mousebutton_down(log_t& LOGGER, MainMenuScreen&, MouseButtonEvent&& mbe, DeltaTime const&)
{
  LOG_INFO("MOUSE BUTTON DOWN");

  auto&      gs         = mbe.game_state;
  auto const update_btn = [&](auto& button) { ui::on_mouse_click(LOGGER, button, gs); };

  auto& stack   = gs.sstack;
  auto& uistate = static_cast<MainMenuScreen&>(stack.top()).uistate;

  for (auto& btn : uistate.text_buttons) {
    update_btn(btn);
  }
  for (auto& btn : uistate.checkboxes) {
    update_btn(btn);
  }
}

void
on_mousebutton_up(log_t& LOGGER, MainMenuScreen&, MouseButtonEvent&&, DeltaTime const&)
{
  LOG_INFO("MOUSE BUTTON UP");
}

void
draw(log_t& LOGGER, GameState& gs, Camera const& camera, Frustum const& fr, render_state& rs,
     DeltaTime const& /*dt*/)
{
  auto         cm = camera::gui_matrices(camera, fr);
  render_state gui_rs{cm, rs.fr, rs.count};

  auto& screen = static_cast<MainMenuScreen&>(gs.sstack.top());
  screen.draw(LOGGER, gs, gui_rs);
}

Result<NOTHING, std::string>
resize(log_t& LOGGER, GameState&, Screen& screen, Frustum const& fr)
{
  auto const dinfo = gl_sdl::current_display_mode(LOGGER);

  auto& mms     = static_cast<MainMenuScreen&>(screen);
  auto& uistate = screen.uistate;

  auto const total = uistate.text_buttons.size() + uistate.checkboxes.size();

  auto const br          = mms.compute_rect(fr);
  auto const height_item = br.height() / static_cast<float>(total);

  auto const space_between = compute_space_between(br);
  auto const h             = height_item - space_between;
  auto const pos           = compute_pos(br);

  auto const resize_btn = [&](auto& btn, size_t const index) -> Result<NOTHING, std::string> {
    {
      char const* text = btn.text;
      auto const& ts   = btn.text_size;
      btn.lrects = TRY(font::compute_letter_rectangles_bounded(mms.finfo, br, text, ts, dinfo));
    }
    {
      auto const r = font::compute_minimal_rect(btn.lrects, br);
      auto const i = static_cast<float>(index);
      auto const y = pos.y + (i * (h + space_between));
      btn.trect    = rect::create(br.left(), y, r.width(), h);
    }
    return OK_NONE;
  };

  size_t i = 0;
  for (auto& btn : uistate.text_buttons) {
    TRY(resize_btn(btn, i));
    ++i;
  }
  for (auto& btn : uistate.checkboxes) {
    TRY(resize_btn(btn, i));
    ui::resize(btn, br);
    ++i;
  }
  return OK_NONE;
}

void
update(log_t& LOGGER, SDLWindow& /*window*/, GameState& gs, Devices& ds, Camera const& /*camera*/,
       DeltaTime const& /*dt*/)
{
  auto& screen = static_cast<MainMenuScreen&>(gs.sstack.top());
  screen.update(LOGGER, gs, ds.mouse);
}

Result<FontInfo, std::string>
make_font(log_t& LOGGER, GameState& gs, Frustum const& fr)
{
  auto& ftable = gs.ldata.ftable;
  auto* pft    = ftable.lookup("my-font");
  assert(pft);
  auto& font = *pft;

  auto const       dinfo = gl_sdl::current_display_mode(LOGGER);
  glm::ivec2 const img_size{font.texture.width(), font.texture.height()};
  glm::vec2 const  ratio{fr.width(), fr.height()};
  glm::vec2 constexpr FONT_SIZE{1};
  return font::create_fontinfo(font.ffile, ratio, FONT_SIZE, dinfo, img_size);
}

} // namespace mm_internal

namespace main_menu
{
Result<ScreenVector, std::string>
create_list_screens(log_t& LOGGER, GameState& gs, Frustum const& fr)
{
  auto const font_info = TRY(mm_internal::make_font(LOGGER, gs, fr));

  using ComputeBR = RectFloat (*)(Frustum const&);

  ScreenVector screens;
  auto const   new_menu = [&](ScreenType const type, ComputeBR const& cbr) -> auto&
  {
    using MMS     = MainMenuScreen;
    auto& screen  = *screens.emplace_back(std::make_unique<MainMenuScreen>(type));
    auto& mms     = static_cast<MMS&>(screen);
    mms.draw_fn   = mm_internal::draw;
    mms.resize_fn = mm_internal::resize;
    mms.update_fn = mm_internal::update;

    mms.on_key_down          = mm_internal::on_key_down;
    mms.on_mouse_button_down = mm_internal::on_mousebutton_down;
    mms.on_mouse_button_up   = mm_internal::on_mousebutton_up;

    mms.finfo    = font_info;
    auto& btimer = mms.blink_timer;
    btimer.turn_on();
    btimer.set_duration(std::chrono::milliseconds{750});
    mms.compute_rect = cbr;
    return mms;
  };
  {
    std::array<BC, 3> constexpr MAIN_MENU_PAGE = {
        BC{"Start Game", ButtonType::Text,
           [](GameState& gs, Button&) { gs.sstack.push(ScreenType::InGame); }},
        BC{"Options", ButtonType::Text,
           [](GameState& gs, Button&) { gs.sstack.push(ScreenType::Options); }},
        BC{"Exit", ButtonType::Text, [](GameState& gs, Button&) { gs.quit = true; }}};

    std::array<BC, 5> constexpr OPTION_PAGE = {
        BC{"Low", ButtonType::Text,
           [](GameState& gs, Button&) { gs.graphics.mode = GraphicsMode::Basic; }},
        BC{"Medium", ButtonType::Text,
           [](GameState& gs, Button&) { gs.graphics.mode = GraphicsMode::Medium; }},
        BC{"High", ButtonType::Text,
           [](GameState& gs, Button&) { gs.graphics.mode = GraphicsMode::Advanced; }},
        BC{"Enable Sunshafts", ButtonType::CheckboxText,
           [](GameState& gs, Button& b) {
             gs.graphics.enable_sunshafts = static_cast<CheckboxTextButton&>(b).pressed;
           }},
        BC{"Back", ButtonType::Text, [](GameState& gs, Button&) { gs.sstack.pop(); }}};

    auto const add_menu = [&](ScreenType const type, ComputeBR const& cbr, auto const& options) {
      auto& menu = new_menu(type, cbr);
      for (auto const& bc : options) {
        switch (bc.type) {
        case ButtonType::Text: {
          auto& tb         = make_tb<TextButton>(menu, bc);
          tb.on_left_click = bc.on_left_click;
        } break;

        case ButtonType::CheckboxText: {
          auto& tb         = make_tb<CheckboxTextButton>(menu, bc);
          tb.on_left_click = bc.on_left_click;
        } break;
        }
      }
    };
    add_menu(
        ScreenType::MainMenu, [](Frustum const& fr) { return fr.rect_float() / 4.0f; },
        MAIN_MENU_PAGE);
    add_menu(
        ScreenType::Options, [](Frustum const& fr) { return fr.rect_float() / 2.0f; }, OPTION_PAGE);
  }
  for (auto& s : screens) {
    TRY(s->resize_fn(LOGGER, gs, *s, fr));
  }
  return OK_MOVE(screens);
}

} // namespace main_menu
