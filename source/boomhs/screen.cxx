#include "screen.hpp"

#include <common/move.hpp>

#include <cassert>

ScreenStack::ScreenStack(ScreenVector&& data)
    : storage_(MOVE(data))
{
}

bool
ScreenStack::empty() const
{
  return stack_.empty();
}

Screen&
ScreenStack::push(ScreenType const type)
{
  stack_.push(type);
  auto* ps = find(type);
  assert(nullptr != ps);
  return *ps;
}

void
ScreenStack::pop()
{
  stack_.pop();
}

void
ScreenStack::pop_all()
{
  while (!empty()) {
    pop();
  }
}

Screen const&
ScreenStack::top() const
{
  assert(!empty());
  auto const type = stack_.top();
  auto*      ps   = find(type);
  assert(nullptr != ps);
  return *ps;
}

Screen const*
ScreenStack::find(ScreenType const type) const
{
  auto const b   = std::cbegin(storage_);
  auto const e   = std::cend(storage_);
  auto const cmp = [type](auto const& s) { return type == s->type; };
  auto       it  = std::find_if(b, e, cmp);

  return (e != it) ? it->get() : nullptr;
}

Screen const&
ScreenStack::find_or_abort(ScreenType const type) const
{
  auto const* ps = find(type);
  assert(ps);
  return *ps;
}

size_t
ScreenStack::size() const
{
  return stack_.size();
}
