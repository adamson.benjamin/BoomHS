#include "scene_renderer.hpp"
#include <boomhs/leveldata.hpp>
#include <boomhs/state.hpp>
#include <boomhs/vertex_factory.hpp>
#include <boomhs/water.hpp>

#include <opengl/gpu.hpp>
#include <opengl/water_renderer.hpp>

#include <common/log.hpp>

using namespace math::constants;
using namespace opengl;

void
StaticRenderers::render(log_t& logger, GameState& gs, render_state& rs, Camera const& camera,
                        DeltaTime const& ft, bool const silhouette_black)
{
  // Render the scene with no culling (setting it zero disables culling mathematically)
  glm::vec4 const NOCULL_VECTOR{0, 0, 0, 0};
  auto const&     ui_debug = gs.uibuffers_classic.debug;
  auto const&     cm       = rs.cm;

  if (ui_debug.draw_terrain) {
    auto& ldata        = gs.ldata;
    auto& terrain_grid = ldata.tgrid;
    auto& ds           = rs.count;

    if (silhouette_black) {
      silhouette_terrain.render(logger, terrain_grid, cm.camera_matrix(), ds);
    }
    else {
      auto&      registry    = ldata.registry;
      auto const dirlights   = find_dirlights(registry);
      auto const pointlights = find_pointlights(registry);

      auto const& mat_table = ldata.mtable;
      auto const& mat       = mat_table.find("terrain");

      auto const& global_light = ldata.ambient;
      auto const& fog          = ldata.fog;
      auto&       ttable       = ldata.ttable;

      bool const draw_normals = ui_debug.draw_normals;

      default_terrain.render(logger, ttable, cm.camera_matrix(), cm.view, mat, dirlights,
                             pointlights, terrain_grid, global_light, fog, NOCULL_VECTOR,
                             draw_normals, ds);
    }
  }
  // DRAW ALL ENTITIES
  {
    if (ui_debug.draw_3d_entities) {
      if (silhouette_black) {
        silhouette_entity.render3d(logger, gs, rs, cm, ft);
      }
      else {
        default_entity.render3d(logger, gs, rs, cm, ft);
      }
    }
    if (ui_debug.draw_2d_billboard_entities) {
      if (silhouette_black) {
        silhouette_entity.render2d_billboard(logger, gs, rs, cm);
      }
      else {
        default_entity.render2d_billboard(logger, gs, rs, cm, ft);
      }
    }
  }

  // DRAW DEBUG
  if (silhouette_black) {
    // do nothing
  }
  else {
    debug.render_scene(logger, gs, rs, camera);
  }
}

void
StaticRenderers::resize(log_t& LOGGER, Viewport const& vp)
{
  sunshaft.resize(LOGGER, vp);
  water.resize(LOGGER, vp);
  ui.resize(vp);
}

StaticRenderers
make_static_renderers(log_t& LOGGER, LevelData& ldata, Viewport const& vp)
{
  auto&      reg                       = ldata.registry;
  auto const make_basic_water_renderer = [&](texture_storage& ttable) {
    auto& diff   = *ttable.find("water-diffuse");
    auto& normal = *ttable.find("water-normal");
    auto& sp     = graphics_mode_to_water_shader(reg, GraphicsMode::Basic);
    return water_renderer_basic{LOGGER, diff, normal, sp};
  };

  auto const make_medium_water_renderer = [&](texture_storage& ttable) {
    auto& diff   = *ttable.find("water-diffuse");
    auto& normal = *ttable.find("water-normal");
    auto& sp     = graphics_mode_to_water_shader(reg, GraphicsMode::Medium);
    return water_renderer_medium{LOGGER, diff, normal, sp};
  };

  auto const make_advanced_water_renderer = [&]() {
    auto& ttable = ldata.ttable;
    auto& ti     = *ttable.find("water-diffuse");
    auto& dudv   = *ttable.find("water-dudv");
    auto& normal = *ttable.find("water-normal");
    auto& sp     = graphics_mode_to_water_shader(reg, GraphicsMode::Advanced);
    return water_renderer_advanced{LOGGER, vp, sp, ti, dudv, normal};
  };

  auto const make_sunshaft_renderer = [&]() {
    auto& sunshaft_sp = shader3d::sunshaft(reg);
    return sunshaft_renderer{LOGGER, vp, sunshaft_sp};
  };

  auto const make_skybox_renderer = [&](texture_storage& ttable) {
    auto&           skybox_sp = shader3d::skybox(reg);
    glm::vec3 const vmin{-0.5f};
    glm::vec3 const vmax{0.5f};

    auto const vertices = VertexFactory::build_cube(vmin, vmax);
    draw_state dinfo    = OG::copy_cube_gpu(LOGGER, vertices, skybox_sp.va());
    auto&      day_ti   = *ttable.find("building_skybox");
    auto&      night_ti = *ttable.find("night_skybox");
    return skybox_renderer{LOGGER, MOVE(dinfo), day_ti, night_ti, skybox_sp};
  };
  auto& ttable           = ldata.ttable;
  auto& silhoutte_shader = shader3d::silhoutte(reg);

  return StaticRenderers{
      terrain_renderer_default{},
      terrain_renderer_silhouette{silhoutte_shader},
      entity_renderer{},
      entity_renderer_silhouette{},
      make_skybox_renderer(ttable),
      make_sunshaft_renderer(),
      debug_renderer{},

      WaterRenderers{make_basic_water_renderer(ttable), make_medium_water_renderer(ttable),
                     make_advanced_water_renderer(), water_renderer_silhouette{silhoutte_shader}},
      ui_renderer{LOGGER, vp}};
}
