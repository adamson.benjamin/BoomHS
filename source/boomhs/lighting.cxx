#include "lighting.hpp"
#include <boomhs/components.hpp>
#include <common/FOR.hpp>
#include <common/algorithm.hpp>
#include <common/move.hpp>
#include <utility>

Attenuation operator*(Attenuation const& att, float const v)
{
  Attenuation result = att;
  result *= v;
  return result;
}

Attenuation&
operator*=(Attenuation& att, float const v)
{
  att.constant *= v;
  att.linear *= v;
  att.quadratic *= v;
  return att;
}

Attenuation
operator/(Attenuation const& att, float const v)
{
  Attenuation result = att;
  result /= v;
  return result;
}

Attenuation&
operator/=(Attenuation& att, float const v)
{
  att.constant /= v;
  att.linear /= v;
  att.quadratic /= v;
  return att;
}

std::ostream&
operator<<(std::ostream& stream, Attenuation const& att)
{
  stream << "{";
  stream << att.constant << ", " << att.linear << ", " << att.quadratic;
  stream << "}";
  return stream;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// GlobalLight
GlobalLight::GlobalLight(color3 const& amb)
    : ambient(amb)
{
}

template <typename C, typename L, typename FN>
auto
find_visible_lights(enttreg_t& registry, FN const& add_to_list)
{
  L list;

  auto const eids = find_all_entities_with_component<C, IsRenderable>(registry);
  FOR(i, eids.size())
  {
    auto const& eid = eids[i];
    if (registry.get<IsRenderable>(eid).hidden) {
      continue;
    }
    add_to_list(eid, list);
  }

  return list;
}

DirectionalLightList
find_dirlights(enttreg_t& registry)
{
  auto const add_to_list = [&registry](eid_t const eid, auto& list) {
    auto& dl = registry.get<DirectionalLight>(eid);
    list.emplace_back(DirectionalLightReference{dl});
  };
  return find_visible_lights<DirectionalLight, DirectionalLightList>(registry, add_to_list);
}

PointLightList
find_pointlights(enttreg_t& registry)
{
  using C = PointLight;

  auto const add_to_list = [&registry](eid_t const eid, auto& list) {
    auto& tr = registry.get<Transform>(eid);
    auto& pl = registry.get<C>(eid);

    list.emplace_back(PointLightAndTransform{tr, pl});
  };
  return find_visible_lights<C, PointLightList>(registry, add_to_list);
}
