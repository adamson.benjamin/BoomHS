#pragma once
#include <math/algorithm.hpp>
#include <math/random.hpp>

#include <extlibs/glm.hpp>

#include <array>
#include <cassert>

namespace color
{
template <typename T, size_t N>
using color_t = std::array<T, N>;

template <typename T>
using color3_t = color_t<T, 3>;

template <typename T>
using color4_t = color_t<T, 4>;

using color3 = color3_t<float>;
using color4 = color4_t<float>;

template <typename T, size_t N>
auto constexpr red(color_t<T, N> const& c)
{
  return c[0];
}

template <typename T, size_t N>
auto constexpr green(color_t<T, N> const& c)
{
  return c[1];
}

template <typename T, size_t N>
auto constexpr blue(color_t<T, N> const& c)
{
  return c[2];
}

template <typename T>
auto constexpr alpha(color_t<T, 4> const& c)
{
  return c[3];
}

template <typename T, size_t N>
void
set_r(color_t<T, N>& c, T const v)
{
  c[0] = v;
}

template <typename T, size_t N>
void
set_g(color_t<T, N>& c, T const v)
{
  c[1] = v;
}

template <typename T, size_t N>
void
set_b(color_t<T, N>& c, T const v)
{
  c[2] = v;
}

template <typename T>
void
set_a(color_t<T, 4>& c, T const v)
{
  c[3] = v;
}

template <typename T, size_t N>
auto constexpr vec3(color_t<T, N> const& c)
{
  return glm::make_vec3(c.data());
}

template <typename T, size_t N>
auto constexpr vec4(color_t<T, N> const& c)
{
  return glm::make_vec4(c.data());
}

constexpr color3
rgb(float const r, float const g, float const b)
{
  return color3{r, g, b};
}

template <typename T>
constexpr auto
rgb(color_t<T, 3> const& c)
{
  auto const r = red(c);
  auto const g = green(c);
  auto const b = blue(c);
  return rgb(r, g, b);
}

template <typename T>
constexpr auto
rgb(color_t<T, 4> const& c)
{
  auto const r = red(c);
  auto const g = green(c);
  auto const b = blue(c);
  return rgb(r, g, b);
}

constexpr color3
rgb(glm::vec3 const& c)
{
  return rgb(c.x, c.y, c.z);
}
constexpr color3
rgb(glm::vec4 const& c)
{
  return rgb(c.x, c.y, c.z);
}

auto constexpr rgba(float const r, float const g, float const b, float const a = 1)
{
  return color4{r, g, b, a};
}

template <typename T>
auto constexpr rgba(color4_t<T> const& c)
{
  return rgba(c[0], c[1], c[2], c[3]);
}

template <typename T>
auto constexpr rgba(color3_t<T> const& c)
{
  return rgba(c[0], c[1], c[2]);
}

auto constexpr rgba(glm::vec3 const& vec) { return rgba(vec.x, vec.y, vec.z); }
auto constexpr rgba(glm::vec4 const& vec) { return rgba(vec.x, vec.y, vec.z, vec.w); }

inline auto
random3(rng_t& rng)
{
  auto const gen = [&rng]() { return rng.gen_0to1(); };
  return rgb(gen(), gen(), gen());
}

inline auto
random3()
{
  rng_t rng;
  return random3(rng);
}

inline auto
random4(rng_t& rng, float const alpha = 1)
{
  auto const gen = [&rng]() { return rng.gen_0to1(); };
  return rgba(gen(), gen(), gen(), alpha);
}

inline auto
random4()
{
  rng_t rng;
  return random4(rng);
}

inline auto
lerp(color3 const& a, color3 const& b, float const dt)
{
  auto const va = vec3(a);
  auto const vb = vec3(b);
  auto const c  = glm::lerp(va, vb, dt);
  return rgb(c);
}

inline auto
lerp(color4 const& a, color4 const& b, float const dt)
{
  return rgba(glm::lerp(vec4(a), vec4(b), dt));
}

template <typename T>
auto
to_string(glm::tvec3<T> const& v)
{
  return glm::to_string(v);
}

template <typename T>
auto
to_string(glm::tvec4<T> const& v)
{
  return glm::to_string(v);
}

template <typename T>
auto
to_string(color_t<T, 3> const& c)
{
  return to_string(glm::make_vec3(c));
}

template <typename T>
auto
to_string(color_t<T, 4> const& c)
{
  return to_string(glm::make_vec4(c));
}

template <typename T, size_t N>
std::ostream&
operator<<(std::ostream& os, color_t<T, N> const& c)
{
  os << to_string(c);
  return os;
}

inline bool
operator==(color4 const& first, color4 const& second)
{
  auto const r = math::float_cmp(red(first), red(second));
  auto const g = math::float_cmp(green(first), green(second));
  auto const b = math::float_cmp(blue(first), blue(second));
  auto const a = math::float_cmp(alpha(first), alpha(second));
  return common::and_all(r, g, b, a);
}

inline bool
operator!=(color4 const& a, color4 const& b)
{
  return !(a == b);
}

#define COLOR_LIST_MACRO(FN)                                                                       \
  constexpr auto INDIAN_RED             = FN(0.804f, 0.361f, 0.361f);                              \
  constexpr auto LIGHT_CORAL            = FN(0.941f, 0.502f, 0.502f);                              \
  constexpr auto SALMON                 = FN(0.980f, 0.502f, 0.447f);                              \
  constexpr auto DARK_SALMON            = FN(0.914f, 0.588f, 0.478f);                              \
  constexpr auto LIGHT_SALMON           = FN(1.000f, 0.627f, 0.478f);                              \
  constexpr auto CRIMSON                = FN(0.863f, 0.078f, 0.235f);                              \
  constexpr auto RED                    = FN(1.000f, 0.000f, 0.000f);                              \
  constexpr auto FIREBRICK              = FN(0.698f, 0.133f, 0.133f);                              \
  constexpr auto DARKRED                = FN(0.545f, 0.000f, 0.000f);                              \
  constexpr auto PINK                   = FN(1.000f, 0.753f, 0.796f);                              \
  constexpr auto LIGHT_PINK             = FN(1.000f, 0.714f, 0.757f);                              \
  constexpr auto HOT_PINK               = FN(1.000f, 0.412f, 0.706f);                              \
  constexpr auto DEEP_PINK              = FN(1.000f, 0.078f, 0.576f);                              \
  constexpr auto MEDIUM_VIOLET_RED      = FN(0.780f, 0.082f, 0.522f);                              \
  constexpr auto PALE_VIOLET_RED        = FN(0.859f, 0.439f, 0.576f);                              \
  constexpr auto CORAL                  = FN(1.000f, 0.498f, 0.314f);                              \
  constexpr auto TOMATO                 = FN(1.000f, 0.388f, 0.278f);                              \
  constexpr auto ORANGE_RED             = FN(1.000f, 0.271f, 0.000f);                              \
  constexpr auto DARK_ORANGE            = FN(1.000f, 0.549f, 0.000f);                              \
  constexpr auto ORANGE                 = FN(1.000f, 0.647f, 0.000f);                              \
  constexpr auto GOLD                   = FN(1.000f, 0.843f, 0.000f);                              \
  constexpr auto YELLOW                 = FN(1.000f, 1.000f, 0.000f);                              \
  constexpr auto LIGHT_YELLOW           = FN(1.000f, 1.000f, 0.878f);                              \
  constexpr auto LEMON_CHION            = FN(1.000f, 0.980f, 0.804f);                              \
  constexpr auto LIGHT_GOLDENROD_YELLOW = FN(0.980f, 0.980f, 0.824f);                              \
  constexpr auto PAPAYAWHIP             = FN(1.000f, 0.937f, 0.835f);                              \
  constexpr auto MOCCASIN               = FN(1.000f, 0.894f, 0.710f);                              \
  constexpr auto PEACHPU                = FN(1.000f, 0.855f, 0.725f);                              \
  constexpr auto PALE_GOLDEN_ROD        = FN(0.933f, 0.910f, 0.667f);                              \
  constexpr auto KHAKI                  = FN(0.941f, 0.902f, 0.549f);                              \
  constexpr auto DARK_KHAKI             = FN(0.741f, 0.718f, 0.420f);                              \
  constexpr auto LAVENDER               = FN(0.902f, 0.902f, 0.980f);                              \
  constexpr auto THISTLE                = FN(0.847f, 0.749f, 0.847f);                              \
  constexpr auto PLUM                   = FN(0.867f, 0.627f, 0.867f);                              \
  constexpr auto VIOLET                 = FN(0.933f, 0.510f, 0.933f);                              \
  constexpr auto ORCHID                 = FN(0.855f, 0.439f, 0.839f);                              \
  constexpr auto FUCHSIA                = FN(1.000f, 0.000f, 1.000f);                              \
  constexpr auto MAGENTA                = FN(1.000f, 0.000f, 1.000f);                              \
  constexpr auto MEDIUM_ORCHID          = FN(0.729f, 0.333f, 0.827f);                              \
  constexpr auto MEDIUM_PURPLE          = FN(0.576f, 0.439f, 0.859f);                              \
  constexpr auto BLUE_VIOLET            = FN(0.541f, 0.169f, 0.886f);                              \
  constexpr auto DARK_VIOLET            = FN(0.580f, 0.000f, 0.827f);                              \
  constexpr auto DARK_ORCHID            = FN(0.600f, 0.196f, 0.800f);                              \
  constexpr auto DARK_MAGENTA           = FN(0.545f, 0.000f, 0.545f);                              \
  constexpr auto PURPLE                 = FN(0.502f, 0.000f, 0.502f);                              \
  constexpr auto INDIGO                 = FN(0.294f, 0.000f, 0.510f);                              \
  constexpr auto SLATE_BLUE             = FN(0.416f, 0.353f, 0.804f);                              \
  constexpr auto DARK_SLATE_BLUE        = FN(0.282f, 0.239f, 0.545f);                              \
  constexpr auto GREEN_YELLOW           = FN(0.678f, 1.000f, 0.184f);                              \
  constexpr auto CHARTREUSE             = FN(0.498f, 1.000f, 0.000f);                              \
  constexpr auto LAWN_GREEN             = FN(0.486f, 0.988f, 0.000f);                              \
  constexpr auto LIME                   = FN(0.000f, 1.000f, 0.000f);                              \
  constexpr auto LIME_GREEN             = FN(0.196f, 0.804f, 0.196f);                              \
  constexpr auto PALE_GREEN             = FN(0.596f, 0.984f, 0.596f);                              \
  constexpr auto LIGHT_GREEN            = FN(0.565f, 0.933f, 0.565f);                              \
  constexpr auto MEDIUM_SPRING_GREEN    = FN(0.000f, 0.980f, 0.604f);                              \
  constexpr auto SPRING_GREEN           = FN(0.000f, 1.000f, 0.498f);                              \
  constexpr auto MEDIUM_SEA_GREEN       = FN(0.235f, 0.702f, 0.443f);                              \
  constexpr auto SEA_GREEN              = FN(0.180f, 0.545f, 0.341f);                              \
  constexpr auto FOREST_GREEN           = FN(0.133f, 0.545f, 0.133f);                              \
  constexpr auto GREEN                  = FN(0.000f, 0.502f, 0.000f);                              \
  constexpr auto DARKGREEN              = FN(0.000f, 0.392f, 0.000f);                              \
  constexpr auto YELLOW_GREEN           = FN(0.604f, 0.804f, 0.196f);                              \
  constexpr auto OLIVE_DRAB             = FN(0.420f, 0.557f, 0.137f);                              \
  constexpr auto OLIVE                  = FN(0.502f, 0.502f, 0.000f);                              \
  constexpr auto DARK_OLIVE_GREEN       = FN(0.333f, 0.420f, 0.184f);                              \
  constexpr auto MEDIUM_AQUAMARINE      = FN(0.400f, 0.804f, 0.667f);                              \
  constexpr auto DARK_SEAGREEN          = FN(0.561f, 0.737f, 0.561f);                              \
  constexpr auto LIGHT_SEAGREEN         = FN(0.125f, 0.698f, 0.667f);                              \
  constexpr auto DARK_CYAN              = FN(0.000f, 0.545f, 0.545f);                              \
  constexpr auto TEAL                   = FN(0.000f, 0.502f, 0.502f);                              \
  constexpr auto AQUA                   = FN(0.000f, 1.000f, 1.000f);                              \
  constexpr auto CYAN                   = FN(0.000f, 1.000f, 1.000f);                              \
  constexpr auto LIGHT_CYAN             = FN(0.878f, 1.000f, 1.000f);                              \
  constexpr auto PALETURQUOISE          = FN(0.686f, 0.933f, 0.933f);                              \
  constexpr auto AQUAMARINE             = FN(0.498f, 1.000f, 0.831f);                              \
  constexpr auto TURQUOISE              = FN(0.251f, 0.878f, 0.816f);                              \
  constexpr auto MEDIUM_TURQUOISE       = FN(0.282f, 0.820f, 0.800f);                              \
  constexpr auto DARK_TURQUOISE         = FN(0.000f, 0.808f, 0.820f);                              \
  constexpr auto CADET_BLUE             = FN(0.373f, 0.620f, 0.627f);                              \
  constexpr auto STEEL_BLUE             = FN(0.275f, 0.510f, 0.706f);                              \
  constexpr auto LIGHT_STEEL_BLUE       = FN(0.690f, 0.769f, 0.871f);                              \
  constexpr auto POWDER_BLUE            = FN(0.690f, 0.878f, 0.902f);                              \
  constexpr auto LIGHT_BLUE             = FN(0.678f, 0.847f, 0.902f);                              \
  constexpr auto SKY_BLUE               = FN(0.529f, 0.808f, 0.922f);                              \
  constexpr auto LIGHT_SKY_BLUE         = FN(0.529f, 0.808f, 0.980f);                              \
  constexpr auto DEEP_SKY_BLUE          = FN(0.000f, 0.749f, 1.000f);                              \
  constexpr auto DODGER_BLUE            = FN(0.118f, 0.565f, 1.000f);                              \
  constexpr auto CORNLOWER_BLUE         = FN(0.392f, 0.584f, 0.929f);                              \
  constexpr auto MEDIUM_SLATE_BLUE      = FN(0.482f, 0.408f, 0.933f);                              \
  constexpr auto ROYAL_BLUE             = FN(0.255f, 0.412f, 0.882f);                              \
  constexpr auto BLUE                   = FN(0.000f, 0.000f, 1.000f);                              \
  constexpr auto MEDIUM_BLUE            = FN(0.000f, 0.000f, 0.804f);                              \
  constexpr auto DARK_BLUE              = FN(0.000f, 0.000f, 0.545f);                              \
  constexpr auto NAVY                   = FN(0.000f, 0.000f, 0.502f);                              \
  constexpr auto MIDNIGHT_BLUE          = FN(0.098f, 0.098f, 0.439f);                              \
  constexpr auto CORNSILK               = FN(1.000f, 0.973f, 0.863f);                              \
  constexpr auto BLANCHED_ALMOND        = FN(1.000f, 0.922f, 0.804f);                              \
  constexpr auto BISQUE                 = FN(1.000f, 0.894f, 0.769f);                              \
  constexpr auto NAVAJ_OWHITE           = FN(1.000f, 0.871f, 0.678f);                              \
  constexpr auto WHEAT                  = FN(0.961f, 0.871f, 0.702f);                              \
  constexpr auto BURLY_WOOD             = FN(0.871f, 0.722f, 0.529f);                              \
  constexpr auto TAN                    = FN(0.824f, 0.706f, 0.549f);                              \
  constexpr auto ROSY_BROWN             = FN(0.737f, 0.561f, 0.561f);                              \
  constexpr auto SANDY_BROWN            = FN(0.957f, 0.643f, 0.376f);                              \
  constexpr auto GOLDENROD              = FN(0.855f, 0.647f, 0.125f);                              \
  constexpr auto DARK_GOLDENROD         = FN(0.722f, 0.525f, 0.043f);                              \
  constexpr auto PERU                   = FN(0.804f, 0.522f, 0.247f);                              \
  constexpr auto CHOCOLATE              = FN(0.824f, 0.412f, 0.118f);                              \
  constexpr auto SADDLE_BROWN           = FN(0.545f, 0.271f, 0.075f);                              \
  constexpr auto SIENNA                 = FN(0.627f, 0.322f, 0.176f);                              \
  constexpr auto BROWN                  = FN(0.647f, 0.165f, 0.165f);                              \
  constexpr auto MAROON                 = FN(0.502f, 0.000f, 0.000f);                              \
  constexpr auto WHITE                  = FN(1.000f, 1.000f, 1.000f);                              \
  constexpr auto SNOW                   = FN(1.000f, 0.980f, 0.980f);                              \
  constexpr auto HONEYDEW               = FN(0.941f, 1.000f, 0.941f);                              \
  constexpr auto MINTCREAM              = FN(0.961f, 1.000f, 0.980f);                              \
  constexpr auto AZURE                  = FN(0.941f, 1.000f, 1.000f);                              \
  constexpr auto ALICEBLUE              = FN(0.941f, 0.973f, 1.000f);                              \
  constexpr auto GHOST_WHITE            = FN(0.973f, 0.973f, 1.000f);                              \
  constexpr auto WHITE_SMOKE            = FN(0.961f, 0.961f, 0.961f);                              \
  constexpr auto SEASHELL               = FN(1.000f, 0.961f, 0.933f);                              \
  constexpr auto BEIGE                  = FN(0.961f, 0.961f, 0.863f);                              \
  constexpr auto OLDLACE                = FN(0.992f, 0.961f, 0.902f);                              \
  constexpr auto FLORAL_WHITE           = FN(1.000f, 0.980f, 0.941f);                              \
  constexpr auto IVORY                  = FN(1.000f, 1.000f, 0.941f);                              \
  constexpr auto ANTIQUE_WHITE          = FN(0.980f, 0.922f, 0.843f);                              \
  constexpr auto LINEN                  = FN(0.980f, 0.941f, 0.902f);                              \
  constexpr auto LAVENDERBLUSH          = FN(1.000f, 0.941f, 0.961f);                              \
  constexpr auto MISTYROSE              = FN(1.000f, 0.894f, 0.882f);                              \
  constexpr auto GAINSBORO              = FN(0.863f, 0.863f, 0.863f);                              \
  constexpr auto LIGHT_GREY             = FN(0.827f, 0.827f, 0.827f);                              \
  constexpr auto SILVER                 = FN(0.753f, 0.753f, 0.753f);                              \
  constexpr auto DARKGRAY               = FN(0.663f, 0.663f, 0.663f);                              \
  constexpr auto GRAY                   = FN(0.502f, 0.502f, 0.502f);                              \
  constexpr auto DIM_GRAY               = FN(0.412f, 0.412f, 0.412f);                              \
  constexpr auto LIGHT_SLATE_GRAY       = FN(0.467f, 0.533f, 0.600f);                              \
  constexpr auto SLATE_GRAY             = FN(0.439f, 0.502f, 0.565f);                              \
  constexpr auto DARK_SLATE_GRAY        = FN(0.184f, 0.310f, 0.310f);                              \
  constexpr auto BLACK                  = FN(0.000f, 0.000f, 0.000f);                              \
                                                                                                   \
  constexpr auto NO_ALPHA = FN(0.000f, 0.000f, 0.000f);

namespace LIST_OF_COLORS3
{
COLOR_LIST_MACRO(rgb)
} // namespace LIST_OF_COLORS3

namespace LIST_OF_COLORS4
{
COLOR_LIST_MACRO(rgba)
} // namespace LIST_OF_COLORS4

} // namespace color

using color3 = ::color::color3;
using color4 = ::color::color4;

template <size_t N>
using color4Array = std::array<color4, N>;

namespace LOC3 = ::color::LIST_OF_COLORS3;
namespace LOC4 = ::color::LIST_OF_COLORS4;
