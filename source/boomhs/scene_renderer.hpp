#pragma once
#include <opengl/debug_renderer.hpp>
#include <opengl/entity_renderer.hpp>
#include <opengl/renderer.hpp>
#include <opengl/skybox_renderer.hpp>
#include <opengl/sunshaft.hpp>
#include <opengl/terrain_renderer.hpp>
#include <opengl/water_renderer.hpp>

#include <opengl/ui_renderer.hpp>

namespace opengl
{
struct render_state;
class shader_type;
} // namespace opengl

struct Camera;
class DeltaTime;
struct GameState;
class LevelManager;
class LevelData;

struct StaticRenderers
{
  opengl::terrain_renderer_default    default_terrain;
  opengl::terrain_renderer_silhouette silhouette_terrain;

  opengl::entity_renderer            default_entity;
  opengl::entity_renderer_silhouette silhouette_entity;

  opengl::skybox_renderer   skybox;
  opengl::sunshaft_renderer sunshaft;

  opengl::debug_renderer debug;
  opengl::WaterRenderers water;

  opengl::ui_renderer ui;

  void render(log_t&, GameState&, opengl::render_state&, Camera const&, DeltaTime const&, bool);

  void resize(log_t&, Viewport const&);
};

StaticRenderers
make_static_renderers(log_t&, LevelData&, Viewport const&);
