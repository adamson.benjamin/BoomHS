#pragma once
#include <common/macros_container.hpp>
#include <common/result.hpp>
#include <extlibs/glm.hpp>
#include <math/rectangle.hpp>

#include <array>
#include <string>
#include <variant>
#include <vector>

struct CharInfo
{
  int c; // stored as an int because it represents the UNICODE number of our char
  int xpos, ypos;
  int width, height;
  int xoffset, yoffset;
  int orig_w, orig_h;
};

struct KerningPair
{
  int   c0, c1;
  float value;
};

struct FontFile
{
  std::string name;
  std::string img_file;

  std::vector<CharInfo>    char_infos;
  std::vector<KerningPair> kerning_pairs;
  std::array<int, 4>       padding;
};

struct FontFiles
{
  std::vector<FontFile> ffiles;
  DEFINE_VECTOR_LIKE_WRAPPER_FNS(ffiles)
};

struct LetterRects
{
  RectFloat rect;
  RectFloat uv_rect;
};
using LetterRectCollection = std::vector<LetterRects>;

struct FontInfo
{
  FontFile   ffile;
  glm::ivec2 texture_size;
  glm::vec2  font_size;

  float line_height;

  LetterRectCollection letter_infos;
};

struct NullTerminatorCharInfo
{
};
using CharLookupResult = std::variant<CharInfo, NullTerminatorCharInfo>;

namespace font
{

/// Compute the height of the line by iterating over all the letters and returning the largest
/// height value.
float
compute_largest_height(LetterRectCollection const&);

unsigned int
compute_howmany_lines_required_for(LetterRectCollection const&, RectFloat const&);

RectFloat
compute_minimal_rect(LetterRectCollection const&, RectFloat const&);

Result<FontInfo, std::string>
create_fontinfo(FontFile const& ffile, glm::vec2 const& ratio, glm::vec2 const& font_size,
                glm::ivec2 const& display_size, glm::ivec2 const& img_size);

using CheckWrapAround = bool;
using CenteredFlag    = bool;

Result<LetterRectCollection, std::string>
compute_letter_rectangles(FontInfo const&, RectFloat const&, std::string const&,
                          glm::vec2 const& font_size, glm::ivec2 const& display_size,
                          CheckWrapAround = false, CenteredFlag = false);

Result<LetterRectCollection, std::string>
compute_letter_rectangles_bounded(FontInfo const&, RectFloat const& rect, std::string const&,
                                  glm::vec2 const& font_size, glm::ivec2 const& display_size);

} // namespace font
