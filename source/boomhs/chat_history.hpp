#pragma once
#include <boomhs/color.hpp>
#include <boomhs/font.hpp>
#include <boomhs/ui_textbox.hpp>

#include <common/algorithm.hpp>
#include <common/macros_class.hpp>
#include <common/time.hpp>

#include <cstdint>
#include <string>
#include <vector>

struct Frustum;
class Mouse;
class rng_t;

using ChannelId = uint32_t;
struct Channel
{
  ChannelId const   id;
  std::string const name;
  color4 const      color;
};

struct ChatMessage : public TextBoxMessage
{
  ChannelId channel_id;
};

using ChatMessages = std::vector<ChatMessage>;
class ChatHistory
{
  ChatMessages         messages_;
  std::vector<Channel> channels_;

  bool           has_channel(ChannelId) const;
  Channel const* find_channel(ChannelId) const;

public:
  ChatHistory() = default;
  MOVE_CONSTRUCTIBLE_ONLY(ChatHistory);

  using MessageType = typename ChatMessages::value_type;

public:
  void add_channel(Channel const&);

  ChatMessage& insert_back(ChatMessage&&);
  ChatMessage& insert_back(ChannelId, std::string&&);

  ChatMessage& insert_front(ChannelId, std::string&&);

  Channel const& channels(ChannelId) const;
  color4 const&  channel_color(ChannelId) const;

  uint32_t num_channels() const;

  ChatMessages&       all_messages();
  ChatMessages const& all_messages() const;
  ChatMessages        all_messages_in_channel(ChannelId) const;
};
