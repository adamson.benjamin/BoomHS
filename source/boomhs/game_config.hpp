#pragma once

enum class GraphicsMode
{
  Basic = 0,
  Medium,
  Advanced
};

struct GraphicsSettings
{
  GraphicsMode mode             = GraphicsMode::Basic;
  bool         enable_sunshafts = true;
};
