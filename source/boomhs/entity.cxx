#include "entity.hpp"
#include <boomhs/transform.hpp>

eid_array
all_nearby_entities(glm::vec3 const& pos, float const max_distance, enttreg_t& registry)
{
  eid_array result;

  auto const fn = [&](eid_t const eid, Transform const& tr) {
    if (glm::distance(tr.translation, pos) <= max_distance) {
      result.emplace_back(eid);
    }
  };

  auto const view = registry.view<Transform>();
  view.each(fn);
  return result;
}
