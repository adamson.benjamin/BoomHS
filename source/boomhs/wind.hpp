#pragma once
#include <extlibs/glm.hpp>

struct Wind
{
  glm::vec2 dir;
  float     speed = 0;
};
