#pragma once
#include <boomhs/font.hpp>

#include <common/result.hpp>

struct log_t;

namespace font
{

Result<FontFile, std::string>
load_font_file(log_t&, std::string const&);

} // namespace font
