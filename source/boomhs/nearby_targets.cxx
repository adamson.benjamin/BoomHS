#include "nearby_targets.hpp"
#include <boomhs/delta_time.hpp>

#include <common/time.hpp>

#include <chrono>

using namespace common;
using namespace std::chrono;

namespace
{

bool
should_change_offset(std::optional<SelectedTarget> const& selected, eid_array const& targets)
{
  return selected && (targets.size() > 1);
}

} // namespace

void
NearbyTargets::add_target(eid_t const target_eid)
{
  targets_.emplace_back(target_eid);
}

float
NearbyTargets::calculate_scale(DeltaTime const& dt) const
{
  // GOAL: After time since target last changed, we always return 1. The caller will assume this
  // means full scale, which in this case will be the renderer. The renderer will draw whatever
  // indicates nearby targets at full scale rect.
  //
  // Between the time the target is last changed and some other time after then, this function will
  // return a value in the range [0, 1].

  duration_t constexpr GROW_TIME_MS{140};

  assert(selected_);
  auto const lc     = last_target_changed_;
  auto const future = lc + GROW_TIME_MS;

  auto const now = dt.since_start();

  using float_duration  = std::chrono::duration<float>;
  auto const difference = duration_cast<float_duration>(future - now).count();

  float constexpr MIN = 0.0f, MAX = 1.0f;
  auto const  lerp    = glm::lerp(MIN, MAX, difference);
  float const inverse = 1 - (lerp / GROW_TIME_MS.count());
  float const rate    = std::abs(inverse);
  float const clamped = glm::clamp(rate, MIN, MAX);
  return clamped;
}

void
NearbyTargets::clear()
{
  targets_.clear();
  selected_ = std::nullopt;
}

void
NearbyTargets::cycle(CycleDirection const cycle_dir, DeltaTime const& dt)
{
  if (CycleDirection::Forward == cycle_dir) {
    cycle_forward(dt);
  }
  else {
    assert(CycleDirection::Backward == cycle_dir);
    cycle_backward(dt);
  }
}

void
NearbyTargets::cycle_forward(DeltaTime const& dt)
{
  if (!selected_ && !targets_.empty()) {
    selected_ = SelectedTarget{};
    update_time(dt);
  }
  else if (should_change_offset(selected_, targets_)) {
    auto& offset = selected_->offset;
    offset += 1;
    if (offset >= targets_.size()) {
      offset = 0;
    }
    update_time(dt);
  }
}

void
NearbyTargets::cycle_backward(DeltaTime const& dt)
{
  if (!selected_ && !targets_.empty()) {
    selected_ = SelectedTarget{};
    update_time(dt);
  }
  else if (should_change_offset(selected_, targets_)) {
    auto& offset = selected_->offset;
    if (offset == 0) {
      offset = targets_.size();
    }
    offset -= 1;
    update_time(dt);
  }
}

bool
NearbyTargets::empty() const
{
  return targets_.empty();
}

std::optional<eid_t>
NearbyTargets::selected() const
{
  if (!selected_) {
    return std::nullopt;
  }

  // If an object is selected, then certainly the list of objects the selected object comes from is
  // not empty.
  assert(!empty());

  auto const offset = selected_->offset;
  assert(targets_.size() > offset);
  return targets_[offset];
}

void
NearbyTargets::set_selected(eid_t const selected)
{
  auto const cmp = [&selected](eid_t const eid) { return selected == eid; };
  auto const it  = std::find_if(targets_.cbegin(), targets_.cend(), cmp);
  if (it == targets_.cend()) {
    // entity must no longer be in LOS
    return;
  }
  assert(it != targets_.cend());

  auto const index = std::distance(targets_.cbegin(), it);
  assert(index >= 0);
  selected_         = SelectedTarget{};
  selected_->offset = static_cast<size_t>(index);
}

void
NearbyTargets::update_time(DeltaTime const& dt)
{
  last_target_changed_ = dt.since_start();
}

color4
NearbyTargets::color_from_level_difference(int const player_level, int const target_level)
{
  int const diff     = target_level - player_level;
  int const abs_diff = std::abs(diff);

  bool const monster_gt = diff > 0;
  bool const monster_lt = diff < 0;
  bool const equals     = diff == 0;

  {
    bool const equals_and_neither_gtlt = (equals && (!monster_gt && !monster_lt));
    bool const gltl_notsame = ((monster_gt && !monster_lt) || (!monster_gt && monster_lt));
    bool const notequals_and_onlyone_gtlt = (!equals && gltl_notsame);

    // Ensure that either they are the same (and gt/lt are false)
    // or
    // they are not the same and exactly one (gt/lt) is true.
    assert(equals_and_neither_gtlt || notequals_and_onlyone_gtlt);
  }

  if (equals) {
    return LOC4::WHITE;
  }
  else if (monster_gt) {
    if (abs_diff <= 2) {
      return LOC4::YELLOW;
    }
    else if (abs_diff <= 3) {
      return LOC4::DARK_ORANGE;
    }
    else if (monster_gt) {
      return LOC4::RED;
    }
  }
  else if (monster_lt) {
    if (abs_diff <= 2) {
      return LOC4::BLUE;
    }
    else if (abs_diff <= 4) {
      return LOC4::DEEP_SKY_BLUE;
    }
    else if (abs_diff <= 6) {
      return LOC4::GREEN;
    }
    else {
      return LOC4::DIM_GRAY;
    }
  }
  std::exit(EXIT_FAILURE);
}
