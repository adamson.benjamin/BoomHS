#include "water.hpp"
#include <boomhs/bounding_object.hpp>
#include <boomhs/camera.hpp>
#include <boomhs/components.hpp>
#include <boomhs/entity.hpp>
#include <boomhs/mesh.hpp>
#include <boomhs/terrain.hpp>

#include <opengl/buffer.hpp>
#include <opengl/gpu.hpp>
#include <opengl/shader.hpp>

#include <opengl/bind.hpp>
#include <opengl/texture.hpp>

#include <common/log.hpp>
#include <math/random.hpp>

#include <cassert>
#include <extlibs/fmt.hpp>
#include <extlibs/glew.hpp>

using namespace opengl;

shader_type&
graphics_mode_to_water_shader(enttreg_t& reg, GraphicsMode const dwo)
{
  switch (dwo) {
  case GraphicsMode::Basic:
    return shader3d::water_basic(reg);

  case GraphicsMode::Medium:
    return shader3d::water_medium(reg);

  case GraphicsMode::Advanced:
    return shader3d::water_advanced(reg);
  }
  std::abort();
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// WaterFactory
ObjData
WaterFactory::generate_water_data(log_t& LOGGER, glm::vec2 const& dimensions,
                                  size_t const num_vertexes)
{
  auto const count = num_vertexes * num_vertexes;
  assert(count <= std::numeric_limits<unsigned int>::max());

  ObjData data;
  data.num_vertexes = static_cast<unsigned int>(count);

  data.vertices = MeshFactory::generate_rectangle_mesh(LOGGER, dimensions, num_vertexes);

  bool constexpr TILE = true;
  data.uvs            = MeshFactory::generate_uvs(LOGGER, dimensions, num_vertexes, TILE);

  data.indices = MeshFactory::generate_indices(LOGGER, num_vertexes);

  return data;
}

WaterInfo&
WaterFactory::make_default(log_t& LOGGER, texture_storage& ttable, eid_t const eid, enttreg_t& reg)
{
  LOG_TRACE("Generating water");

  auto texture_o = ttable.find("water-diffuse");
  assert(texture_o);
  auto& ti = *texture_o;

  bind_for(LOGGER, ti, [&]() {
    ti.set_fieldi(GL_TEXTURE_WRAP_S, GL_REPEAT);
    ti.set_fieldi(GL_TEXTURE_WRAP_T, GL_REPEAT);
    ti.set_fieldi(GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    ti.set_fieldi(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  });
  LOG_TRACE("Finished generating water");

  auto& wi        = reg.emplace<WaterInfo>(eid);
  wi.tinfo        = &ti;
  wi.dimensions   = glm::vec2{20};
  wi.num_vertexes = 4;

  reg.emplace<IsRenderable>(eid);
  auto& tr = reg.emplace<Transform>(eid);

  // hack
  tr.translation.y = 0.05f;

  auto const xdim             = wi.dimensions.x;
  auto const zdim             = wi.dimensions.y;
  auto constexpr WATER_HEIGHT = 0.005f;

  auto const min = glm::vec3{0, -WATER_HEIGHT, 0};
  auto const max = glm::vec3{xdim, WATER_HEIGHT, zdim};
  AABoundingBox::add_to_entity(LOGGER, reg, eid, min, max);
  return wi;
}
