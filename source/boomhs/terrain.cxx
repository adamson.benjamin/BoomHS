#include "terrain.hpp"
#include <boomhs/mesh.hpp>
#include <boomhs/obj.hpp>

#include <opengl/bind.hpp>
#include <opengl/buffer.hpp>
#include <opengl/gpu.hpp>
#include <opengl/shader.hpp>

#include <opengl/uniform.hpp>

#include <cassert>
#include <common/FOR.hpp>
#include <common/algorithm.hpp>
#include <common/log.hpp>

using namespace opengl;

#define APPEND_COMMA_SEPERATED_LIST(sstr, list, fn)                                                \
  {                                                                                                \
    bool first = true;                                                                             \
    sstr << "{";                                                                                   \
    for (auto const& tn : list) {                                                                  \
      if (!first) {                                                                                \
        sstr << ", ";                                                                              \
      }                                                                                            \
      else {                                                                                       \
        first = false;                                                                             \
      }                                                                                            \
      sstr << fn(tn);                                                                              \
    }                                                                                              \
    sstr << "}";                                                                                   \
  }

namespace
{
ObjData
generate_terrain_data(log_t& LOGGER, TerrainGridConfig const& tgc, TerrainConfig const& tc,
                      Heightmap const& heightmap)
{
  auto const numv_oneside = tc.num_vertexes_along_one_side;
  auto const num_vertexes = math::squared(numv_oneside);
  assert(num_vertexes <= std::numeric_limits<unsigned int>::max());

  ObjData data;
  data.num_vertexes = static_cast<unsigned int>(num_vertexes);

  data.vertices = MeshFactory::generate_rectangle_mesh(LOGGER, tgc.dimensions, numv_oneside);
  heightmap::update_vertices_from_heightmap(LOGGER, tc, heightmap, data.vertices);

  {
    GenerateNormalData const gnd{tc.invert_normals, heightmap, numv_oneside};
    data.normals = MeshFactory::generate_normals(LOGGER, gnd);
  }

  data.uvs     = MeshFactory::generate_uvs(LOGGER, tgc.dimensions, numv_oneside, tc.tile_textures);
  data.indices = MeshFactory::generate_indices(LOGGER, numv_oneside);
  return data;
}

Terrain
generate_piece(log_t& LOGGER, glm::vec2 const& pos, TerrainGridConfig const& tgc,
               TerrainConfig const& tc, Heightmap&& heightmap, shader_type& sp)
{
  auto const data = generate_terrain_data(LOGGER, tgc, tc, heightmap);
  LOG_TRACE("Generated terrain piece: %s", data.to_string());

  buffer_flags const flags{true, true, false, true};
  auto const         buffer = vertex_buffer::create_interleaved(LOGGER, data, flags);
  auto               di     = gpu::copy_gpu(LOGGER, sp.va(), buffer);

  // These uniforms only need to be set once.
  bind_for(LOGGER, sp, [&]() {
    uniform::set(LOGGER, sp, "u_bgsampler", 0);
    uniform::set(LOGGER, sp, "u_rsampler", 1);
    uniform::set(LOGGER, sp, "u_gsampler", 2);
    uniform::set(LOGGER, sp, "u_bsampler", 3);
    uniform::set(LOGGER, sp, "u_blendsampler", 4);
  });

  Terrain terrain{tc, pos, MOVE(di), sp, MOVE(heightmap)};
  auto&   tn = terrain.texture_names;
  tn.clear();

  tn.heightmap_path = "Area0-HM";
  tn.textures.emplace_back("floor");
  tn.textures.emplace_back("grass");
  tn.textures.emplace_back("mud");
  tn.textures.emplace_back("brick_path");
  tn.textures.emplace_back("blendmap");
  return terrain;
}

float
barry_centric(glm::vec3 const& p1, glm::vec3 const& p2, glm::vec3 const& p3, glm::vec2 const& pos)
{
  float const det = (p2.z - p3.z) * (p1.x - p3.x) + (p3.x - p2.x) * (p1.z - p3.z);
  float const l1  = ((p2.z - p3.z) * (pos.x - p3.x) + (p3.x - p2.x) * (pos.y - p3.z)) / det;
  float const l2  = ((p3.z - p1.z) * (pos.x - p3.x) + (p1.x - p3.x) * (pos.y - p3.z)) / det;
  float const l3  = 1.0f - l1 - l2;
  return l1 * p1.y + l2 * p2.y + l3 * p3.y;
}

} // namespace

////////////////////////////////////////////////////////////////////////////////////////////////////
// TerrainTextureNames
std::string
TerrainTextureNames::to_string() const
{
  std::stringstream sstr;
  sstr << "heightmap_path: " << heightmap_path;
  sstr << ", textures: ";

  APPEND_COMMA_SEPERATED_LIST(sstr, textures, [](auto const& t) { return t; })
  return sstr.str();
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// Terrain
Terrain::Terrain(TerrainConfig const& tc, glm::vec2 const& pos, draw_state&& di, shader_type& sp,
                 Heightmap&& hmap)
    : TerrainConfig(tc)
    , pos_(pos)
    , di_(MOVE(di))
    , sp_(&sp)
    , heightmap(MOVE(hmap))
{
}

std::string const&
Terrain::texture_name(size_t const index) const
{
  auto const& names = texture_names;
  assert(index < names.textures.size());
  return names.textures[index];
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// TerrainGrid
TerrainGrid::TerrainGrid(TerrainGridConfig const& tgc)
    : TerrainGridConfig(tgc)
{
  terrain_.reserve(num_rows() * num_cols());
}

glm::vec2
TerrainGrid::max_xz() const
{
  // The last Terrain in our array will be the further "away" (x,z) coordinates of all the
  // terrains.
  assert(!terrain_.empty());
  auto const& last = terrain_.back();
  auto const& dim  = dimensions();

  return (last.position() * dim) + dim;
}

void
TerrainGrid::add(Terrain&& t)
{
  terrain_.emplace_back(MOVE(t));
}

TerrainOutOfBoundsResult
TerrainGrid::out_of_bounds(float const x, float const z) const
{
  auto const max_pos = max_xz();
  bool const x_oob   = x >= max_pos.x || x < 0;
  bool const y_oob   = z >= max_pos.y || z < 0;
  return TerrainOutOfBoundsResult{x_oob, y_oob};
}

float
TerrainGrid::get_height(log_t& LOGGER, float const x, float const z) const
{
  if (out_of_bounds(x, z)) {
    auto const max_pos = max_xz();
    LOG_ERROR("Out of bounds pos:{%f, %f} max world pos:{%f, %f}", x, z, max_pos.x, max_pos.y);
    return 0.0f;
  }
  auto const& dim = dimensions();

  auto const get_terrain_under_coords = [&]() -> Terrain const& {
    // Determine which Terrain instance the world coordinates (x, z) fall into.
    auto const xcoord = static_cast<size_t>(x / dim.x);
    auto const zcoord = static_cast<size_t>(z / dim.y);

    size_t const terrain_index = (num_rows() * zcoord) + xcoord;
    return terrain_[terrain_index];
  };

  auto const& t       = get_terrain_under_coords();
  auto const  t_pos   = t.position();
  float const local_x = x - (t_pos.x * dim.x);
  float const local_z = z - (t_pos.y * dim.y);
  assert(!out_of_bounds(local_x, local_z));

  auto const num_vertexes_minus1 = t.num_vertexes_along_one_side - 1;

  assert(math::float_cmp(dim.x, dim.y));
  float const grid_squaresize = dim.x / static_cast<float>(num_vertexes_minus1);

  auto const grid_x = static_cast<size_t>(glm::floor(local_x / grid_squaresize));
  auto const grid_z = static_cast<size_t>(glm::floor(local_z / grid_squaresize));

  float const x_coord = ::fmodf(local_x, grid_squaresize) / grid_squaresize;
  float const z_coord = ::fmodf(local_z, grid_squaresize) / grid_squaresize;

  glm::vec3       p1, p2, p3;
  glm::vec2 const p4 = glm::vec2{x_coord, z_coord};
  {
    auto const& hmap = t.heightmap;
    if (x_coord <= (1.0f - z_coord)) {
      p1 = glm::vec3{0.0f, hmap.data(grid_x, grid_z), 0.0f};
      p2 = glm::vec3{1.0f, hmap.data(grid_x + 1, grid_z), 0.0f};
      p3 = glm::vec3{0.0f, hmap.data(grid_x, grid_z + 1), 1.0f};
    }
    else {
      p1 = glm::vec3{1.0f, hmap.data(grid_x + 1, grid_z), 0.0f};
      p2 = glm::vec3{1.0f, hmap.data(grid_x + 1, grid_z + 1), 1.0f};
      p3 = glm::vec3{0.0f, hmap.data(grid_x, grid_z + 1), 1.0f};
    }
  }
  float const theight         = barry_centric(p1, p2, p3, p4);
  float const bitmap_adjusted = theight / 255.0f;
  return bitmap_adjusted * t.height_multiplier;
}

namespace terrain
{
TerrainGrid
generate_grid(log_t& LOGGER, TerrainGridConfig const& tgc, TerrainConfig const& tc,
              Heightmap const& heightmap, shader_type& sp)
{
  LOG_TRACE("Generating Terrain");
  size_t const rows = tgc.num_rows, cols = tgc.num_cols;
  TerrainGrid  tgrid{tgc};

  FOR(j, rows)
  {
    FOR(i, cols)
    {
      auto const pos  = glm::vec2{i, j};
      auto       hmap = heightmap.clone();
      auto       t    = generate_piece(LOGGER, pos, tgc, tc, MOVE(hmap), sp);
      tgrid.add(MOVE(t));
    }
  }

  LOG_TRACE("Finished Generating Terrain");
  return tgrid;
}

} // namespace terrain
