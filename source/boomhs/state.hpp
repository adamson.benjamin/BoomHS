#pragma once
#include <boomhs/audio.hpp>
#include <boomhs/game_config.hpp>
#include <boomhs/leveldata.hpp>
#include <boomhs/screen.hpp>

#include <boomhs/ui.hpp>
#include <boomhs/ui_buffers_classic.hpp>

#include <extlibs/glm.hpp>
#include <optional>

class rng_t;
class WallClock;

struct GameState
{
  bool&      quit;
  WallClock& wclock;
  rng_t&     rng;

  LevelData        ldata;
  WaterAudioSystem was;

  bool game_running = false;

  UiBuffersClassic uibuffers_classic;
  ScreenStack      sstack;

  GraphicsSettings graphics;

  // Current player movement vector
  // MovementState movement_state = {};

public:
  MOVE_CONSTRUCTIBLE_ONLY(GameState);
  GameState(bool& q, WallClock& wc, rng_t& rngp, LevelData&& ld, WaterAudioSystem&& wasp,
            GraphicsSettings const gsettings)
      : quit(q)
      , wclock(wc)
      , rng(rngp)
      , ldata(MOVE(ld))
      , was(MOVE(wasp))
      , uibuffers_classic()
      , graphics(gsettings)
  {
  }
};
