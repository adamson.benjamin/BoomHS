#pragma once
#include <boomhs/color.hpp>
#include <boomhs/event.hpp>
#include <boomhs/font.hpp>
#include <boomhs/screen.hpp>
#include <boomhs/ui_controls.hpp>

#include <common/macros_class.hpp>
#include <common/macros_container.hpp>
#include <common/result.hpp>
#include <common/timer.hpp>

#include <math/frustum.hpp>
#include <math/rectangle.hpp>

#include <extlibs/glm.hpp>

#include <memory>
#include <string>
#include <vector>

struct log_t;
struct Camera;
class DeltaTime;
struct Devices;
struct GameState;
class MouseState;
struct FontInfo;
struct Frustum;
struct SDLEventProcessArgs;

namespace opengl
{
class font_storage;
struct render_state;
class texture_storage;
} // namespace opengl

struct MainMenuScreen final : public Screen
{
  FontInfo finfo;

  RectFloat (*compute_rect)(Frustum const&) = [](Frustum const& fr) {
    return fr.rect_float() / 2.0f;
  };

  common::TwoStateTimer blink_timer;

public:
  MainMenuScreen(ScreenType const st)
      : Screen(st)
  {
  }

  void draw(log_t&, GameState&, opengl::render_state&);
  void update(log_t&, GameState&, Mouse const&);

public:
  OnKeyDownFN<MainMenuScreen> on_key_down = event_handler::noop;
  OnKeyDownFN<MainMenuScreen> on_key_up   = event_handler::noop;

  OnMouseButtonDownFN<MainMenuScreen> on_mouse_button_down = event_handler::noop;
  OnMouseButtonUpFN<MainMenuScreen>   on_mouse_button_up   = event_handler::noop;

  OnMouseMotionFN<MainMenuScreen> on_mouse_motion = event_handler::noop;
  OnMouseWheelFN<MainMenuScreen>  on_mouse_wheel  = event_handler::noop;
};

namespace main_menu
{
Result<ScreenVector, std::string>
create_list_screens(log_t&, GameState&, Frustum const&);
} // namespace main_menu
