#include "chat_history.hpp"
#include <boomhs/delta_time.hpp>
#include <boomhs/mouse.hpp>

#include <vector>

namespace
{
void
sanitize(std::string& t)
{
  common::trim(t);
  t.erase(std::remove(t.begin(), t.end(), '\n'), t.end());
  {
    auto const both_spaces = [](char const a, char const b) { return (a == b) && (a == ' '); };
    auto const new_end     = std::unique(t.begin(), t.end(), both_spaces);
    t.erase(new_end, t.end());
  }
}

} // namespace

Channel const*
ChatHistory::find_channel(ChannelId const id) const
{
  Channel const* p = nullptr;
  for (auto const& c : channels_) {
    if (c.id == id) {
      p = &c;
      break;
    }
  }
  return p;
}

bool
ChatHistory::has_channel(ChannelId const id) const
{
  auto const* pc = find_channel(id);
  return (pc == nullptr) ? false : true;
}

void
ChatHistory::add_channel(Channel const& c)
{
  assert(!has_channel(c.id));
  channels_.emplace_back(c);
}

Channel const&
ChatHistory::channels(ChannelId const id) const
{
  assert(has_channel(id));
  return channels_[id];
}

color4 const&
ChatHistory::channel_color(ChannelId const id) const
{
  auto const* pc = find_channel(id);
  assert(pc);

  return pc->color;
}

ChatMessage&
ChatHistory::insert_back(ChatMessage&& m)
{
  assert(has_channel(m.channel_id));
  return messages_.emplace_back(MOVE(m));
}

ChatMessage&
ChatHistory::insert_back(ChannelId const cid, std::string&& text)
{
  sanitize(text);
  return insert_back(ChatMessage{{MOVE(text)}, cid});
}

ChatMessage&
ChatHistory::insert_front(ChannelId const cid, std::string&& text)
{
  sanitize(text);

  auto const  b = std::begin(messages_);
  ChatMessage msg{{MOVE(text)}, cid};
  messages_.emplace(b, MOVE(msg));
  return messages_.front();
}

ChatMessages&
ChatHistory::all_messages()
{
  return messages_;
}

ChatMessages const&
ChatHistory::all_messages() const
{
  return messages_;
}

ChatMessages
ChatHistory::all_messages_in_channel(ChannelId const id) const
{
  ChatMessages filtered;
  for (auto const& m : all_messages()) {
    if (m.channel_id == id) {
      filtered.push_back(m);
    }
  }
  return filtered;
}

uint32_t
ChatHistory::num_channels() const
{
  auto const numc = channels_.size();
  assert(numc <= std::numeric_limits<uint32_t>::max());
  return static_cast<uint32_t>(numc);
}
