#include "level_loader.hpp"
#include <boomhs/billboard.hpp>
#include <boomhs/components.hpp>
#include <boomhs/entity.hpp>
#include <boomhs/font_loader.hpp>
#include <boomhs/material.hpp>
#include <boomhs/obj.hpp>
#include <boomhs/skybox.hpp>
#include <boomhs/tree.hpp>
#include <boomhs/water.hpp>

#include <math/random.hpp>

#include <common/algorithm.hpp>
#include <common/result.hpp>
#include <common/type_traits.hpp>

#include <opengl/shader.hpp>
#include <opengl/texture.hpp>

#include <common/os.hpp>

#include <extlibs/boost_algorithm.hpp>
#include <extlibs/toml.hpp>

using namespace common;
using namespace opengl;

namespace
{
std::optional<std::string_view>
get_stringview(std::string_view const& sv, toml::table const& table)
{
  auto const node = table[sv];
  if (!node) {
    return std::nullopt;
  }
  assert(node.is_string());
  return node.as_string()->get();
}

toml::array const*
get_array(std::string_view const& sv, toml::table const& table)
{
  return table.get_as<toml::array>(sv);
}

template <typename T>
std::optional<T>
get_floating_point(std::string_view const& sv, toml::table const& table)
{
  auto const& node = table[sv];
  if (!node) {
    return std::nullopt;
  }
  assert(node.is_floating_point());
  auto const fp = node.as_floating_point()->get();
  return static_cast<T>(fp);
}

template <typename R, typename FN, typename T, T... ints>
auto constexpr from_sequence(FN const& fn, std::integer_sequence<T, ints...>)
{
  size_t i = 0;
  R      arr;
  ((arr[i++] = fn(ints)), ...);

  // TODO: ? R{{(fn(ints), ...)}};
  return arr;
}

template <typename T, size_t N, typename R>
std::optional<R>
get_array_n(std::string_view const& name, toml::table const& table)
{
  static_assert(N > 0, "n must be >0");

  auto const nview = table[name];
  if (!nview) {
    return std::nullopt;
  }

  assert(nview.is_array());
  auto const& arr = *nview.as_array();
  assert(arr.is_homogeneous());
  assert(arr.size() == N);
  assert(arr[0].is_number());

  using itype = typename toml::array::size_type;
  if constexpr (std::is_floating_point_v<T>) {
    assert(arr[0].is_floating_point());
    auto const as_fp = [&](itype const i) { return static_cast<T>(arr[i].as_floating_point()->get()); };
    return from_sequence<R>(as_fp, std::make_integer_sequence<itype, N>{});
  }
  else if constexpr (std::is_integral_v<T>) {
    auto const as_int = [&](itype const i) { return static_cast<T>(arr[i].as_integer()->get()); };
    return from_sequence<R>(as_int, std::make_integer_sequence<itype, N>{});
  }
  else {
    static_assert(always_false<T>, "only floating-point and integral types are supported.");
  }
}

template <typename T>
auto constexpr get_array3 = get_array_n<T, 3, std::array<T, 3>>;

template <typename T>
auto constexpr get_array4 = get_array_n<T, 4, std::array<T, 4>>;

template <typename T>
auto constexpr get_vec3 = get_array_n<T, 3, glm::vec3>;

// template <typename T>
// auto constexpr get_vec4 = get_array_n<T, 4, glm::vec4>;

std::optional<bool>
get_bool(std::string_view const& name, toml::table const& table)
{
  auto const nview = table[name];
  if (!nview) {
    return std::nullopt;
  }
  assert(nview.is_boolean());
  return nview.as_boolean()->get();
}

std::optional<int64_t>
get_int64(std::string_view const& name, toml::table const& table)
{
  auto const nview = table[name];
  if (!nview) {
    return std::nullopt;
  }
  assert(nview.is_integer());
  return nview.as_integer()->get();
}
} // namespace

namespace
{
template <auto fn>
auto const&
get_or_abort_ref(std::string_view const& sv, toml::table const& table)
{
  auto const opt = fn(sv, table);
  assert(opt);
  return *opt;
}

template <auto fn>
auto
get_or_abort(std::string_view const& sv, toml::table const& table)
{
  auto const opt = fn(sv, table);
  assert(opt);
  return *opt;
}

auto constexpr get_string_or_abort = get_or_abort<get_stringview>;
auto constexpr get_array_or_abort  = get_or_abort_ref<get_array>;

template <typename T>
auto constexpr get_floating_point_or_abort = get_or_abort<get_floating_point<T>>;

template <typename T>
auto constexpr get_array3_or_abort = get_or_abort<get_array3<T>>;

template <typename T>
auto constexpr get_vec3_or_abort = get_or_abort<get_vec3<T>>;

template <typename T>
auto constexpr get_array4_or_abort = get_or_abort<get_array4<T>>;

auto constexpr get_int64_or_abort = get_or_abort<get_int64>;

/// unused

// template <typename T>
// auto constexpr get_vec4_or_abort = get_or_abort<get_vec4<T>>;

// auto constexpr get_bool_or_abort  = get_or_abort<get_bool>;
} // namespace

namespace
{
texture_and_filenames
read_3dtexture_filename(toml::table const& table, std::string_view const& name)
{
  auto const front  = std::string{get_string_or_abort("front", table)};
  auto const right  = std::string{get_string_or_abort("right", table)};
  auto const back   = std::string{get_string_or_abort("back", table)};
  auto const left   = std::string{get_string_or_abort("left", table)};
  auto const top    = std::string{get_string_or_abort("top", table)};
  auto const bottom = std::string{get_string_or_abort("bottom", table)};
  return texture_and_filenames{std::string{name}, {front, right, back, left, top, bottom}};
}

struct LoadedTextureInfo
{
  texture_and_filenames names;
  texture_ar            texture;
};

using RLTI = Result<LoadedTextureInfo, std::string>;
RLTI
parse_texture(log_t& LOGGER, toml::table const& textures)
{
  auto const* pname = textures.get("name");
  assert(pname && pname->is_string());
  auto const& name = pname->as_string()->get();

  auto const* ptype = textures.get("type");
  assert(ptype && ptype->is_string());
  auto const& type = ptype->as_string()->get();

  std::string_view const wrap_s = textures["wrap"].value_or("clamp_edge");
  GLint const            wrap   = texture::wrap_mode_from_string(LOGGER, wrap_s);

  auto const parse_2dtex = [&](GLenum const format) -> RLTI {
    texture_cfg cfg{GL_TEXTURE_2D};
    cfg.format = format;
    cfg.wrap   = wrap;

    auto const* pfilename = textures.get("filename");
    assert(pfilename && pfilename->is_string());
    auto const& filename = pfilename->as_string()->get();

    auto t  = TRY(texture::upload_2d_texture(LOGGER, filename, cfg));
    auto tn = texture_and_filenames{name, {filename}};
    return Ok(LoadedTextureInfo{MOVE(tn), MOVE(t)});
  };
  auto const parse_3dtex = [&](GLenum const format) -> RLTI {
    auto tn = read_3dtexture_filename(textures, name);

    texture_cfg cfg{GL_TEXTURE_CUBE_MAP};
    cfg.format = format;
    cfg.wrap   = wrap;

    auto t = TRY(texture::upload_3dcube_texture(LOGGER, tn.filenames, cfg));
    return Ok(LoadedTextureInfo{MOVE(tn), MOVE(t)});
  };

  if (type == "texture:3dcube-RGB") {
    return parse_3dtex(GL_RGB);
  }
  else if (type == "texture:3dcube-RGBA") {
    return parse_3dtex(GL_RGBA);
  }
  else if (type == "texture:2d-RGBA") {
    return parse_2dtex(GL_RGBA);
  }
  else if (type == "texture:2d-RGB") {
    return parse_2dtex(GL_RGB);
  }
  else {
    // TODO: implement more.
    LOG_ERROR("error, type is: %s", type);
    std::exit(EXIT_FAILURE);
  }
}

using resource_file_table = toml::table;

Result<texture_storage, std::string>
parse_textures(log_t& LOGGER, resource_file_table const& rf_table)
{
  texture_storage storage;
  auto const      parse_texture_table = [&](char const* name) -> Result<NOTHING, std::string> {
    auto const* ptextures = rf_table.get(name);
    assert(ptextures);
    assert(ptextures->is_array());
    auto const& textures = *ptextures->as_array();
    assert(textures.is_array());

    for (auto const& tex : *textures.as_array()) {
      assert(tex.is_table());
      auto lti = TRY(parse_texture(LOGGER, *tex.as_table()));
      storage.add(MOVE(lti.names), MOVE(lti.texture));
    }
    return OK_NONE;
  };
  TRY(parse_texture_table("texture"));
  TRY(parse_texture_table("font"));
  return OK_MOVE(storage);
}

Result<FontFiles, std::string>
parse_font_files(log_t& LOGGER, toml::table const& rf_table)
{
  auto const& array = get_array_or_abort("font", rf_table);

  FontFiles result;
  for (auto const& it : array) {
    assert(it.is_table());
    auto const& table        = *it.as_table();
    auto const  fnt          = get_string_or_abort("fnt", table);
    auto const  fnt_contents = TRY(common::read_file(fnt));

    auto ffile     = TRY(font::load_font_file(LOGGER, fnt_contents));
    ffile.name     = get_string_or_abort("name", table);
    ffile.img_file = get_string_or_abort("filename", table);
    ffile.padding  = get_array4<int>("padding", table).value_or(std::array{0, 0, 0, 0});
    result.emplace_back(MOVE(ffile));
  }
  return OK_MOVE(result);
}

Result<font_storage, std::string>
parse_fonts(log_t& LOGGER, texture_storage& storage, toml::table const& rf_table)
{
  auto         fonts = TRY(parse_font_files(LOGGER, rf_table));
  font_storage ft;
  for (auto&& f : fonts) {
    auto* pti = storage.find(f.name);
    assert(pti);

    font_and_texture ftexture{MOVE(f), *pti};
    ft.add(MOVE(ftexture));
  }
  return OK_MOVE(ft);
}

auto
parse_fog(log_t&, toml::table const& level_table)
{
  auto const& array = get_array_or_abort("fog", level_table);
  assert(1 == array.size());

  auto const& value = array[0];
  assert(value.is_table());

  auto const& fog_table = *value.as_table();

  auto const density  = get_floating_point_or_abort<float>("density", fog_table);
  auto const gradient = get_floating_point_or_abort<float>("gradient", fog_table);
  auto const color    = get_array4_or_abort<float>("color", fog_table);
  return Fog{density, gradient, color};
}

auto
parse_global_lighting(log_t&, toml::table const& level_table)
{
  auto const& array = get_array_or_abort("global-lighting", level_table);
  assert(1 == array.size());

  auto const& value = array[0];
  assert(value.is_table());

  auto const& ambient = get_array3_or_abort<float>("ambient", *value.as_table());
  return GlobalLight{ambient};
}

Result<ObjStore, std::string>
parse_meshes(log_t& LOGGER, resource_file_table const& rf_table)
{
  auto const* pmeshes = rf_table.get("meshes");
  if (!pmeshes) {
    return ERR_SPRINTF("no meshes");
  }
  assert(pmeshes->is_array());
  auto const& meshes = *pmeshes->as_array();

  ObjStore store;
  for (auto const& mesh : meshes) {
    assert(mesh.is_table());
    auto const& mt = *mesh.as_table();
    assert(2 == mt.size());

    auto const& name = (*mt.get("name")).as_string()->get();
    auto const& path = (*mt.get("path")).as_string()->get();

    auto const fs_path = std::filesystem::path{path};
    ObjData    objdata = TRY(load_objfile(LOGGER, fs_path / name).map_error(loadstatus_to_string));
    store.add_obj(name, MOVE(objdata));
  }
  return OK_MOVE(store);
}

auto
parse_attenuations(toml::table const& rf_table)
{
  auto const& table = get_array_or_abort("attenuation", rf_table);

  std::vector<NameAttenuation> result;
  for (auto const& it : table) {
    auto const& atable    = *it.as_table();
    auto const  name      = std::string{get_string_or_abort("name", atable)};
    auto const  constant  = get_floating_point_or_abort<float>("constant", atable);
    auto const  linear    = get_floating_point_or_abort<float>("linear", atable);
    auto const  quadratic = get_floating_point_or_abort<float>("quadratic", atable);
    result.emplace_back(NameAttenuation{name, Attenuation{constant, linear, quadratic}});
  }
  return result;
}

auto
parse_materials(toml::table const& rf_table)
{
  auto const& table = get_array_or_abort("material", rf_table);

  MaterialTable result;
  for (auto const& it : table) {
    assert(it.is_table());
    auto const& mtable    = *it.as_table();
    auto const  name      = std::string{get_string_or_abort("name", mtable)};
    auto const  ambient   = get_vec3_or_abort<float>("ambient", mtable);
    auto const  diffuse   = get_vec3_or_abort<float>("diffuse", mtable);
    auto const  specular  = get_vec3_or_abort<float>("specular", mtable);
    auto const  shininess = get_floating_point_or_abort<float>("shininess", mtable);

    Material     material{ambient, diffuse, specular, shininess};
    NameMaterial nm{name, MOVE(material)};
    result.add(MOVE(nm));
  }
  return result;
}

void
parse_entities(log_t& LOGGER, toml::table const& level_table, texture_storage& ttable, MaterialTable const& mtable,
               AttenuationList const& attenuations, ObjStore& obj_store, enttreg_t& reg, rng_t& rng)
{
  auto const parse_entity = [&](toml::node const& value) {
    // clang-format off

    assert(value.is_table());
    auto const& entity = *value.as_table();

    auto const name           = entity["name"].value_or("FromFileUnnamed");
    auto const shader         = get_string_or_abort(        "shader", entity);
    auto const geometry       = get_string_or_abort(        "geometry", entity);
    auto const pos            = get_vec3_or_abort<float>(   "position", entity);
    auto const scale_o        = get_vec3<float>(   "scale", entity);
    auto const rotation_o     = get_vec3<float>(   "rotation", entity);
    auto const color_o        = get_array4<float>(   "color", entity);
    auto const material_o     = entity["material"].value<std::string_view>();
    auto const texture_name_o = entity["texture"].value<std::string_view>();
    auto const texture_uvs    = get_array4<int64_t>("uvs", entity).value_or(std::array<int64_t, 4>{0, 0, 1, 1});
    auto const is_hidden      = get_bool(                   "hidden", entity).value_or(false);
    bool const random_junk    = get_bool(                   "random_junk_from_file", entity).value_or(false);

    // sub-tables or "inner"-tables
    auto const orbital_o           = entity.get_as<toml::table>("orbital-body");
    auto const pointlight_o        = entity.get_as<toml::table>("pointlight");
    auto const directional_light_o = entity.get_as<toml::table>("directional-light");
    // clang-format on

    // texture OR color fields, not both
    assert((!color_o && !texture_name_o) || (!color_o && texture_name_o) || (color_o && !texture_name_o));

    LOG_INFO("2, name: %s", name);
    auto eid = reg.create();
    LOG_INFO("2.5");
    reg.emplace<Name>(eid, name);
    LOG_INFO("3");

    auto& transform       = reg.emplace<Transform>(eid);
    transform.translation = pos;
    LOG_INFO("4");

    reg.emplace<IsRenderable>(eid, is_hidden);
    LOG_INFO("5");
    reg.emplace<ShaderComponent>(eid, shader);
    LOG_INFO("6");

    if (scale_o) {
      transform.scale = *scale_o;
    }
    if (rotation_o) {
      transform.rotate_xyz_degrees(*rotation_o);
    }

    if (random_junk) {
      reg.emplace<JunkEntityFromFILE>(eid);
    }

    if (orbital_o) {
      assert(texture_name_o);

      assert(orbital_o->is_table());
      auto const& orb = *orbital_o->as_table();
      {
        auto const x = get_floating_point_or_abort<float>("x", orb);
        auto const y = get_floating_point_or_abort<float>("y", orb);
        auto const z = get_floating_point_or_abort<float>("z", orb);

        auto const radius = glm::vec3{x, y, z};
        auto const offset = rng.gen(math::constants::TWO_PI);
        reg.emplace<OrbitalBody>(eid, radius, offset);
      }

      auto const glow_o = get_vec3<float>("glow", orb);
      if (glow_o) {
        reg.emplace<SunGlow>(eid, color::rgb(*glow_o));
      }

      if (directional_light_o) {
        auto& dlight = reg.emplace<DirectionalLight>(eid);

        auto const& dlo       = *directional_light_o;
        auto const  diffuse   = color::rgb(get_array3_or_abort<float>("diffuse", dlo));
        auto const  specular  = color::rgb(get_array3_or_abort<float>("specular", dlo));
        auto const  direction = get_vec3_or_abort<float>("direction", dlo);
        dlight.light          = Light{diffuse, specular};
        dlight.direction      = direction;
      }
    }
    LOG_INFO("7");

    if (scale_o) {
      transform.scale = *scale_o;
    }

    if (geometry == "cube") {
      auto&       cr  = reg.emplace<CubeRenderable>(eid);
      auto const* pcv = entity.get_as<toml::table>("cube_vertices");
      assert(pcv);
      auto const& cv = *pcv;
      cr.min         = get_vec3<float>("min", cv).value_or(glm::vec3{1.0f});
      cr.max         = get_vec3<float>("max", cv).value_or(glm::vec3{1.0f});
    }
    else if (boost::starts_with(geometry, "mesh")) {
      auto const parse_meshname = [](auto const& field) {
        auto const len = ::strlen("mesh:");
        assert(0 < len);
        return field.substr(len, field.length() - len);
      };
      auto mesh_name = parse_meshname(geometry);
      reg.emplace<MeshRenderable>(eid, MOVE(mesh_name));
    }
    else if (boost::starts_with(geometry, "billboard")) {
      auto const parse_billboard = [](auto const& field) {
        auto const len = ::strlen("billboard:");
        assert(0 < len);
        auto const str = field.substr(len, field.length() - len);
        return billboard::from_string(str);
      };
      auto& billboard = reg.emplace<BillboardRenderable>(eid);
      billboard.value = parse_billboard(geometry);
    }
    LOG_INFO("8");
    if (color_o) {
      reg.emplace<color4>(eid, *color_o);
    }
    LOG_INFO("9");
    if (texture_name_o) {
      auto const& tname = *texture_name_o;
      LOG_DEBUG("Looking up texture %s", tname);
      auto& tr        = reg.emplace<TextureRenderable>(eid);
      auto  texture_o = ttable.find(tname);
      assert(texture_o);

      tr.texture_info = &*texture_o;
      tr.uvs          = rect::create_float(texture_uvs);
    }
    LOG_INFO("10");

    if (pointlight_o) {
      auto const attenuation = get_string_or_abort("attenuation", *pointlight_o);

      auto const cmp = [&](NameAttenuation const& na) { return na.name == attenuation; };
      auto const it  = std::find_if(attenuations.cbegin(), attenuations.cend(), cmp);
      assert(it != attenuations.cend());

      auto& pl       = reg.emplace<PointLight>(eid);
      pl.attenuation = it->value;

      auto& light    = pl.light;
      light.diffuse  = color::rgb(get_array3_or_abort<float>("diffuse", *pointlight_o));
      light.specular = color::rgb(get_array3_or_abort<float>("specular", *pointlight_o));
    }
    LOG_INFO("11");

    if (name == "TreeLowpoly") {
      auto& obj = obj_store.get(LOGGER, reg.get<MeshRenderable>(eid).name);
      auto& tc  = reg.emplace<TreeComponent>(eid, obj);
      tc.add_color(Treecolor4Type::Leaf, LOC4::GREEN);
      tc.add_color(Treecolor4Type::Leaf, LOC4::PINK);
      tc.add_color(Treecolor4Type::Trunk, LOC4::BROWN);
    }
    LOG_INFO("12");
    if (name == "Tree2") {
      auto& obj = obj_store.get(LOGGER, reg.get<MeshRenderable>(eid).name);
      auto& tc  = reg.emplace<TreeComponent>(eid, obj);
      tc.add_color(Treecolor4Type::Leaf, LOC4::YELLOW);
      tc.add_color(Treecolor4Type::Stem, LOC4::RED);
      tc.add_color(Treecolor4Type::Stem, LOC4::BLUE);
      tc.add_color(Treecolor4Type::Trunk, LOC4::GREEN);
    }
    LOG_INFO("13");

    // An object receives light, if it has ALL ambient/diffuse/specular fields
    if (material_o) {
      auto const& material      = mtable.find(*material_o);
      reg.emplace<Material>(eid) = material;
    }
    LOG_INFO("14");
  };

  auto const& entity_table = get_array_or_abort("entity", level_table);
  for (auto const& it : entity_table) {
    parse_entity(it);
  }
}

struct LoadShaderInfo
{
  shader_type sp;
  bool const  is_2d;
};

using VertexAttributesMap = std::map<std::string, vertex_attribute>;

using LoadResult = Result<LoadShaderInfo, std::string>;
LoadResult
parse_shader(log_t& LOGGER, VertexAttributesMap const& va_map, NumLights const num_lights, toml::table const& table)
{
  auto       name     = std::string{get_string_or_abort("name", table)};
  auto       vertex   = std::string{get_string_or_abort("vertex", table)};
  auto       fragment = std::string{get_string_or_abort("fragment", table)};
  auto const va_name  = std::string{get_string_or_abort("va", table)};

  auto it = va_map.find(va_name);
  assert(va_map.cend() != it);

  shader_cfg spc{MOVE(name), MOVE(vertex), MOVE(fragment), it->second};
  spc.is_2d          = get_bool("is_2d", table).value_or(false);
  spc.instance_count = get_int64("instance_count", table);

  auto program = TRY(shader::from_files(LOGGER, MOVE(spc), num_lights));
  return Ok(LoadShaderInfo{MOVE(program), spc.is_2d});
}

auto
parse_vertex_attributes(toml::table const& engine_table)
{
  auto const read_data = [&](toml::table const& table, char const* fieldname,
                             size_t& index) -> std::optional<attribute_pointer> {
    auto const* pxx = table.get_as<toml::table>(fieldname);
    if (!pxx) {
      return std::nullopt;
    }
    auto const& data_table = *pxx;
    auto const  datatype_s = get_string_or_abort("datatype", data_table);

    // TODO: FOR NOW, only support floats. Easy todo rest
    assert("float" == datatype_s);
    auto const datatype = GL_FLOAT;
    auto const num      = get_int64_or_abort("num", data_table);
    assert(num <= std::numeric_limits<GLsizei>::max());
    auto const component_count = static_cast<GLsizei>(num);

    auto const        uint_index     = static_cast<GLuint>(index);
    auto const        attribute_type = attribute_type_from_string(fieldname);
    attribute_pointer api{uint_index, datatype, attribute_type, component_count};

    ++index;
    return std::make_optional(MOVE(api));
  };

  auto const add_next_found = [&read_data](auto& apis, toml::table const& table, char const* fieldname, size_t& index) {
    auto       data_o    = read_data(table, fieldname, index);
    bool const data_read = !!data_o;
    if (data_read) {
      auto data = MOVE(*data_o);
      apis.emplace_back(MOVE(data));
    }
  };

  VertexAttributesMap va_map;
  auto const*         pvas_array = engine_table.get_as<toml::array>("vas");
  assert(pvas_array);
  for (auto const& it : *pvas_array) {
    assert(it.is_table());
    auto const& vtable = *it.as_table();

    size_t                         i = 0u;
    std::vector<attribute_pointer> apis;
    add_next_found(apis, vtable, "position", i);
    add_next_found(apis, vtable, "normal", i);
    add_next_found(apis, vtable, "color", i);
    add_next_found(apis, vtable, "uv", i);

    auto const& name_node = vtable["name"];
    assert(name_node.is_string());
    auto const& name = name_node.as_string()->get();
    assert(!va_map.contains(name));
    va_map.emplace(name, make_vertex_attribute(apis));
  }
  return va_map;
}

Result<NOTHING, std::string>
parse_shaders(log_t& LOGGER, enttreg_t& reg, NumLights const num_lights, toml::table const& etable)
{
  auto& sps_2d = reg.set<shader_cache_2d>();
  auto& sps_3d = reg.set<shader_cache_3d>();
  TRY(shader3d::set_num_lights(LOGGER, reg, num_lights));

  auto const* pshaders_table = etable.get_as<toml::array>("shaders");
  assert(pshaders_table);

  auto pvas = parse_vertex_attributes(etable);

  for (auto const& it : *pshaders_table) {
    assert(it.is_table());
    auto const& v = *it.as_table();

    auto       lr    = TRY(parse_shader(LOGGER, pvas, num_lights, v));
    bool const is_2d = lr.is_2d;
    auto       sp    = MOVE(lr.sp);

    if (is_2d) {
      sps_2d.add(MOVE(sp));
    }
    else {
      sps_3d.add(MOVE(sp));
    }
  }
  return OK_NONE;
}

} // namespace

Result<LevelAssets, std::string>
load_level(log_t& LOGGER, enttreg_t& reg, rng_t& rng, std::string const& filename)
{
  LOG_INFO("loading level: %s", filename);
  auto presult = toml::parse_file("levels/resources.toml");
  if (!presult) {
    return ERR_SPRINTF("Error parsing file resources.toml error=%s", presult.error());
  }
  auto const& rf_table = presult.get();
  ObjStore    objstore = TRY(parse_meshes(LOGGER, rf_table));
  LOG_INFO("num mehes=%lu", objstore.size());

  LOG_INFO("textures ...");
  auto ttable = TRY(parse_textures(LOGGER, rf_table));
  assert(!ttable.empty());

  LOG_INFO("fonts ...");
  auto fonts = TRY(parse_fonts(LOGGER, ttable, rf_table));

  LOG_INFO("materials ...");
  auto mtable = parse_materials(rf_table);
  assert(!mtable.empty());

  LOG_INFO("attenuations ...");
  auto attenuations = parse_attenuations(rf_table);
  assert(!attenuations.empty());

  auto const sn     = "levels/" + filename;
  auto       plevel = toml::parse_file(sn.c_str());
  if (!plevel) {
    return ERR_SPRINTF("Error parsing file resources.toml error=%s", plevel.error());
  }
  auto const& level_table = plevel.get();

  LOG_INFO("global lighting ...");
  auto glight = parse_global_lighting(LOGGER, level_table);

  LOG_INFO("global fog ...");
  auto fog = parse_fog(LOGGER, level_table);

  LOG_INFO("entities ...");
  parse_entities(LOGGER, level_table, ttable, mtable, attenuations, objstore, reg, rng);

  auto const num_dirlights   = find_dirlights(reg).size();
  auto const num_pointlights = find_pointlights(reg).size();
  LOG_ERROR("ndl %u, npl: %u", num_dirlights, num_pointlights);

  auto eresult = toml::parse_file("engine.toml");
  if (!eresult) {
    return ERR_SPRINTF("Error parsing file engine.toml error=%s", eresult.error());
  }
  auto const& etable = eresult.get();

  NumLights const num_lights{num_dirlights, num_pointlights};
  TRY(parse_shaders(LOGGER, reg, num_lights, etable));

  LOG_INFO("loading level finished successfully!");
  LevelAssets assets{MOVE(glight),   MOVE(fog),    MOVE(mtable), MOVE(attenuations),
                     MOVE(objstore), MOVE(ttable), MOVE(fonts)};
  return OK_MOVE(assets);
}
