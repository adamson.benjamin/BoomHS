#pragma once
#include <array>
#include <boomhs/terrain.hpp>
#include <common/macros_class.hpp>

struct SunshaftBuffer
{
  float blur_width  = -0.85f;
  int   num_samples = 50;
};

struct SkyboxBuffer
{
  int selected_day   = -1;
  int selected_night = -1;
};

struct TerrainBuffer
{
  int selected_winding  = 0;
  int selected_culling  = 0;
  int selected_wrapmode = 0;
  int selected_terrain  = 0;

  int selected_heightmap = -1;

  std::array<int, 5> selected_textures = {-1, -1, -1, -1, -1};
  int                selected_shader   = -1;

  TerrainConfig     terrain_config;
  TerrainGridConfig grid_config;
};

struct AudioUiBuffer
{
  float ambient = 0.227f;
};

struct WaterBuffer
{
  bool draw               = true;
  int  selected_waterinfo = -1;

  float refract_reflect_ratio = 1.0f;
  float depth_divider         = 50.0f;

  float weight_light      = 0.50f;
  float weight_texture    = 0.70f;
  float weight_mix_effect = 1.00f;
};

struct LogBuffer
{
  int log_level = 2;
};

struct UiDebugState
{
  bool lock_debugselected       = false;
  bool disable_controller_input = true;
  bool player_collision         = false;
  bool mariolike_edges          = false;
  bool update_orbital_bodies    = true;

  bool draw_debugwindow_mainmenu = true;

  bool draw_debug_ui  = true;
  bool draw_ingame_ui = true;

  bool draw_bounding_boxes            = true;
  bool draw_view_frustum              = false;
  bool draw_2d_billboard_entities     = true;
  bool draw_3d_entities               = true;
  bool draw_2dtextured_rect           = true;
  bool draw_terrain                   = true;
  bool draw_normals                   = false;
  bool draw_skybox                    = true;
  bool show_global_axis               = false;
  bool show_player_localspace_vectors = false;
  bool show_player_worldspace_vectors = false;
  bool wireframe_override             = false;

  struct GridLinesInfo
  {
    bool      show       = false;
    glm::vec3 dimensions = VEC3{10, 1, 10};
  };
  GridLinesInfo grid_lines;
};

struct UiBuffersClassic
{
  AudioUiBuffer audio;
  UiDebugState  debug;
  LogBuffer     log;

  SunshaftBuffer sunshaft;
  SkyboxBuffer   skybox;
  TerrainBuffer  terrain;
  WaterBuffer    water;
};
