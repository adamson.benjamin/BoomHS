#pragma once
#include <boomhs/heightmap.hpp>
#include <opengl/draw_info.hpp>

#include <common/FOR.hpp>
#include <common/algorithm.hpp>
#include <common/macros_class.hpp>
#include <common/macros_container.hpp>

#include <extlibs/glm.hpp>

#include <array>
#include <functional>
#include <vector>

struct log_t;

namespace opengl
{
class shader_type;
} // namespace opengl

struct TerrainTextureNames
{
  std::string              heightmap_path;
  std::vector<std::string> textures;

  DEFINE_VECTOR_LIKE_WRAPPER_FNS(textures)
  std::string to_string() const;
};

struct TerrainConfig
{
  size_t num_vertexes_along_one_side = 128;
  float  height_multiplier           = 1;
  bool   invert_normals              = false;
  bool   tile_textures               = false;

  GLint wrap_mode   = GL_MIRRORED_REPEAT;
  float uv_modifier = 1.0f;

  std::string         shader_name = "terrain";
  TerrainTextureNames texture_names;
};

class Terrain final : public TerrainConfig
{
  glm::vec2            pos_;
  opengl::draw_state   di_;
  opengl::shader_type* sp_;

public:
  MOVE_DEFAULT_ONLY(Terrain);
  Terrain(TerrainConfig const&, glm::vec2 const&, opengl::draw_state&&, opengl::shader_type&,
          Heightmap&&);

public:
  Heightmap heightmap;

  auto const& draw_info() const { return di_; }

  auto const& position() const { return pos_; }

  std::string const& texture_name(size_t) const;

  auto& shader() const { return *sp_; }
};

// The result of checking whether a position is outside of the terrain grid.
//
// It is implicitely convertible to a bool for easy checking, or you can check the individual
// components if you need.
struct TerrainOutOfBoundsResult
{
  bool const x;
  bool const z;

public:
  bool     is_out() const { return x || z; }
  explicit operator bool() const { return is_out(); }
};

struct TerrainGridConfig
{
  size_t    num_rows = 1;
  size_t    num_cols = 1;
  glm::vec2 dimensions{20, 20};
};

using TerrainData = std::vector<Terrain>;
class TerrainGrid final : private TerrainGridConfig
{
  TerrainData terrain_;

public:
  MOVE_DEFAULT_ONLY(TerrainGrid);
  explicit TerrainGrid(TerrainGridConfig const&);

public:
  DEFINE_VECTOR_LIKE_WRAPPER_FNS(terrain_)

public:
  auto const& dimensions() const { return TerrainGridConfig::dimensions; }
  auto        num_cols() const { return TerrainGridConfig::num_cols; }
  auto        num_rows() const { return TerrainGridConfig::num_rows; }

  glm::vec2 max_xz() const;

  void add(Terrain&&);
  auto count() const { return terrain_.size(); }

  float                    get_height(log_t&, float, float) const;
  TerrainOutOfBoundsResult out_of_bounds(float, float) const;
};

namespace terrain
{
TerrainGrid
generate_grid(log_t&, TerrainGridConfig const&, TerrainConfig const&, Heightmap const&,
              opengl::shader_type&);

} // namespace terrain
