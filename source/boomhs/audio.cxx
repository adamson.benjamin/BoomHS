#include "audio.hpp"
#include <common/breakpoint.hpp>
#include <common/log.hpp>
#include <common/macros_scope_exit.hpp>

#include <extlibs/sdl.hpp>
#include <type_traits>

#ifdef DEBUG_BUILD
#define AL_CHECK(stmt)                                                                             \
  [&]() {                                                                                          \
    stmt;                                                                                          \
    ::al_checkerrors(LOGGER, #stmt, __FILE__, __LINE__);                                           \
  }()
#else
#define AL_CHECK(stmt) stmt
#endif

namespace
{
ALsizei constexpr NUM_BUFFERS = 1;
ALsizei constexpr NUM_SOURCES = 1;

constexpr char const*
al_error_tostring(ALenum const err)
{
  switch (err) {
  case AL_NO_ERROR:
    return nullptr;
  case AL_INVALID_NAME:
    return "AL_INVALID_NAME: a bad name(ID) was passed to an OpenAL function";
  case AL_INVALID_ENUM:
    return "AL_INVALID_ENUM: an invalid enum value was passed to an OpenAL function";
  case AL_INVALID_VALUE:
    return "AL_INVALID_VALUE: an invalid value was passed to an OpenAL function";
  case AL_INVALID_OPERATION:
    return "AL_INVALID_OPERATION: the requested operation is not valid";
  case AL_OUT_OF_MEMORY:
    return "AL_OUT_OF_MEMORY: the requested operation resulted in OpenAL running out of memory ";
  }
  std::exit(EXIT_FAILURE);
}

void
al_checkerrors(log_t& LOGGER, char const* stmt, const char* file, int const line)
{
  auto const err = ::alGetError();
  if (AL_NO_ERROR != err) {
    LOG_ERROR("OpenAL error: %s %s:%i error: %s", stmt, file, line, al_error_tostring(err));
    common::cpu_breakpoint();
  }
}

void
al_checkerrors(log_t* pLOGGER, char const* stmt, const char* file, int const line)
{
  al_checkerrors(*pLOGGER, stmt, file, line);
}

} // namespace

namespace
{
void
attach_source_to_buffer(log_t& LOGGER, audio_source& src, audio_buffer& buf)
{
  AL_CHECK(::alSourcei(src.handle, AL_BUFFER, static_cast<ALint>(buf.handle)));
}

void
detach_source_from_all_buffers(log_t& LOGGER, audio_source& src)
{
  AL_CHECK(::alSourcei(src.handle, AL_BUFFER, 0));
}

// bool
// enumerating_devices_supported()
//{
//  ALboolean const enumeration = ::alcIsExtensionPresent(nullptr, "ALC_ENUMERATION_EXT");
//  return enumeration == AL_TRUE;
//}

// std::vector<std::string>
// get_all_devices(log_t& LOGGER)
//{
//  auto* devices_str = ::alcGetString(nullptr, ALC_DEVICE_SPECIFIER);
//
//  ALCchar const *pdevs = devices_str, *next = devices_str + 1;
//
//  std::vector<std::string> device_names;
//  while (pdevs && *pdevs != '\0' && next && *next != '\0') {
//    device_names.emplace_back(std::string{pdevs});
//    size_t const len = ::strlen(pdevs);
//    pdevs += (len + 1);
//    next += (len + 2);
//  }
//  return device_names;
//}

ALenum
waveinfo_to_al_format(short const channels, short const bit_size)
{
  bool const stereo = (channels > 1);

  switch (bit_size) {
  case 16:
    if (stereo) {
      return AL_FORMAT_STEREO16;
    }
    else {
      return AL_FORMAT_MONO16;
    }
  case 8:
    if (stereo) {
      return AL_FORMAT_STEREO8;
    }
    else {
      return AL_FORMAT_MONO8;
    }
  default:
    // TODO: implement
    std::exit(EXIT_FAILURE);
  }
}

Result<NOTHING, std::string>
fill_buffer_data(log_t& LOGGER, char const* path, audio_buffer& buffer)
{
  SDL_AudioSpec wav_spec;
  uint32_t      wav_length;
  uint8_t*      wav_buffer;

  if (nullptr == ::SDL_LoadWAV(path, &wav_spec, &wav_buffer, &wav_length)) {
    return Err(fmt::sprintf("Could not open '%s' error: %s\n", path, SDL_GetError()));
  }
  ON_SCOPE_EXIT([wav_buffer]() { ::SDL_FreeWAV(wav_buffer); });

  auto const bitsize = SDL_AUDIO_BITSIZE(wav_spec.format);
  assert(bitsize < std::numeric_limits<short>::max());
  auto const bitsize_short = static_cast<short>(bitsize);

  auto const format = waveinfo_to_al_format(wav_spec.channels, bitsize_short);

  auto const signed_wavlength = static_cast<ALsizei>(wav_length);
  AL_CHECK(::alBufferData(buffer.handle, format, wav_buffer, signed_wavlength, wav_spec.freq));
  return OK_NONE;
}

} // namespace

audio_source::audio_source(log_t& logger)
    : LOGGER(&logger)
{
  AL_CHECK(::alGenSources(NUM_SOURCES, &handle));
}

audio_source::~audio_source() { ::alDeleteSources(NUM_SOURCES, &handle); }

audio_buffer::audio_buffer(log_t& logger)
    : LOGGER(&logger)
{
  AL_CHECK(::alGenBuffers(NUM_BUFFERS, &handle));
}

audio_buffer::~audio_buffer() { AL_CHECK(::alDeleteBuffers(NUM_BUFFERS, &handle)); }

void
audio_source::set_looping(bool const loop)
{
  AL_CHECK(::alSourcei(handle, AL_LOOPING, loop ? AL_TRUE : AL_FALSE));
}

void
audio_source::set_volume(float const v)
{
  AL_CHECK(::alSourcef(handle, AL_GAIN, v));
}

void
audio_source::set_pitch(float const p)
{
  AL_CHECK(::alSourcef(handle, AL_PITCH, p));
}

void
audio_source::set_position(float const x, float const y, float const z)
{
  AL_CHECK(::alSource3f(handle, AL_POSITION, x, y, z));
}

void
audio_source::set_position(glm::vec3 const& pos)
{
  set_position(pos.x, pos.y, pos.z);
}

void
audio_source::set_velocity(float const x, float const y, float const z)
{
  AL_CHECK(::alSource3f(handle, AL_VELOCITY, x, y, z));
}

void
audio_source::set_velocity(glm::vec3 const& vel)
{
  set_velocity(vel.x, vel.y, vel.z);
}

void
audio_source::pause()
{
  AL_CHECK(::alSourcePause(handle));
}

void
audio_source::play()
{
  AL_CHECK(::alSourcePlay(handle));
}

void
audio_source::stop()
{
  AL_CHECK(::alSourceStop(handle));
}

bool
audio_source::is_playing() const
{
  ALint value;
  AL_CHECK(::alGetSourcei(handle, AL_SOURCE_STATE, &value));
  return value == AL_PLAYING;
}

audio_object::audio_object(log_t& logger, audio_buffer& b, audio_source& s)
    : LOGGER_(&logger)
    , source_(&s)
    , buffer_(&b)
{
}
void
destroy_audio_object(audio_object& ao)
{
  auto& source = ao.source();
  source.stop();
  detach_source_from_all_buffers(ao.LOGGER(), source);
}

WaterAudioSystem::WaterAudioSystem(log_t& logger, audio_buffer& b, audio_source& s)
    : LOGGER(logger)
    , ao_(logger, b, s)
{
}

bool
WaterAudioSystem::is_playing_watersound() const
{
  return ao_->source().is_playing();
}

void
WaterAudioSystem::play_inwater_sound()
{
  if (!source().is_playing()) {
    source().play();
    LOG_INFO("RESTARTING SOUND");
  }
}

void
WaterAudioSystem::stop_inwater_sound()
{
  source().stop();
}

void
WaterAudioSystem::set_volume(float const v)
{
  source().set_volume(v);
}

namespace audio
{
Result<OpenALState, char const*>
init(log_t& LOGGER)
{
  auto* pdevice = ::alcOpenDevice(nullptr);
  if (!pdevice) {
    return Err("Error opening openal device");
  }
  ALCcontext* pctx = ::alcCreateContext(pdevice, nullptr);
  if (!pctx) {
    return Err("Error making openal context current");
  }
  AL_CHECK(::alcMakeContextCurrent(pctx));
  return Ok(OpenALState{pdevice, pctx});
}

void
shutdown(log_t& LOGGER, OpenALState& state)
{
  LOG_TRACE("AUDIO SHUTDOWN");

  // After this point, we cannot call AL_CHECK() as ::alGetError() requires a valid context set.
  // Any call to AL_CHECK() (which calls ::alGetError()) would always return AL_INVALID_OPERATION
  // which is obviously not useful.
  ::alcMakeContextCurrent(nullptr);

  assert(state.pdevice);
  ::alcCloseDevice(state.pdevice);

  assert(state.pcontext);
  ::alcDestroyContext(state.pcontext);
}

Result<WaterAudioSystem, std::string>
create_water_audio(log_t& LOGGER, audio_buffer& buffer, audio_source& source)
{
  WaterAudioSystem was{LOGGER, buffer, source};

  source.set_pitch(1.0f);
  source.set_volume(1.0f);
  source.set_position(0, 0, 0);
  source.set_velocity(2, 0, 0);
  source.set_looping(false);

  constexpr char const* FILEPATH = "assets/audio/water stream.wav";

  // The buffer must be filled with data before it as associated with a source
  TRY(fill_buffer_data(LOGGER, FILEPATH, buffer));
  attach_source_to_buffer(LOGGER, source, buffer);
  return OK_MOVE(was);
}

} // namespace audio
