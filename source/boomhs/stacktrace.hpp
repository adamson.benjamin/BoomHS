#pragma once

struct log_t;

namespace stacktrace
{
void
check_on_disk(log_t&);

void
dump_to_file(log_t&);

} // namespace stacktrace