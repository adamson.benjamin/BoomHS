#pragma once
#include <boomhs/delta_time.hpp>
#include <boomhs/device.hpp>
#include <boomhs/mouse.hpp>
#include <boomhs/world_object.hpp>

#include <math/matrices.hpp>
#include <math/spherical.hpp>

#include <common/macros_class.hpp>

#include <array>

struct Frustum;
struct ScreenSize;
struct ViewSettings
{
  float field_of_view;
};

enum class CameraMode
{
  ThirdPerson = 0,
  FPS,

  Ortho,
  Fullscreen_2DUI,

  FREE_FLOATING,
  MAX
};

struct CameraModes
{
  using ModeNamePair = std::pair<CameraMode, char const*>;
  CameraModes()      = delete;

  static auto constexpr CAMERA_MODES = {
      ModeNamePair{CameraMode::ThirdPerson, "ThirdPerson"}, ModeNamePair{CameraMode::FPS, "FPS"},
      ModeNamePair{CameraMode::Ortho, "Ortho"},
      ModeNamePair{CameraMode::Fullscreen_2DUI, "Fullscreen_2DUI"},
      ModeNamePair{CameraMode::FREE_FLOATING, "Free Floating"}};

  static std::vector<std::string> string_list();
};

class CameraTarget
{
  Transform* t_ = nullptr;

  // methods
  void validate() const;

public:
  CameraTarget() = default;

  Transform&       get();
  Transform const& get() const;

  void set(Transform& t) { t_ = &t; }
};

struct CameraState
{
  DeviceSensitivity sensitivity{10.0f, 10.0f};

  bool rotation_lock = false;

  bool flip_y = false;
  bool flip_x = false;
};

using CameraPosition = glm::vec3;
using CameraCenter   = glm::vec3;
using CameraForward  = glm::vec3;
using CameraUp       = glm::vec3;

struct CameraFPS
{
  float            xrot = 0;
  float            yrot = 0;
  WorldOrientation orientation;

  CameraTarget target;

  CameraFPS(WorldOrientation const&);

  // fields
  CameraState cs;

  // methods
  glm::vec3 position() const;
  void      set_target(Transform&);
};

struct CameraORTHO
{
  CameraORTHO(WorldOrientation const&, glm::vec3 const&);

public:
  WorldOrientation orientation;
  glm::vec2        zoom;

  glm::vec3 position;
  bool      flip_rightv = false;
};

struct CameraArcball
{
  CameraArcball(glm::vec3 const&);

  CameraTarget         target;
  SphericalCoordinates scoords;

  bool      up_inverted;
  glm::vec3 world_up;

  CameraState cs;

  // methods
  glm::vec3 local_position() const;
  glm::vec3 position() const;
  glm::vec3 target_position() const;

  void set_target(Transform&);
};

struct Camera
{
  Camera(CameraMode, ViewSettings&&, CameraArcball&&, CameraFPS&&, CameraORTHO&&);

  ViewSettings view_settings;
  CameraMode   mode;

  // public fields
  CameraArcball arcball;
  CameraFPS     fps;
  CameraORTHO   ortho;

  Transform&       target();
  Transform const& target() const;

  void set_mode(CameraMode);
  void next_mode();

  bool is_firstperson() const { return CameraMode::FPS == mode; }
  bool is_thirdperson() const { return CameraMode::ThirdPerson == mode; }

  void toggle_rotation_lock();

  void set_target(Transform&);
  void set_target(WorldObject& wo) { set_target(wo.transform()); }

  glm::vec3 position() const;

  CameraMatrices matrices(Frustum const&) const;
  ProjMatrix     proj_matrix(Frustum const&) const;
  ViewMatrix     view_matrix(Frustum const&) const;
};

namespace camera
{
Camera
make_default(CameraMode, WorldOrientation const&, WorldOrientation const&);

/// Compute the view/pm matrix needed to render to the GUI.
CameraMatrices
gui_matrices(Camera const&, Frustum const&);

CameraMatrices
compute_matrices(Camera const&, Frustum const&, CameraMode, glm::vec3 const&);

} // namespace camera
