#pragma once
#include <boomhs/color.hpp>
#include <boomhs/font.hpp>
#include <math/rectangle.hpp>

struct GameState;

enum class ButtonType
{
  Text,
  CheckboxText
};

struct Button
{
  bool mouse_is_over = false;

  void (*on_left_click)(GameState&, Button&) = nullptr;
  void (*on_mouseover)(GameState&, Button&)  = nullptr;
  void (*on_mouseexit)(GameState&, Button&)  = nullptr;
};

struct TextButton : public Button
{
  RectFloat trect;

  char const*          text   = nullptr;
  LetterRectCollection lrects = {};

  color3    text_color = LOC3::ANTIQUE_WHITE;
  glm::vec2 text_size;
};

struct CheckboxTextButton : public TextButton
{
  bool      pressed = true;
  RectFloat cbox;
};