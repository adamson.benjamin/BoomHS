#include "perspective_renderer.hpp"
#include <boomhs/billboard.hpp>
#include <boomhs/boomhs.hpp>
#include <boomhs/bounding_object.hpp>
#include <boomhs/camera.hpp>
#include <boomhs/collision.hpp>
#include <boomhs/components.hpp>
#include <boomhs/controller.hpp>
#include <boomhs/delta_time.hpp>
#include <boomhs/engine.hpp>
#include <boomhs/entity.hpp>
#include <boomhs/game_config.hpp>
#include <boomhs/heightmap.hpp>
#include <boomhs/io_sdl.hpp>
#include <boomhs/item.hpp>
#include <boomhs/item_factory.hpp>
#include <boomhs/mouse.hpp>
#include <boomhs/npc.hpp>
#include <boomhs/player.hpp>
#include <boomhs/scene_renderer.hpp>
#include <boomhs/screen.hpp>
#include <boomhs/shape.hpp>
#include <boomhs/skybox.hpp>
#include <boomhs/start_area_generator.hpp>
#include <boomhs/terrain.hpp>
#include <boomhs/tree.hpp>
#include <boomhs/ui_chatwindow.hpp>
#include <boomhs/water.hpp>

#include <opengl/bind.hpp>
#include <opengl/gpu.hpp>
#include <opengl/texture.hpp>
#include <opengl/uniform.hpp>

#include <math/random.hpp>

#include <common/algorithm.hpp>
#include <common/log.hpp>
#include <common/result.hpp>

#include <extlibs/fastnoise.hpp>
#include <extlibs/sdl.hpp>

#include <cassert>
#include <chrono>
#include <cstdlib>
#include <ctime>
#include <iomanip>
#include <sstream>
#include <string>

using namespace opengl;

namespace
{
void
draw_scene_3d(log_t& LOGGER, GameState& gs, render_state& rs, Camera const& camera,
              StaticRenderers& srs, DeltaTime const& dt)
{
  auto& ldata    = gs.ldata;
  auto& registry = ldata.registry;

  auto const& graphics               = gs.graphics;
  bool const  graphics_mode_advanced = GraphicsMode::Advanced == graphics.mode;

  auto const& water_buffer        = gs.uibuffers_classic.water;
  bool const  draw_water          = water_buffer.draw;
  bool const  draw_water_advanced = draw_water && graphics_mode_advanced;

  auto& skybox_renderer = srs.skybox;

  auto const draw_scene = [&](bool const silhouette_black) {
    auto&      water_renderer = srs.water;
    auto const draw_advanced  = [&](auto& terrain_renderer, auto& entity_renderer) {
      water_renderer.advanced.render_reflection(LOGGER, gs, rs, camera, entity_renderer,
                                                skybox_renderer, terrain_renderer, dt);
      water_renderer.advanced.render_refraction(LOGGER, gs, rs, entity_renderer, skybox_renderer,
                                                terrain_renderer, dt);
    };
    if (draw_water && draw_water_advanced && !silhouette_black) {
      // Render the scene to the refraction and reflection FBOs
      draw_advanced(srs.default_terrain, srs.default_entity);
    }

    auto const clear_color = silhouette_black ? LOC4::BLACK : ldata.fog.color;
    render::clear_screen(clear_color);

    // render scene
    auto const& ui_debug = gs.uibuffers_classic.debug;
    if (ui_debug.draw_skybox) {
      if (!silhouette_black) {
        skybox_renderer.render(LOGGER, gs, rs, camera);
      }
    }

    // The water must be drawn BEFORE rendering the scene the last time, otherwise it shows up
    // ontop of the ingame UI nearby target indicators.
    if (draw_water) {
      water_renderer.render(LOGGER, gs, rs, camera, dt, silhouette_black);
    }
    srs.render(LOGGER, gs, rs, camera, dt, silhouette_black);
  };

  auto const draw_scene_normal_render = [&]() { draw_scene(false); };

  auto&      ds                          = rs.count;
  auto const render_scene_with_sunshafts = [&]() {
    // draw scene with black silhouttes into the sunshaft FBO.
    auto& sunshaft_renderer = srs.sunshaft;
    sunshaft_renderer.with_sunshaft_fbo(LOGGER, [&]() { draw_scene(true); });

    // draw the scene (normal render) to the screen
    draw_scene_normal_render();

    // With additive blending enabled, render the FBO ontop of the previously rendered scene to
    // obtain the sunglare effect.
    ENABLE_ADDITIVE_BLENDING_UNTIL_SCOPE_EXIT();

    auto const dlights           = find_dirlights(registry);
    auto&      uibuffers_classic = gs.uibuffers_classic;
    sunshaft_renderer.render(LOGGER, dlights, uibuffers_classic.sunshaft, ds);
  };

  if (graphics.enable_sunshafts) {
    render_scene_with_sunshafts();
  }
  else {
    draw_scene_normal_render();
  }
}

void
draw_chatwindow_scrollbar(log_t& LOGGER, LevelData& ldata, TextBox const& textbox, render_state& rs)
{
  struct ButtonInfo
  {
    RectFloat   rect;
    char const* texture = "push_button";
  };
  auto const draw_button = [&](ButtonInfo const& btn, shader_type& sp, texture_storage& ttable,
                               ProjMatrix const& pm, draw_call_counter& ds) {
    auto& ti = *ttable.find(btn.texture);
    BIND_UNTIL_END_OF_SCOPE(LOGGER, ti);

    auto const uvs = rect::create_float(0, 0, 1, 1);
    render::draw_2dtextured_rect(LOGGER, pm, btn.rect, uvs, sp, ti, ds);
  };

  auto const&      sb = textbox.scrollbar();
  ButtonInfo const btn{sb.slider_rect};
  auto&            sp = shader2d::ref(ldata.registry, "2dtexture_alpha");

  BIND_UNTIL_END_OF_SCOPE(LOGGER, sp);
  uniform::set(LOGGER, sp, "u_alpha", 0.5f);
  {
    auto& ttable = ldata.ttable;
    draw_button(btn, sp, ttable, rs.cm.proj, rs.count);
    {
      ButtonInfo const bumperbtn{sb.bumpers.top};
      draw_button(bumperbtn, sp, ttable, rs.cm.proj, rs.count);
    }
    {
      ButtonInfo const bumperbtn{sb.bumpers.bottom};
      draw_button(bumperbtn, sp, ttable, rs.cm.proj, rs.count);
    }
  }
}

void
draw_lrc(log_t& LOGGER, TextBox const& tbox, LetterRectCollection const& lrects,
         DrawLetterConfig const& cfg, glm::vec2& vcursor, RectFloat const& cwr,
         float& num_lines_skipped)
{
  auto&      sb                   = tbox.scrollbar();
  auto const move_cursor_nextline = [&](float const line_height) {
    vcursor.x = cwr.left();
    if (num_lines_skipped >= sb.current) {
      vcursor.y += line_height;
    }
    else {
      ++num_lines_skipped;
    }
  };
  auto const update_cursor = [&](float const next_w, float const line_height) {
    if ((vcursor.x + next_w) >= cwr.right()) {
      move_cursor_nextline(line_height);
    }
  };

  auto const line_height = font::compute_largest_height(lrects);
  for (auto const& letter : lrects) {
    auto const width_letter = letter.rect.width();
    update_cursor(width_letter, line_height);
    if ((vcursor.y + line_height) >= cwr.bottom()) {
      return;
    }
    if (num_lines_skipped >= sb.current) {
      render::draw_2dgui_letter(LOGGER, letter, vcursor, cfg);
    }
    vcursor.x += width_letter;
  }
  move_cursor_nextline(line_height);
}

template <typename T, typename FN>
void
draw_textbox_text(log_t& LOGGER, T const& tbox, shader_type& sp, render_state& rs,
                  font_and_texture& ftexture, FN const& per_msg_fn)
{
  auto& ti = ftexture.texture;
  BIND_UNTIL_END_OF_SCOPE(LOGGER, ti);

  assert(is_bound(sp));

  auto const&            proj = rs.cm.proj;
  auto&                  ds   = rs.count;
  DrawLetterConfig const cfg{proj, sp, ti, ds};

  auto        num_lines_skipped = 0.0f;
  auto const& cwr               = tbox.text_rect();
  glm::vec2   vcursor{cwr.left(), cwr.top()};

  for (auto const& msg : tbox.history().all_messages()) {
    assert(is_bound(sp));
    per_msg_fn(msg, sp);

    auto const& lrects = msg.letter_rects;
    ::draw_lrc(LOGGER, tbox, lrects, cfg, vcursor, cwr, num_lines_skipped);
  }
}

void
draw_chatwindow(log_t& LOGGER, ChatWindow const& cw, GameState& gs, render_state& rs,
                shader_type& sp)
{
  auto const& font_name = cw.font_info().ffile.name;
  auto&       ftable    = gs.ldata.ftable;
  auto*       pft       = ftable.lookup(font_name);
  assert(pft);
  auto& font = *pft;

  auto const& history    = cw.history();
  auto const  per_msg_fn = [&](ChatMessage const& msg, shader_type& sp) {
    auto const c = history.channel_color(msg.channel_id);
    uniform::set(LOGGER, sp, "u_color", color::rgb(c));
  };
  draw_textbox_text(LOGGER, cw, sp, rs, font, per_msg_fn);
}

void
draw_dialog_box(log_t& LOGGER, DialogTextBox const& tbox, GameState& gs, render_state& rs,
                shader_type& sp, Frustum const&)
{
  auto* pft = gs.ldata.ftable.lookup("purisa");
  assert(pft);
  auto& font = *pft;

  auto text_rect = tbox.overlay_rect();
  text_rect.shrink(10.0f);

  auto const color = LOC3::WHITE_SMOKE;
  uniform::set(LOGGER, sp, "u_color", color);

  auto const per_msg_fn = [&](TextBoxMessage const&, shader_type&) {};
  draw_textbox_text(LOGGER, tbox, sp, rs, font, per_msg_fn);
}

void
draw_overlay_rectangles(GameState& gs, ui_renderer& uir, render_state& rs, Frustum const& fr)
{
  auto const& textboxes = gs.sstack.top().uistate.textboxes;
  {
    auto& tbox = static_cast<ChatWindow const&>(*textboxes[0]);
    auto  c    = LOC4::BLUE;
    color::set_a(c, 1.00f);
    uir.draw_rect(tbox.overlay_rect(), c, rs.count);
  }
  {
    auto& tbox                 = static_cast<DialogTextBox const&>(*textboxes[1]);
    auto constexpr transparent = 0.90f;
    auto bg_color              = LOC4::BLACK;
    color::set_a(bg_color, transparent);

    auto border_color = LOC4::WHITE_SMOKE;
    color::set_a(border_color, 0.75f);

    bordered_rect_cfg const bor{tbox.overlay_rect(), bg_color, border_color};
    uir.draw_bordered_rect(bor, fr, rs.count);
  }
}

void
draw_text(log_t& LOGGER, GameState& gs, render_state& rs, Frustum const& fr)
{
  auto&       sp        = shader2d::font(gs.ldata.registry);
  auto const& textboxes = gs.sstack.top().uistate.textboxes;

  BIND_UNTIL_END_OF_SCOPE(LOGGER, sp);
  uniform::set(LOGGER, sp, "u_sampler", 0);
  {
    auto& tbox = static_cast<ChatWindow const&>(*textboxes[0]);
    draw_chatwindow(LOGGER, tbox, gs, rs, sp);
  }
  {
    auto& tbox = static_cast<DialogTextBox const&>(*textboxes[1]);
    draw_dialog_box(LOGGER, tbox, gs, rs, sp, fr);
  }
}

void
draw_scrollbars(log_t& LOGGER, GameState& gs, render_state& rs)
{
  auto const& textboxes = gs.sstack.top().uistate.textboxes;
  for (auto const& ptbox : textboxes) {
    auto const& tbox = *ptbox;
    if (tbox.scrollbar().visible) {
      draw_chatwindow_scrollbar(LOGGER, gs.ldata, tbox, rs);
    }
  }
}

void
draw_ui(log_t& LOGGER, GameState& gs, render_state& rs, ui_renderer& uir, Frustum const& fr)
{
  ENABLE_ALPHA_BLENDING_UNTIL_SCOPE_EXIT();

  draw_overlay_rectangles(gs, uir, rs, fr);
  draw_text(LOGGER, gs, rs, fr);
  draw_scrollbars(LOGGER, gs, rs);
}

} // namespace

namespace perspective
{
void
draw_scene(log_t& LOGGER, GameState& gs, render_state& rs, Camera const& camera,
           StaticRenderers& srs, DeltaTime const& dt)
{
  draw_scene_3d(LOGGER, gs, rs, camera, srs, dt);
  {
    auto const&  fr = rs.fr;
    auto const   cm = camera::gui_matrices(camera, fr);
    render_state gui_rs{cm, rs.fr, rs.count};
    draw_ui(LOGGER, gs, gui_rs, srs.ui, fr);
  }
}

} // namespace perspective
