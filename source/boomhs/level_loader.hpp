#pragma once
#include <boomhs/entity.hpp>
#include <boomhs/leveldata.hpp>

#include <common/result.hpp>

#include <string>

struct log_t;
class rng_t;

Result<LevelAssets, std::string>
load_level(log_t&, enttreg_t&, rng_t&, std::string const&);
