#include "ortho_renderer.hpp"

#include <boomhs/camera.hpp>
#include <boomhs/components.hpp>
#include <boomhs/delta_time.hpp>
#include <boomhs/engine.hpp>
#include <boomhs/entity.hpp>
#include <boomhs/perspective_renderer.hpp>
#include <boomhs/vertex_factory.hpp>

#include <opengl/gpu.hpp>
#include <opengl/renderer.hpp>

#include <common/log.hpp>
#include <math/random.hpp>
#include <math/viewport.hpp>

#include <cassert>

using namespace math::constants;
using namespace opengl;

namespace ortho
{

void
draw_scene(log_t& logger, GameState& gs, render_state& rs, Camera const& camera,
           StaticRenderers& srs, DeltaTime const& ft)
{
  auto const vp = Viewport::from_frustum(rs.fr);
  {
    Viewport const RHS{vp.left(), vp.top(), vp.width(), vp.height()};
    render::set_viewport_and_scissor(RHS, rs.fr.height());
  }
  perspective::draw_scene(logger, gs, rs, camera, srs, ft);
}

} // namespace ortho
