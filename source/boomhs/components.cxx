#include "components.hpp"

////////////////////////////////////////////////////////////////////////////////////////////////////
// FollowTransform
FollowTransform::FollowTransform(eid_t const eid)
    : target_eid(eid)
{
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// OrbitalBody
OrbitalBody::OrbitalBody(glm::vec3 const& r, float const o)
    : radius(r)
    , offset(o)
{
}
