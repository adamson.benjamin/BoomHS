#pragma once
#include <boomhs/obj.hpp>
#include <opengl/buffer.hpp>
#include <opengl/vertex_attribute.hpp>

#include <ostream>
#include <string>
#include <utility>
#include <vector>

struct log_t;

struct ObjQuery
{
  std::string                name;
  opengl::buffer_flags const flags;
};

bool
operator==(ObjQuery const&, ObjQuery const&);

bool
operator!=(ObjQuery const&, ObjQuery const&);

std::ostream&
operator<<(std::ostream&, ObjQuery const&);

class ObjStore
{
  using pair_t      = std::pair<std::string, ObjData>;
  using datastore_t = std::vector<pair_t>;

  // This holds the data
  mutable datastore_t data_;

public:
  ObjStore() = default;
  MOVE_CONSTRUCTIBLE_ONLY(ObjStore);

  void add_obj(std::string const&, ObjData&&) const;

  ObjData&       get(log_t&, std::string const&);
  ObjData const& get(log_t&, std::string const&) const;

  auto size() const { return data_.size(); }
  bool empty() const { return data_.empty(); }
};

std::ostream&
operator<<(std::ostream&, ObjStore const&);
