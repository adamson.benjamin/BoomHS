#pragma once
#include <boomhs/color.hpp>
#include <common/macros_class.hpp>

struct Fog
{
  float density  = 0.007f;
  float gradient = 1.5f;

  color4 color = LOC4::NO_ALPHA;
};
