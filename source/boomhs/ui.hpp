#pragma once
#include <common/macros_container.hpp>
#include <common/result.hpp>
#include <common/timer.hpp>

#include <boomhs/entity.hpp>
#include <boomhs/ui_controls.hpp>
#include <boomhs/ui_textbox.hpp>

#include <memory>
#include <string>

struct log_t;
struct Button;
struct GameState;
class Mouse;

namespace opengl
{
struct render_state;
class shader_type;
class texture_h;
class texture_storage;
} // namespace opengl

struct UiState
{
  std::vector<CheckboxTextButton>       checkboxes;
  std::vector<TextButton>               text_buttons;
  std::vector<std::unique_ptr<TextBox>> textboxes;

public:
  UiState() = default;
};

namespace ui
{
void
draw(log_t&, TextButton const&, opengl::shader_type&, opengl::texture_h&, opengl::render_state&);

void
draw(log_t&, enttreg_t&, CheckboxTextButton const&, opengl::texture_storage&, opengl::shader_type&,
     opengl::texture_h&, opengl::render_state&);

bool
mouse_is_over(Mouse const&, TextButton const&);

bool
mouse_is_over(Mouse const&, CheckboxTextButton const&);

void
resize(CheckboxTextButton&, RectFloat const&);

void
on_mouse_over(log_t&, TextButton&, GameState&, Mouse const&);

void
on_mouse_over(log_t&, CheckboxTextButton&, GameState&, Mouse const&);

void
on_mouse_click(log_t&, TextButton&, GameState&);

void
on_mouse_click(log_t&, CheckboxTextButton&, GameState&);
} // namespace ui
