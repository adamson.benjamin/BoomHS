#pragma once
#include <boomhs/color.hpp>
#include <boomhs/entity.hpp>

#include <common/macros_class.hpp>
#include <common/macros_container.hpp>

#include <array>
#include <initializer_list>
#include <string>
#include <vector>

struct Attenuation
{
  float constant;
  float linear;
  float quadratic;
};
struct NameAttenuation
{
  std::string const name;
  Attenuation const value;
};
using AttenuationList = std::vector<NameAttenuation>;

Attenuation operator*(Attenuation const&, float);

Attenuation&
operator*=(Attenuation&, float);

Attenuation
operator/(Attenuation const&, float);

Attenuation&
operator/=(Attenuation&, float const);

std::ostream&
operator<<(std::ostream&, Attenuation const&);

// https://learnopengl.com/#!Lighting/Light-casters
static constexpr std::array ATTENUATION_VALUE_TABLE = {
    Attenuation{1.0f, 0.7f, 1.8f},      Attenuation{1.0f, 0.35f, 0.44f},
    Attenuation{1.0f, 0.22f, 0.20f},    Attenuation{1.0f, 0.14f, 0.07f},
    Attenuation{1.0f, 0.09f, 0.032f},   Attenuation{1.0f, 0.07f, 0.017f},
    Attenuation{1.0f, 0.045f, 0.0075f}, Attenuation{1.0f, 0.027f, 0.0028f},
    Attenuation{1.0f, 0.022f, 0.0019f}, Attenuation{1.0f, 0.014f, 0.0007f},
    Attenuation{1.0f, 0.007f, 0.0002f}, Attenuation{1.0f, 0.0014f, 0.000007f}};
// https://learnopengl.com/#!Lighting/Light-casters
static constexpr auto ATTENUATION_DISTANCE_STRINGS = "7\0"
                                                     "13\0"
                                                     "20\0"
                                                     "32\0"
                                                     "50\0"
                                                     "65\0"
                                                     "100\0"
                                                     "160\0"
                                                     "200\0"
                                                     "325\0"
                                                     "600\0"
                                                     "3250\0"
                                                     "\0";

struct Light
{
  color3 diffuse  = LOC3::WHITE;
  color3 specular = LOC3::BLACK;
};

struct DirectionalLight
{
  Light     light;
  glm::vec3 direction       = {};
  glm::vec2 screenspace_pos = {};
};

struct GlobalLight
{
  color3 ambient;
  explicit GlobalLight(color3 const&);
};

struct PointLight
{
  Light light;

  static constexpr auto INIT_ATTENUATION_INDEX = ATTENUATION_VALUE_TABLE.size() - 1;
  Attenuation           attenuation            = ATTENUATION_VALUE_TABLE[INIT_ATTENUATION_INDEX];
};

struct PointLightAndTransform
{
  Transform&  transform;
  PointLight& pointlight;
};

struct DirectionalLightReference
{
  DirectionalLight& dirlight;
};

template <typename T>
class LightList
{
  std::vector<T> list_;

public:
  MOVE_CONSTRUCTIBLE_ONLY(LightList);
  DEFINE_VECTOR_LIKE_WRAPPER_FNS(list_)

  LightList() = default;
  explicit LightList(std::initializer_list<T> ilist)
      : list_(ilist)
  {
  }
};

using DirectionalLightList = LightList<DirectionalLightReference>;
using PointLightList       = LightList<PointLightAndTransform>;

using NumDirectionalLights = decltype(std::declval<DirectionalLightList>().size());
using NumPointLights       = decltype(std::declval<PointLightList>().size());

struct NumLights
{
  NumDirectionalLights ndl = 0;
  NumPointLights       npl = 0;
};

DirectionalLightList
find_dirlights(enttreg_t&);

PointLightList
find_pointlights(enttreg_t&);
