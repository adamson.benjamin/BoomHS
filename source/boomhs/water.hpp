#pragma once
#include <boomhs/color.hpp>
#include <boomhs/entity.hpp>
#include <boomhs/game_config.hpp>
#include <boomhs/obj.hpp>

#include <common/macros_class.hpp>

#include <array>
#include <extlibs/glm.hpp>

namespace opengl
{
class shader_type;
class texture_h;
class texture_storage;
} // namespace opengl

struct log_t;

opengl::shader_type&
graphics_mode_to_water_shader(enttreg_t&, GraphicsMode);

struct WaterInfo
{
  eid_t              eid;
  opengl::texture_h* tinfo = nullptr;

  glm::vec2    dimensions;
  unsigned int num_vertexes;

  color4 mix_color     = LOC4::SLATE_BLUE;
  float  mix_intensity = 0.25f;

  float     flow_speed     = 1.0f;
  glm::vec2 flow_direction = glm::normalize(glm::vec2{1.0f, 1.0f});

  //
  // constructors
  NO_COPY(WaterInfo);
  MOVE_DEFAULT(WaterInfo);
  WaterInfo() = default;
};

struct WaterFactory
{
  static ObjData generate_water_data(log_t&, glm::vec2 const&, size_t);

  static WaterInfo& make_default(log_t&, opengl::texture_storage&, eid_t, enttreg_t&);
};
