#pragma once

namespace opengl
{
struct render_state;
} // namespace opengl

struct Camera;
class DeltaTime;
struct GameState;
struct log_t;
struct StaticRenderers;

namespace perspective
{
void
draw_scene(log_t&, GameState&, opengl::render_state&, Camera const&, StaticRenderers&,
           DeltaTime const&);

} // namespace perspective
