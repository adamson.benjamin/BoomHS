#pragma once
#include <boomhs/obj.hpp>

#include <common/macros_class.hpp>
#include <common/result.hpp>

#include <string>
#include <vector>

struct log_t;
struct TerrainConfig;

namespace opengl
{
struct image_data;
class texture_storage;
} // namespace opengl

class Heightmap
{
  using HeightmapData = std::vector<uint8_t>;
  size_t        width_;
  HeightmapData data_;

  COPY_DEFAULT(Heightmap);

public:
  explicit Heightmap(size_t);
  MOVE_DEFAULT(Heightmap);

  // methods
  Heightmap clone() const { return *this; }

  uint8_t&       data(size_t, size_t);
  uint8_t const& data(size_t, size_t) const;

  void reserve(size_t);
  void add(uint8_t);
  auto size() const { return data_.size(); }

  std::string to_string() const;
};

using HeightmapResult = Result<Heightmap, std::string>;
namespace heightmap
{

HeightmapResult
load_fromtable(log_t&, opengl::texture_storage const&, std::string const&);

ObjVertices
generate_normals(size_t, size_t, bool, Heightmap const&);

HeightmapResult
parse(opengl::image_data const&);

HeightmapResult
parse(log_t&, char const*);

HeightmapResult
parse(log_t&, std::string const&);

void
update_vertices_from_heightmap(log_t&, TerrainConfig const&, Heightmap const&, ObjVertices&);

} // namespace heightmap
