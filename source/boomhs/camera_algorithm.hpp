#pragma once
#include <boomhs/camera.hpp>

namespace camera::ortho
{

// Compute the projection-matrix.
glm::mat4
compute_pm(Frustum const&, ScreenSize const&, CameraPosition const&, glm::ivec2 const&);

// Compute the view-matrix.
glm::mat4
compute_vm(CameraPosition const&, CameraCenter const&, CameraUp const&, bool);

} // namespace camera::ortho

namespace camera
{
template <typename C>
auto&
transform(C& camera)
{
  return camera.target.get();
}

template <typename C>
auto const&
transform(C const& camera)
{
  return camera.target.get();
}

CameraMatrices
calc_cm(CameraFPS const&, ViewSettings const&, Frustum const&, glm::vec3 const&);
CameraMatrices
calc_cm(CameraORTHO const&, Frustum const&, ScreenSize const&);
CameraMatrices
calc_cm(CameraArcball const&, ViewSettings const&, Frustum const&, glm::vec3 const&);
CameraMatrices
calc_cm(Camera const&, ViewSettings const&, Frustum const&, ScreenSize const&, glm::vec3 const&);

CameraFPS&
rotate_radians(CameraFPS&, float, float, DeltaTime const&);
CameraArcball&
rotate_radians(CameraArcball&, float, float, DeltaTime const&);
Camera&
rotate_radians(Camera&, float, float, DeltaTime const&);

glm::vec3
eye_forward(CameraFPS const&);

glm::vec3
eye_up(CameraFPS const&);

glm::vec3
eye_forward(CameraORTHO const&);

glm::vec3
eye_up(CameraORTHO const&);

glm::vec3
eye_forward(CameraArcball const&);

glm::vec3
eye_up(CameraArcball const&);

glm::vec3
eye_forward(Camera const&);

glm::vec3
eye_up(Camera const&);

glm::vec3
eye_backward(Camera const&);

glm::vec3
eye_right(Camera const&);

glm::vec3
eye_left(Camera const&);

void
scroll(CameraORTHO&, glm::vec2 const&);

void
zoom_in(CameraORTHO&, glm::vec2 const&, DeltaTime const&);
void
zoom_out(CameraORTHO&, glm::vec2 const&, DeltaTime const&);

void
zoom(CameraArcball&, float, DeltaTime const&);
void
zoom_out(CameraArcball&, float, DeltaTime const&);
void
zoom_in(CameraArcball&, float, DeltaTime const&);

void
zoom_out(Camera&, float, DeltaTime const&);
void
zoom_in(Camera&, float, DeltaTime const&);

} // namespace camera
