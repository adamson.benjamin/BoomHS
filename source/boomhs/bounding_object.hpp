#pragma once
#include <boomhs/entity.hpp>
#include <math/cube.hpp>
#include <opengl/draw_info.hpp>

#include <extlibs/glm.hpp>

// AxisAlignedBoundingBox
struct AABoundingBox
{
  Cube               cube;
  opengl::draw_state draw_info;

public:
  AABoundingBox(glm::vec3 const&, glm::vec3 const&, opengl::draw_state&&);

public:
  static AABoundingBox&
  add_to_entity(log_t&, enttreg_t&, eid_t, glm::vec3 const&, glm::vec3 const&);
};
