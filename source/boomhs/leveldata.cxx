#include "leveldata.hpp"
#include <boomhs/scene_renderer.hpp>

LevelData::LevelData(log_t& logger, LevelAssets&& la, TerrainGrid&& tg, enttreg_t& er,
                     Viewport const& vp)
    : LevelAssets(MOVE(la))
    , registry(er)
    , tgrid(MOVE(tg))
    , srenders(make_static_renderers(logger, *this, vp))
{
}
