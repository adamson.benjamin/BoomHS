#pragma once
#include <boomhs/color.hpp>
#include <boomhs/entity.hpp>

#include <common/time.hpp>

#include <optional>
#include <vector>

class DeltaTime;

struct SelectedTarget
{
  // Offset into the array of target entity id's that this selected target represents.
  size_t offset = 0;
};

enum class CycleDirection
{
  Forward = 0,
  Backward
};

class NearbyTargets
{
  eid_array targets_ = {};

  duration_t                    last_target_changed_{0};
  std::optional<SelectedTarget> selected_ = std::nullopt;

  bool empty() const;
  void update_time(DeltaTime const&);

public:
  NearbyTargets() = default;

  float calculate_scale(DeltaTime const&) const;

  void add_target(eid_t);
  void clear();
  void cycle(CycleDirection, DeltaTime const&);
  void cycle_forward(DeltaTime const&);
  void cycle_backward(DeltaTime const&);

  std::optional<eid_t> selected() const;
  void                 set_selected(eid_t);

  static color4 color_from_level_difference(int, int);
};
