#pragma once
#include <boomhs/obj.hpp>
#include <extlibs/glm.hpp>

struct log_t;
class Heightmap;

struct GenerateNormalData
{
  bool const       invert_normals;
  Heightmap const& heightmap;
  size_t const     num_vertexes;
};

struct MeshFactory
{
  MeshFactory() = delete;

  static ObjVertices generate_rectangle_mesh(log_t&, glm::vec2 const&, size_t);

  static ObjVertices generate_uvs(log_t&, glm::vec2 const&, size_t, bool);

  static ObjIndices generate_indices(log_t&, size_t);

  static ObjVertices generate_normals(log_t&, GenerateNormalData const&);
  static ObjVertices generate_flat_normals(log_t&, size_t);
};
