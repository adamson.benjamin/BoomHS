#pragma once
#include <opengl/framebuffer.hpp>
#include <opengl/renderbuffer.hpp>
#include <opengl/renderer.hpp>
#include <opengl/texture.hpp>

#include <common/log.hpp>
#include <common/macros_class.hpp>

class DeltaTime;
struct Camera;
struct Frustum;
struct GameState;
class Viewport;

namespace opengl
{
class entity_renderer;
class terrain_renderer_default;
struct render_state;
class shader_type;
class skybox_renderer;

static float constexpr CUTOFF_HEIGHT      = 0.4f;
static glm::vec4 constexpr ABOVE_VECTOR   = {0, -1, 0, CUTOFF_HEIGHT};
static glm::vec4 constexpr BENEATH_VECTOR = {0, 1, 0, -CUTOFF_HEIGHT};

class water_renderer_silhouette
{
  opengl::shader_type* sp_;

public:
  water_renderer_silhouette(opengl::shader_type&);
  NOCOPY_MOVE_DEFAULT(water_renderer_silhouette);

  void render_water(log_t&, GameState&, render_state&, Camera const&, DeltaTime const&);
};

class water_renderer_basic
{
  opengl::shader_type* sp_;
  opengl::texture_h*   diffuse_;
  opengl::texture_h*   normal_;

public:
  water_renderer_basic(log_t&, opengl::texture_h&, opengl::texture_h&, opengl::shader_type&);
  NOCOPY_MOVE_DEFAULT(water_renderer_basic);

  void render_water(log_t&, GameState&, render_state&, Camera const&, DeltaTime const&);
};

class water_renderer_medium
{
  opengl::shader_type* sp_;
  opengl::texture_h*   diffuse_;
  opengl::texture_h*   normal_;

public:
  water_renderer_medium(log_t&, opengl::texture_h&, opengl::texture_h&, opengl::shader_type&);
  NOCOPY_MOVE_DEFAULT(water_renderer_medium);

  void render_water(log_t&, GameState&, render_state&, Camera const&, DeltaTime const&);
};

struct reflection_buffer
{
  opengl::frame_buffer_ar  fbo;
  opengl::texture_h        tbo;
  opengl::render_buffer_ar rbo;

  reflection_buffer(log_t&, Viewport const&);

  NOCOPY_MOVE_DEFAULT(reflection_buffer);
};

struct refraction_buffer
{
  opengl::frame_buffer_ar fbo;
  opengl::texture_h       tbo;
  opengl::texture_h       dbo;

  refraction_buffer(log_t&, Viewport const&);

  NOCOPY_MOVE_DEFAULT(refraction_buffer);
};

class water_renderer_advanced
{
  opengl::shader_type* sp_;
  opengl::texture_h*   diffuse_;
  opengl::texture_h*   dudv_;
  opengl::texture_h*   normal_;

  reflection_buffer reflection_;
  refraction_buffer refraction_;

  template <typename FN>
  void with_reflection_fbo(log_t& LOGGER, FN const& fn)
  {
    bind_for(LOGGER, *reflection_.fbo, fn);
  }

  template <typename FN>
  void with_refraction_fbo(log_t& LOGGER, FN const& fn)
  {
    bind_for(LOGGER, *refraction_.fbo, fn);
  }

public:
  NOCOPY_MOVE_DEFAULT(water_renderer_advanced);

  explicit water_renderer_advanced(log_t&, Viewport const&, shader_type&, texture_h&, texture_h&,
                                   texture_h&);

  void resize(log_t&, Viewport const&);

  void
  render_reflection(log_t&, GameState&, render_state&, Camera const& camera, entity_renderer& er,
                    skybox_renderer&, terrain_renderer_default& tr, DeltaTime const&);

  void render_refraction(log_t&, GameState&, render_state&, entity_renderer&, skybox_renderer&,
                         terrain_renderer_default&, DeltaTime const&);

  void render_water(log_t&, GameState&, render_state&, Camera const&, DeltaTime const&);

  auto& reflection_ti() { return reflection_.tbo; }
  auto& refraction_ti() { return refraction_.tbo; }
};

struct WaterRenderers
{
  opengl::water_renderer_basic    basic;
  opengl::water_renderer_medium   medium;
  opengl::water_renderer_advanced advanced;

  opengl::water_renderer_silhouette silhouette;

  void render(log_t&, GameState&, render_state&, Camera const&, DeltaTime const&, bool);

  void resize(log_t&, Viewport const&);
};

} // namespace opengl
