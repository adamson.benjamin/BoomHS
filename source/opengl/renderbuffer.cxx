#include "renderbuffer.hpp"
#include <opengl/bind.hpp>

#include <common/log.hpp>

namespace opengl
{
render_buffer_h::render_buffer_h() { ::glGenRenderbuffers(1, &id); }

void
destroy_render_buffer(render_buffer_h& rb)
{
  OPENGL_ASSERT_NOT_BOUND(rb);
  ::glDeleteRenderbuffers(1, &rb.id);
}

std::string
render_buffer_h::to_string() const
{
  return fmt::sprintf("(RBInfo) id: %u", id);
}

void
bind(log_t& LOGGER, render_buffer_h& rb)
{
  LOG_TRACE("Binding renderbuffer: %u", rb.id);
  ::glBindRenderbuffer(GL_RENDERBUFFER, rb.id);
}

void
unbind(log_t& LOGGER, render_buffer_h& rb)
{
  LOG_TRACE("Unbinding renderbuffer: %u", rb.id);
  ::glBindRenderbuffer(GL_RENDERBUFFER, 0);
}

} // namespace opengl
