#include "vao.hpp"
#include <common/log.hpp>

#include <cassert>
#include <extlibs/fmt.hpp>

namespace
{
static constexpr GLsizei NUM_BUFFERS = 1;
} // namespace

namespace opengl
{

vao_h::vao_h() { ::glGenVertexArrays(NUM_BUFFERS, &vao_); }

vao_h::~vao_h() { ::glDeleteVertexArrays(NUM_BUFFERS, &vao_); }

vao_h::vao_h(vao_h&& other)
    : vao_(other.vao_)
{
  other.vao_ = 0;
}

vao_h&
vao_h::operator=(vao_h&& other)
{
  vao_              = other.vao_;
  debug_bind_state_ = MOVE(other.debug_bind_state_);

  other.vao_ = 0;
  return *this;
}

GLuint
vao_h::gl_raw_value() const
{
  assert(0 != vao_);
  return vao_;
}

std::string
vao_h::to_string() const
{
  auto constexpr fmt = "(VAO) NUM_BUFFERS: %li, raw: %u";
  return fmt::sprintf(fmt, NUM_BUFFERS, gl_raw_value());
}

std::ostream&
operator<<(std::ostream& stream, vao_h const& vao)
{
  stream << vao.to_string();
  return stream;
}

void
bind(log_t&, vao_h const& vao)
{
  auto const handle = vao.gl_raw_value();
  ::glBindVertexArray(handle);
}
void
unbind(log_t&, vao_h const&)
{
  ::glBindVertexArray(0);
}

} // namespace opengl
