#pragma once
#include <common/macros_class.hpp>
#include <opengl/draw_info.hpp>

struct Camera;
class DeltaTime;
struct GameState;
struct log_t;

namespace opengl
{
struct render_state;
class draw_state;
class shader_type;
class texture_h;

class skybox_renderer
{
  opengl::draw_state dinfo_;

  opengl::texture_h*   day_;
  opengl::texture_h*   night_;
  opengl::shader_type* sp_;

public:
  NOCOPY_MOVE_DEFAULT(skybox_renderer);
  skybox_renderer(log_t&, opengl::draw_state&&, opengl::texture_h&, opengl::texture_h&,
                  opengl::shader_type&);

  void render(log_t&, GameState&, render_state&, Camera const&);
  void set_day(opengl::texture_h*);
  void set_night(opengl::texture_h*);
};

} // namespace opengl
