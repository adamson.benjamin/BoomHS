#pragma once
#include <common/macros_class.hpp>

#include <extlibs/glm.hpp>

struct Frustum;
struct log_t;

struct Camera;
class DeltaTime;
struct GameState;
class rng_t;

namespace opengl
{
struct render_state;

class debug_renderer
{
public:
  NOCOPY_MOVE_DEFAULT(debug_renderer);
  debug_renderer() = default;

  void render_scene(log_t&, GameState&, render_state&, Camera const&);
};

class scene_renderer_silhouette
{
public:
  NOCOPY_MOVE_DEFAULT(scene_renderer_silhouette);
  scene_renderer_silhouette() = default;

  void render_scene();
};

} // namespace opengl
