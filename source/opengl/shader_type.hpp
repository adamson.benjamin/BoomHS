#pragma once
#include <boomhs/lighting.hpp>

#include <opengl/bind_type.hpp>
#include <opengl/vertex_attribute.hpp>

#include <common/algorithm.hpp>
#include <common/compiler.hpp>
#include <common/log.hpp>
#include <common/macros_class.hpp>
#include <common/macros_container.hpp>
#include <common/result.hpp>
#include <common/type_alias.hpp>

#include <map>
#include <optional>
#include <string>

namespace opengl
{
auto constexpr INVALID_PROGRAM_ID = 0;

using vertex_shader_contents   = std::string;
using fragment_shader_contents = std::string;

class program_h
{
  GLuint handle_;

public:
  NO_COPY(program_h);
  explicit program_h(GLuint const);
  ~program_h();

public:
  program_h(program_h&&);
  program_h& operator=(program_h&&);

public:
  GLuint handle() const;

public:
  debug_bind_state debug_bind_state_;
};

struct shader_cfg
{
  // required
  std::string              name;
  vertex_shader_contents   vertex;
  fragment_shader_contents fragment;
  vertex_attribute         va;

  // optional
  std::optional<GLsizei> instance_count = std::nullopt;
  bool                   is_2d          = false;
};

inline auto
make_shader_cfg(std::string&& sn, vertex_shader_contents&& vs, fragment_shader_contents&& fs,
                attribute_pointer&& ap)
{
  return shader_cfg{MOVE(sn), MOVE(vs), MOVE(fs), make_vertex_attribute(ap)};
}

template <size_t N>
auto
make_shader_cfg(std::string&& sn, vertex_shader_contents&& vs, fragment_shader_contents&& fs,
                std::array<attribute_pointer, N> const& arr)
{
  return shader_cfg{MOVE(sn), MOVE(vs), MOVE(fs), make_vertex_attribute(arr)};
}

class shader_type
{
  program_h        program_;
  vertex_attribute va_;

  shader_cfg config_;

public:
  NOCOPY_MOVE_DEFAULT(shader_type);
  explicit shader_type(program_h&&, vertex_attribute&&, shader_cfg&&);

  // public data members
  debug_bind_state debug_bind_state_;

  // public member fns
  auto        handle() const { return program_.handle(); }
  auto const& va() const { return va_; }

  GLint get_uniform_location(log_t&, GLchar const*);

  auto const& name() const { return config_.name; }
  auto        copy_config() const { return config_; }
  auto        instance_count() const { return config_.instance_count; }

  std::string to_string() const;
};
} // namespace opengl

namespace opengl
{
void
bind(log_t&, shader_type&);

void
unbind(log_t&, shader_type&);
} // namespace opengl

namespace opengl::detail
{
class shader_cache
{
  std::map<std::string, shader_type> map_;

public:
  NO_MOVE_OR_COPY(shader_cache);
  BEGIN_END_FORWARD_FNS(map_)
  shader_cache() = default;

  shader_type* lookup(std::string const&);
  shader_type& ref(std::string const&);

  void add(shader_type&&);
  void clear();
};
} // namespace opengl::detail

struct shader_cache_2d : public ::opengl::detail::shader_cache
{
};
struct shader_cache_3d : public ::opengl::detail::shader_cache
{
};
