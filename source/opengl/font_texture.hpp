#pragma once
#include <boomhs/font.hpp>
#include <common/macros_class.hpp>

#include <opengl/texture.hpp>

#include <map>
#include <string>

namespace opengl
{
struct font_and_texture
{
  FontFile   ffile;
  texture_h& texture;
};

class font_storage
{
  std::map<std::string, font_and_texture> data_;

public:
  NOCOPY_MOVE_DEFAULT(font_storage);
  font_storage() = default;

  font_and_texture const* lookup(std::string const&) const;
  font_and_texture*       lookup(std::string const&);

  void add(font_and_texture&&);
};

} // namespace opengl
