#include "draw_info.hpp"
#include <opengl/bind.hpp>
#include <opengl/buffer.hpp>
#include <opengl/gpu.hpp>
#include <opengl/shader.hpp>
#include <opengl/vao.hpp>

#include <boomhs/bounding_object.hpp>
#include <boomhs/components.hpp>
#include <boomhs/obj_store.hpp>
#include <boomhs/vertex_factory.hpp>

#include <common/FOR.hpp>
#include <common/algorithm.hpp>

#include <cstdlib>

namespace opengl
{
////////////////////////////////////////////////////////////////////////////////////////////////////
// BufferHandles
buffer_handles::buffer_handles()
{
  ::glGenBuffers(NUM_BUFFERS, &vbo_);
  ::glGenBuffers(NUM_BUFFERS, &ebo_);
}

buffer_handles::~buffer_handles()
{
  ::glDeleteBuffers(NUM_BUFFERS, &ebo_);
  ::glDeleteBuffers(NUM_BUFFERS, &vbo_);
}

buffer_handles::buffer_handles(buffer_handles&& other)
    : vbo_(other.vbo_)
    , ebo_(other.ebo_)
{
  assert(this != &other);

  other.vbo_ = 0;
  other.ebo_ = 0;
}

buffer_handles&
buffer_handles::operator=(buffer_handles&& other)
{
  assert(this != &other);

  vbo_ = MOVE(other.vbo_);
  ebo_ = MOVE(other.ebo_);

  other.vbo_ = 0;
  other.ebo_ = 0;
  return *this;
}

std::string
buffer_handles::to_string() const
{
  return fmt::format("vbo: {}, ebo: {}", vbo(), ebo());
}

std::ostream&
operator<<(std::ostream& stream, buffer_handles const& buffers)
{
  stream << buffers.to_string();
  return stream;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// draw_state
draw_state::draw_state(size_t const num_vertexes, GLsizei const num_indices)
    : num_vertexes_(num_vertexes)
    , num_indices_(num_indices)
{
}

draw_state::draw_state(draw_state&& other)
    : num_vertexes_(other.num_vertexes_)
    , num_indices_(other.num_indices_)
    , buffers_(MOVE(other.buffers_))
    , vao_(MOVE(other.vao_))
{
  assert(this != &other);
}

draw_state&
draw_state::operator=(draw_state&& other)
{
  assert(this != &other);

  num_vertexes_       = other.num_vertexes_;
  num_indices_        = other.num_indices_;
  other.num_vertexes_ = 0;
  other.num_indices_  = 0;

  buffers_ = MOVE(other.buffers_);
  vao_     = MOVE(other.vao_);
  return *this;
}

std::string
draw_state::to_string() const
{
  auto const& dinfo = *this;

  std::string result;
  result += fmt::format("NumIndices: {} ", dinfo.num_indices());
  result += "VAO: " + dinfo.vao().to_string() + " ";
  result += "BufferHandles: {" + dinfo.buffers_.to_string() + "} ";
  // auto const num_indices = dinfo.num_indices();

  // TODO: maybe need to bind the element buffer before calling glMapBuffer?
  /*
  {
    auto const target = GL_ELEMENT_ARRAY_BUFFER;
    GLuint const*const pmem = static_cast<GLuint const*const>(glMapBuffer(target, GL_READ_ONLY));
    assert(pmem);
    ON_SCOPE_EXIT([]() { assert(true == glUnmapBuffer((target))); });

    FOR(i, num_indices) {
      // (void* + GLuint) -> GLuint*
      GLuint const* p_element = pmem + i;
      result += std::to_string(*p_element) + " ";
    }
    result += "\n";
  }
  */

  /*
  auto const stride = va.stride();
  auto const num_vertexes = dinfo.num_vertexes();

  auto const target = GL_ARRAY_BUFFER;
  GLfloat *pmem = static_cast<GLfloat *>(glMapBuffer(target, GL_READ_ONLY));
  assert(pmem);
  ON_SCOPE_EXIT([]() { assert(glUnmapBuffer(target)); });

  result += fmt::format("vbo id: {} VBO contents:\n", dinfo.vbo());
  result += "VBO stride: '" + std::to_string(stride) + "'\n";
  result += "VBO num_vertexes: '" + std::to_string(num_vertexes) + "'\n";
  result += "VBO contents:\n";

  auto count{0};
  for(auto i = 0u; i < num_vertexes; ++i, ++count) {
    if (count == stride) {
      count = 0;
      result += "\n";
    } else if (i > 0) {
      result += " ";
    }
    result += std::to_string(*(pmem + i));
  }
  */
  return result;
}

void
bind(log_t& LOGGER, draw_state const& ds)
{
  bind(LOGGER, ds.vao());
}

void
unbind(log_t& LOGGER, draw_state const& ds)
{
  unbind(LOGGER, ds.vao());
}

} // namespace opengl
