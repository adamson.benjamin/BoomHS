#pragma once
#include <common/macros_class.hpp>
#include <common/move_only_type.hpp>
#include <common/result.hpp>

#include <opengl/bind_type.hpp>
#include <opengl/renderbuffer.hpp>
#include <opengl/texture.hpp>

#include <extlibs/glew.hpp>
#include <string>

struct log_t;

namespace opengl
{
struct frame_buffer_cfg final
{
  GLenum const target = 0;
  GLint        width  = -1;
  GLint        height = -1;
};

struct frame_buffer_h
{
  debug_bind_state debug_bind_state_;
  GLuint           id;

public:
  frame_buffer_h();
  NO_COPY(frame_buffer_h);
  MOVE_DEFAULT(frame_buffer_h);

public:
  std::string to_string() const;

  texture_h        attach_color_buffer(log_t&, int, int, GLenum);
  texture_h        attach_depth_buffer(log_t&, int, int, GLenum);
  render_buffer_ar attach_render_buffer(log_t&, int, int);

  static size_t constexpr NUM_BUFFERS = 1;
};

void
destroy_frame_buffer(frame_buffer_h&);

using frame_buffer_ar = move_only_type<frame_buffer_h, destroy_frame_buffer>;

frame_buffer_ar
make_fbo(log_t&);
} // namespace opengl

namespace opengl
{
void
bind(log_t&, frame_buffer_h&);

void
unbind(log_t&, frame_buffer_h&);
} // namespace opengl