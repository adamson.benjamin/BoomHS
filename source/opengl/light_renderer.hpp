#pragma once
#include <boomhs/lighting.hpp>
#include <math/matrices.hpp>

struct DirectionalLight;
struct GlobalLight;
struct log_t;
struct Material;

namespace opengl
{
class shader_type;

class light_renderer
{
  light_renderer() = delete;

public:
  static void set_light_uniforms(log_t&, DirectionalLightList const&, PointLightList const&,
                                 shader_type&, Material const&, ViewMatrix const&,
                                 ModelMatrix const&, GlobalLight const&, bool);
};

} // namespace opengl
