#pragma once
#include <boomhs/color.hpp>
#include <boomhs/obj.hpp>
#include <string>

namespace opengl
{
class vertex_attribute;

struct buffer_flags
{
  bool const vertices, normals, colors, uvs;

  explicit constexpr buffer_flags(bool const v, bool const n, bool const c, bool const u)
      : vertices(v)
      , normals(n)
      , colors(c)
      , uvs(u)
  {
  }

  std::string to_string() const;

  static buffer_flags from_va(vertex_attribute const&);
};

bool
operator==(buffer_flags const&, buffer_flags const&);

bool
operator!=(buffer_flags const&, buffer_flags const&);

std::ostream&
operator<<(std::ostream&, buffer_flags const&);

struct vertex_buffer
{
  ObjVertices        vertices;
  ObjIndices         indices;
  buffer_flags const flags;

private:
  vertex_buffer(buffer_flags const&);
  COPY_CONSTRUCTIBLE(vertex_buffer);
  NO_COPY_ASSIGN(vertex_buffer);

public:
  MOVE_CONSTRUCTIBLE(vertex_buffer);
  NO_MOVE_ASSIGN(vertex_buffer);

  std::string to_string() const;

  // Public copy method
  vertex_buffer copy() const;

  void                 set_colors(color4 const&);
  static vertex_buffer create_interleaved(log_t&, ObjData const&, buffer_flags const&);
};

} // namespace opengl
