#pragma once
#include <common/macros_class.hpp>
#include <common/move_only_type.hpp>
#include <common/result.hpp>

#include <opengl/bind_type.hpp>

#include <extlibs/glew.hpp>
#include <string>

struct log_t;

namespace opengl
{
struct render_buffer_h
{
  debug_bind_state debug_bind_state_;
  GLuint           id;

  render_buffer_h();
  NO_COPY(render_buffer_h);
  MOVE_DEFAULT(render_buffer_h);

public:
  std::string to_string() const;

  static size_t constexpr NUM_BUFFERS = 1;
};

void
destroy_render_buffer(render_buffer_h&);

using render_buffer_ar = move_only_type<render_buffer_h, destroy_render_buffer>;

} // namespace opengl

namespace opengl
{
void
bind(log_t&, render_buffer_h&);

void
unbind(log_t&, render_buffer_h&);
} // namespace opengl