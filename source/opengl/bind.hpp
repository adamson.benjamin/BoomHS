#pragma once
#include <common/log.hpp>
#include <opengl/bind_type.hpp>

#ifdef OPENGL_RUNTIME_CHECKS
#define OPENGL_ONLY_IF_RCE(fn) fn()
#else
#define OPENGL_ONLY_IF_RCE(fn) []() {}
#endif // OPENGL_RUNTIME_CHECKS

namespace opengl
{
#ifdef OPENGL_RUNTIME_CHECKS
bool constexpr is_bound(debug_bind_state const& bs)
{
  using glbs = debug_bind_state_type;
  return glbs::BOUND == bs.bound_state;
}

bool constexpr is_not_bound(debug_bind_state const& bs)
{
  using glbs = debug_bind_state_type;
  return glbs::BOUND != bs.bound_state;
}
#else
bool constexpr is_bound(debug_bind_state const&) { return true; }
bool constexpr is_not_bound(debug_bind_state const&) { return true; }
#endif

template <typename T>
bool constexpr is_bound(T const& obj)
{
  return is_bound(obj.debug_bind_state_);
}
template <typename T>
bool constexpr is_not_bound(T const& obj)
{
  return is_not_bound(obj.debug_bind_state_);
}

} // namespace opengl

#define OPENGL_ASSERT_BOUND(obj)                                                                   \
  OPENGL_ONLY_IF_RCE([&]() {                                                                       \
    bool const nbound = opengl::is_bound(obj);                                                     \
    assert(nbound);                                                                                \
  })

#define OPENGL_ASSERT_NOT_BOUND(obj)                                                               \
  OPENGL_ONLY_IF_RCE([&]() {                                                                       \
    bool const not_bound = opengl::is_not_bound(obj);                                              \
    assert(not_bound);                                                                             \
  })

#define BIND_UNTIL_END_OF_SCOPE(LOGGER, ...)                                                       \
  bind_impl(LOGGER, __VA_ARGS__);                                                                  \
  ON_SCOPE_EXIT([&]() { unbind_impl(LOGGER, __VA_ARGS__); })

#define BIND_UNTIL_END_OF_SCOPE_IF(LOGGER, CONDITION, ...)                                         \
  if ((CONDITION)) {                                                                               \
    bind_impl(LOGGER, __VA_ARGS__);                                                                \
  }                                                                                                \
  ON_SCOPE_EXIT([&]() {                                                                            \
    if ((CONDITION)) {                                                                             \
      unbind_impl(LOGGER, __VA_ARGS__);                                                            \
    }                                                                                              \
  })

// This file contains the macros and classes that allow the application/game code to bind/unbind
// opengl resources easily, and help ensure that the minimal number of opengl calls to bind
// resources occur. The macro's make sure that a opengl resource bound, is unbound before it is
// bound again. This helps ensure optimal performance, as you can be sure no resource is repeatedly
// bound. All resources must be unbound before they can be bound again. In release mode, unbinding
// the resource currently doesn't do anything. In release mode, so far, it's been find to just let
// everything bind itself (since we know from debug builds things are only bound one time) and
// do without the explicit unbinding call. More testing will have to see if this will need to
// change or not.
//
// *** These macro's compile down to nothing (no source code generated) outside of debug builds.
//
//  There are two sets of macros available from here:
//   1) Macro's that allow easy bind/unbind of opengl resources on demand. This application drawing
//   code that needs to bind a VAO, shader program, before drawing a 3d object.
//
//   {
//     auto vao = ...;
//     BIND_UNTIL_END_OF_SCOPE(LOGGER, vao);
//     ...
//   } // the vao is unbound here
//

namespace opengl
{
template <typename T, typename... Args>
void
bind_unpack_obj(log_t& LOGGER, T& obj, Args&&... args)
{
  // Forward declaration required to prevent template two-phase lookup from failing.
  void bind(log_t&, T&, Args && ...);

  OPENGL_ASSERT_NOT_BOUND(obj);
  bind(LOGGER, obj, FORWARD(args));

  OPENGL_ONLY_IF_RCE(
      [&obj]() { (obj).debug_bind_state_.bound_state = ::debug_bind_state_type::BOUND; });
}

template <typename T, typename... Args>
void
unbind_unpack_obj(log_t& LOGGER, T& obj, Args&&... args)
{
  // Forward declaration required to prevent template two-phase lookup from failing.
  void unbind(log_t&, T&, Args && ...);

  OPENGL_ASSERT_BOUND(obj);
  unbind(LOGGER, obj, FORWARD(args));

  OPENGL_ONLY_IF_RCE(
      [&obj]() { (obj).debug_bind_state_.bound_state = ::debug_bind_state_type::NOT_BOUND; });
}

} // namespace opengl

template <typename... Args>
void
bind_impl(log_t& LOGGER, Args&&... args)
{
  ::opengl::bind_unpack_obj(LOGGER, FORWARD(args));
}

template <typename... Args>
void
unbind_impl(log_t& LOGGER, Args&&... args)
{
  ::opengl::unbind_unpack_obj(LOGGER, FORWARD(args));
}

namespace opengl
{
template <typename T, typename FN, typename... Args>
void
bind_for(log_t& LOGGER, T& obj, FN const& fn, Args&&... args)
{
  auto const obj_s = obj.to_string();
  LOG_TRACE("OPENGL binding: %s", obj_s);
  ON_SCOPE_EXIT([&]() { LOG_TRACE("OPENGL unbinding: %s", obj_s); });

  BIND_UNTIL_END_OF_SCOPE(LOGGER, obj, FORWARD(args));
  fn();
}

template <typename T, typename FN, typename... Args>
void
bind_for(log_t& LOGGER, T* pobj, FN const& fn, Args&&... args)
{
  assert(pobj);
  bind_for(LOGGER, *pobj, fn, FORWARD(args));
}

} // namespace opengl
