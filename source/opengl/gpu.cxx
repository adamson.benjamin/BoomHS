#include "gpu.hpp"
#include <opengl/bind.hpp>
#include <opengl/draw_info.hpp>
#include <opengl/global.hpp>
#include <opengl/shader.hpp>
#include <opengl/texture.hpp>
#include <opengl/vertex_attribute.hpp>

#include <boomhs/components.hpp>
#include <boomhs/entity.hpp>
#include <boomhs/obj.hpp>
#include <boomhs/obj_store.hpp>
#include <boomhs/terrain.hpp>
#include <boomhs/vertex_factory.hpp>

#include <common/algorithm.hpp>
#include <common/macros_class.hpp>

#include <array>
#include <extlibs/fmt.hpp>
#include <extlibs/glew.hpp>

using namespace opengl;
using namespace opengl::gpu;
using namespace glm;

namespace
{

template <typename VERTICES, typename INDICES>
void
copy_synchronous(log_t& LOGGER, vertex_attribute const& va, draw_state const& dinfo,
                 VERTICES const& vertices, INDICES const& indices)
{
  auto const bind_and_copy = [&]() {
    ::glBindBuffer(GL_ARRAY_BUFFER, dinfo.vbo());
    ::glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, dinfo.ebo());

    va.upload_vertex_format_to_glbound_vao(LOGGER);

    // copy the vertices
    LOG_DEBUG("inserting '%i' vertices into GL_BUFFER_ARRAY\n", vertices.size());
    auto const  vertices_size = static_cast<GLsizeiptr>(vertices.size() * sizeof(GLfloat));
    auto const& vertices_data = vertices.data();
    ::glBufferData(GL_ARRAY_BUFFER, vertices_size, vertices_data, GL_STATIC_DRAW);

    // copy the vertice rendering order
    LOG_DEBUG("inserting '%i' indices into GL_ELEMENT_BUFFER_ARRAY\n", indices.size());
    auto const  indices_size = static_cast<GLsizeiptr>(sizeof(GLuint) * indices.size());
    auto const& indices_data = indices.data();
    ::glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices_size, indices_data, GL_STATIC_DRAW);
  };

  LOG_TRACE("Starting synchronous cpu -> gpu copy");
  auto const& vao = dinfo.vao();
  bind_for(LOGGER, vao, bind_and_copy);
  LOG_TRACE("cpu -> gpu copy complete");
}

template <typename V, typename I>
draw_state
copy_gpu_impl(log_t& LOGGER, vertex_attribute const& va, V const& vertices, I const& indices)
{
  auto const num_indices = static_cast<GLsizei>(indices.size());
  draw_state dinfo{vertices.size(), num_indices};
  copy_synchronous(LOGGER, va, dinfo, vertices, indices);
  return dinfo;
}

template <typename V, typename I>
draw_state
make_drawinfo(log_t& LOGGER, vertex_attribute const& va, V const& vertex_data, I const& indices)
{
  auto const num_indices = static_cast<GLsizei>(indices.size());
  draw_state dinfo{vertex_data.size(), num_indices};
  copy_synchronous(LOGGER, va, dinfo, vertex_data, indices);
  return dinfo;
}

} // namespace

namespace opengl::gpu
{

draw_state
copy(log_t& LOGGER, vertex_attribute const& va, VertexFactory::ArrowVertices const& verts)
{
  return make_drawinfo(LOGGER, va, verts, VertexFactory::ARROW_INDICES);
}

draw_state
copy(log_t& LOGGER, vertex_attribute const& va, VertexFactory::LineVertices const& verts)
{
  return make_drawinfo(LOGGER, va, verts, VertexFactory::LINE_INDICES);
}

draw_state
copy(log_t& LOGGER, vertex_attribute const& va, VertexFactory::GridVerticesIndices const& grid)
{
  return make_drawinfo(LOGGER, va, grid.vertices, grid.indices);
}

draw_state
copy_cube_gpu(log_t& LOGGER, CubeVertices const& cv, vertex_attribute const& va)
{
  return make_drawinfo(LOGGER, va, cv, VertexFactory::CUBE_INDICES);
}

draw_state
copy_cube_wireframe_gpu(log_t& LOGGER, CubeVertices const& cv, vertex_attribute const& va)
{
  return make_drawinfo(LOGGER, va, cv, VertexFactory::CUBE_WIREFRAME_INDICES);
}

draw_state
copy_gpu(log_t& LOGGER, vertex_attribute const& va, ObjData const& data)
{
  auto const qa          = buffer_flags::from_va(va);
  auto       interleaved = vertex_buffer::create_interleaved(LOGGER, data, qa);

  return copy_gpu_impl(LOGGER, va, interleaved.vertices, interleaved.indices);
}

draw_state
copy_gpu(log_t& LOGGER, vertex_attribute const& va, vertex_buffer const& object)
{
  auto const& v = object.vertices;
  auto const& i = object.indices;
  return copy_gpu_impl(LOGGER, va, v, i);
}

draw_state
copy_rectangle(log_t& LOGGER, vertex_attribute const& va, RectBuffer const& buffer)
{
  auto const& v = buffer.vertices;
  auto const& i = buffer.indices;
  return copy_gpu_impl(LOGGER, va, v, i);
}

draw_state
copy_rectangle(log_t& LOGGER, vertex_attribute const& va, RectLineBuffer const& buffer)
{
  auto const& v = buffer.vertices;
  return copy_gpu_impl(LOGGER, va, v, VertexFactory::RECTANGLE_LINE_INDICES);
}

draw_state
copy_rectangle(log_t& LOGGER, vertex_attribute const& va, RectangleUvVertices const& vertices)
{
  auto const& i = VertexFactory::RECTANGLE_DEFAULT_INDICES;

  draw_state dinfo{vertices.size(), i.size()};
  copy_synchronous(LOGGER, va, dinfo, vertices, i);
  return dinfo;
}

void
overwrite_vertex_buffer(log_t& LOGGER, vertex_attribute const& va, draw_state const& dinfo,
                        ObjData const& objdata)
{
  auto const upload = [&]() {
    ::glBindBuffer(GL_ARRAY_BUFFER, dinfo.vbo());
    va.upload_vertex_format_to_glbound_vao(LOGGER);

    auto const qa          = buffer_flags::from_va(va);
    auto       interleaved = vertex_buffer::create_interleaved(LOGGER, objdata, qa);

    auto const& vertices      = interleaved.vertices;
    auto const  vertices_size = vertices.size() * sizeof(GLfloat);
    auto const& vertices_data = vertices.data();
    glBufferSubData(GL_ARRAY_BUFFER, 0, vertices_size, vertices_data);
  };

  auto const& vao = dinfo.vao();
  bind_for(LOGGER, vao, upload);
}

} // namespace opengl::gpu
