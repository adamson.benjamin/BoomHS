#pragma once
#include <opengl/bind.hpp>
#include <opengl/debug.hpp>
#include <opengl/shader.hpp>

#include <common/macros_type.hpp>

#include <extlibs/glew.hpp>
#include <extlibs/glm.hpp>

namespace opengl::detail
{
template <typename F, typename T>
void
set_uniform_value(GLint const loc, F const& f, T const& value)
{
  f(loc, value);
}

template <typename F, typename A>
void
set_uniform_array(GLint const loc, F const& f, A const& array)
{

  // https://www.opengl.org/sdk/docs/man/html/glUniform.xhtml
  //
  // For the vector (glUniform*v) commands, specifies the number of elements that are to be
  // modified.
  // This should be 1 if the targeted uniform variable is not an array, and 1 or more if it is an
  // array.
  GLsizei constexpr COUNT = 1;
  f(loc, COUNT, array);
}

template <typename F, typename T>
void
set_uniform_matrix(GLint const loc, F const& f, T const& matrix)
{
  // https://www.opengl.org/sdk/docs/man/html/glUniform.xhtml
  //
  // count:
  // For the matrix (glUniformMatrix*) commands, specifies the number of matrices that are to be
  // modified.
  // This should be 1 if the targeted uniform variable is not an array of matrices, and 1 or more
  // if it is an array of matrices.
  GLsizei constexpr COUNT                = 1;
  GLboolean constexpr TRANSPOSE_MATRICES = GL_FALSE;

  f(loc, COUNT, TRANSPOSE_MATRICES, glm::value_ptr(matrix));
}

} // namespace opengl::detail

namespace opengl::uniform
{
// Set a uniform variable for the shader program (argument).
//
// This function maps user types to types understood by OpenGL, ie: primitives, array's, and
// matrices.
//
// This mapping from user-type to OpenGL type happens at compile time.
template <typename Uniform>
void
set(log_t& LOGGER, shader_type& sp, GLchar const* name, Uniform const& uniform)
{
  static_assert(!std::is_const<Uniform>::value);
  static_assert(!std::is_reference<Uniform>::value);

  OPENGL_ASSERT_BOUND(sp);

  // Lookup the location of the uniform in the shader program.
  auto const loc = sp.get_uniform_location(LOGGER, name);

  // Helper function that firsts log's information about the uniform varible being set, and then
  // executes the function to set the uniform variable.
  auto const log_then_set = [&](char const* type_name, auto const& data_string,
                                auto const& set_uniform_fn, auto&&... args) {
    LOG_DEBUG("Setting OpenGL Program uniform, name: '%s:%s' location: '%d' data: '%s'", name,
              type_name, loc, data_string);
    set_uniform_fn(FORWARD(args));
  };

  // Setup a bunch of lambda functions that handle capturing the relavent variables so we can make
  // the table below as simple as possible.
  auto const set_pod = [&](auto const& gl_fn, auto const& value, char const* type_name) {
    auto const fn = [&]() { detail::set_uniform_value(loc, gl_fn, value); };
    log_then_set(type_name, std::to_string(value), fn);
  };
  auto const set_array = [&](auto const& gl_fn, auto const& array, char const* type_name) {
    auto const fn = [&]() { detail::set_uniform_array(loc, gl_fn, array.data()); };
    log_then_set(type_name, common::stringify(array), fn);
  };
  auto const set_vecn = [&](auto const& gl_fn, auto const& vecn, char const* type_name) {
    auto const fn = [&]() { detail::set_uniform_array(loc, gl_fn, glm::value_ptr(vecn)); };
    log_then_set(type_name, glm::to_string(vecn), fn);
  };
  auto const set_color = [&](auto const& gl_fn, auto const& color, char const* type_name) {
    if constexpr (TYPES_MATCH(Uniform, color3)) {
      set_vecn(gl_fn, color::vec3(color), type_name);
    }
    else if constexpr (TYPES_MATCH(Uniform, color4)) {
      set_vecn(gl_fn, color::vec4(color), type_name);
    }
    else {
      LOG_ERROR("Invalid color4 Type, cannot set.");
      std::exit(EXIT_FAILURE);
    }
  };
  auto const set_matrix = [&](auto const& gl_fn, auto const& matrix, char const* type_name) {
    auto const fn = [&]() { detail::set_uniform_matrix(loc, gl_fn, matrix); };
    log_then_set(type_name, glm::to_string(matrix), fn);
  };

  // clang-format on
  // This table uses the above macros to select exactly 1 implementation based on the uniform
  // type. Effectively a bunch of template specialization's, except using "constexpr if" to pick one
  // lambda function to call per-uniform type.
  //
  // The table below maps user types (such as int, vec3, ...) to OpenGL types, for the purposes of
  // setting an opengl program uniform value.
  // Inside the macro **_TYPE_TO_FN (defined below) we call through to the lambdas eventually
  // setting the uniform by calling the OpenGL function provided. This is all handled
  // automatically.
  //
  // Type's not explicitely mapped in the table result in execution ending with a call to
  // std::exit(EXIT_FAILURE).
  //
  // NOTE: The 'uniform' field must remain in the table below, because it's type is evaluated
  // polymorphically at compile time thanks to "constexpr if" and polymorhic lambdas inside the
  // macro.
  //
  // Unfortunately for now this means 'uniform' must remain as an explicit column. Trying to capture
  // 'uniform' in the helper macros above result's in a compile-time error (this due to the lambda
  // not being in a polymorphic context when instantiated outside of the 'constexpr if').
  //
  // Macro for Mapping a user type -> OpenGL type.
#define IF___MAP_TYPE(UserType, FN, ...)                                                           \
  if constexpr (all_all_same<Uniform, UserType>) {                                                 \
    FN(__VA_ARGS__);                                                                               \
  }
#define ELIF_MAP_TYPE(UserType, ...) else IF___MAP_TYPE(UserType, __VA_ARGS__)

  //              TYPE         HELPER_FN   OPENGL_FN           UNIFORM                  UNIFORM_TYPE
  //              ----------------------------------------------------------------------------------
  IF___MAP_TYPE(int, set_pod, glUniform1i, uniform, "int1")
  ELIF_MAP_TYPE(bool, set_pod, glUniform1i, uniform, "bool1")
  ELIF_MAP_TYPE(float, set_pod, glUniform1f, uniform, "float")

  ELIF_MAP_TYPE(color3, set_color, glUniform3fv, uniform, "color3")
  ELIF_MAP_TYPE(color4, set_color, glUniform4fv, uniform, "color4")

  ELIF_MAP_TYPE(FloatArray2, set_array, glUniform2fv, uniform, "array2")
  ELIF_MAP_TYPE(FloatArray3, set_array, glUniform3fv, uniform, "array3")
  ELIF_MAP_TYPE(FloatArray4, set_array, glUniform4fv, uniform, "array4")

  ELIF_MAP_TYPE(glm::vec2, set_vecn, glUniform2fv, uniform, "vec2")
  ELIF_MAP_TYPE(glm::vec3, set_vecn, glUniform3fv, uniform, "vec3")
  ELIF_MAP_TYPE(glm::vec4, set_vecn, glUniform4fv, uniform, "vec4")

  ELIF_MAP_TYPE(glm::mat2, set_matrix, glUniformMatrix2fv, uniform, "mat2")
  ELIF_MAP_TYPE(glm::mat3, set_matrix, glUniformMatrix3fv, uniform, "mat3")
  ELIF_MAP_TYPE(glm::mat4, set_matrix, glUniformMatrix4fv, uniform, "mat4")
  else
  {
    // This code path only gets instantiated by unhandled types.
    LOG_ERROR("Programming Error. Invalid type of uniform, cannot set. See set_uniform() table.");
    std::exit(EXIT_FAILURE);
  }
  // clang-format off
#undef IF___MAP_TYPE
#undef ELIF_MAP_TYPE
}

template <typename T>
void
set(log_t& logger, shader_type& sp, std::string const& name, T const& value)
{
  set(logger, sp, name.c_str(), value);
}

} // namespace opengl::uniform
