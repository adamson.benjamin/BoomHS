#pragma once
#include <boomhs/color.hpp>
#include <boomhs/font.hpp>
#include <boomhs/lighting.hpp>

#include <math/matrices.hpp>
#include <math/rectangle.hpp>

#include <common/macros_class.hpp>

#include <extlibs/glew.hpp>
#include <extlibs/glm.hpp>
#include <extlibs/sdl.hpp>

#include <string>
#include <vector>

class DeltaTime;
struct Fog;
class HandleManager;
class LevelManager;
struct log_t;
struct Material;
class Player;
class SDLWindow;
class TerrainGrid;
class Viewport;

namespace opengl
{
class draw_state;
class shader_type;
class texture_h;

struct draw_call_counter
{
  size_t num_vertices  = 0;
  size_t num_drawcalls = 0;

  bool draw_wireframes = false;

  draw_call_counter() = default;
  NO_MOVE_OR_COPY(draw_call_counter);

  std::string to_string() const;
};

struct culling_state
{
  GLboolean enabled;
  GLenum    mode;
};

struct culling_and_winding_state
{
  GLenum        winding;
  culling_state culling;
};

struct blend_state
{
  GLboolean enabled;
  GLint     source_alpha, dest_alpha;
  GLint     source_rgb, dest_rgb;
};

} // namespace opengl

#define PUSH_CW_STATE_UNTIL_END_OF_SCOPE()                                                         \
  auto const cw_state = ::opengl::render::read_cwstate();                                          \
  ON_SCOPE_EXIT([&]() { ::opengl::render::set_cwstate(cw_state); })

#define PUSH_BLEND_STATE_UNTIL_END_OF_SCOPE()                                                      \
  auto const blend_state = ::opengl::render::read_blendstate();                                    \
  ON_SCOPE_EXIT([&]() { ::opengl::render::set_blendstate(blend_state); })

#define ENABLE_ALPHA_BLENDING_UNTIL_SCOPE_EXIT()                                                   \
  PUSH_BLEND_STATE_UNTIL_END_OF_SCOPE();                                                           \
  ::glEnable(GL_BLEND);                                                                            \
  ::glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

#define ENABLE_ADDITIVE_BLENDING_UNTIL_SCOPE_EXIT()                                                \
  PUSH_BLEND_STATE_UNTIL_END_OF_SCOPE();                                                           \
  ::glEnable(GL_BLEND);                                                                            \
  ::glBlendFunc(GL_SRC_ALPHA, GL_ONE)

#define ENABLE_SCISSOR_TEST_UNTIL_SCOPE_EXIT()                                                     \
  glEnable(GL_SCISSOR_TEST);                                                                       \
  ON_SCOPE_EXIT([]() { ::glDisable(GL_SCISSOR_TEST); })

struct DrawLetterConfig
{
  ProjMatrix const&          pm;
  opengl::shader_type&       sp;
  opengl::texture_h&         ti;
  opengl::draw_call_counter& ds;
};

namespace opengl::render
{

culling_and_winding_state
read_cwstate();

void
set_cwstate(culling_and_winding_state const&);

blend_state
read_blendstate();

void
set_blendstate(blend_state const&);

void
init(log_t&, SDLWindow&);

void
clear_screen(color4 const&);

// TODO: keep these extract rest to sub-renderers
void
draw_2dgui(log_t&, GLenum, shader_type&, draw_state const&, draw_call_counter&);

void
draw_2dgui_letter(log_t&, LetterRects const&, glm::vec2 const&, DrawLetterConfig const&);

void
draw_2dgui_letter(log_t&, glm::vec2 const&);

float
draw_2dgui_sentence(log_t&, RectFloat const&, LetterRectCollection const&, color3 const&,
                    shader_type&, texture_h&, ProjMatrix const&, draw_call_counter&);

void
draw_3dlightsource(log_t&, GLenum, CameraMatrix const&, ModelMatrix const&, shader_type&,
                   draw_state const&, draw_call_counter&);

void
draw_3dshape(log_t&, GLenum, CameraMatrix const&, ViewMatrix const&, ModelMatrix const&, Fog const&,
             shader_type&, draw_state const&, bool, draw_call_counter&);

void
draw_3d_no_lighting(log_t&, GLenum, CameraMatrix const&, ModelMatrix const&, shader_type&,
                    draw_state const&, draw_call_counter&);

struct shape_and_lighting
{
  glm::vec3 const&   position;
  ModelMatrix const& model_matrix;

  draw_state const& dinfo;
  shader_type&      sp;
  Material const&   material;
  bool const        set_normalmatrix;
  bool const        draw_normals;
};

void
draw_3dlit_shape(log_t&, GLenum, CameraMatrix const&, ViewMatrix const&, GlobalLight const&,
                 DirectionalLightList const&, PointLightList const&, Fog const&,
                 shape_and_lighting&, draw_call_counter&);

// TODO: move rest to sub-renderers or something
void
draw_drawinfo(log_t&, draw_call_counter&, GLenum, shader_type&, draw_state const&);

//////////
// Direct drawing API
void
draw_2delements(log_t&, GLenum, shader_type const&, GLsizei, draw_call_counter&);

void
draw_2delements(log_t&, GLenum, shader_type const&, texture_h&, GLsizei, draw_call_counter&);

void
draw_elements(GLenum, shader_type const&, GLsizei, draw_call_counter&);

//////////

void
draw_arrow(log_t&, shader_type&, CameraMatrix const&, glm::vec3 const&, glm::vec3 const&,
           color4 const&, draw_call_counter&);

void
draw_2dtextured_rect(log_t&, ProjMatrix const&, RectFloat, RectFloat, shader_type&, texture_h&,
                     draw_call_counter&);

void
draw_line(log_t&, shader_type&, glm::vec3 const&, glm::vec3 const&, color4 const&,
          draw_call_counter&);

void
draw_grid_lines(log_t&, enttreg_t&, glm::vec3 const&, CameraMatrix const&, draw_call_counter&);

void
set_modelmatrix(log_t&, glm::mat4 const&, shader_type&);

void
set_mvpmatrix(log_t&, glm::mat4 const&, glm::mat4 const&, shader_type&);

void
set_scissor(Viewport const&, int);

void
set_viewport(Viewport const&, int);

void
set_viewport_and_scissor(Viewport const&, int);

} // namespace opengl::render

namespace OR = opengl::render;
