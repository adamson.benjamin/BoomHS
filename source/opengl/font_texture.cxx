#include "font_texture.hpp"

#include <algorithm>
#include <extlibs/boost_algorithm.hpp>

namespace opengl
{

font_and_texture const*
font_storage::lookup(std::string const& name) const
{
  auto const cmp = [&name](auto const& it) { return boost::iequals(it.first, name); };
  auto const it  = std::find_if(data_.begin(), data_.end(), cmp);
  return it == data_.cend() ? nullptr : &it->second;
}

font_and_texture*
font_storage::lookup(std::string const& name)
{
  // https://stackoverflow.com/a/47369227/562174
  return const_cast<font_and_texture*>(std::as_const(*this).lookup(name));
}

void
font_storage::add(font_and_texture&& ft)
{
  auto pair = std::make_pair(ft.ffile.name, MOVE(ft));
  data_.emplace(MOVE(pair));
}

} // namespace opengl
