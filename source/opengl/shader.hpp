#pragma once
#include <boomhs/entity.hpp>
#include <boomhs/lighting.hpp>
#include <common/result.hpp>
#include <opengl/shader_type.hpp>
#include <string>

struct log_t;

namespace shader
{
using load_r = Result<opengl::shader_type, std::string>;

load_r
from_sourcecode(log_t&, opengl::shader_cfg&&, NumLights);

load_r
from_files(log_t&, opengl::shader_cfg&&, NumLights);

Result<NOTHING, std::string>
reload_from_files(log_t&, enttreg_t&, NumLights);

} // namespace shader

namespace shader2d
{
opengl::shader_type*
lookup(enttreg_t&, std::string const&);

opengl::shader_type&
ref(enttreg_t&, std::string const&);

opengl::shader_type&
font(enttreg_t&);

opengl::shader_type&
silhoutte(enttreg_t&);

opengl::shader_type&
texture2d(enttreg_t&);

} // namespace shader2d

namespace shader3d
{
/// Pointlight Functions
using AdjustNumPointLightsResult = Result<NOTHING, std::string>;

AdjustNumPointLightsResult
reload_shaders(log_t&, enttreg_t&);

AdjustNumPointLightsResult
decrement_num_pointlights(log_t&, enttreg_t&);

AdjustNumPointLightsResult
increment_num_pointlights(log_t&, enttreg_t&);

AdjustNumPointLightsResult
set_num_lights(log_t&, enttreg_t&, NumLights);

NumLights
num_lights(enttreg_t const&);

/// General-purpose lookup
opengl::shader_type*
lookup(enttreg_t&, std::string const&);

opengl::shader_type&
ref(enttreg_t&, std::string const&);

/// Specific lookup
opengl::shader_type&
silhoutte(enttreg_t&);

opengl::shader_type&
skybox(enttreg_t&);

opengl::shader_type&
sunshaft(enttreg_t&);

opengl::shader_type&
water_basic(enttreg_t&);

opengl::shader_type&
water_medium(enttreg_t&);

opengl::shader_type&
water_advanced(enttreg_t&);

opengl::shader_type&
wireframe(enttreg_t&);

} // namespace shader3d
