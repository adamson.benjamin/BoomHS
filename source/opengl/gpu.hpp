#pragma once
#include <boomhs/color.hpp>
#include <boomhs/vertex_factory.hpp>

#include <boomhs/shape.hpp>
#include <opengl/draw_info.hpp>

#include <common/log.hpp>

#include <extlibs/glm.hpp>

class Obj;
class ObjData;

namespace opengl
{
struct vertex_buffer;
class vertex_attribute;
struct GridVerticesIndices;

} // namespace opengl

namespace opengl::gpu
{

namespace detail
{

template <typename VERTICES, typename INDICES>
void
copy_synchronous(log_t& LOGGER, vertex_attribute const& va, draw_state const& dinfo,
                 VERTICES const& vertices, INDICES const& indices)
{
  auto const bind_and_copy = [&]() {
    glBindBuffer(GL_ARRAY_BUFFER, dinfo.vbo());
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, dinfo.ebo());

    va.upload_vertex_format_to_glbound_vao(LOGGER);

    // copy the vertices
    LOG_DEBUG("inserting '%i' vertices into GL_BUFFER_ARRAY\n", vertices.size());
    auto const  vertices_size = vertices.size() * sizeof(GLfloat);
    auto const& vertices_data = vertices.data();
    glBufferData(GL_ARRAY_BUFFER, vertices_size, vertices_data, GL_STATIC_DRAW);

    // copy the vertice rendering order
    LOG_DEBUG("inserting '%i' indices into GL_ELEMENT_BUFFER_ARRAY\n", indices.size());
    auto const  indices_size = sizeof(GLuint) * indices.size();
    auto const& indices_data = indices.data();
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices_size, indices_data, GL_STATIC_DRAW);
  };

  LOG_TRACE("Starting synchronous cpu -> gpu copy");
  auto& vao = dinfo.vao();
  bind_for(LOGGER, vao, bind_and_copy);
  LOG_TRACE("cpu -> gpu copy complete");
}

} // namespace detail

draw_state
copy(log_t&, vertex_attribute const&, VertexFactory::ArrowVertices const&);

draw_state
copy(log_t&, vertex_attribute const&, VertexFactory::LineVertices const&);

draw_state
copy(log_t&, vertex_attribute const&, VertexFactory::GridVerticesIndices const&);

///////////////////////////////////////////////////////////////////////////////////////////////////
// Cubes
draw_state
copy_cube_gpu(log_t&, CubeVertices const&, vertex_attribute const&);

draw_state
copy_cube_wireframe_gpu(log_t&, CubeVertices const&, vertex_attribute const&);

///////////////////////////////////////////////////////////////////////////////////////////////////
// Rectangles
draw_state
copy_rectangle(log_t&, vertex_attribute const&, RectBuffer const&);

draw_state
copy_rectangle(log_t&, vertex_attribute const&, RectLineBuffer const&);

draw_state
copy_rectangle(log_t&, vertex_attribute const&, RectangleUvVertices const&);

///////////////////////////////////////////////////////////////////////////////////////////////////
// General

draw_state
create_modelnormals(log_t&, vertex_attribute const&, glm::mat4 const&, Obj const&, color4 const&);

draw_state
copy_gpu(log_t&, vertex_attribute const&, ObjData const&);

draw_state
copy_gpu(log_t&, vertex_attribute const&, vertex_buffer const&);

void
overwrite_vertex_buffer(log_t&, vertex_attribute const&, draw_state const&, ObjData const&);

} // namespace opengl::gpu
namespace OG = opengl::gpu;
