#pragma once
#include <common/macros_container.hpp>

#include <extlibs/glew.hpp>

#include <array>
#include <string>
#include <vector>

struct log_t;

namespace opengl
{
enum class vertex_attribute_type
{
  POSITION = 0,
  NORMAL,
  COLOR,
  UV,

  // We hardcode the others, but this allows things to stay flexible.
  OTHER
};

vertex_attribute_type
attribute_type_from_string(char const*);

struct attribute_pointer
{
  // fields
  GLuint                index           = 0;
  GLenum                datatype        = 0;
  vertex_attribute_type typezilla       = vertex_attribute_type::OTHER;
  GLsizei               component_count = 0;

  // constructors
  attribute_pointer() = default;

  constexpr attribute_pointer(GLuint const i, GLenum const t, vertex_attribute_type const at,
                              GLsizei const cc)
      : index(i)
      , datatype(t)
      , typezilla(at)
      , component_count(cc)
  {
  }

  COPYMOVE_DEFAULT(attribute_pointer);

  // methods
  std::string to_string() const;
};

std::ostream&
operator<<(std::ostream&, attribute_pointer const&);

class vertex_attribute
{
public:
  static constexpr GLsizei API_BUFFER_SIZE = 4;

private:
  size_t                                         num_apis_;
  GLsizei                                        stride_;
  std::array<attribute_pointer, API_BUFFER_SIZE> apis_;

public:
  COPYMOVE_DEFAULT(vertex_attribute);
  explicit vertex_attribute(size_t, GLsizei, std::array<attribute_pointer, API_BUFFER_SIZE>&&);

  void upload_vertex_format_to_glbound_vao(log_t&) const;
  auto stride() const { return stride_; }

  bool has_vertices() const;
  bool has_normals() const;
  bool has_colors() const;
  bool has_uvs() const;

  std::string to_string() const;

  BEGIN_END_FORWARD_FNS(apis_)
};

std::ostream&
operator<<(std::ostream&, vertex_attribute const&);

vertex_attribute make_vertex_attribute(std::initializer_list<attribute_pointer>);

vertex_attribute
make_vertex_attribute(attribute_pointer const&);

vertex_attribute
make_vertex_attribute(std::vector<attribute_pointer> const&);

template <size_t N>
vertex_attribute
make_vertex_attribute(std::array<attribute_pointer, N> const& arr)
{
  auto const b = std::cbegin(arr);
  auto const e = std::cend(arr);
  auto const v = std::vector<attribute_pointer>{b, e};
  return make_vertex_attribute(v);
}

} // namespace opengl
