#include "debug_renderer.hpp"
#include <opengl/bind.hpp>
#include <opengl/gpu.hpp>
#include <opengl/renderer.hpp>
#include <opengl/shader.hpp>

#include <boomhs/camera.hpp>
#include <boomhs/components.hpp>
#include <boomhs/delta_time.hpp>
#include <boomhs/player.hpp>
#include <boomhs/state.hpp>
#include <boomhs/vertex_factory.hpp>

#include <common/FOR.hpp>

#include <math/frustum.hpp>

#include <extlibs/glm.hpp>

#include <array>

using namespace math;
using namespace opengl;

namespace
{

struct world_origin_arrows
{
  draw_state x_dinfo;
  draw_state y_dinfo;
  draw_state z_dinfo;
};

world_origin_arrows
create_axis_arrows(log_t& LOGGER, vertex_attribute const& va)
{
  auto constexpr ORIGIN = constants::ZERO;
  ArrowTemplate constexpr acx{LOC4::RED, ORIGIN, constants::X_UNIT_VECTOR};
  ArrowTemplate constexpr acy{LOC4::GREEN, ORIGIN, constants::Y_UNIT_VECTOR};
  ArrowTemplate constexpr acz{LOC4::BLUE, ORIGIN, constants::Z_UNIT_VECTOR};

  auto const avx = VertexFactory::build(acx);
  auto const avy = VertexFactory::build(acy);
  auto const avz = VertexFactory::build(acz);

  auto x = OG::copy(LOGGER, va, avx);
  auto y = OG::copy(LOGGER, va, avy);
  auto z = OG::copy(LOGGER, va, avz);
  return world_origin_arrows{MOVE(x), MOVE(y), MOVE(z)};
}

void
draw_axis(log_t& LOGGER, GameState& gs, render_state& rstate, glm::vec3 const& pos)
{
  auto& ldata        = gs.ldata;
  auto& sp           = shader3d::ref(ldata.registry, "3d_pos_color");
  auto  world_arrows = create_axis_arrows(LOGGER, sp.va());

  Transform transform;
  transform.translation = pos;

  auto const draw_axis_arrow = [&](draw_state const& dinfo) {
    BIND_UNTIL_END_OF_SCOPE(LOGGER, dinfo);
    render::draw_2dgui(LOGGER, GL_LINES, sp, dinfo, rstate.count);
  };

  auto const& cm = rstate.cm;
  bind_for(LOGGER, sp, [&]() {
    render::set_mvpmatrix(LOGGER, cm.camera_matrix(), transform.model_matrix(), sp);
    draw_axis_arrow(world_arrows.x_dinfo);
    draw_axis_arrow(world_arrows.y_dinfo);
    draw_axis_arrow(world_arrows.z_dinfo);
  });

  LOG_TRACE("Finished Drawing Global Axis");
}

void
conditionally_draw_player_vectors(log_t& LOGGER, GameState& gs, render_state& rstate,
                                  Player const& player)
{
  auto&       ui_debug = gs.uibuffers_classic.debug;
  auto&       ldata    = gs.ldata;
  auto&       sp       = shader3d::ref(ldata.registry, "3d_pos_color");
  auto const& cm       = rstate.cm;

  auto const draw_local_axis = [&](auto const& wo) {
    glm::vec3 const pos = wo.world_position();
    // local-space
    //
    // eye-forward
    auto const fwd = wo.eye_forward();
    render::draw_arrow(LOGGER, sp, cm.camera_matrix(), pos, pos + (2.0f * fwd), LOC4::PURPLE,
                       rstate.count);

    // eye-up
    auto const up = wo.eye_up();
    render::draw_arrow(LOGGER, sp, cm.camera_matrix(), pos, pos + up, LOC4::YELLOW, rstate.count);

    // eye-right
    auto const right = wo.eye_right();
    render::draw_arrow(LOGGER, sp, cm.camera_matrix(), pos, pos + right, LOC4::ORANGE,
                       rstate.count);
  };

  if (ui_debug.show_player_localspace_vectors) {
    draw_local_axis(player.world_object());
    draw_local_axis(player.head_world_object());
  }
  if (ui_debug.show_player_worldspace_vectors) {
    draw_axis(LOGGER, gs, rstate, player.world_position());
    draw_axis(LOGGER, gs, rstate, player.head_world_object().transform().translation);
  }
}

void
draw_frustum(log_t& LOGGER, GameState& gs, render_state& rs, Camera const& camera)
{
  auto const& fr   = rs.fr;
  auto const  proj = camera.proj_matrix(fr);
  auto const  view = camera.view_matrix(fr);

  auto const model = [&]() {
    Transform camera_transform;
    camera_transform.translation = camera.position();
    return camera_transform.model_matrix();
  }();

  auto const      mvp  = math::mvp_matrix(model, view, proj);
  glm::mat4 const invm = glm::inverse(mvp);

  std::array<glm::vec4, 8> const f = {{{-1, -1, fr.near, 1.0f}, // near face
                                       {1, -1, fr.near, 1.0f},
                                       {1, 1, fr.near, 1.0f},
                                       {-1, 1, fr.near, 1.0f},

                                       {-1, -1, fr.far, 1.0f}, // far face
                                       {1, -1, fr.far, 1.0f},
                                       {1, 1, fr.far, 1.0f},
                                       {-1, 1, fr.far, 1.0f}}};

  std::array<glm::vec3, 8> v;
  FOR(i, v.size())
  {
    glm::vec4 const ff = invm * f[i];
    v[i].x             = ff.x / ff.w;
    v[i].y             = ff.y / ff.w;
    v[i].z             = ff.z / ff.w;
  }

  auto& ldata = gs.ldata;
  auto& sp    = shader3d::ref(ldata.registry, "line");
  auto& ds    = rs.count;

  auto const draw_line = [&](auto const& head, auto const& tail, auto const& color) {
    render::draw_line(LOGGER, sp, head, tail, color, ds);
  };

  draw_line(v[0], v[1], LOC4::BLUE);
  draw_line(v[1], v[2], LOC4::BLUE);
  draw_line(v[2], v[3], LOC4::BLUE);
  draw_line(v[3], v[0], LOC4::BLUE);

  draw_line(v[4], v[5], LOC4::GREEN);
  draw_line(v[5], v[6], LOC4::GREEN);
  draw_line(v[6], v[7], LOC4::GREEN);
  draw_line(v[7], v[4], LOC4::GREEN);

  draw_line(v[0], v[4], LOC4::RED);
  draw_line(v[1], v[5], LOC4::RED);
  draw_line(v[2], v[6], LOC4::RED);
  draw_line(v[3], v[7], LOC4::RED);
}

} // namespace

namespace opengl
{

////////////////////////////////////////////////////////////////////////////////////////////////////
// DebugRenderer
void
debug_renderer::render_scene(log_t& LOGGER, GameState& gs, render_state& rstate,
                             Camera const& camera)
{
  auto& ui_debug = gs.uibuffers_classic.debug;
  auto& ldata    = gs.ldata;
  auto& reg      = ldata.registry;

  auto const& grid_lines = ui_debug.grid_lines;
  if (grid_lines.show) {
    auto const& grid_dimensions = grid_lines.dimensions;
    auto const& cm              = rstate.cm;
    render::draw_grid_lines(LOGGER, reg, grid_dimensions, cm.camera_matrix(), rstate.count);
  }

  auto& player = find_player(reg);
  if (ui_debug.show_global_axis) {
    draw_axis(LOGGER, gs, rstate, constants::ZERO);
  }
  if (ui_debug.draw_view_frustum) {
    draw_frustum(LOGGER, gs, rstate, camera);
  }

  // if checks happen inside fn
  conditionally_draw_player_vectors(LOGGER, gs, rstate, player);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// BlackSceneRenderer
void
scene_renderer_silhouette::render_scene()
{
}

} // namespace opengl
