#include "entity_renderer.hpp"
#include <opengl/bind.hpp>
#include <opengl/gpu.hpp>
#include <opengl/renderer.hpp>
#include <opengl/shader_type.hpp>
#include <opengl/texture.hpp>
#include <opengl/uniform.hpp>

#include <boomhs/billboard.hpp>
#include <boomhs/bounding_object.hpp>
#include <boomhs/components.hpp>
#include <boomhs/delta_time.hpp>
#include <boomhs/leveldata.hpp>
#include <boomhs/material.hpp>
#include <boomhs/npc.hpp>
#include <boomhs/player.hpp>
#include <boomhs/state.hpp>
#include <boomhs/tree.hpp>
#include <boomhs/vertex_interleave.hpp>
#include <boomhs/water.hpp>

#include <gl_sdl/global.hpp>

#include <math/view_frustum.hpp>

using namespace math::constants;
using namespace opengl;

namespace
{
void
draw_shape_with_light(log_t& LOGGER, GameState& gs, render_state& rs, CameraMatrices const& cm,
                      GLenum const dm, eid_t const eid, enttreg_t& reg, shader_type& sp,
                      draw_state const& dinfo, glm::vec3 const& tr, glm::mat4 const& model_matrix)
{
  Material const& material    = reg.get<Material>(eid);
  auto const      pointlights = find_pointlights(reg);
  auto const      dirlights   = find_dirlights(reg);

  auto const& ui_debug     = gs.uibuffers_classic.debug;
  bool const  draw_normals = ui_debug.draw_normals;

  // When drawing entities, we always want the normal matrix set.
  bool constexpr SET_NORMALMATRIX = true;

  render::shape_and_lighting lit_shape{tr,       model_matrix,     dinfo,       sp,
                                       material, SET_NORMALMATRIX, draw_normals};

  auto const  vm           = cm.view;
  auto&       ldata        = gs.ldata;
  auto const& global_light = ldata.ambient;
  auto const& fog          = ldata.fog;
  auto&       ds           = rs.count;

  render::draw_3dlit_shape(LOGGER, dm, cm.camera_matrix(), vm, global_light, dirlights, pointlights,
                           fog, lit_shape, ds);
}

template <typename... Args>
void
draw_entity(log_t& LOGGER, GameState& gs, render_state& rs, CameraMatrices const& cm,
            GLenum const dm, shader_type& sp, eid_t const eid, draw_state const& dinfo,
            Transform& transform, IsRenderable& is_r, AABoundingBox& bbox, Args&&...)
{
  // If entity is not visible, just return.
  if (is_r.hidden) {
    return;
  }
  if (!ViewFrustum::bbox_inside(cm.view, cm.proj, transform, bbox)) {
    return;
  }

  auto&      ldata = gs.ldata;
  auto&      reg   = ldata.registry;
  auto const mm    = transform.model_matrix();

  bool const is_lightsource = reg.try_get<PointLight>(eid);
  bool const receives_light = reg.try_get<Material>(eid);

  auto& ds = rs.count;
  BIND_UNTIL_END_OF_SCOPE(LOGGER, dinfo);
  if (is_lightsource) {
    render::draw_3dlightsource(LOGGER, dm, cm.camera_matrix(), mm, sp, dinfo, ds);
  }
  else if (receives_light) {
    auto const& tr = transform.translation;
    draw_shape_with_light(LOGGER, gs, rs, cm, dm, eid, reg, sp, dinfo, tr, mm);
    return;
  }
  else {
    render::draw_drawinfo(LOGGER, ds, dm, sp, dinfo);
  }
}

auto
compute_billboard_mvp(CameraMatrices const& cm, Transform const& tr, BillboardType const bb_type)
{
  auto const view_model = billboard::compute_viewmodel(tr, cm.view, bb_type);
  return cm.proj * view_model;
}

void
draw_orbital_body(log_t& LOGGER, GameState& gs, render_state& rs, CameraMatrices const& cm,
                  shader_type& sp, eid_t const eid, Transform& transform, IsRenderable& is_r,
                  AABoundingBox& bbox, BillboardRenderable& bboard, OrbitalBody&, TextureRenderable)
{
  {
    auto const mvp = compute_billboard_mvp(cm, transform, bboard.value);
    uniform::set(LOGGER, sp, "u_mv", mvp);
  }

  auto& ldata = gs.ldata;
  auto& reg   = ldata.registry;
  auto* pti   = reg.get<TextureRenderable>(eid).texture_info;
  assert(pti);
  auto&       ti    = *pti;
  auto const& dinfo = reg.get<opengl::draw_state>(eid);

  BIND_UNTIL_END_OF_SCOPE(LOGGER, ti);
  draw_entity(LOGGER, gs, rs, cm, GL_TRIANGLES, sp, eid, dinfo, transform, is_r, bbox);
}

void
draw_billboard_nodepth_tests(log_t& LOGGER, LevelData& ldata, draw_call_counter& ds,
                             shader_type& sp, char const* texture_name)
{
  auto& ttable = ldata.ttable;

  auto texture_o = ttable.find(texture_name);
  assert(texture_o);
  auto& ti = *texture_o;

  auto const       v     = VertexFactory::build_default();
  auto const       uv    = uv_factory::build_rectangle(0, 0, 1, 1);
  auto const       vuvs  = vertex_interleave(v, uv);
  draw_state const dinfo = OG::copy_rectangle(LOGGER, sp.va(), vuvs);

  BIND_UNTIL_END_OF_SCOPE(LOGGER, dinfo);
  BIND_UNTIL_END_OF_SCOPE(LOGGER, ti);
  render::draw_2dgui(LOGGER, GL_TRIANGLES, sp, dinfo, ds);
}

void
draw_targetreticle(log_t& LOGGER, LevelData& ldata, render_state& rs, CameraMatrices const& cm,
                   eid_t const eid, DeltaTime const& dt)
{
  auto& reg = ldata.registry;
  assert(reg.try_get<Transform>(eid));
  auto& npc_transform = reg.get<Transform>(eid);

  Transform transform;
  transform.translation = npc_transform.translation;

  auto const scale = ldata.nearby_targets.calculate_scale(dt);
  transform.scale  = glm::vec3{scale};

  auto const mvp          = compute_billboard_mvp(cm, transform, BillboardType::Spherical);
  auto&      ds           = rs.count;
  auto const draw_reticle = [&](auto& sp) {
    auto constexpr ROTATION_SPEED = 8.0f;
    {
      float const angle   = dt.since_start_millis<float>() / ROTATION_SPEED;
      auto const  rot     = glm::angleAxis(glm::radians(angle), Z_UNIT_VECTOR);
      auto const  rmatrix = glm::toMat4(rot);
      uniform::set(LOGGER, sp, "u_mv", mvp * rmatrix);
    }
    {
      auto const& player       = find_player(reg);
      auto const  target_level = reg.get<NPCData>(eid).level;
      auto const  blendc = NearbyTargets::color_from_level_difference(player.level, target_level);
      uniform::set(LOGGER, sp, "u_blendcolor", blendc);
    }
    draw_billboard_nodepth_tests(LOGGER, ldata, ds, sp, "TargetReticle");
  };
  auto const draw_glow = [&](auto& sp) {
    uniform::set(LOGGER, sp, "u_mv", mvp);
    draw_billboard_nodepth_tests(LOGGER, ldata, ds, sp, "NearbyTargetGlow");
  };
  bool const should_draw_glow = scale < 1.0f;
  auto&      sp = shader2d::ref(reg, should_draw_glow ? "billboard" : "target_reticle");
  BIND_UNTIL_END_OF_SCOPE(LOGGER, sp);
  if (should_draw_glow) {
    draw_glow(sp);
  }
  else {
    draw_reticle(sp);
  }
}

template <typename... Common, typename... PointlightCommon, typename TorchFN, typename DefaultFN,
          typename CommonFN, typename PointlightFN>
void
draw_3d_entities(log_t& LOGGER, enttreg_t& er, TorchFN const& tfn, DefaultFN const& dfn,
                 CommonFN const& cfn, PointlightFN const& pfn)
{
  LOG_TRACE("Rendering Torch");
  er.view<Common..., TextureRenderable, Torch>().each(tfn);

  LOG_TRACE("Rendering Book");
  er.view<Common..., TextureRenderable, Book>().each(dfn);

  LOG_TRACE("Rendering Weapon");
  er.view<Common..., TextureRenderable, Weapon>().each(dfn);

  LOG_TRACE("Rendering Junk");
  er.view<Common..., JunkEntityFromFILE>().each(dfn);

  LOG_TRACE("Rendering Trees");
  er.view<Common..., TreeComponent>().each(cfn);

  // CUBES
  LOG_TRACE("Rendering Pointlights");
  er.view<Common..., PointlightCommon..., CubeRenderable, PointLight>().each(pfn);

  LOG_TRACE("Rendering NPCs");
  er.view<Common..., MeshRenderable, NPCData>().each(cfn);

  // Only render the player if the camera isn't in FPS mode.
  // if (false == camera.is_firstperson()){
  LOG_TRACE("Rendering Player");
  er.view<Common..., MeshRenderable, Player>().each(cfn);
  //}
}

} // namespace

namespace opengl
{

////////////////////////////////////////////////////////////////////////////////////////////////////
// EntityRenderer
void
entity_renderer::render2d_billboard(log_t& LOGGER, GameState& gs, render_state& rs,
                                    CameraMatrices const& cm, DeltaTime const& dt)
{
  auto& ldata = gs.ldata;
  auto& reg   = ldata.registry;

  auto const draw_orbital_fn = [&](auto const eid, ShaderComponent& sc, auto& transform,
                                   IsRenderable& is_r, AABoundingBox& bbox, auto&&... args) {
    auto& sp = shader2d::ref(reg, sc.value);
    BIND_UNTIL_END_OF_SCOPE(LOGGER, sp);

    draw_orbital_body(LOGGER, gs, rs, cm, sp, eid, transform, is_r, bbox, FORWARD(args));
  };

  ENABLE_ALPHA_BLENDING_UNTIL_SCOPE_EXIT();
  reg.view<ShaderComponent, Transform, IsRenderable, AABoundingBox, BillboardRenderable,
           OrbitalBody, TextureRenderable>()
      .each(draw_orbital_fn);

  auto const& nearby_targets = ldata.nearby_targets;
  auto const  selected       = nearby_targets.selected();
  if (std::nullopt != selected) {
    auto const eid = *selected;
    draw_targetreticle(LOGGER, ldata, rs, cm, eid, dt);
  }

  auto const dinfo = gl_sdl::current_display_mode(LOGGER);

  auto& ftable = ldata.ftable;
  auto* pft    = ftable.lookup("my-font");
  assert(pft);
  auto& font = *pft;

  auto const img_size = glm::ivec2{font.texture.width(), font.texture.height()};

  auto& ti = font.texture;
  auto& sp = shader2d::ref(reg, "2dfont-billboard");

  BIND_UNTIL_END_OF_SCOPE(LOGGER, sp);
  BIND_UNTIL_END_OF_SCOPE(LOGGER, ti);

  auto const draw_entity_names = [&](eid_t const eid, Transform& tr, auto&&...) {
    char const* name      = "UNKNOWN";
    auto        color     = LOC3::RED;
    auto        font_size = glm::vec2{8};

    if (reg.try_get<Name>(eid)) {
      name  = reg.get<Name>(eid).value.c_str();
      color = LOC3::LEMON_CHION;
    }
    if (reg.try_get<Player>(eid)) {
      name      = "You :)";
      color     = LOC3::DEEP_SKY_BLUE;
      font_size = glm::vec2{12};
    }
    glm::vec2 constexpr RATIO{1.0f, 1.0f};
    FontInfo const finfo = font::create_fontinfo(font.ffile, RATIO, font_size, dinfo, img_size)
                               .expect("DO ME OUTSIDE RENDERING");

    auto constexpr rect = rect::create_float(0, 0, RATIO.x, RATIO.y);
    auto const lrects =
        font::compute_letter_rectangles(finfo, rect, name, font_size, dinfo, false, true)
            .expect("SHOULDA WOULDA COULDA");
    {
      auto const mvp        = compute_billboard_mvp(cm, tr, BillboardType::Spherical);
      auto const bound_rect = rect::create_float(0, 0, dinfo.x, dinfo.y);
      render::draw_2dgui_sentence(LOGGER, bound_rect, lrects, color, sp, ti, mvp, rs.count);
    }
  };
  reg.view<Transform, MeshRenderable, NPCData>().each(draw_entity_names);
  reg.view<Transform, MeshRenderable, Player>().each(draw_entity_names);
}

void
entity_renderer::render3d(log_t& LOGGER, GameState& gs, render_state& rs, CameraMatrices const& cm,
                          DeltaTime const& dt)
{
  auto& ldata = gs.ldata;
  auto& reg   = ldata.registry;
  auto& ds    = rs.count;

  auto const draw_common_fn = [&](auto const eid, ShaderComponent& sc, auto&&... args) {
    auto&       sp    = shader3d::ref(reg, sc.value);
    auto const& dinfo = reg.get<opengl::draw_state>(eid);
    BIND_UNTIL_END_OF_SCOPE(LOGGER, sp);
    draw_entity(LOGGER, gs, rs, cm, GL_TRIANGLES, sp, eid, dinfo, FORWARD(args));
  };

  auto const draw_default_entity_fn = [&](eid_t const eid, auto&&... args) {
    texture_h* pti = nullptr;

    if (reg.try_get<TextureRenderable>(eid)) {
      assert(!reg.try_get<color4>(eid));

      auto& trend = reg.get<TextureRenderable>(eid);
      auto* ti    = trend.texture_info;
      assert(ti);
      pti = ti;
    }
    BIND_UNTIL_END_OF_SCOPE_IF(LOGGER, pti != nullptr, *pti);
    draw_common_fn(eid, FORWARD(args));
  };

  auto const draw_torch_fn = [&](auto const eid, ShaderComponent& sc, auto& transform,
                                 IsRenderable& is_r, AABoundingBox& bbox,
                                 TextureRenderable& trenderable, Torch& torch) {
    {
      auto& sp = shader3d::ref(reg, sc.value);

      // Describe glow
      static constexpr auto MIN   = 0.3f;
      static constexpr auto MAX   = 1.0f;
      auto const            SPEED = 0.135f;
      auto constexpr PI           = math::constants::PI;
      auto const  a               = std::sin(dt.since_start_millis<float>() * PI * SPEED);
      float const glow            = glm::lerp(MIN, MAX, std::fabs(a));

      bind_for(LOGGER, sp, [&]() {
        uniform::set(LOGGER, sp, "u_glow", glow);
        auto const diffuse = reg.get<PointLight>(eid).light.diffuse;
        uniform::set(LOGGER, sp, "u_lightcolor", diffuse);
      });
    }

    // randomize the position slightly
    static constexpr auto DISPLACEMENT_MAX = 0.0015f;

    auto copy_transform = transform;
    {
      auto& rng = gs.rng;
      copy_transform.translation.x += rng.gen(-DISPLACEMENT_MAX, DISPLACEMENT_MAX);
      copy_transform.translation.y += rng.gen(-DISPLACEMENT_MAX, DISPLACEMENT_MAX);
      copy_transform.translation.z += rng.gen(-DISPLACEMENT_MAX, DISPLACEMENT_MAX);
    }

    auto* ti = trenderable.texture_info;
    assert(ti);
    bind_for(LOGGER, ti, [&]() { draw_common_fn(eid, sc, copy_transform, is_r, bbox, torch); });
  };

  auto const draw_boundingboxes = [&](std::pair<color4, color4> const& colors, eid_t,
                                      Transform& transform, AABoundingBox& bbox, Selectable& sel,
                                      auto&&...) {
    auto const& ui_debug = gs.uibuffers_classic.debug;
    if (!ui_debug.draw_bounding_boxes) {
      return;
    }
    color4 const wire_color = sel.selected ? colors.first : colors.second;

    auto& sp = shader3d::wireframe(reg);
    auto  tr = transform;

    BIND_UNTIL_END_OF_SCOPE(LOGGER, sp);
    uniform::set(LOGGER, sp, "u_wirecolor", wire_color);

    // We needed to bind the shader program to set the uniforms above, no reason to pay to bind
    // it again.
    auto const  model_matrix = tr.model_matrix();
    auto const& dinfo        = bbox.draw_info;

    BIND_UNTIL_END_OF_SCOPE(LOGGER, dinfo);
    render::set_mvpmatrix(LOGGER, cm.camera_matrix(), model_matrix, sp);
    render::draw_drawinfo(LOGGER, ds, GL_LINES, sp, dinfo);
  };

  auto const draw_pointlight_fn = [&](auto const eid, ShaderComponent& sc, auto&&... args) {
    auto& sp = shader3d::ref(reg, sc.value);
    bind_for(LOGGER, sp, [&]() {
      auto const diffuse = reg.get<PointLight>(eid).light.diffuse;
      uniform::set(LOGGER, sp, "u_lightcolor", diffuse);
    });
    draw_common_fn(eid, sc, FORWARD(args));
  };

  draw_3d_entities<ShaderComponent, Transform, IsRenderable, AABoundingBox>(
      LOGGER, reg, draw_torch_fn, draw_default_entity_fn, draw_common_fn, draw_pointlight_fn);

  reg.view<Transform, AABoundingBox, Selectable>().each([&](eid_t const eid, auto&&... args) {
    draw_boundingboxes(PAIR(LOC4::GREEN, LOC4::RED), eid, FORWARD(args));
  });
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// SilhouetteEntityRenderer
void
entity_renderer_silhouette::render2d_billboard(log_t& LOGGER, GameState& gs, render_state& rs,
                                               CameraMatrices const& cm)
{
  auto& ldata = gs.ldata;
  auto& reg   = ldata.registry;
  auto& sp    = shader2d::silhoutte(reg);

  auto const draw_orbital_fn = [&](auto const eid, auto& transform, IsRenderable& is_r,
                                   AABoundingBox& bbox, BillboardRenderable& br, OrbitalBody& ob,
                                   TextureRenderable& tr, SunGlow& sg) {
    uniform::set(LOGGER, sp, "u_color", color::vec3(sg));
    draw_orbital_body(LOGGER, gs, rs, cm, sp, eid, transform, is_r, bbox, br, ob, tr);
  };

  ENABLE_ALPHA_BLENDING_UNTIL_SCOPE_EXIT();
  BIND_UNTIL_END_OF_SCOPE(LOGGER, sp);

  // Only those orbital body with a "SunGlow" component get rendered as a silhouette
  reg.view<Transform, IsRenderable, AABoundingBox, BillboardRenderable, OrbitalBody,
           TextureRenderable, SunGlow>()
      .each(draw_orbital_fn);
}

void
entity_renderer_silhouette::render3d(log_t& LOGGER, GameState& gs, render_state& rs,
                                     CameraMatrices const& cm, DeltaTime const&)
{
  auto& ldata = gs.ldata;
  auto& reg   = ldata.registry;

  auto& sp = shader3d::silhoutte(reg);
  BIND_UNTIL_END_OF_SCOPE(LOGGER, sp);

  auto const cmatrix               = cm.camera_matrix();
  auto const draw_black_silhouette = [&](eid_t const eid, auto& transform, auto&&...) {
    auto const& dinfo = reg.get<opengl::draw_state>(eid);
    BIND_UNTIL_END_OF_SCOPE(LOGGER, dinfo);
    {
      auto const mm = transform.model_matrix();
      render::set_mvpmatrix(LOGGER, cmatrix, mm, sp);
    }
    render::draw_drawinfo(LOGGER, rs.count, GL_TRIANGLES, sp, dinfo);
  };

  draw_3d_entities<Transform, ShaderComponent, IsRenderable, AABoundingBox>(
      LOGGER, reg, draw_black_silhouette, draw_black_silhouette, draw_black_silhouette,
      draw_black_silhouette);
}

} // namespace opengl
