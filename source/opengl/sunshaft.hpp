#pragma once
#include <boomhs/lighting.hpp>

#include <opengl/framebuffer.hpp>
#include <opengl/renderbuffer.hpp>
#include <opengl/texture.hpp>

#include <common/macros_class.hpp>

struct Camera;
class DeltaTime;
class LevelManager;
struct log_t;
struct SunshaftBuffer;
class Viewport;

namespace opengl
{
struct draw_call_counter;
class shader_type;

struct sunshaft_buffer
{
  opengl::frame_buffer_ar  fbo;
  opengl::texture_h        tbo;
  opengl::render_buffer_ar rbo;

  sunshaft_buffer(log_t&);

  NOCOPY_MOVE_DEFAULT(sunshaft_buffer);
};

class sunshaft_renderer
{
  sunshaft_buffer      buffers_;
  opengl::shader_type* sp_;

public:
  NOCOPY_MOVE_DEFAULT(sunshaft_renderer);
  explicit sunshaft_renderer(log_t&, Viewport const&, opengl::shader_type&);
  void resize(log_t&, Viewport const&);

  auto& texture_info() { return buffers_.tbo; }

  template <typename FN>
  void with_sunshaft_fbo(log_t& LOGGER, FN const& fn)
  {
    bind_for(LOGGER, *buffers_.fbo, fn);
  }

  void render(log_t& logger, DirectionalLightList const&, SunshaftBuffer&, draw_call_counter&);
};

} // namespace opengl
