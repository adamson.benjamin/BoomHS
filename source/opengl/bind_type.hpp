#pragma once
#include <common/compiler.hpp>
#include <common/macros_scope_exit.hpp>

enum class debug_bind_state_type
{
  BOUND,
  NOT_BOUND,
  MOVED_FROM
};

struct release_bind_state_impl
{
};

struct debug_bind_state_impl
{
  mutable debug_bind_state_type bound_state = debug_bind_state_type::NOT_BOUND;

public:
  debug_bind_state_impl() = default;
  NO_COPY(debug_bind_state_impl);

public:
  debug_bind_state_impl(debug_bind_state_impl&& other)
      : bound_state(other.bound_state)
  {
    other.bound_state = debug_bind_state_type::MOVED_FROM;
  }

  auto& operator=(debug_bind_state_impl&& other)
  {
    assert(&other != this);
    bound_state       = other.bound_state;
    other.bound_state = debug_bind_state_type::MOVED_FROM;
    return *this;
  }
};

struct debug_bind_state : public
#ifdef OPENGL_RUNTIME_CHECKS
                          debug_bind_state_impl
#else
                          release_bind_state_impl
#endif
{
};
