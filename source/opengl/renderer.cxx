#include "renderer.hpp"
#include <boomhs/camera.hpp>
#include <boomhs/components.hpp>
#include <boomhs/delta_time.hpp>
#include <boomhs/engine.hpp>
#include <boomhs/entity.hpp>
#include <boomhs/fog.hpp>
#include <boomhs/font.hpp>
#include <boomhs/material.hpp>
#include <boomhs/npc.hpp>
#include <boomhs/player.hpp>
#include <boomhs/shape.hpp>
#include <boomhs/terrain.hpp>
#include <boomhs/vertex_factory.hpp>
#include <boomhs/vertex_interleave.hpp>

#include <gl_sdl/gl_sdl_log.hpp>
#include <gl_sdl/sdl_window.hpp>

#include <opengl/draw_info.hpp>
#include <opengl/global.hpp>
#include <opengl/gpu.hpp>
#include <opengl/light_renderer.hpp>
#include <opengl/shader.hpp>
#include <opengl/texture.hpp>
#include <opengl/uniform.hpp>

#include <math/random.hpp>
#include <math/rectangle.hpp>
#include <math/viewport.hpp>

#include <common/log.hpp>
#include <common/overload.hpp>

#include <extlibs/fmt.hpp>
#include <extlibs/glew.hpp>
#include <extlibs/sdl.hpp>

#include <variant>

using namespace common;
using namespace math::constants;
using namespace opengl;

namespace
{
void
enable_depth_tests(log_t& LOGGER)
{
  ::glEnable(GL_DEPTH_TEST);
  LOG_ANY_GL_GENERAL_ERRORS(LOGGER, "glEnable(GL_DEPTH_TEST)");
}

void
disable_depth_tests(log_t& LOGGER)
{
  ::glDisable(GL_CULL_FACE);
  LOG_ANY_GL_GENERAL_ERRORS(LOGGER, "glDisable(GL_CULL_FACE)");

  ::glDisable(GL_DEPTH_TEST);
  LOG_ANY_GL_GENERAL_ERRORS(LOGGER, "glDisable(GL_DEPTH_TEST)");
}

void
set_fog(log_t& LOGGER, Fog const& fog, ViewMatrix const& vm, shader_type& sp)
{
  uniform::set(LOGGER, sp, "u_viewmatrix", vm);

  uniform::set(LOGGER, sp, "u_fog.density", fog.density);
  uniform::set(LOGGER, sp, "u_fog.gradient", fog.gradient);
  uniform::set(LOGGER, sp, "u_fog.color", fog.color);
}

// Table from: https://www.khronos.org/opengl/wiki/Debug_Output#Message_Components
auto
type_to_string(log_t& LOGGER, GLenum const type)
{
  switch (type) {
  case GL_DEBUG_TYPE_ERROR:
    return "GL_DEBUG_TYPE_ERROR (typically API error)";
  case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
    return "GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR";
  case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
    return "GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR";
  case GL_DEBUG_TYPE_PORTABILITY:
    return "GL_DEBUG_TYPE_PORTABILITY";
  case GL_DEBUG_TYPE_PERFORMANCE:
    return "GL_DEBUG_TYPE_PERFORMANCE";
  case GL_DEBUG_TYPE_MARKER:
    return "GL_DEBUG_TYPE_MARKER";
  case GL_DEBUG_TYPE_PUSH_GROUP:
    return "GL_DEBUG_TYPE_PUSH_GROUP";
  case GL_DEBUG_TYPE_POP_GROUP:
    return "GL_DEBUG_TYPE_POP_GROUP";
  case GL_DEBUG_TYPE_OTHER:
    return "GL_DEBUG_TYPE_OTHER";

  default:
    LOG_ERROR("invalid type enum");
    std::exit(EXIT_FAILURE);
  }
}

auto
severity_to_string(log_t& LOGGER, GLenum const severity)
{
  switch (severity) {
  case GL_DEBUG_SEVERITY_HIGH:
    return "High";
  case GL_DEBUG_SEVERITY_MEDIUM:
    return "Medium";
  case GL_DEBUG_SEVERITY_LOW:
    return "Low";
  case GL_DEBUG_SEVERITY_NOTIFICATION:
    return "Debug";

  default:
    LOG_ERROR("invalid severity enum");
    std::exit(EXIT_FAILURE);
  }
}

void
gl_log_callback(GLenum const source, GLenum const type, GLuint const id, GLenum const severity,
                GLsizei const length, GLchar const* message, void const* user_data)
{
  auto* plogger = reinterpret_cast<log_t const*>(user_data);
  auto& LOGGER  = *const_cast<log_t*>(plogger);

  auto const LOG_GL_MESSAGE = [&]() {
    constexpr auto const* FMT =
        "OpenGL message detected! "
        "source: %u, type: %u, id: %u, length: %lu, severity: %s, type: %s, message: %s";
    auto const severity_str = severity_to_string(LOGGER, severity);
    auto const type_str     = type_to_string(LOGGER, type);
    if (severity == GL_DEBUG_SEVERITY_MEDIUM || severity == GL_DEBUG_SEVERITY_HIGH) {
      LOG_ERROR(FMT, source, type, id, length, severity_str, type_str, message);
    }
    else {
      LOG_DEBUG(FMT, source, type, id, length, severity_str, type_str, message);
    }
    if (GL_DEBUG_TYPE_PERFORMANCE != type && GL_DEBUG_TYPE_OTHER != type) {
      std::exit(EXIT_FAILURE);
    }
  };

  switch (severity) {
  case GL_DEBUG_SEVERITY_HIGH:
    LOG_GL_MESSAGE();
    break;

  case GL_DEBUG_SEVERITY_MEDIUM:
    LOG_GL_MESSAGE();
    break;

  case GL_DEBUG_SEVERITY_LOW:
    LOG_GL_MESSAGE();
    break;

  case GL_DEBUG_SEVERITY_NOTIFICATION:
    LOG_GL_MESSAGE();
    break;
  }
} // namespace

} // namespace

namespace opengl
{

///////////////////////////////////////////////////////////////////////////////////////////////
// DrawState

std::string
draw_call_counter::to_string() const
{
  return fmt::sprintf("{vertices: %lu, drawcalls: %lu}", num_vertices, num_drawcalls);
}

} // namespace opengl

namespace opengl::render
{

culling_and_winding_state
read_cwstate()
{
  culling_and_winding_state cwstate;
  {
    GLint buffer;
    ::glGetIntegerv(GL_FRONT_FACE, &buffer);
    cwstate.winding = static_cast<GLenum>(buffer);
  }

  auto& culling = cwstate.culling;
  glGetBooleanv(GL_CULL_FACE, &culling.enabled);
  {
    GLint buffer;
    glGetIntegerv(GL_CULL_FACE_MODE, &buffer);
    culling.mode = static_cast<GLenum>(buffer);
  }
  return cwstate;
}

void
set_cwstate(culling_and_winding_state const& cw_state)
{
  ::glFrontFace(cw_state.winding);

  auto& culling = cw_state.culling;
  if (culling.enabled) {
    ::glEnable(GL_CULL_FACE);
    ::glCullFace(culling.mode);
  }
  else {
    ::glDisable(GL_CULL_FACE);
  }
}

blend_state
read_blendstate()
{
  blend_state bstate;
  bstate.enabled = ::glIsEnabled(GL_BLEND);

  ::glGetIntegerv(GL_BLEND_SRC_ALPHA, &bstate.source_alpha);
  ::glGetIntegerv(GL_BLEND_DST_ALPHA, &bstate.dest_alpha);

  ::glGetIntegerv(GL_BLEND_SRC_RGB, &bstate.source_rgb);
  ::glGetIntegerv(GL_BLEND_DST_RGB, &bstate.dest_rgb);
  return bstate;
}

void
set_blendstate(blend_state const& state)
{
  glBlendFuncSeparate(state.source_rgb, state.dest_rgb, state.source_alpha, state.dest_alpha);

  if (state.enabled) {
    ::glEnable(GL_BLEND);
  }
  else {
    ::glDisable(GL_BLEND);
  }
}

void
init(log_t& LOGGER, SDLWindow& window)
{
  window.make_current();

  ::glDisable(GL_BLEND);
  LOG_ANY_GL_GENERAL_ERRORS(LOGGER, "glDisable(GL_BLEND)");

  ::glEnable(GL_CULL_FACE);
  LOG_ANY_GL_GENERAL_ERRORS(LOGGER, "glEnable(GL_CULL_FACE)");

  ::glCullFace(GL_BACK);
  LOG_ANY_GL_GENERAL_ERRORS(LOGGER, "glCullFace(GL_BACK)");

  ::glFrontFace(GL_CCW);
  LOG_ANY_GL_GENERAL_ERRORS(LOGGER, "glFrontFace(GL_CCW)");

  ::glEnable(GL_SCISSOR_TEST);
  LOG_ANY_GL_GENERAL_ERRORS(LOGGER, "glEnable(GL_SCISSOR_TEST)");

  LOG_INFO("supported versions: '%s'", ::glGetString(GL_SHADING_LANGUAGE_VERSION));

  enable_depth_tests(LOGGER);

  ::glEnable(GL_DEBUG_OUTPUT);
  ::glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);

  // The LOGGER is thread safe
  ::glDebugMessageCallback(static_cast<GLDEBUGPROC>(gl_log_callback), static_cast<void*>(&LOGGER));

  int max_texunits_infragshader;
  ::glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS, &max_texunits_infragshader);

  LOG_TRACE("GL_MAX_TEXTURE_IMAGE_UNITS (maximum number of texture units available in "
            "fragment shader: %i",
            max_texunits_infragshader);
}

void
clear_screen(color4 const& c)
{
  // https://stackoverflow.com/a/23944124/562174
  // glDisable(GL_DEPTH_TEST);
  // ON_SCOPE_EXIT([]() { enable_depth_tests(); });

  // Render
  ::glClearColor(color::red(c), color::green(c), color::blue(c), color::alpha(c));
  ::glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void
draw_2dgui(log_t& LOGGER, GLenum const dm, shader_type& sp, draw_state const& dinfo,
           draw_call_counter& ds)
{
  disable_depth_tests(LOGGER);
  ON_SCOPE_EXIT([&]() { enable_depth_tests(LOGGER); });

  draw_drawinfo(LOGGER, ds, dm, sp, dinfo);
}

void
draw_2dgui_letter(log_t& LOGGER, LetterRects const& lrs, glm::vec2 const& vcursor,
                  DrawLetterConfig const& cfg)
{
  auto rect = lrs.rect;
  rect.left() += vcursor.x;
  rect.top() += vcursor.y;

  auto const& uvs = lrs.uv_rect;
  render::draw_2dtextured_rect(LOGGER, cfg.pm, rect, uvs, cfg.sp, cfg.ti, cfg.ds);
}

float
draw_2dgui_sentence(log_t& LOGGER, RectFloat const& bounding_rect,
                    LetterRectCollection const& lrects, color3 const& color, shader_type& sp,
                    texture_h& ti, ProjMatrix const& pm, draw_call_counter& ds)
{
  assert(is_bound(sp));
  assert(is_bound(ti));
  uniform::set(LOGGER, sp, "u_sampler", 0);
  uniform::set(LOGGER, sp, "u_color", color);

  glm::vec2  vcursor{bounding_rect.left(), bounding_rect.top()};
  auto const line_height  = font::compute_largest_height(lrects);
  auto       delta_height = line_height;

  auto const update_cursor = [&](float const next_w) {
    if ((vcursor.x + next_w) > bounding_rect.right()) {
      vcursor.x = bounding_rect.left();
      vcursor.y += line_height;
      delta_height += line_height;
    }
  };

  DrawLetterConfig const cfg{pm, sp, ti, ds};
  for (auto const& lrect : lrects) {
    auto const width_letter = lrect.rect.width();
    update_cursor(width_letter);
    if (vcursor.y > bounding_rect.bottom()) {
      goto ENDOFFN;
    }
    draw_2dgui_letter(LOGGER, lrect, vcursor, cfg);
    vcursor.x += width_letter;
  }

ENDOFFN:
  return delta_height;
}

void
draw_3dlightsource(log_t& LOGGER, GLenum const dm, CameraMatrix const& cm,
                   ModelMatrix const& model_matrix, shader_type& sp, draw_state const& dinfo,
                   draw_call_counter& ds)
{
  set_mvpmatrix(LOGGER, cm, model_matrix, sp);
  draw_drawinfo(LOGGER, ds, dm, sp, dinfo);
}

void
draw_3dshape(log_t& LOGGER, GLenum const dm, CameraMatrix const& camera_matrix,
             ViewMatrix const& vm, ModelMatrix const& model_matrix, Fog const& fog, shader_type& sp,
             draw_state const& dinfo, bool const draw_normals, draw_call_counter& ds)
{
  set_modelmatrix(LOGGER, model_matrix, sp);
  set_mvpmatrix(LOGGER, camera_matrix, model_matrix, sp);
  set_fog(LOGGER, fog, vm, sp);

  // misc
  uniform::set(LOGGER, sp, "u_drawnormals", draw_normals);
  draw_drawinfo(LOGGER, ds, dm, sp, dinfo);
}

void
draw_3d_no_lighting(log_t& LOGGER, GLenum const dm, CameraMatrix const& cm, ModelMatrix const& mm,
                    shader_type& sp, draw_state const& dinfo, draw_call_counter& ds)
{
  set_mvpmatrix(LOGGER, cm, mm, sp);
  draw_drawinfo(LOGGER, ds, dm, sp, dinfo);
}

void
draw_3dlit_shape(log_t& LOGGER, GLenum const dm, CameraMatrix const& cm, ViewMatrix const& vm,
                 GlobalLight const& global_light, DirectionalLightList const& dirlights,
                 PointLightList const& pointlights, Fog const& fog, shape_and_lighting& shape,
                 draw_call_counter& ds)

{
  if (!shape.draw_normals) {
    light_renderer::set_light_uniforms(LOGGER, dirlights, pointlights, shape.sp, shape.material, vm,
                                       shape.model_matrix, global_light, shape.set_normalmatrix);
  }

  bool const  draw_normals = shape.draw_normals;
  auto const& mm           = shape.model_matrix;
  draw_3dshape(LOGGER, dm, cm, vm, mm, fog, shape.sp, shape.dinfo, draw_normals, ds);
}

void
draw_drawinfo(log_t&, draw_call_counter& ds, GLenum const dm, shader_type& sp,
              draw_state const& dinfo)
{
  auto const draw_mode   = ds.draw_wireframes ? GL_LINE_LOOP : dm;
  auto const num_indices = dinfo.num_indices();

  /*
  LOG_DEBUG("---------------------------------------------------------------------------");
  LOG_DEBUG("drawing object!");
  LOG_DEBUG("sp: %s", sp.to_string());
  LOG_DEBUG("draw_info: %s", dinfo.to_string(sp.va()));
  LOG_DEBUG("---------------------------------------------------------------------------");
  */

  FOR_DEBUG_ONLY([&]() { assert(is_bound(sp)); });
  FOR_DEBUG_ONLY([&]() { assert(is_bound(dinfo)); });
  draw_elements(draw_mode, sp, num_indices, ds);
}

void
draw_elements(GLenum const draw_mode, shader_type const& sp, GLsizei const num_indices,
              draw_call_counter& ds)
{
  auto constexpr INDICES_PTR = nullptr;

  auto const ic = sp.instance_count();
  if (ic) {
    auto const prim_count = *ic;
    ::glDrawElementsInstanced(draw_mode, num_indices, GL_UNSIGNED_INT, INDICES_PTR, prim_count);
  }
  else {
    ::glDrawElements(draw_mode, num_indices, GL_UNSIGNED_INT, INDICES_PTR);
  }

  ds.num_vertices += static_cast<size_t>(num_indices);
  ++ds.num_drawcalls;
}

void
draw_2delements(log_t& LOGGER, GLenum const draw_mode, shader_type const& sp,
                GLsizei const num_indices, draw_call_counter& ds)
{
  disable_depth_tests(LOGGER);
  ON_SCOPE_EXIT([&]() { enable_depth_tests(LOGGER); });

  draw_elements(draw_mode, sp, num_indices, ds);
}

void
draw_2delements(log_t& LOGGER, GLenum const draw_mode, shader_type const& sp, texture_h& ti,
                GLsizei const num_indices, draw_call_counter& ds)
{
  BIND_UNTIL_END_OF_SCOPE(LOGGER, ti);
  draw_2delements(LOGGER, draw_mode, sp, num_indices, ds);
}

void
draw_arrow(log_t& LOGGER, shader_type& sp, CameraMatrix const& cm, glm::vec3 const& start,
           glm::vec3 const& head, color4 const& color, draw_call_counter& ds)
{
  auto const acp     = ArrowTemplate{color, start, head};
  auto const arrow_v = VertexFactory::build(acp);
  auto const dinfo   = OG::copy(LOGGER, sp.va(), arrow_v);

  Transform transform;
  BIND_UNTIL_END_OF_SCOPE(LOGGER, sp);

  set_mvpmatrix(LOGGER, cm, transform.model_matrix(), sp);

  BIND_UNTIL_END_OF_SCOPE(LOGGER, dinfo);
  draw_drawinfo(LOGGER, ds, GL_LINES, sp, dinfo);
}

void
draw_line(log_t& LOGGER, shader_type& sp, glm::vec3 const& start, glm::vec3 const& end,
          color4 const& color, draw_call_counter& ds)
{
  BIND_UNTIL_END_OF_SCOPE(LOGGER, sp);
  uniform::set(LOGGER, sp, "u_linecolor", color);

  LineTemplate const lt{start, end};
  auto const         line_v = VertexFactory::build(lt);
  auto const         dinfo  = OG::copy(LOGGER, sp.va(), line_v);

  BIND_UNTIL_END_OF_SCOPE(LOGGER, dinfo);
  draw_2dgui(LOGGER, GL_LINES, sp, dinfo, ds);
}

void
draw_2dtextured_rect(log_t& LOGGER, ProjMatrix const& pm, RectFloat rect, RectFloat uvs,
                     shader_type& sp, texture_h& ti, draw_call_counter& ds)
{
  FOR_DEBUG_ONLY([&]() { assert(is_bound(sp)); });
  FOR_DEBUG_ONLY([&]() { assert(is_bound(ti)); });
  rect.top() = rect.bottom();

  auto const v    = VertexFactory::build(rect);
  auto const uv   = uv_factory::build_rectangle(uvs);
  auto const vuvs = vertex_interleave(v, uv);

  draw_state const dinfo = OG::copy_rectangle(LOGGER, sp.va(), vuvs);
  uniform::set(LOGGER, sp, "u_mv", pm);

  glActiveTexture(GL_TEXTURE0);
  BIND_UNTIL_END_OF_SCOPE(LOGGER, dinfo);
  draw_2dgui(LOGGER, GL_TRIANGLES, sp, dinfo, ds);
}

void
draw_grid_lines(log_t& LOGGER, enttreg_t& reg, glm::vec3 const& grid_dimensions,
                CameraMatrix const& cm, draw_call_counter& ds)
{
  auto const draw_the_terrain_grid = [&](auto const& color) {
    Transform  transform;
    auto const mm = transform.model_matrix();

    auto const grid_t = GridTemplate{grid_dimensions, color};
    auto const grid_v = VertexFactory::build(grid_t);

    auto&      sp    = shader3d::ref(reg, "3d_pos_color");
    auto const dinfo = OG::copy(LOGGER, sp.va(), grid_v);

    BIND_UNTIL_END_OF_SCOPE(LOGGER, sp);
    set_mvpmatrix(LOGGER, cm, mm, sp);

    BIND_UNTIL_END_OF_SCOPE(LOGGER, dinfo);
    draw_drawinfo(LOGGER, ds, GL_LINES, sp, dinfo);
  };

  draw_the_terrain_grid(LOC4::RED);
}

void
set_modelmatrix(log_t& LOGGER, glm::mat4 const& model_matrix, shader_type& sp)
{
  uniform::set(LOGGER, sp, "u_modelmatrix", model_matrix);
}

void
set_mvpmatrix(log_t& LOGGER, CameraMatrix const& cm, ModelMatrix const& mm, shader_type& sp)
{
  auto const mvp = cm * mm;
  uniform::set(LOGGER, sp, "u_mv", mvp);
}

namespace detail
{

// Invoke OpenGL function using information from the Viewport, handling coordinate conversions.
// ie converts:
//   left, top, width, height
//
// to:
//   left, bottom, width, height
void
gl_fn_using_viewport(Viewport const& vp, int const screen_height, void (*fn)(int, int, int, int))
{
  auto const left = vp.left();

  // OpenGL assumes (0, 0) is the BOTTOM-left corner.
  // Our Viewport deals with (0, 0) is the TOP-left corner.
  //
  // Convert viewport's "top" to OpenGL's "bottom".
  // https://www.opengl.org/discussion_boards/showthread.php/129422-glViewport-from-top-left-not-bottom-left?p=970469&viewfull=1#post970469
  auto const bottom = screen_height - vp.top() - vp.height();

  auto const width  = vp.width();
  auto const height = vp.height();

  // OpenGL functions (using viewport data) assume this argument order.
  fn(left, bottom, width, height);
}

} // namespace detail

void
set_viewport(Viewport const& vp, int const screen_height)
{
  detail::gl_fn_using_viewport(vp, screen_height, glViewport);
}

void
set_scissor(Viewport const& vp, int const screen_height)
{
  detail::gl_fn_using_viewport(vp, screen_height, glScissor);
}

void
set_viewport_and_scissor(Viewport const& vp, int const screen_height)
{
  set_viewport(vp, screen_height);
  set_scissor(vp, screen_height);
}

} // namespace opengl::render
