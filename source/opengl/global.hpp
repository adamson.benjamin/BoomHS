#pragma once
#include <boomhs/world_object.hpp>
#include <math/matrices.hpp>

struct Frustum;

namespace opengl
{
struct draw_call_counter;

struct render_state
{
  CameraMatrices const cm;
  Frustum const&       fr;

  draw_call_counter& count;
};

} // namespace opengl

namespace opengl::world_orientation
{
// -Z is pointing FORWARD into the scene, +Y is pointing UP on the screen.
auto constexpr FORWARDZ_FORWARD = -math::constants::Z_UNIT_VECTOR;
auto constexpr FORWARDZ_UP      = +math::constants::Y_UNIT_VECTOR;
auto constexpr FORWARDZ         = WorldOrientation{FORWARDZ_FORWARD, FORWARDZ_UP};

// +Z is pointing BACKWARD into the scene, +Y is pointing UP on the screen.
auto constexpr REVERSEZ_FORWARD = +math::constants::Z_UNIT_VECTOR;
auto constexpr REVERSEZ_UP      = +math::constants::Y_UNIT_VECTOR;
auto constexpr REVERSEZ         = WorldOrientation{REVERSEZ_FORWARD, REVERSEZ_UP};

// -Y is pointing FORWARD into the scene, +Z is pointing UP on the screen.
auto constexpr TOPDOWN_FORWARD = -math::constants::Y_UNIT_VECTOR;
auto constexpr TOPDOWN_UP      = +math::constants::Z_UNIT_VECTOR;
auto constexpr TOPDOWN         = WorldOrientation{TOPDOWN_FORWARD, TOPDOWN_UP};

// +Y is pointing FORWARD into the scene, +Z is pointing UP on the screen.
auto constexpr BOTTOMUP_FORWARD = +math::constants::Y_UNIT_VECTOR;
auto constexpr BOTTOMUP_UP      = +math::constants::Z_UNIT_VECTOR;
auto constexpr BOTTOMUP         = WorldOrientation{BOTTOMUP_FORWARD, BOTTOMUP_UP};

// -X is pointing FORWARD into the scene, +Y is pointing UP on the screen.
auto constexpr FORWARDX_FORWARD = -math::constants::X_UNIT_VECTOR;
auto constexpr FORWARDX_UP      = +math::constants::Y_UNIT_VECTOR;
auto constexpr FORWARDX         = WorldOrientation{FORWARDX_FORWARD, FORWARDX_UP};

// +X is pointing FORWARD into the scene, +Y is pointing UP on the screen.
// lol
auto constexpr REVERSEX_FORWARD = +math::constants::X_UNIT_VECTOR;
auto constexpr REVERSEX_UP      = +math::constants::Y_UNIT_VECTOR;
auto constexpr REVERSEX         = WorldOrientation{REVERSEX_FORWARD, REVERSEX_UP};

} // namespace opengl::world_orientation
