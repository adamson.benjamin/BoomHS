#pragma once
#include <boomhs/entity.hpp>
#include <boomhs/lighting.hpp>
#include <boomhs/terrain.hpp>

#include <opengl/bind_type.hpp>

#include <math/matrices.hpp>

#include <common/macros_class.hpp>

#include <extlibs/glm.hpp>

#include <string>

class DeltaTime;
struct log_t;
struct Fog;
struct Material;
struct MaterialTable;
class Terrain;

namespace opengl
{
struct render_state;
class shader_type;
class texture_storage;

class terrain_renderer_default
{
public:
  NOCOPY_MOVE_DEFAULT(terrain_renderer_default);
  terrain_renderer_default() = default;

  // fields
  debug_bind_state debug_bind_state_;

  // methods
  void render(log_t&, texture_storage&, CameraMatrix const&, ViewMatrix const&, Material const&,
              DirectionalLightList const&, PointLightList const&, TerrainGrid&, GlobalLight const&,
              Fog const&, glm::vec4 const&, bool, draw_call_counter&);

  std::string to_string() const;
};

class terrain_renderer_silhouette
{
  opengl::shader_type* sp_;

public:
  NOCOPY_MOVE_DEFAULT(terrain_renderer_silhouette);
  terrain_renderer_silhouette(opengl::shader_type&);

  // methods
  void render(log_t&, TerrainGrid&, CameraMatrix const&, draw_call_counter&);
};

} // namespace opengl

namespace opengl
{
void
bind(log_t&, terrain_renderer_default&, Terrain const&, texture_storage&);

void
unbind(log_t&, terrain_renderer_default&, Terrain const&, texture_storage&);
} // namespace opengl
