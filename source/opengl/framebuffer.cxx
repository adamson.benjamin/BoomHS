#include "framebuffer.hpp"
#include <opengl/bind.hpp>
#include <opengl/renderer.hpp>

#include <common/macros_scope_exit.hpp>

using namespace opengl;

namespace
{
texture_h
make_fb(log_t& LOGGER, int const width, int const height, GLenum const tu)
{
  assert(width > 0 && height > 0);
  frame_buffer_cfg const cfg{GL_TEXTURE_2D, width, height};

  ::glActiveTexture(tu);
  ON_SCOPE_EXIT([]() { ::glActiveTexture(GL_TEXTURE0); });

  auto ti = texture::from_config(cfg);
  bind_for(LOGGER, ti, [&]() {
    ti.set_fieldi(GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    ti.set_fieldi(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  });
  return ti;
}

} // namespace

namespace opengl
{
frame_buffer_h::frame_buffer_h() { ::glGenFramebuffers(1, &id); }

void
destroy_frame_buffer(frame_buffer_h& fb)
{
  ::glDeleteFramebuffers(1, &fb.id);
}

std::string
frame_buffer_h::to_string() const
{
  return fmt::sprintf("(FBInfo) id: %u", id);
}

texture_h
frame_buffer_h::attach_color_buffer(log_t& LOGGER, int const width, int const height,
                                    GLenum const tu)
{
  auto ti = make_fb(LOGGER, width, height, tu);

  bind_for(LOGGER, ti, [&]() {
    // allocate GPU memory for texture
    ::glTexImage2D(ti.target(), 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, nullptr);
  });

  //// attach texture to FBO
  bind_for(LOGGER, *this, [&]() {
    ::glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, ti.target(), ti.texture(), 0);
  });
  return ti;
}

texture_h
frame_buffer_h::attach_depth_buffer(log_t& LOGGER, int const width, int const height,
                                    GLenum const tu)
{
  auto ti = make_fb(LOGGER, width, height, tu);
  bind_for(LOGGER, ti, [&]() {
    // allocate GPU memory for texture
    ::glTexImage2D(ti.target(), 0, GL_DEPTH_COMPONENT32, width, height, 0, GL_DEPTH_COMPONENT,
                   GL_FLOAT, nullptr);
  });
  // attach texture to FBO
  bind_for(LOGGER, *this, [&]() {
    ::glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, ti.target(), ti.texture(), 0);
  });
  return ti;
}

render_buffer_ar
frame_buffer_h::attach_render_buffer(log_t& LOGGER, int const width, int const height)
{
  render_buffer_h rbinfo;
  bind_for(LOGGER, rbinfo,
           [&]() { ::glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height); });

  // attach render buffer to FBO
  bind_for(LOGGER, *this, [&]() {
    ::glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rbinfo.id);
  });
  return render_buffer_ar{MOVE(rbinfo)};
} // namespace opengl

void
bind(log_t& LOGGER, frame_buffer_h& fb)
{
  LOG_TRACE("Binding frame_buffer: %u", fb.id);
  ::glBindFramebuffer(GL_FRAMEBUFFER, fb.id);
}

void
unbind(log_t& LOGGER, frame_buffer_h& fb)
{
  LOG_TRACE("Unbinding Framebuffer: %u", fb.id);
  ::glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

frame_buffer_ar
make_fbo(log_t& LOGGER)
{
  frame_buffer_ar fb;
  bind_for(LOGGER, *fb, []() { ::glDrawBuffer(GL_COLOR_ATTACHMENT0); });
  return fb;
}

} // namespace opengl
