#include "texture.hpp"
#include <opengl/bind.hpp>
#include <opengl/framebuffer.hpp>
#include <opengl/global.hpp>

#include <gl_sdl/gl_sdl_log.hpp>

#include <common/algorithm.hpp>
#include <common/tuple.hpp>

#include <math/viewport.hpp>

#include <extlibs/boost_algorithm.hpp>
#include <extlibs/fmt.hpp>
#include <extlibs/glew.hpp>
#include <extlibs/soil.hpp>

#include <algorithm>
#include <sstream>
#include <string>
#include <utility>
#include <vector>

using namespace common;
using namespace opengl;

namespace
{
void
upload_image_gpu(GLenum const target, image_data const& idata)
{

  auto const  width  = idata.width;
  auto const  height = idata.height;
  auto const* data   = idata.data.get();

  // The "internal" format and the data format should match so opengl doesn't need to do runtime
  // conversions.
  //
  // I'm not sure why the internal format is an integer, where as the data format
  // (represented by format here) is a GLenum.
  // https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glTexImage2D.xhtml
  auto const internal_format = static_cast<GLint>(idata.format);

  ::glTexImage2D(target, 0, internal_format, width, height, 0, idata.format, GL_UNSIGNED_BYTE,
                 data);
}

template <typename Iter>
auto
find_tf(std::string_view const& name, Iter const& b, Iter const& e)
{
  auto const cmp = [&name](auto const& it) { return boost::iequals(it.first.name, name); };
  return std::find_if(b, e, cmp);
}

} // namespace

namespace opengl
{
void
bind(log_t& LOGGER, texture_h& th)
{
  LOG_TRACE("Binding Texture: %u", th.texture());
  ::glBindTexture(th.target(), th.texture());
}

void
unbind(log_t& LOGGER, texture_h& th)
{
  LOG_TRACE("Unbinding Texture: %u", th.texture());
  ::glBindTexture(th.target(), 0);
}

void
destroy_texture(texture_h& handle)
{
  auto const t = handle.texture();
  ::glDeleteTextures(1, &t);
}

texture_cfg::texture_cfg(GLenum const targetp)
    : target(targetp)
{
}

texture_h::texture_h(texture_cfg const cfg)
    : texture_cfg(cfg)
{
  ::glGenTextures(1, &texture_);
}

GLint
texture_h::get_fieldi(GLenum const name)
{
  OPENGL_ASSERT_BOUND(*this);

  GLint value;
  ::glGetTexParameteriv(target(), name, &value);
  return value;
}

void
texture_h::set_fieldi(GLenum const name, GLint const value)
{
  OPENGL_ASSERT_BOUND(*this);

  ::glTexParameteri(target(), name, value);
}

std::string
texture_h::to_string() const
{
  return fmt::sprintf("(TextureInfo) target: %i, texture: %u, (w, h) : (%i, %i)", target(),
                      texture(), width(), height());
}

void
texture_storage::add(texture_and_filenames&& tf, texture_ar&& t)
{
  auto pair = std::make_pair(MOVE(tf), MOVE(t));
  data_.emplace(MOVE(pair));
}

std::string
texture_storage::list_of_all_names(char const delim) const
{
  std::stringstream buffer;
  for (auto const& it : *this) {
    buffer << it.first.name;
    buffer << delim;
  }
  auto result = buffer.str();
  boost::trim(result);
  return result;
}

std::optional<size_t>
texture_storage::index_of_nickname(std::string_view const& name) const
{
  size_t i = 0;
  for (auto const& it : *this) {
    if (boost::iequals(it.first.name, name)) {
      return std::make_optional(i);
    }
    ++i;
  }
  return std::nullopt;
}

std::optional<std::string>
texture_storage::nickname_at_index(size_t const index) const
{
  size_t i = 0;
  for (auto const& it : *this) {
    if (i++ == index) {
      return std::make_optional(it.first.name);
    }
  }
  return std::nullopt;
}

texture_and_filenames const*
texture_storage::lookup_nickname(std::string_view const& name) const
{
  auto const b  = std::cbegin(data_);
  auto const e  = std::cend(data_);
  auto const it = find_tf(name, b, e);
  return (it == e) ? nullptr : &it->first;
}

texture_h*
texture_storage::find(std::string_view const& name)
{
  auto const b  = std::begin(data_);
  auto const e  = std::end(data_);
  auto const it = find_tf(name, b, e);
  return (it == e) ? nullptr : &it->second.ref();
}

texture_h const*
texture_storage::find(std::string_view const& name) const
{
  auto const b  = std::cbegin(data_);
  auto const e  = std::cend(data_);
  auto const it = find_tf(name, b, e);
  return (it == e) ? nullptr : &it->second.ref();
}

} // namespace opengl

namespace opengl::texture
{
texture_h
from_config(texture_cfg const& cfg)
{
  assert(cfg.target > 0);
  assert(cfg.format > 0);

  assert(cfg.wrap != -1);

  assert(cfg.width != -1);
  assert(cfg.height != -1);
  return texture_h{cfg};
}

texture_h
from_config(frame_buffer_cfg const& cfg)
{
  assert(cfg.target != 0);
  assert(cfg.width > 0);
  assert(cfg.height > 0);
  return texture_h{cfg.target};
}

texture_h
from_config(frame_buffer_cfg cfg, Viewport const& vp)
{
  assert(cfg.target != 0);
  assert(vp.width() > 0);
  assert(vp.height() > 0);

  cfg.width  = vp.width();
  cfg.height = vp.height();
  return from_config(cfg);
}

image_r
load_image(char const* path, GLenum const format)
{
  int w = 0, h = 0;

  int const      soil_format = format == GL_RGBA ? SOIL_LOAD_RGBA : SOIL_LOAD_RGB;
  unsigned char* pimage      = ::SOIL_load_image(path, &w, &h, nullptr, soil_format);
  if (nullptr == pimage) {
    auto const fmt =
        fmt::sprintf("image at path '%s' failed to load, reason '%s'", path, SOIL_last_result());
    return Err(fmt);
  }
  image_unique_ptr image_ptr{pimage, &::SOIL_free_image_data};
  return Ok(image_data{w, h, format, MOVE(image_ptr)});
}

GLint
wrap_mode_from_string(log_t& LOGGER, std::string_view const& name)
{
  if ("clamp_edge" == name) {
    return GL_CLAMP_TO_EDGE;
  }
  else if ("clamp_border" == name) {
    return GL_CLAMP_TO_BORDER;
  }
  else if ("repeat" == name) {
    return GL_REPEAT;
  }
  else if ("mirror_repeat" == name) {
    return GL_MIRRORED_REPEAT;
  }

  LOG_ERROR("Invalid wrap mode for texture '%s'", name);
  // Invalid
  std::exit(EXIT_FAILURE);
}

texture_r
upload_2d_texture(log_t& LOGGER, std::string const& filename, texture_cfg cfg)
{
  assert(or_all(cfg.format == GL_RGB, cfg.format == GL_RGBA));

  auto image_data = TRY(texture::load_image(filename.c_str(), cfg.format));
  cfg.width       = image_data.width;
  cfg.height      = image_data.height;
  auto ti         = texture::from_config(cfg);

  // This next bit comes from tracking down a weird bug. Without this extra scope, the texture info
  // does not get unbound because the move constructor for the AutoResource(Texture) moves the
  // TextureInfo instance inside the TextureResult. This zeroes out the id value inside the local
  // variable "ti", where at this point the ON_SCOP_EXIT lambda executes using this moved-from
  // instance of "ti".
  //
  // Using the explicit scope guarantees the texture is unbound when the stack frame unwinds.
  {
    BIND_UNTIL_END_OF_SCOPE(LOGGER, ti);
    ::upload_image_gpu(ti.target(), image_data);

    ::glGenerateMipmap(ti.target());
    LOG_ANY_GL_GENERAL_ERRORS(LOGGER, "glGenerateMipmap");
  }
  return Ok(texture_ar{MOVE(ti)});
}

texture_r
upload_3dcube_texture(log_t& LOGGER, std::vector<std::string> const& paths, texture_cfg cfg)
{
  auto const format = cfg.format;
  assert(paths.size() == 6);
  assert(or_all(format == GL_RGB, format == GL_RGBA));

  using LP_R             = Result<std::pair<image_data, GLenum>, std::string>;
  auto const load_3dpath = [&](std::string const& path, GLenum const face) -> LP_R {
    auto imgdata = TRY(texture::load_image(path.c_str(), cfg.format));
    // auto second = static_cast<GLenum>(face);
    return OK_PAIR(MOVE(imgdata), face);
  };

  std::array const CUBE_3D_TARGETS = {
      load_3dpath(paths[0], GL_TEXTURE_CUBE_MAP_POSITIVE_Z), // back
      load_3dpath(paths[1], GL_TEXTURE_CUBE_MAP_POSITIVE_X), // right
      load_3dpath(paths[2], GL_TEXTURE_CUBE_MAP_NEGATIVE_Z), // front
      load_3dpath(paths[3], GL_TEXTURE_CUBE_MAP_NEGATIVE_X), // left
      load_3dpath(paths[4], GL_TEXTURE_CUBE_MAP_POSITIVE_Y), // top
      load_3dpath(paths[5], GL_TEXTURE_CUBE_MAP_NEGATIVE_Y), // bottom
  };

  int w = -1, h = -1;
  [&]() {
    for (auto const& ct : CUBE_3D_TARGETS) {
      assert(ct.is_ok());
      auto const& img_data = ct.unwrap().first;
      assert(and_all(-1 == w, -1 == h) || and_all(w == img_data.width, h == img_data.height));
      w = img_data.width;
      h = img_data.height;
    }
  }();

  cfg.width  = w;
  cfg.height = h;
  auto ti    = texture::from_config(cfg);
  {
    BIND_UNTIL_END_OF_SCOPE(LOGGER, ti);
    for (auto const& iter : CUBE_3D_TARGETS) {
      auto const& x = iter.unwrap();
      ::upload_image_gpu(x.second, x.first);
    }
    ::glGenerateMipmap(GL_TEXTURE_CUBE_MAP);
    LOG_ANY_GL_GENERAL_ERRORS(LOGGER, "glGenerateMipmap");
  }
  return Ok(texture_ar{MOVE(ti)});
}

} // namespace opengl::texture
