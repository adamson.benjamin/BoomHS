#pragma once
#include <math/rectangle.hpp>
#include <opengl/bind_type.hpp>

#include <common/macros_class.hpp>
#include <common/move_only_type.hpp>
#include <common/result.hpp>

#include <extlibs/glew.hpp>
#include <map>
#include <memory>
#include <optional>
#include <string>
#include <string_view>
#include <vector>

struct log_t;
class Viewport;

namespace opengl
{
struct frame_buffer_cfg;

struct texture_cfg
{
  GLenum target = 0;
  GLenum format = 0;
  GLint  wrap   = -1;
  GLint  width = -1, height = -1;

  texture_cfg(GLenum);
};

class texture_h final : private texture_cfg
{
public:
  GLuint           texture_ = 0;
  debug_bind_state debug_bind_state_;

public:
  texture_h(texture_cfg);
  NOCOPY_MOVE_DEFAULT(texture_h);

public:
  auto format() const { return texture_cfg::format; }
  auto target() const { return texture_cfg::target; }
  auto texture() const { return texture_; }
  auto wrap() const { return texture_cfg::wrap; }

  auto width() const { return texture_cfg::width; }
  auto height() const { return texture_cfg::height; }

public:
  GLint get_fieldi(GLenum);
  void  set_fieldi(GLenum, GLint);

  bool        is_2d() const { return GL_TEXTURE_2D == target(); }
  bool        is_3d_cube() const { return GL_TEXTURE_CUBE_MAP == target(); }
  std::string to_string() const;
};

struct texture_and_filenames
{
  using string_list = std::vector<std::string>;

  std::string name;
  string_list filenames;

  auto num_filenames() const { return filenames.size(); }
};

inline bool
operator<(texture_and_filenames const& a, texture_and_filenames const& b)
{
  return a.name < b.name;
}

void
destroy_texture(texture_h&);

using texture_ar = move_only_type<texture_h, destroy_texture>;
class texture_storage
{
  using pair_t = std::pair<texture_and_filenames, texture_ar>;
  std::map<texture_and_filenames, texture_ar> data_;

public:
  texture_storage() = default;
  NOCOPY_MOVE_DEFAULT(texture_storage);
  BEGIN_END_FORWARD_FNS(data_)

  void add(texture_and_filenames&&, texture_ar&&);

  // Get a concatenated list of all texture names as a single string.
  //
  // Pass in a delimeter character to seperate the names.
  std::string list_of_all_names(char const delim = ' ') const;

  std::optional<size_t>      index_of_nickname(std::string_view const&) const;
  std::optional<std::string> nickname_at_index(size_t) const;

  texture_and_filenames const* lookup_nickname(std::string_view const&) const;
  texture_h*                   find(std::string_view const&);
  texture_h const*             find(std::string_view const&) const;

  auto size() const { return data_.size(); }
  auto empty() const { return data_.empty(); }
};

using image_unique_ptr = std::unique_ptr<unsigned char, void (*)(unsigned char*)>;
struct image_data
{
  int              width, height;
  GLenum           format;
  image_unique_ptr data;
};

} // namespace opengl

namespace opengl
{
void
bind(log_t&, texture_h&);

void
unbind(log_t&, texture_h&);
} // namespace opengl

namespace opengl::texture
{
texture_h
from_config(texture_cfg const&);

texture_h
from_config(frame_buffer_cfg const&);

texture_h
from_config(frame_buffer_cfg, Viewport const&);

using image_r   = Result<image_data, std::string>;
using texture_r = Result<texture_ar, std::string>;

image_r
load_image(char const*, GLenum const);

GLint
wrap_mode_from_string(log_t&, std::string_view const&);

texture_r
upload_2d_texture(log_t&, std::string const&, texture_cfg);

texture_r
upload_3dcube_texture(log_t&, std::vector<std::string> const&, texture_cfg);
} // namespace opengl::texture
