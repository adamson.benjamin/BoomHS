#pragma once
#include <opengl/bind_type.hpp>

#include <common/macros_class.hpp>

#include <extlibs/glew.hpp>
#include <ostream>
#include <string>

struct log_t;

namespace opengl
{
class vao_h
{
  GLuint vao_ = 0;

public:
  debug_bind_state debug_bind_state_;

public:
  NO_COPY(vao_h);
  explicit vao_h();
  ~vao_h();

public:
  vao_h(vao_h&&);
  vao_h& operator=(vao_h&&);

public:
  GLuint      gl_raw_value() const;
  std::string to_string() const;
};

std::ostream&
operator<<(std::ostream&, vao_h const&);

} // namespace opengl

namespace opengl
{
void
bind(log_t&, vao_h const&);

void
unbind(log_t&, vao_h const&);
} // namespace opengl