#pragma once
#include <opengl/bind_type.hpp>
#include <opengl/global.hpp>
#include <opengl/vao.hpp>
#include <opengl/vertex_attribute.hpp>

#include <boomhs/entity.hpp>

#include <common/macros_class.hpp>

#include <optional>
#include <string>

struct log_t;
class ObjStore;

namespace opengl
{
class buffer_handles
{
  GLuint vbo_ = 0, ebo_ = 0;
  static auto constexpr NUM_BUFFERS = 1;

  explicit buffer_handles();
  NO_COPY(buffer_handles);

public:
  friend class draw_state;
  ~buffer_handles();

public:
  buffer_handles(buffer_handles&&);
  buffer_handles& operator=(buffer_handles&&);

public:
  auto vbo() const { return vbo_; }
  auto ebo() const { return ebo_; }

  std::string to_string() const;
};

std::ostream&
operator<<(std::ostream&, buffer_handles const&);

class draw_state
{
  size_t         num_vertexes_;
  GLsizei        num_indices_;
  buffer_handles buffers_;
  vao_h          vao_;

public:
  debug_bind_state debug_bind_state_;

  NO_COPY(draw_state);
  explicit draw_state(size_t, GLsizei);

  draw_state(draw_state&&);
  draw_state& operator=(draw_state&&);

  auto vbo() const { return buffers_.vbo(); }
  auto ebo() const { return buffers_.ebo(); }
  auto num_vertexes() const { return num_vertexes_; }
  auto num_indices() const { return num_indices_; }

  auto const& vao() const { return vao_; }

  std::string to_string() const;
};

} // namespace opengl

namespace opengl
{
void
bind(log_t&, draw_state const&);

void
unbind(log_t&, draw_state const&);
} // namespace opengl
