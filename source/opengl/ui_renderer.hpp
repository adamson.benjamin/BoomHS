#pragma once
#include <boomhs/color.hpp>

// TODO: this header is only included for static_shaders::BasicMvWithUniformcolor4
#include <opengl/draw_info.hpp>
#include <opengl/renderer.hpp>
#include <opengl/shader.hpp>
#include <opengl/types.hpp>

struct log_t;
struct Frustum;
class Viewport;

namespace opengl
{
class draw_state;
struct draw_call_counter;

struct shader_and_draw_state
{
  shader_type program;
  draw_state  dinfo;
};

struct bordered_rect_cfg
{
  RectFloat const rect;
  color4          background;
  color4          border;
};

class ui_renderer
{
  log_t&      LOGGER;
  shader_type sp2d_;
  shader_type sp2d_border_;

  glm::mat4 pm_;

  void draw_rect_impl(ModelViewMatrix const&, draw_state const&, color4 const&, shader_type&,
                      GLenum, draw_call_counter&);

public:
  ui_renderer(log_t&, Viewport const&);

  void resize(Viewport const&);

  void draw_rect(ModelMatrix const&, draw_state const&, color4 const&, draw_call_counter&);
  void draw_rect(draw_state const&, color4 const&, draw_call_counter&);
  void draw_rect(RectFloat const&, color4 const&, draw_call_counter&);

  void draw_line_rect(draw_state const&, color4 const&, draw_call_counter&);

  void draw_bordered_rect(bordered_rect_cfg const&, Frustum const&, draw_call_counter&);

  void draw_mouseselect_rect(glm::ivec2 const&, glm::ivec2 const&, color4 const&, Viewport const&,
                             draw_call_counter&);
};

// TODO: Rename this class to something more general/sane.
//
// Render's a rectangle from an initial click location to where the user is currently clicking,
// within a supplied viewport. Maintains it's own state, but needs to be updated when the mouse
// is moved.
//
// Used for click-and-drag selection operations.
class mouse_rectangle_renderer
{
  shader_and_draw_state padi_;
  ui_renderer           ui_renderer_;

public:
  mouse_rectangle_renderer(log_t&, shader_and_draw_state&&, Viewport const&);

  void draw(color4 const&, draw_call_counter&);
};

} // namespace opengl

namespace static_shaders
{
opengl::shader_type
new_basic_mv_uniform_color(log_t&);

opengl::shader_type
new_basic_mv_uniform_color_with_rounded_border(log_t&);

} // namespace static_shaders
