#include "light_renderer.hpp"
#include <opengl/renderer.hpp>
#include <opengl/shader.hpp>
#include <opengl/uniform.hpp>

#include <boomhs/engine.hpp>
#include <boomhs/entity.hpp>
#include <boomhs/lighting.hpp>
#include <boomhs/material.hpp>
#include <boomhs/transform.hpp>

#include <extlibs/glm.hpp>

#include <vector>

using namespace common;
using namespace opengl;

namespace
{

auto
make_field(std::string const& varname, char const* fieldname)
{
  return varname + "." + fieldname;
}

template <typename FN>
void
set_attenuation(log_t& logger, shader_type& sp, Attenuation const& att, FN const& read_field)
{
  auto const constant  = read_field("constant");
  auto const linear    = read_field("linear");
  auto const quadratic = read_field("quadratic");

  uniform::set(logger, sp, constant, att.constant);
  uniform::set(logger, sp, linear, att.linear);
  uniform::set(logger, sp, quadratic, att.quadratic);
}

void
set_dirlight(log_t& logger, shader_type& sp, size_t const index, DirectionalLight const& dirlight)
{
  std::string const varname = "u_dirlights[" + std::to_string(index) + "]";
  auto const mf = [&varname](char const* field_name) { return make_field(varname, field_name); };

  uniform::set(logger, sp, mf("diffuse"), dirlight.light.diffuse);
  uniform::set(logger, sp, mf("specular"), dirlight.light.specular);

  uniform::set(logger, sp, mf("direction"), dirlight.direction);
  uniform::set(logger, sp, mf("screenspace_pos"), dirlight.screenspace_pos);
}

void
set_pointlight(log_t& logger, shader_type& sp, size_t const index, PointLight const& pointlight,
               glm::vec3 const& pointlight_position)
{
  std::string const varname = "u_pointlights[" + std::to_string(index) + "]";
  auto const mf = [&varname](char const* field_name) { return make_field(varname, field_name); };

  uniform::set(logger, sp, mf("diffuse"), pointlight.light.diffuse);
  uniform::set(logger, sp, mf("specular"), pointlight.light.specular);
  uniform::set(logger, sp, mf("position"), pointlight_position);

  auto const& attenuation    = pointlight.attenuation;
  auto const  attenuation_fn = [&mf](char const* fieldname) {
    return mf("attenuation.") + fieldname;
  };

  set_attenuation(logger, sp, attenuation, attenuation_fn);
}

} // namespace

namespace opengl
{

void
light_renderer::set_light_uniforms(log_t& logger, DirectionalLightList const& dirlights,
                                   PointLightList const& pointlights, shader_type& sp,
                                   Material const& material, ViewMatrix const& view_matrix,
                                   ModelMatrix const& model_matrix, GlobalLight const& global_light,
                                   bool const set_normalmatrix)
{
  // auto const& player       = ldata.player;
  render::set_modelmatrix(logger, model_matrix, sp);
  if (set_normalmatrix) {
    glm::mat3 const nmatrix = glm::inverseTranspose(glm::mat3{model_matrix});
    uniform::set(logger, sp, "u_normalmatrix", nmatrix);
  }

  // ambient
  uniform::set(logger, sp, "u_ambient.color", global_light.ambient);

  // specular
  uniform::set(logger, sp, "u_reflectivity", 1.0f);

  {
    auto const num_dl = dirlights.size();
    uniform::set(logger, sp, "u_numdirlights", static_cast<int>(num_dl));
    FOR(i, num_dl)
    {
      auto const& dirlight = dirlights[i];
      set_dirlight(logger, sp, i, dirlight.dirlight);
    }
  }
  {
    // pointlights
    glm::mat4 const inv_viewmatrix = glm::inverse(glm::mat3{view_matrix});
    uniform::set(logger, sp, "u_invviewmatrix", inv_viewmatrix);

    auto const num_pl = pointlights.size();
    uniform::set(logger, sp, "u_numpointlights", static_cast<int>(num_pl));

    FOR(i, num_pl)
    {
      auto const& transform  = pointlights[i].transform;
      auto const& pointlight = pointlights[i].pointlight;
      set_pointlight(logger, sp, i, pointlight, transform.translation);
    }
  }

  // Material uniforms
  uniform::set(logger, sp, "u_material.ambient", material.ambient);
  uniform::set(logger, sp, "u_material.diffuse", material.diffuse);
  uniform::set(logger, sp, "u_material.specular", material.specular);
  uniform::set(logger, sp, "u_material.shininess", material.shininess);

  // TODO: when re-implementing LOS restrictions
  // uniform::set(logger, sp, "u_player.position",  player.world_position());
  // uniform::set(logger, sp, "u_player.direction",  player.forward_vector());
  // uniform::set(logger, sp, "u_player.cutoff",  glm::cos(glm::radians(90.0f)));
}

} // namespace opengl
