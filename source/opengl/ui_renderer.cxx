#include "ui_renderer.hpp"
#include <opengl/bind.hpp>
#include <opengl/draw_info.hpp>
#include <opengl/gpu.hpp>
#include <opengl/renderer.hpp>
#include <opengl/shader.hpp>
#include <opengl/uniform.hpp>

#include <gl_sdl/global.hpp>

#include <boomhs/camera.hpp>
#include <boomhs/camera_algorithm.hpp>
#include <boomhs/shape.hpp>

#include <math/frustum.hpp>
#include <math/rectangle.hpp>
#include <math/viewport.hpp>

using namespace opengl;

namespace
{
auto
make_2d_rectshader(log_t& LOGGER, char const* vert_source, char const* frag_source)
{
  auto api = attribute_pointer{0, GL_FLOAT, vertex_attribute_type::POSITION, 3};
  auto spc = make_shader_cfg("BasicRectOnecolor4", vert_source, frag_source, MOVE(api));

  NumLights constexpr NUM_LIGHTS{0, 0};
  auto sp = shader::from_sourcecode(LOGGER, MOVE(spc), NUM_LIGHTS)
                .expect("Error creating 2d_rectshader shader.");
  return sp;
}

auto
make_2d_shader_uv_coords(log_t& LOGGER, char const* vert_source, char const* frag_source)
{
  auto constexpr apis = std::array<attribute_pointer, 2>{
      attribute_pointer{0, GL_FLOAT, vertex_attribute_type::POSITION, 3},
      attribute_pointer{1, GL_FLOAT, vertex_attribute_type::UV, 2}};

  auto spc = make_shader_cfg("Basic2dOnecolor4WithBorder", vert_source, frag_source, apis);
  NumLights constexpr NUM_LIGHTS{0, 0};
  auto sp = shader::from_sourcecode(LOGGER, MOVE(spc), NUM_LIGHTS)
                .expect("Error creating 2d_rectshader shader.");
  return sp;
}

} // namespace

namespace
{
shader_and_draw_state
new_from_rect(log_t& LOGGER, RectFloat const& rect, GLenum const dm)
{
  auto program = static_shaders::new_basic_mv_uniform_color(LOGGER);

  auto builder      = RectBuilder{rect};
  builder.draw_mode = dm;
  auto const& va    = program.va();
  auto        dinfo = OG::copy_rectangle(LOGGER, va, builder.build());

  return shader_and_draw_state{MOVE(program), MOVE(dinfo)};
}

shader_and_draw_state
create_rect(log_t& LOGGER, glm::ivec2 const& initial_click_pos, glm::ivec2 const& mouse_pos_now,
            GLenum const dm)
{
  auto const minx       = static_cast<float>(initial_click_pos.x);
  auto const miny       = static_cast<float>(initial_click_pos.y);
  auto const maxx       = static_cast<float>(mouse_pos_now.x);
  auto const maxy       = static_cast<float>(mouse_pos_now.y);
  auto const mouse_rect = rect::create(minx, miny, maxx, maxy);

  return new_from_rect(LOGGER, mouse_rect, dm);
}

mouse_rectangle_renderer
make_rect_under_mouse(log_t& LOGGER, glm::ivec2 const& init, glm::ivec2 const& now,
                      Viewport const& viewport)
{
  auto mouse_rect = create_rect(LOGGER, init, now, GL_LINE_LOOP);
  return mouse_rectangle_renderer{LOGGER, MOVE(mouse_rect), viewport};
}

} // namespace

namespace opengl
{
ui_renderer::ui_renderer(log_t& logger, Viewport const& vp)
    : LOGGER(logger)
    , sp2d_(static_shaders::new_basic_mv_uniform_color(LOGGER))
    , sp2d_border_(static_shaders::new_basic_mv_uniform_color_with_rounded_border(LOGGER))
{
  resize(vp);
}

void
ui_renderer::resize(Viewport const& vp)
{
  float constexpr NEAR_PM = -1.0;
  float constexpr FAR_PM  = 1.0f;
  Frustum const fr{vp.left(), vp.right(), vp.bottom(), vp.top(), NEAR_PM, FAR_PM};

  auto constexpr ZOOM = glm::ivec2{0};
  pm_                 = camera::ortho::compute_pm(fr, vp.size(), mc::ZERO, ZOOM);
}

void
ui_renderer::draw_rect_impl(ModelViewMatrix const& mv, draw_state const& di, color4 const& color,
                            shader_type& sp, GLenum const dm, draw_call_counter& ds)
{
  BIND_UNTIL_END_OF_SCOPE(LOGGER, sp);
  uniform::set(LOGGER, sp, "u_mv", mv);
  uniform::set(LOGGER, sp, "u_color", color);

  BIND_UNTIL_END_OF_SCOPE(LOGGER, di);
  OR::draw_2delements(LOGGER, dm, sp, di.num_indices(), ds);
}

void
ui_renderer::draw_rect(ModelMatrix const& mmatrix, draw_state const& di, color4 const& color,
                       draw_call_counter& ds)
{
  auto const mv = pm_ * mmatrix;
  draw_rect_impl(mv, di, color, sp2d_, GL_TRIANGLES, ds);
}

void
ui_renderer::draw_rect(draw_state const& di, color4 const& color, draw_call_counter& ds)
{
  auto const& mv = pm_;
  draw_rect_impl(mv, di, color, sp2d_, GL_TRIANGLES, ds);
}

void
ui_renderer::draw_rect(RectFloat const& rect, color4 const& color, draw_call_counter& ds)
{
  auto&            sp     = sp2d_;
  auto             buffer = RectBuilder{rect}.build();
  draw_state const di     = OG::copy_rectangle(LOGGER, sp.va(), buffer);

  auto const& mv = pm_;
  draw_rect_impl(mv, di, color, sp, GL_TRIANGLES, ds);
}

void
ui_renderer::draw_line_rect(draw_state const& di, color4 const& color, draw_call_counter& ds)
{
  auto&       sp = sp2d_;
  auto const& mv = pm_;
  draw_rect_impl(mv, di, color, sp, GL_LINE_LOOP, ds);
}

void
ui_renderer::draw_bordered_rect(bordered_rect_cfg const& info, Frustum const& fr,
                                draw_call_counter& ds)
{
  auto const& rect = info.rect;
  auto&       sp   = sp2d_border_;

  auto buffer = [&rect]() {
    RectBuilder rb{rect};
    rb.uvs = uv_factory::build_rectangle(0, 0, 1, 1);
    return rb.build();
  }();
  draw_state const di = OG::copy_rectangle(LOGGER, sp.va(), buffer);

  // auto const v    = VertexFactory::build_default();
  // auto const uv   = uv_factory::build_rectangle(0, 0, 1, 1);
  // auto const vuvs = vertex_interleave(v, uv);
  // DrawInfo   di   = OG::copy_rectangle(LOGGER, sp.va(), vuvs);

  // DrawInfo di = OG::copy_rectangle(LOGGER, sp.va(), buffer);

  auto const& mv = pm_;

  {
    BIND_UNTIL_END_OF_SCOPE(LOGGER, sp);
    // uniform::set(LOGGER, sp, "u_aspect", rect.width() / rect.height());
    {
      auto const rf = fr.rect_float();
      uniform::set(LOGGER, sp, "u_dimensions", rf.size());
      uniform::set(LOGGER, sp, "u_radius", rf.width() * 0.002f);
    }
    uniform::set(LOGGER, sp, "u_border_color", info.border);
    uniform::set(LOGGER, sp, "u_border_width", 0.0055f);
  }
  draw_rect_impl(mv, di, info.background, sp, GL_TRIANGLES, ds);
}

void
ui_renderer::draw_mouseselect_rect(glm::ivec2 const& pos_init, glm::ivec2 const& pos_now,
                                   color4 const& color, Viewport const& viewport,
                                   draw_call_counter& ds)
{
  auto mrr = ::make_rect_under_mouse(LOGGER, pos_init, pos_now, viewport);
  mrr.draw(color, ds);
}

mouse_rectangle_renderer::mouse_rectangle_renderer(log_t& logger, shader_and_draw_state&& padi,
                                                   Viewport const& viewport)
    : padi_(MOVE(padi))
    , ui_renderer_(ui_renderer{logger, viewport})
{
}

void
mouse_rectangle_renderer::draw(color4 const& color, draw_call_counter& ds)
{
  ui_renderer_.draw_line_rect(padi_.dinfo, color, ds);
}

} // namespace opengl

namespace static_shaders
{
shader_type
new_basic_mv_uniform_color(log_t& LOGGER)
{
  constexpr auto vert_source = R"GLSL(
in vec3 a_position;

uniform mat4 u_mv;

out vec4 v_color;

void main()
{
  gl_Position = u_mv * vec4(a_position, 1.0);
}
)GLSL";

  constexpr auto frag_source = R"GLSL(
in vec4 v_color;

uniform vec4 u_color;

out vec4 fragment_color;

void main()
{
  fragment_color = u_color;
}
)GLSL";
  return make_2d_rectshader(LOGGER, vert_source, frag_source);
}

opengl::shader_type
new_basic_mv_uniform_color_with_rounded_border(log_t& LOGGER)
{
  constexpr auto vert_source = R"GLSL(
in vec3 a_position;
in vec2 a_uv;

uniform mat4 u_mv;

out vec2 v_uv;

void main()
{
  gl_Position = u_mv * vec4(a_position, 1.0);
  v_uv = a_uv;
}
)GLSL";

  constexpr auto frag_source = R"GLSL(
in vec2 v_uv;

uniform vec2 u_dimensions;
uniform float u_radius;

uniform float u_border_width;
uniform vec4 u_border_color;
uniform vec4 u_color;

out vec4 fragment_color;

void main() {
  vec2 coords = (v_uv * u_dimensions);
  if (length(coords - vec2(0)) < u_radius
    || length(coords - vec2(0, u_dimensions.y)) < u_radius
    || length(coords - vec2(u_dimensions.x, 0)) < u_radius
    || length(coords - u_dimensions) < u_radius) {
    discard;
  }

   float max_x = 1.0 - u_border_width;
   float min_x = u_border_width;
   float max_y = max_x;
   float min_y = min_x;

   if (v_uv.x < max_x && v_uv.x > min_x &&
       v_uv.y < max_y && v_uv.y > min_y) {
     fragment_color = u_color;
   } else {
     fragment_color = u_border_color;
   }
}
)GLSL";
  return make_2d_shader_uv_coords(LOGGER, vert_source, frag_source);
}

} // namespace static_shaders
