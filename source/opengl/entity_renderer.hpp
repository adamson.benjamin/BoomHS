#pragma once
#include <common/macros_class.hpp>

struct CameraMatrices;
class DeltaTime;
struct GameState;
struct log_t;

namespace opengl
{
struct render_state;
} // namespace opengl

namespace opengl
{

class entity_renderer
{
public:
  entity_renderer() = default;
  NOCOPY_MOVE_DEFAULT(entity_renderer);

  void
  render2d_billboard(log_t&, GameState&, render_state&, CameraMatrices const&, DeltaTime const&);

  void render3d(log_t&, GameState&, render_state&, CameraMatrices const&, DeltaTime const&);
};

class entity_renderer_silhouette
{
public:
  entity_renderer_silhouette() = default;
  NOCOPY_MOVE_DEFAULT(entity_renderer_silhouette);

  void render2d_billboard(log_t&, GameState&, render_state&, CameraMatrices const&);
  void render3d(log_t&, GameState&, render_state&, CameraMatrices const&, DeltaTime const&);
};
} // namespace opengl
