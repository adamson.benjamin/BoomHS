#include "shader_type.hpp"
#include <opengl/bind.hpp>
#include <opengl/debug.hpp>
#include <opengl/global.hpp>
#include <opengl/shader.hpp>
#include <opengl/vertex_attribute.hpp>

#include <gl_sdl/gl_sdl_log.hpp>

#include <common/FOR.hpp>
#include <common/algorithm.hpp>
#include <common/macros_class.hpp>
#include <common/move.hpp>
#include <common/os.hpp>
#include <common/result.hpp>

#include <extlibs/fmt.hpp>
#include <extlibs/glew.hpp>

#include <cassert>
#include <cstring>

using namespace opengl;

namespace
{
std::string
attrib_type_to_string(GLenum const type)
{
  auto const& table = debug::attrib_to_string_table();
  auto const  it    = std::find_if(table.cbegin(), table.cend(),
                               [&type](auto const& pair) { return pair.first == type; });
  assert(it != table.cend());

  auto const index = common::distance_size_t(table.cbegin(), it);
  return table[index].second;
}

std::string
uniform_type_to_string(GLenum const type)
{
  auto const& table = debug::uniform_to_string_table();

  auto const cmp = [&type](auto const& tuple) { return std::get<0>(tuple) == type; };
  auto const it  = std::find_if(table.cbegin(), table.cend(), cmp);
  assert(it != table.cend());

  auto const index  = common::distance_size_t(table.cbegin(), it);
  auto const string = std::get<1>(table[index]);
  return string;
}

std::string
glchar_ptr_to_string(GLchar const* ptr)
{
  char const* cstring = static_cast<char const*>(ptr);
  return std::string{cstring};
}

template <typename GetFN>
auto
get_active_string(GLuint const program, GLenum const max_len, GLenum const param,
                  char const* prefix, std::string (*type_to_str)(GLenum), GetFN const& fn)
{
  GLint buffer_size = 0;
  glGetProgramiv(program, max_len, &buffer_size);

  GLint count = 0;
  glGetProgramiv(program, param, &count);

  GLsizei length = 0;
  GLint   size   = 0;
  GLenum  type   = 0;

  std::vector<GLchar> buffer;
  buffer.reserve(static_cast<size_t>(buffer_size));
  auto* const pbuffer = buffer.data();

  std::string result = fmt::sprintf("num active %s: {%i}(", prefix, count);
  FOR(i, count)
  {
    if (i > 0) {
      result += ", ";
    }
    auto const index = static_cast<GLuint>(i);
    fn(program, index, buffer_size, &length, &size, &type, pbuffer);

    auto const type_string = type_to_str(type);
    auto const name_string = glchar_ptr_to_string(pbuffer);
    result += fmt::sprintf("[%s: %i, name: %s, type: %s]", prefix, i, name_string, type_string);
  }
  result += ")";
  return result;
}

auto
active_attributes_string(GLuint const program)
{
  return get_active_string(program, GL_ACTIVE_ATTRIBUTE_MAX_LENGTH, GL_ACTIVE_ATTRIBUTES, "attr",
                           attrib_type_to_string, glGetActiveAttrib);
}

auto
active_uniforms_string(GLuint const program)
{
  return get_active_string(program, GL_ACTIVE_UNIFORM_MAX_LENGTH, GL_ACTIVE_UNIFORMS, "uniform",
                           uniform_type_to_string, glGetActiveUniform);
}

void
assert_not_invalid_program_handle(GLuint const p)
{
  // Initially when a ProgramHandle is constructed from a GLuint, the ProgramHandle "assumes
  // ownership", or will assume the responsibility of deleting the underlying opengl program.
  assert(p != INVALID_PROGRAM_ID);
}

} // namespace

namespace opengl
{
program_h::program_h(GLuint const p)
    : handle_(p)
{
  assert_not_invalid_program_handle(p);
}

program_h::program_h(program_h&& other)
    : handle_(MOVE(other.handle_))
{
  // The "moved-from" handle no longer has the responsibility of freeing the underlying opengl
  // program.
  other.handle_ = INVALID_PROGRAM_ID;

  debug_bind_state_ = MOVE(other.debug_bind_state_);
}

program_h&
program_h::operator=(program_h&& other)
{
  assert_not_invalid_program_handle(other.handle());

  handle_       = other.handle_;
  other.handle_ = INVALID_PROGRAM_ID;

  debug_bind_state_ = MOVE(other.debug_bind_state_);
  return *this;
}

program_h::~program_h()
{
  OPENGL_ASSERT_NOT_BOUND(*this);

  if (handle_ != INVALID_PROGRAM_ID) {
    ::glDeleteProgram(handle_);
    handle_ = INVALID_PROGRAM_ID;
  }
}

GLuint
program_h::handle() const
{
  assert_not_invalid_program_handle(handle_);
  return handle_;
}

void
bind(log_t& LOGGER, shader_type& s)
{
  ::glUseProgram(s.handle());
  LOG_TRACE("Binding shader '%s' program: '%u'", s.name(), s.handle());
  LOG_ANY_GL_GENERAL_ERRORS_AND_ABORT(LOGGER, "Shader use/enable");
}

void
unbind(log_t& LOGGER, shader_type& s)
{
#ifdef DEBUG_BUILD
  ::glUseProgram(0);
#endif
  LOG_TRACE("Unbinding program: %u", s.handle());
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// shader_type
shader_type::shader_type(program_h&& ph, vertex_attribute&& va, shader_cfg&& cfg)
    : program_(MOVE(ph))
    , va_(MOVE(va))
    , config_(MOVE(cfg))
{
}

GLint
shader_type::get_uniform_location(log_t& LOGGER, GLchar const* uniform)
{
  OPENGL_ASSERT_BOUND(*this);
  LOG_DEBUG("getting uniform '%s' location.", uniform);
  GLint const loc = ::glGetUniformLocation(program_.handle(), uniform);
  LOG_DEBUG("uniform '%s' found at '%d'.", uniform, loc);

  LOG_ANY_GL_GENERAL_ERRORS_AND_ABORT(LOGGER, "get_uniform_location");
  if (-1 == loc) {
    LOG_ERROR("Unable to retrieve uniform '%s' location for shader.", uniform);
    LOG_ERROR(to_string());
    std::exit(EXIT_FAILURE);
  }
  return loc;
}

std::string
shader_type::to_string() const
{
  auto const& sphandle   = handle();
  auto const  attributes = active_attributes_string(sphandle);
  auto const  uniforms   = active_uniforms_string(sphandle);
  return fmt::sprintf("ShaderProgram name: '%s' attributes: '%s'", name(), attributes);
}

} // namespace opengl

namespace opengl::detail
{
void
shader_cache::add(shader_type&& sp)
{
  auto const& name = sp.name();
  assert(!map_.contains(name));
  map_.emplace(name, MOVE(sp));
}

void
shader_cache::clear()
{
  map_.clear();
}

shader_type*
shader_cache::lookup(std::string const& name)
{
  auto const it = map_.find(name);
  return it == std::cend(map_) ? nullptr : &it->second;
}

shader_type&
shader_cache::ref(std::string const& name)
{
  auto* ptr = lookup(name);
  assert(ptr);
  return *ptr;
}

} // namespace opengl::detail
