#include "terrain_renderer.hpp"
#include <opengl/bind.hpp>
#include <opengl/buffer.hpp>
#include <opengl/gpu.hpp>
#include <opengl/renderer.hpp>
#include <opengl/shader.hpp>
#include <opengl/uniform.hpp>

#include <boomhs/engine.hpp>
#include <boomhs/heightmap.hpp>
#include <boomhs/leveldata.hpp>
#include <boomhs/material.hpp>
#include <boomhs/terrain.hpp>

#include <common/FOR.hpp>
#include <common/algorithm.hpp>
#include <common/log.hpp>

#include <cassert>

using namespace common;
using namespace opengl;

namespace
{

template <typename FN>
void
render_terrain(TerrainGrid& terrain_grid, FN const& fn)
{
  auto const& dimensions = terrain_grid.dimensions();

  // backup state to restore after drawing terrain
  PUSH_CW_STATE_UNTIL_END_OF_SCOPE();

  auto const draw_piece = [&](auto const& terrain) {
    auto const& terrain_pos = terrain.position();

    Transform tr;
    tr.translation.x = terrain_pos.x * dimensions.x;
    tr.translation.z = terrain_pos.y * dimensions.y;
    fn(terrain, tr);
  };

  for (auto& t : terrain_grid) {
    draw_piece(t);
  }
}

} // namespace

namespace opengl
{

////////////////////////////////////////////////////////////////////////////////////////////////////
// terrain_renderer_default
void
terrain_renderer_default::render(log_t& LOGGER, texture_storage& ttable, CameraMatrix const& cm,
                                 ViewMatrix const& vm, Material const& material,
                                 DirectionalLightList const& dirlights,
                                 PointLightList const& pointlights, TerrainGrid& terrain_grid,
                                 GlobalLight const& global_light, Fog const& fog,
                                 glm::vec4 const& cull_plane, bool const draw_normals,
                                 draw_call_counter& ds)
{
  auto const fn = [&](Terrain const& terrain, auto const& tr) {
    auto& sp = terrain.shader();
    BIND_UNTIL_END_OF_SCOPE(LOGGER, sp);

    uniform::set(LOGGER, sp, "u_uvmodifier", terrain.uv_modifier);
    uniform::set(LOGGER, sp, "u_clipPlane", cull_plane);

    BIND_UNTIL_END_OF_SCOPE(LOGGER, *this, terrain, ttable);

    auto const& dinfo = terrain.draw_info();
    BIND_UNTIL_END_OF_SCOPE(LOGGER, dinfo);

    auto const mm = tr.model_matrix();

    bool constexpr SET_NORMALMATRIX = true;
    render::shape_and_lighting lit_shape{tr.translation,   mm,          dinfo, sp, material,
                                         SET_NORMALMATRIX, draw_normals};

    auto const dm = GL_TRIANGLE_STRIP;
    render::draw_3dlit_shape(LOGGER, dm, cm, vm, global_light, dirlights, pointlights, fog,
                             lit_shape, ds);
  };

  render_terrain(terrain_grid, fn);
}

std::string
terrain_renderer_default::to_string() const
{
  return "DefaultTerrainRenderer";
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// terrain_renderer_silhouette
terrain_renderer_silhouette::terrain_renderer_silhouette(shader_type& sp)
    : sp_(&sp)
{
}

void
terrain_renderer_silhouette::render(log_t& LOGGER, TerrainGrid& terrain_grid,
                                    CameraMatrix const& cm, draw_call_counter& ds)
{
  auto const fn = [&](auto& terrain, auto const& tr) {
    auto const& dinfo = terrain.draw_info();

    auto const mm = tr.model_matrix();

    BIND_UNTIL_END_OF_SCOPE(LOGGER, *sp_);
    BIND_UNTIL_END_OF_SCOPE(LOGGER, dinfo);
    render::draw_3d_no_lighting(LOGGER, GL_TRIANGLE_STRIP, cm, mm, *sp_, dinfo, ds);
  };

  render_terrain(terrain_grid, fn);
}

} // namespace opengl

namespace opengl
{
void
bind(log_t& LOGGER, terrain_renderer_default&, Terrain const& terrain, texture_storage& ts)
{
  auto const fn = [&](size_t const offset) {
    assert(offset < std::numeric_limits<GLenum>::max());
    ::glActiveTexture(GL_TEXTURE0 + static_cast<GLenum>(offset));

    auto const& tn = terrain.texture_name(offset);
    LOG_TRACE("Looking up texture '%s' (for binding)", tn);
    auto* ptinfo = ts.find(tn);
    assert(ptinfo);
    bind(LOGGER, *ptinfo);
  };

  auto const& bound_textures = terrain.texture_names;
  FOR(i, bound_textures.textures.size()) { fn(i); }
}

void
unbind(log_t& LOGGER, terrain_renderer_default&, Terrain const& terrain, texture_storage& ts)
{
  auto const fn = [&](size_t const tunit) {
    auto const& tn = terrain.texture_name(tunit);
    LOG_TRACE("Looking up texture '%s' (for unbinding)", tn);

    auto* ptinfo = ts.find(tn);
    assert(ptinfo);
    unbind(LOGGER, *ptinfo);
  };

  auto const& bound_textures = terrain.texture_names;
  FOR(i, bound_textures.textures.size()) { fn(i); }
  ::glActiveTexture(GL_TEXTURE0);
}
} // namespace opengl