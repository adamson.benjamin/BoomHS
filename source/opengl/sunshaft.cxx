#include "sunshaft.hpp"
#include <opengl/bind.hpp>
#include <opengl/gpu.hpp>
#include <opengl/renderbuffer.hpp>
#include <opengl/renderer.hpp>
#include <opengl/shader.hpp>
#include <opengl/uniform.hpp>

#include <boomhs/delta_time.hpp>
#include <boomhs/engine.hpp>
#include <boomhs/ui_buffers_classic.hpp>
#include <boomhs/vertex_factory.hpp>
#include <boomhs/vertex_interleave.hpp>

#include <math/viewport.hpp>

using namespace opengl;

namespace
{

void
setup(log_t& LOGGER, texture_h& ti, GLenum const v)
{
  ::glActiveTexture(v);
  bind_for(LOGGER, ti, [&]() {
    ti.set_fieldi(GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    ti.set_fieldi(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  });
}

} // namespace

namespace opengl
{

sunshaft_buffer::sunshaft_buffer(log_t& logger)
    : fbo(opengl::make_fbo(logger))
    , tbo(GL_TEXTURE_2D)
    , rbo(render_buffer_h{})
{
}

sunshaft_renderer::sunshaft_renderer(log_t& logger, Viewport const& vp, shader_type& sp)
    : buffers_(sunshaft_buffer{logger})
    , sp_(&sp)
{
  resize(logger, vp);

  ON_SCOPE_EXIT([]() { glActiveTexture(GL_TEXTURE0); });
  setup(logger, texture_info(), GL_TEXTURE0);
}

void
sunshaft_renderer::resize(log_t& logger, Viewport const& vp)
{
  auto const w = vp.width(), h = vp.height();

  auto& fbo    = buffers_.fbo;
  buffers_.tbo = fbo->attach_color_buffer(logger, w, h, GL_TEXTURE1);
  buffers_.rbo = fbo->attach_render_buffer(logger, w, h);
}

void
sunshaft_renderer::render(log_t& logger, DirectionalLightList const& dirlights,
                          SunshaftBuffer& sunbuffer, draw_call_counter& ds)
{
  // TODO: Don't create the vertices every frame, we can do this work in the constructor and cache
  // the result.
  auto&      ti   = texture_info();
  auto const v    = VertexFactory::build_default();
  auto const uvs  = rect::create_float(0, 0, 1, 1);
  auto const uv   = uv_factory::build_rectangle(uvs);
  auto const vuvs = vertex_interleave(v, uv);

  auto&            sp    = *sp_;
  draw_state const dinfo = OG::copy_rectangle(logger, sp.va(), vuvs);

  auto const model_matrix = Transform{}.model_matrix();
  BIND_UNTIL_END_OF_SCOPE(logger, sp);
  render::set_modelmatrix(logger, model_matrix, sp);

  auto const num_dirlights = dirlights.size();
  uniform::set(logger, sp, "u_numdirlights", static_cast<int>(num_dirlights));

  uniform::set(logger, sp, "u_blurwidth", sunbuffer.blur_width);
  uniform::set(logger, sp, "u_numsamples", sunbuffer.num_samples);

  FOR(i, num_dirlights)
  {
    auto const& dlight = dirlights[i].dirlight;
    auto const  uname  = fmt::sprintf("u_dirlight[%i].screenspace_pos", i);
    uniform::set(logger, sp, uname, dlight.screenspace_pos);
  }

  ::glActiveTexture(GL_TEXTURE0);
  BIND_UNTIL_END_OF_SCOPE(logger, dinfo);
  BIND_UNTIL_END_OF_SCOPE(logger, ti);
  render::draw_2dgui(logger, GL_TRIANGLES, sp, dinfo, ds);
  // glActiveTexture(GL_TEXTURE0);
}

} // namespace opengl
