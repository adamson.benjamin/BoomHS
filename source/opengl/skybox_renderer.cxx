#include "skybox_renderer.hpp"
#include <opengl/renderer.hpp>
#include <opengl/shader.hpp>
#include <opengl/texture.hpp>
#include <opengl/uniform.hpp>

#include <boomhs/camera.hpp>
#include <boomhs/delta_time.hpp>
#include <boomhs/engine.hpp>
#include <boomhs/leveldata.hpp>
#include <boomhs/state.hpp>

#include <cassert>

using namespace opengl;

skybox_renderer::skybox_renderer(log_t& logger, draw_state&& dinfo, texture_h& day,
                                 texture_h& night, shader_type& sp)
    : dinfo_(MOVE(dinfo))
    , day_(&day)
    , night_(&night)
    , sp_(&sp)
{
  bind_for(logger, sp_, [&]() {
    uniform::set(logger, *sp_, "u_cube_sampler1", 0);
    uniform::set(logger, *sp_, "u_cube_sampler1", 1);
  });

  auto const set_fields = [&](auto& ti, GLenum const tunit) {
    glActiveTexture(tunit);
    ON_SCOPE_EXIT([&]() { glActiveTexture(tunit); });
    bind_for(logger, ti, [&]() {
      ti.set_fieldi(GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
      ti.set_fieldi(GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
      ti.set_fieldi(GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    });
  };
  set_fields(*day_, GL_TEXTURE0);
  set_fields(*night_, GL_TEXTURE1);
}

void
skybox_renderer::render(log_t& logger, GameState& gs, render_state& rstate, Camera const& camera)
{
  auto& ldata    = gs.ldata;
  auto& ui_debug = gs.uibuffers_classic.debug;

  if (!ui_debug.draw_skybox) {
    return;
  }

  auto const& fog = ldata.fog;

  ::glActiveTexture(GL_TEXTURE0);
  ON_SCOPE_EXIT([]() { glActiveTexture(GL_TEXTURE0); });

  BIND_UNTIL_END_OF_SCOPE(logger, *day_);

  ::glActiveTexture(GL_TEXTURE1);
  BIND_UNTIL_END_OF_SCOPE(logger, *night_);

  // Converting the "current hour" to a value in [0.0, 1.0]
  auto const calculate_blend = [&]() {
    auto const& hour_of_day = static_cast<float>(gs.wclock.hours());
    auto const  frac        = hour_of_day / 24.0f;
    auto        blend       = 1.0f - std::abs(math::squared(-frac) + frac);
    if (blend < 0.0f) {
      blend = -blend;
    }
    return blend;
  };

  // Create a view matrix that has it's translation components zero'd out.
  //
  // The effect of this is the view matrix contains just the rotation, which is what's desired
  // for rendering the skybox.
  auto const& fr          = rstate.fr;
  auto        view_matrix = camera.view_matrix(fr);
  view_matrix[3][0]       = 0.0f;
  view_matrix[3][1]       = 0.0f;
  view_matrix[3][2]       = 0.0f;

  auto const proj_matrix = camera.proj_matrix(fr);
  auto const draw_fn     = [&]() {
    render::draw_2dgui(logger, GL_TRIANGLES, *sp_, dinfo_, rstate.count);
  };
  bind_for(logger, sp_, [&]() {
    {
      auto const& skybox    = ldata.skybox;
      auto const& transform = skybox.transform();

      auto const model_matrix = transform.model_matrix();
      auto const mvp_matrix   = math::mvp_matrix(model_matrix, view_matrix, proj_matrix);
      uniform::set(logger, *sp_, "u_mv", mvp_matrix);
    }
    uniform::set(logger, *sp_, "u_fog.color", fog.color);

    auto const blend = calculate_blend();
    uniform::set(logger, *sp_, "u_blend_factor", blend);

    auto const& di = dinfo_;
    bind_for(logger, di, draw_fn);
  });
}

void
skybox_renderer::set_day(opengl::texture_h* ti)
{
  assert(ti);
  day_ = ti;
}

void
skybox_renderer::set_night(opengl::texture_h* ti)
{
  assert(ti);
  night_ = ti;
}
