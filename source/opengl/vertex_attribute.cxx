#include "vertex_attribute.hpp"

#include <common/algorithm.hpp>
#include <common/log.hpp>
#include <common/macros_class.hpp>
#include <extlibs/fmt.hpp>

namespace
{
using namespace opengl;

class upload_format_state
{
  GLsizei const stride_;
  GLsizei       components_skipped_ = 0;

public:
  GLenum component_type_ = 0;

  explicit upload_format_state(GLsizei const stride)
      : stride_(stride)
  {
  }

  auto stride_size() const
  {
    assert(0 != component_type_);
    return stride_ * static_cast<GLsizei>(sizeof(component_type_));
  }

  auto offset_size() const
  {
    assert(0 != component_type_);
    return components_skipped_ * static_cast<GLsizei>(sizeof(component_type_));
  }

  void increase_offset(GLsizei const amount) { components_skipped_ += amount; }
  void set_type(GLenum const type) { component_type_ = type; }
};

void
ensure_backend_has_enough_vertex_attributes(log_t& LOGGER, GLint const num_apis)
{
  auto max_attribs = 0;
  ::glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &max_attribs);

  LOG_DEBUG("Max number of vertex attributes, found {}", max_attribs);

  if (max_attribs <= num_apis) {
    LOG_ERROR("Error requested '%d' vertex attributes from opengl, only '%d' available", num_apis,
              max_attribs);
    std::exit(EXIT_FAILURE);
  }
}

void
configure_and_enable_attrib_pointer(log_t& LOGGER, attribute_pointer const& info,
                                    upload_format_state& ufs)
{
  ensure_backend_has_enough_vertex_attributes(LOGGER, info.component_count);

  // enable vertex attibute arrays
  ::glEnableVertexAttribArray(info.index);

  static auto constexpr DONT_NORMALIZE_THE_DATA = GL_FALSE;

  // clang-format off
  ufs.set_type(info.datatype);
  auto const stride_size_in_bytes = ufs.stride_size();
  auto const offset_size_in_bytes = ufs.offset_size();
  auto const offset_ptr = reinterpret_cast<GLvoid*>(offset_size_in_bytes);

  ::glVertexAttribPointer(
      info.index,              // global index id
      info.component_count,    // number of components per attribute
      info.datatype,           // data-type of the components
      DONT_NORMALIZE_THE_DATA, // don't normalize our data
      stride_size_in_bytes,    // byte-offset between consecutive vertex attributes
      offset_ptr);             // offset from beginning of buffer
  // clang-format on
  ufs.increase_offset(info.component_count);

  auto const make_decimals = [](auto const a0, auto const a1, auto const a2, auto const a3,
                                auto const a4) {
    return fmt::sprintf("%-15d %-15d %-15d %-15d %-15d", a0, a1, a2, a3, a4);
  };

  auto const make_strings = [](auto const a0, auto const a1, auto const a2, auto const a3,
                               auto const a4) {
    return fmt::sprintf("%-15s %-15s %-15s %-15s %-15s", a0, a1, a2, a3, a4);
  };

  auto const s = make_decimals(info.index, info.component_count, DONT_NORMALIZE_THE_DATA,
                               stride_size_in_bytes, offset_size_in_bytes);
  auto const z =
      make_strings("attribute_index", "component_count", "normalize_data", "stride", "offset");

  LOG_DEBUG(z);
  LOG_DEBUG(s);
}

bool
va_has_attribute_type(vertex_attribute const& va, vertex_attribute_type const at)
{
  auto const cmp = [&at](auto const& api) { return api.typezilla == at; };
  auto const it  = std::find_if(va.cbegin(), va.cend(), cmp);
  return it != va.cend();
}

template <typename Iter>
auto constexpr create_va(Iter const b, Iter const e)
{
  // Requested to many APIs. Increase maximum number (more memory per instance required)
  auto const num_vas = static_cast<size_t>(std::distance(b, e));
  assert(num_vas <= vertex_attribute::API_BUFFER_SIZE);

  std::array<attribute_pointer, vertex_attribute::API_BUFFER_SIZE> infos;
  std::copy(b, e, infos.begin());

  GLsizei stride = 0;
  for (auto it = b; it != e; std::advance(it, 1)) {
    stride += it->component_count;
  }
  return vertex_attribute{num_vas, stride, MOVE(infos)};
}

} // namespace

namespace opengl
{
vertex_attribute_type
attribute_type_from_string(char const* str)
{
  if (common::cstrcmp(str, "position")) {
    return vertex_attribute_type::POSITION;
  }
  if (common::cstrcmp(str, "normal")) {
    return vertex_attribute_type::NORMAL;
  }
  if (common::cstrcmp(str, "color")) {
    return vertex_attribute_type::COLOR;
  }
  if (common::cstrcmp(str, "uv")) {
    return vertex_attribute_type::UV;
  }
  if (common::cstrcmp(str, "other")) {
    return vertex_attribute_type::OTHER;
  }

  // ERROR
  std::exit(EXIT_FAILURE);
}

vertex_attribute::vertex_attribute(size_t const n_apis, GLsizei const stride_p,
                                   std::array<attribute_pointer, API_BUFFER_SIZE>&& array)
    : num_apis_(n_apis)
    , stride_(stride_p)
    , apis_(MOVE(array))
{
}

void
vertex_attribute::upload_vertex_format_to_glbound_vao(log_t& LOGGER) const
{
  upload_format_state ufs{stride_};
  FOR(i, num_apis_)
  {
    auto const& api = apis_[i];
    assert(api.datatype != 0);
    assert(api.component_count > 0);
    configure_and_enable_attrib_pointer(LOGGER, api, ufs);
  }
}

bool
vertex_attribute::has_vertices() const
{
  return va_has_attribute_type(*this, vertex_attribute_type::POSITION);
}

bool
vertex_attribute::has_normals() const
{
  return va_has_attribute_type(*this, vertex_attribute_type::NORMAL);
}

bool
vertex_attribute::has_colors() const
{
  return va_has_attribute_type(*this, vertex_attribute_type::COLOR);
}

bool
vertex_attribute::has_uvs() const
{
  return va_has_attribute_type(*this, vertex_attribute_type::UV);
}

std::string
attribute_pointer::to_string() const
{
  return fmt::format("(API) -- '{}' datatype: {}' component_count: '{}'", this->index,
                     this->datatype, this->component_count);
}

std::ostream&
operator<<(std::ostream& stream, attribute_pointer const& api)
{
  stream << api.to_string();
  return stream;
}

std::string
vertex_attribute::to_string() const
{
  std::string result;
  result += fmt::format("(VA) -- num_apis: '{}' stride: '{}'\n", num_apis_, stride_);
  for (auto const& api : apis_) {
    result += "    ";
    result += api.to_string();
    result += "\n";
  }
  return result;
}

std::ostream&
operator<<(std::ostream& stream, vertex_attribute const& va)
{
  auto const print_delim = [&stream]() { stream << "-----------\n"; };
  print_delim();

  stream << va.to_string();
  print_delim();
  return stream;
}

vertex_attribute
make_vertex_attribute(std::initializer_list<attribute_pointer> apis)
{
  auto const b = std::cbegin(apis);
  auto const e = std::cend(apis);
  return create_va(b, e);
}

vertex_attribute
make_vertex_attribute(attribute_pointer const& api)
{
  std::array<attribute_pointer, 1> const apis{{api}};
  auto const                             b = std::cbegin(apis);
  auto const                             e = std::cend(apis);
  return create_va(b, e);
}

vertex_attribute
make_vertex_attribute(std::vector<attribute_pointer> const& container)
{
  auto const b = std::cbegin(container);
  auto const e = std::cend(container);
  return create_va(b, e);
}

} // namespace opengl
