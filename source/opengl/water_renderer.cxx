#include "water_renderer.hpp"
#include <opengl/bind.hpp>
#include <opengl/buffer.hpp>
#include <opengl/gpu.hpp>
#include <opengl/renderer.hpp>
#include <opengl/shader.hpp>
#include <opengl/skybox_renderer.hpp>
#include <opengl/uniform.hpp>

#include <boomhs/bounding_object.hpp>
#include <boomhs/camera.hpp>
#include <boomhs/camera_algorithm.hpp>
#include <boomhs/components.hpp>
#include <boomhs/delta_time.hpp>
#include <boomhs/material.hpp>
#include <boomhs/mesh.hpp>
#include <boomhs/state.hpp>
#include <boomhs/terrain.hpp>
#include <boomhs/water.hpp>

#include <common/log.hpp>

#include <math/random.hpp>
#include <math/view_frustum.hpp>
#include <math/viewport.hpp>

#include <cassert>
#include <extlibs/fmt.hpp>
#include <extlibs/glew.hpp>

using namespace opengl;

// auto static constexpr WIGGLE_UNDERATH_OFFSET = -0.2f;

namespace
{

void
setup(log_t& LOGGER, texture_h& ti, GLenum const v)
{
  ::glActiveTexture(v);
  bind_for(LOGGER, ti, [&]() {
    ti.set_fieldi(GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    ti.set_fieldi(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  });
}

template <typename FN>
void
render_water_common(log_t& LOGGER, GameState& gs, shader_type& sp, render_state& rstate,
                    Camera const& camera, DeltaTime const& dt, FN const& fn)
{
  auto&       ldata    = gs.ldata;
  auto&       registry = ldata.registry;
  auto const& wind     = ldata.wind;

  auto const render = [&](WaterInfo& winfo) {
    auto const eid = winfo.eid;
    if (registry.get<IsRenderable>(eid).hidden) {
      return;
    }
    auto const& tr   = registry.get<Transform>(eid);
    auto const& bbox = registry.get<AABoundingBox>(eid);

    glm::mat4 const& view_mat = camera.view_matrix(rstate.fr);
    glm::mat4 const& proj_mat = camera.proj_matrix(rstate.fr);
    if (!ViewFrustum::bbox_inside(view_mat, proj_mat, tr, bbox)) {
      return;
    }

    auto const& dinfo = registry.get<opengl::draw_state>(eid);

    BIND_UNTIL_END_OF_SCOPE(LOGGER, sp);
    uniform::set(LOGGER, sp, "u_clipPlane", ABOVE_VECTOR);

    {
      auto const wind_contrib  = (wind.dir * wind.speed);
      auto const water_contrib = (winfo.flow_direction * winfo.flow_speed);
      auto const combined      = wind_contrib + water_contrib;

      auto constexpr SPEED_REDUCTION = 20;
      float const time_offset        = dt.since_start_millis<float>() * dt() / SPEED_REDUCTION;
      auto const  combined_wt        = combined * time_offset;
      uniform::set(LOGGER, sp, "u_flowdir", combined_wt);
    }
    {
      auto& wbuffer = gs.uibuffers_classic.water;
      uniform::set(LOGGER, sp, "u_water.weight_texture", wbuffer.weight_texture);
    }

    BIND_UNTIL_END_OF_SCOPE(LOGGER, dinfo);
    fn(winfo, tr);
  };

  auto const winfos = find_all_entities_with_component<WaterInfo>(registry);
  for (auto const eid : winfos) {
    auto& wi = registry.get<WaterInfo>(eid);
    render(wi);
  }
  LOG_TRACE("Finished rendering water");
}

void
advanced_common(log_t& LOGGER, GameState& gs, render_state& rstate, entity_renderer& er,
                skybox_renderer&, terrain_renderer_default& tr, DeltaTime const& ft)
{
  auto& ldata    = gs.ldata;
  auto& registry = ldata.registry;

  auto const& fog_color = ldata.fog.color;
  render::clear_screen(fog_color);

  auto&       ui_debug = gs.uibuffers_classic.debug;
  auto const& cm       = rstate.cm;
  if (ui_debug.draw_terrain) {
    auto const dirlights   = find_dirlights(registry);
    auto const pointlights = find_pointlights(registry);

    auto&       tgrid     = ldata.tgrid;
    auto const& mat_table = ldata.mtable;
    auto const& mat       = mat_table.find("terrain");

    auto const& ambient = ldata.ambient;
    auto const& fog     = ldata.fog;

    auto& ttable = ldata.ttable;
    auto& ds     = rstate.count;

    bool const draw_normals = ui_debug.draw_normals;
    tr.render(LOGGER, ttable, cm.camera_matrix(), cm.view, mat, dirlights, pointlights, tgrid,
              ambient, fog, ABOVE_VECTOR, draw_normals, ds);
  }
  if (ui_debug.draw_3d_entities) {
    // NOTE: is this wrong? should we be passing in the cm?
    er.render3d(LOGGER, gs, rstate, cm, ft);
  }
}

} // namespace

namespace opengl
{

////////////////////////////////////////////////////////////////////////////////////////////////////
// BasicWaterRenderer
water_renderer_basic::water_renderer_basic(log_t& LOGGER, texture_h& diff, texture_h& norm,
                                           shader_type& sp)
    : sp_(&sp)
    , diffuse_(&diff)
    , normal_(&norm)
{
  ON_SCOPE_EXIT([]() { glActiveTexture(GL_TEXTURE0); });
  setup(LOGGER, *diffuse_, GL_TEXTURE0);
  setup(LOGGER, *normal_, GL_TEXTURE1);

  // connect texture units to shader program
  bind_for(LOGGER, sp_, [&]() {
    uniform::set(LOGGER, *sp_, "u_diffuse_sampler", 0);
    uniform::set(LOGGER, *sp_, "u_normal_sampler", 1);
  });
}

void
water_renderer_basic::render_water(log_t& LOGGER, GameState& gs, render_state& rstate,
                                   Camera const& camera, DeltaTime const& dt)
{
  auto const fn = [&](WaterInfo& winfo, Transform const& transform) {
    uniform::set(LOGGER, *sp_, "u_water.mix_color", winfo.mix_color);
    uniform::set(LOGGER, *sp_, "u_water.mix_intensity", winfo.mix_intensity);

    ::glActiveTexture(GL_TEXTURE0);
    ON_SCOPE_EXIT([]() { glActiveTexture(GL_TEXTURE0); });

    BIND_UNTIL_END_OF_SCOPE(LOGGER, *diffuse_);

    ::glActiveTexture(GL_TEXTURE1);
    BIND_UNTIL_END_OF_SCOPE(LOGGER, *normal_);

    auto&       ldata    = gs.ldata;
    auto&       registry = ldata.registry;
    auto const& dinfo    = registry.get<opengl::draw_state>(winfo.eid);

    auto const cm = camera.matrices(rstate.fr);
    auto const vm = camera.view_matrix(rstate.fr);
    auto const mm = transform.model_matrix();

    auto const& fog = ldata.fog;

    auto const& ui_debug     = gs.uibuffers_classic.debug;
    bool const  draw_normals = ui_debug.draw_normals;
    render::draw_3dshape(LOGGER, GL_TRIANGLE_STRIP, cm.camera_matrix(), vm, mm, fog, *sp_, dinfo,
                         draw_normals, rstate.count);
  };

  render_water_common(LOGGER, gs, *sp_, rstate, camera, dt, fn);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// MediumWaterRenderer
water_renderer_medium::water_renderer_medium(log_t& LOGGER, texture_h& diff, texture_h& norm,
                                             shader_type& sp)
    : sp_(&sp)
    , diffuse_(&diff)
    , normal_(&norm)
{
  {
    ON_SCOPE_EXIT([]() { glActiveTexture(GL_TEXTURE0); });
    setup(LOGGER, *diffuse_, GL_TEXTURE0);
    setup(LOGGER, *normal_, GL_TEXTURE1);
  }

  // connect texture units to shader program
  bind_for(LOGGER, sp_, [&]() {
    uniform::set(LOGGER, *sp_, "u_diffuse_sampler", 0);
    uniform::set(LOGGER, *sp_, "u_normal_sampler", 1);
  });
}

void
water_renderer_medium::render_water(log_t& LOGGER, GameState& gs, render_state& rstate,
                                    Camera const& camera, DeltaTime const& dt)
{
  auto& ldata    = gs.ldata;
  auto& registry = ldata.registry;

  Material const water_material{};

  auto const fn = [&](WaterInfo& winfo, Transform const& transform) {
    uniform::set(LOGGER, *sp_, "u_water.mix_color", winfo.mix_color);
    uniform::set(LOGGER, *sp_, "u_water.mix_intensity", winfo.mix_intensity);

    {
      auto& wbuffer = gs.uibuffers_classic.water;
      uniform::set(LOGGER, *sp_, "u_water.weight_light", wbuffer.weight_light);
    }

    ::glActiveTexture(GL_TEXTURE0);
    ON_SCOPE_EXIT([]() { ::glActiveTexture(GL_TEXTURE0); });

    BIND_UNTIL_END_OF_SCOPE(LOGGER, *diffuse_);

    ::glActiveTexture(GL_TEXTURE1);
    BIND_UNTIL_END_OF_SCOPE(LOGGER, *normal_);

    auto const& dinfo = registry.get<opengl::draw_state>(winfo.eid);

    auto const& ui_debug     = gs.uibuffers_classic.debug;
    bool const  draw_normals = ui_debug.draw_normals;
    auto const  model_matrix = transform.model_matrix();
    auto const  dirlights    = find_dirlights(registry);
    auto const  pointlights  = find_pointlights(registry);

    bool constexpr SET_NORMALMATRIX = false;
    render::shape_and_lighting lit_shape{transform.translation, model_matrix,     dinfo,       *sp_,
                                         water_material,        SET_NORMALMATRIX, draw_normals};

    auto const cm = camera.matrices(rstate.fr);
    auto const vm = camera.view_matrix(rstate.fr);

    auto const& global_light = ldata.ambient;
    auto const& fog          = ldata.fog;
    auto&       ds           = rstate.count;
    auto const  dm           = GL_TRIANGLE_STRIP;
    render::draw_3dlit_shape(LOGGER, dm, cm.camera_matrix(), vm, global_light, dirlights,
                             pointlights, fog, lit_shape, ds);
  };

  render_water_common(LOGGER, gs, *sp_, rstate, camera, dt, fn);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// ReflectionBuffers
reflection_buffer::reflection_buffer(log_t& LOGGER, Viewport const& vp)
    : fbo(opengl::make_fbo(LOGGER))
    , tbo(texture::from_config(frame_buffer_cfg{GL_TEXTURE_2D}, vp))
    , rbo(render_buffer_h{})
{
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// RefractionBuffers
refraction_buffer::refraction_buffer(log_t& LOGGER, Viewport const& vp)
    : fbo(opengl::make_fbo(LOGGER))
    , tbo(texture::from_config(frame_buffer_cfg{GL_TEXTURE_2D}, vp))
    , dbo(texture::from_config(frame_buffer_cfg{GL_TEXTURE_2D}, vp))
{
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// AdvancedWaterRenderer
water_renderer_advanced::water_renderer_advanced(log_t& LOGGER, Viewport const& vp, shader_type& sp,
                                                 texture_h& diffuse, texture_h& dudv,
                                                 texture_h& normal)
    : sp_(&sp)
    , diffuse_(&diffuse)
    , dudv_(&dudv)
    , normal_(&normal)
    , reflection_(LOGGER, vp)
    , refraction_(LOGGER, vp)
{
  resize(LOGGER, vp);
  {
    ON_SCOPE_EXIT([]() { glActiveTexture(GL_TEXTURE0); });
    setup(LOGGER, *diffuse_, GL_TEXTURE0);
    setup(LOGGER, reflection_.tbo, GL_TEXTURE1);
    setup(LOGGER, refraction_.tbo, GL_TEXTURE2);
    setup(LOGGER, *dudv_, GL_TEXTURE3);
    setup(LOGGER, *normal_, GL_TEXTURE4);
    setup(LOGGER, refraction_.dbo, GL_TEXTURE5);
  }

  // connect texture units to shader program
  bind_for(LOGGER, sp_, [&]() {
    uniform::set(LOGGER, *sp_, "u_diffuse_sampler", 0);
    uniform::set(LOGGER, *sp_, "u_reflect_sampler", 1);
    uniform::set(LOGGER, *sp_, "u_refract_sampler", 2);
    uniform::set(LOGGER, *sp_, "u_dudv_sampler", 3);
    uniform::set(LOGGER, *sp_, "u_normal_sampler", 4);
    uniform::set(LOGGER, *sp_, "u_depth_sampler", 5);
  });
}

void
water_renderer_advanced::resize(log_t& LOGGER, Viewport const& vp)
{
  auto const w = vp.width(), h = vp.height();
  {
    auto& fbo       = reflection_.fbo;
    reflection_.tbo = fbo->attach_color_buffer(LOGGER, w, h, GL_TEXTURE1);
    reflection_.rbo = fbo->attach_render_buffer(LOGGER, w, h);
  }
  {
    GLenum const tu  = GL_TEXTURE2;
    auto&        fbo = refraction_.fbo;
    refraction_.tbo  = fbo->attach_color_buffer(LOGGER, w, h, tu);
    refraction_.dbo  = fbo->attach_depth_buffer(LOGGER, w, h, tu);
  }
}

void
water_renderer_advanced::render_water(log_t& LOGGER, GameState& gs, render_state& rstate,
                                      Camera const& camera, DeltaTime const& dt)
{
  auto& ldata    = gs.ldata;
  auto& registry = ldata.registry;

  Material const water_material{};

  auto const fn = [&](WaterInfo& winfo, Transform const& transform) {
    auto const& dinfo = registry.get<opengl::draw_state>(winfo.eid);

    // TODO: These don't need to be set every frame, but only when the view frustum is updated.
    //
    // Since I don't have a routine that allows the frustum to be changed at runtime if I don't
    // compute it every frame, for now.. compute these every frame
    {
      auto& wbuffer = gs.uibuffers_classic.water;
      uniform::set(LOGGER, *sp_, "u_refract_reflect_ratio", wbuffer.refract_reflect_ratio);
      uniform::set(LOGGER, *sp_, "u_water.weight_light", wbuffer.weight_light);

      uniform::set(LOGGER, *sp_, "u_depth_divider", wbuffer.depth_divider);
      uniform::set(LOGGER, *sp_, "u_water.weight_mix_effect", wbuffer.weight_mix_effect);

      auto const& fr = rstate.fr;
      uniform::set(LOGGER, *sp_, "u_near", fr.near);
      uniform::set(LOGGER, *sp_, "u_far", fr.far);
    }

    // Set per-water instance data
    uniform::set(LOGGER, *sp_, "u_camera_position", camera.position());

    uniform::set(LOGGER, *sp_, "u_water.mix_color", winfo.mix_color);
    uniform::set(LOGGER, *sp_, "u_water.mix_intensity", winfo.mix_intensity);

    ::glActiveTexture(GL_TEXTURE0);
    ON_SCOPE_EXIT([]() { ::glActiveTexture(GL_TEXTURE0); });

    BIND_UNTIL_END_OF_SCOPE(LOGGER, *diffuse_);

    ::glActiveTexture(GL_TEXTURE1);
    BIND_UNTIL_END_OF_SCOPE(LOGGER, reflection_.tbo);
    BIND_UNTIL_END_OF_SCOPE(LOGGER, reflection_.rbo.ref());

    ::glActiveTexture(GL_TEXTURE2);
    BIND_UNTIL_END_OF_SCOPE(LOGGER, refraction_.tbo);

    ::glActiveTexture(GL_TEXTURE3);
    BIND_UNTIL_END_OF_SCOPE(LOGGER, *dudv_);

    ::glActiveTexture(GL_TEXTURE4);
    BIND_UNTIL_END_OF_SCOPE(LOGGER, *normal_);

    ::glActiveTexture(GL_TEXTURE5);
    BIND_UNTIL_END_OF_SCOPE(LOGGER, refraction_.dbo);

    ENABLE_ALPHA_BLENDING_UNTIL_SCOPE_EXIT();

    auto const& ui_debug     = gs.uibuffers_classic.debug;
    bool const  draw_normals = ui_debug.draw_normals;
    auto const  model_matrix = transform.model_matrix();

    auto const dirlights   = find_dirlights(registry);
    auto const pointlights = find_pointlights(registry);

    bool constexpr SET_NORMALMATRIX = false;
    render::shape_and_lighting lit_shape{transform.translation, model_matrix,     dinfo,       *sp_,
                                         water_material,        SET_NORMALMATRIX, draw_normals};

    auto const cm = camera.matrices(rstate.fr);
    auto const vm = camera.view_matrix(rstate.fr);

    auto const& global_light = ldata.ambient;
    auto const& fog          = ldata.fog;
    auto&       ds           = rstate.count;
    auto const  dm           = GL_TRIANGLE_STRIP;
    render::draw_3dlit_shape(LOGGER, dm, cm.camera_matrix(), vm, global_light, dirlights,
                             pointlights, fog, lit_shape, ds);
  };
  render_water_common(LOGGER, gs, *sp_, rstate, camera, dt, fn);
}

void
water_renderer_advanced::render_reflection(log_t& LOGGER, GameState& gs, render_state& rstate,
                                           Camera const& camera, entity_renderer& er,
                                           skybox_renderer& sr, terrain_renderer_default& tr,
                                           DeltaTime const& ft)
{
  // Compute the camera position beneath the water for capturing the reflective image the camera
  // will see.
  //
  // By inverting the camera's Y position before computing the view matrices, we can render the
  // world as if the camera was beneath the water's surface. This is how computing the reflection
  // texture works.
  glm::vec3 camera_pos = camera.position();
  camera_pos.y         = -camera_pos.y;

  auto const   cm = camera::compute_matrices(camera, rstate.fr, camera.mode, camera_pos);
  render_state underwater_rstate{cm, rstate.fr, rstate.count};

  auto const fn = [&]() { advanced_common(LOGGER, gs, underwater_rstate, er, sr, tr, ft); };
  with_reflection_fbo(LOGGER, fn);
}

void
water_renderer_advanced::render_refraction(log_t& LOGGER, GameState& gs, render_state& rstate,
                                           entity_renderer& er, skybox_renderer& sr,
                                           terrain_renderer_default& tr, DeltaTime const& ft)
{
  auto const fn = [&]() { advanced_common(LOGGER, gs, rstate, er, sr, tr, ft); };
  with_refraction_fbo(LOGGER, fn);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// water_renderer_silhouette
water_renderer_silhouette::water_renderer_silhouette(shader_type& sp)
    : sp_(&sp)
{
}

void
water_renderer_silhouette::render_water(log_t& LOGGER, GameState& gs, render_state& rstate,
                                        Camera const& camera, DeltaTime const&)
{
  auto& ldata    = gs.ldata;
  auto& registry = ldata.registry;

  auto const render = [&](WaterInfo& winfo) {
    auto const eid = winfo.eid;
    if (registry.get<IsRenderable>(eid).hidden) {
      return;
    }
    auto const& tr   = registry.get<Transform>(eid);
    auto const& bbox = registry.get<AABoundingBox>(eid);

    auto const& view_mat = camera.view_matrix(rstate.fr);
    auto const& proj_mat = camera.proj_matrix(rstate.fr);
    if (!ViewFrustum::bbox_inside(view_mat, proj_mat, tr, bbox)) {
      return;
    }

    auto const& dinfo = registry.get<opengl::draw_state>(winfo.eid);

    auto const mm = tr.model_matrix();
    auto const cm = camera.matrices(rstate.fr);

    BIND_UNTIL_END_OF_SCOPE(LOGGER, *sp_);
    BIND_UNTIL_END_OF_SCOPE(LOGGER, dinfo);
    render::draw_3d_no_lighting(LOGGER, GL_TRIANGLE_STRIP, cm.camera_matrix(), mm, *sp_, dinfo,
                                rstate.count);
  };

  LOG_TRACE("Rendering silhouette water");
  auto const winfos = find_all_entities_with_component<WaterInfo>(registry);
  for (auto const eid : winfos) {
    auto& wi = registry.get<WaterInfo>(eid);
    render(wi);
  }
  LOG_TRACE("Finished rendering silhouette water");
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// WaterRenderers
void
WaterRenderers::render(log_t& LOGGER, GameState& gs, render_state& rstate, Camera const& camera,
                       DeltaTime const& dt, bool const silhouette_black)
{
  if (silhouette_black) {
    silhouette.render_water(LOGGER, gs, rstate, camera, dt);
  }
  else {
    switch (gs.graphics.mode) {
    case GraphicsMode::Basic:
      basic.render_water(LOGGER, gs, rstate, camera, dt);
      break;

    case GraphicsMode::Medium:
      medium.render_water(LOGGER, gs, rstate, camera, dt);
      break;

    case GraphicsMode::Advanced:
      advanced.render_water(LOGGER, gs, rstate, camera, dt);
      break;
    }
  }
}

void
WaterRenderers::resize(log_t& LOGGER, Viewport const& vp)
{
  advanced.resize(LOGGER, vp);
}

} // namespace opengl
