#include "shader.hpp"
#include <opengl/debug.hpp>
#include <opengl/global.hpp>
#include <opengl/shader.hpp>
#include <opengl/vertex_attribute.hpp>

#include <gl_sdl/gl_sdl_log.hpp>

#include <common/FOR.hpp>
#include <common/algorithm.hpp>
#include <common/macros_class.hpp>
#include <common/move_only_type.hpp>
#include <common/os.hpp>
#include <common/result.hpp>

#include <cstring>
#include <extlibs/fmt.hpp>
#include <extlibs/glew.hpp>

using namespace opengl;

namespace
{
struct shader_sources
{
  vertex_shader_contents   vertex;
  fragment_shader_contents fragment;
};

void
delete_shader_wrapper(GLuint const v)
{
  ::glDeleteShader(v);
}

using compiled_shader_handle = move_only_type<GLuint, delete_shader_wrapper>;

struct compiled_shader
{
  compiled_shader_handle handle;
  std::string            source;
};

bool
is_compiled(GLuint const handle)
{
  GLint is_compiled = GL_FALSE;
  ::glGetShaderiv(handle, GL_COMPILE_STATUS, &is_compiled);
  return GL_FALSE != is_compiled;
}

Result<compiled_shader, std::string>
compile_shader(log_t& LOGGER, GLenum const type, std::string&& source)
{
  GLuint const handle = ::glCreateShader(type);
  LOG_ANY_GL_GENERAL_ERRORS_AND_ABORT(LOGGER, "glCreateShader");

  {
    constexpr char const* VERSION_STR = R"GLSL(#version 140
  precision mediump float;

)GLSL";
    source                            = VERSION_STR + source;
  }
  {
    auto constexpr NUM_SOURCE_STRINGS = 1;
    char const* cstr                  = source.c_str();
    ::glShaderSource(handle, NUM_SOURCE_STRINGS, &cstr, nullptr);
  }
  LOG_ANY_GL_GENERAL_ERRORS_AND_ABORT(LOGGER, "glShaderSource");
  LOG_ANY_GL_SHADER_ERRORS_AND_ABORT(LOGGER, handle);

  ::glCompileShader(handle);
  LOG_ANY_GL_GENERAL_ERRORS_AND_ABORT(LOGGER, "glCompileShader");
  LOG_ANY_GL_SHADER_ERRORS_AND_ABORT(LOGGER, handle);

  if (false == is_compiled(handle)) {
    return Err(gl_error::get_shader_log(handle));
  }
  compiled_shader_handle cs_handle{handle};
  compiled_shader        cs{MOVE(cs_handle), MOVE(source)};
  return OK_MOVE(cs);
}

auto
compile_fs(log_t& LOGGER, std::string&& source, NumLights nl)
{
  // The minimum number of pointlights must be non-zero, as this number
  // gets converted to a glsl array, which is required to have non-zero rect.
  auto constexpr MIN_LIGHTS = 1;

  auto const compute_num_lights = [MIN_LIGHTS](auto const nlights) {
    return nlights < MIN_LIGHTS ? MIN_LIGHTS : nlights;
  };

  nl.ndl = compute_num_lights(nl.ndl);
  nl.npl = compute_num_lights(nl.npl);

  auto combined = fmt::sprintf("#define MAX_NUM_DIRLIGHTS %i\n#define MAX_NUM_POINTLIGHTS %i\n%s",
                               nl.ndl, nl.npl, MOVE(source));
  return compile_shader(LOGGER, GL_FRAGMENT_SHADER, MOVE(combined));
}

auto
compile_vs(log_t& LOGGER, std::string&& source)
{
  return compile_shader(LOGGER, GL_VERTEX_SHADER, MOVE(source));
}

inline Result<program_h, std::string>
create_program()
{
  GLuint const program_id = ::glCreateProgram();
  if (INVALID_PROGRAM_ID == program_id) {
    return Err(std::string{"GlCreateProgram returned 0."});
  }
  return Ok(program_h{program_id});
}

inline Result<common::none_t, std::string>
link_program(log_t& LOGGER, GLuint const program_id)
{
  // Link the program
  LOG_ANY_GL_GENERAL_ERRORS_AND_ABORT(LOGGER, "glLinkProgram before");
  ::glLinkProgram(program_id);
  LOG_ANY_GL_GENERAL_ERRORS_AND_ABORT(LOGGER, "glLinkProgram after");

  // Check the program
  GLint result;
  ::glGetProgramiv(program_id, GL_LINK_STATUS, &result);
  if (result == GL_FALSE) {
    auto const program_log = gl_error::get_program_log(program_id);
    auto const shader_log  = gl_error::get_shader_log(program_id);
    auto const fmt = fmt::sprintf("Linking the shader failed. Progam log '%s'. Shader Log '%s'",
                                  program_log, shader_log);
    return Err(fmt);
  }
  return OK_NONE;
}

struct attribute_variable_info
{
  std::string variable;
};

struct vertex_shader_info
{
  std::string const& filename;
  std::string const& source;

  std::vector<attribute_variable_info> attribute_infos;
};

struct fragment_shader_info
{
  std::string const& filename;
  std::string const& source;
};

using attribute_variable_infos = std::vector<attribute_variable_info>;

Result<attribute_variable_infos, std::string>
avi_from_vertex_shader(std::string const& source)
{
  int         idx = 0;
  std::string buffer;
  auto const  make_error = [&](auto const& reason) {
    auto constexpr PREAMBLE = "Error parsing vertex shader for attribute information. Reason: '";
    auto constexpr SUFFIX   = "'. line number: '%s', line string: '%s.";
    auto const error        = PREAMBLE + std::string{reason} + SUFFIX;
    auto const fmt          = fmt::sprintf(error, std::to_string(idx), buffer);
    return Err(fmt);
  };

  std::vector<attribute_variable_info> infos;
  std::istringstream                   iss(source.c_str());
  for (; std::getline(iss, buffer); ++idx) {
    auto constexpr IN_PREFIX         = "in ";
    bool const begins_with_in_prefix = buffer.compare(0, ::strlen(IN_PREFIX), IN_PREFIX) == 0;
    if (!begins_with_in_prefix) {
      continue;
    }

    // The characters between the white-space and the semi-colon are the variable name.
    auto const semicolon_position = buffer.find(";", 0);
    if (semicolon_position == std::string::npos) {
      auto constexpr REASON = "No semi-colon found on variable line. ";
      return make_error(REASON);
    }

    // The character after the last whitespace before the semi-colon is the first character of the
    // input variable.
    auto const last_whitespace_position = buffer.find_last_of(" ", semicolon_position);
    if (last_whitespace_position == std::string::npos) {
      auto constexpr REASON = "No white-space found on input variable declaration. "
                              "Unexpected syntax.";
      return make_error(REASON);
    }

    auto const start_pos     = last_whitespace_position + 1;
    auto const length        = semicolon_position - start_pos;
    auto       variable_name = buffer.substr(start_pos, length);

    attribute_variable_info avi{MOVE(variable_name)};
    infos.emplace_back(MOVE(avi));

    // We don't have to parse whole shader, can stop at out variable declarations.
    auto constexpr OUT_PREFIX           = "out ";
    bool const begins_with_out_variable = buffer.compare(0, ::strlen(OUT_PREFIX), OUT_PREFIX) == 0;
    if (begins_with_out_variable) {
      break;
    }
  }
  return OK_MOVE(infos);
}

Result<program_h, std::string>
from_sources(log_t& LOGGER, attribute_variable_infos const& avis, vertex_shader_contents&& vs,
             fragment_shader_contents&& fs, NumLights const nl)
{
  auto vertex_shader = TRY(compile_vs(LOGGER, MOVE(vs)));
  auto frag_shader   = TRY(compile_fs(LOGGER, MOVE(fs), nl));

  auto       program    = TRY(create_program());
  auto const program_id = program.handle();

  FOR(i, avis.size())
  {
    auto const& vinfo = avis[i];
    LOG_DEBUG("binding program_id: %u, name: %s, index: %i", program_id, vinfo.variable, i);

    auto const index = static_cast<GLuint>(i);
    ::glBindAttribLocation(program_id, index, vinfo.variable.c_str());
  }

  ::glAttachShader(program_id, *vertex_shader.handle);
  ON_SCOPE_EXIT([&]() { ::glDetachShader(program_id, *vertex_shader.handle); });

  ::glAttachShader(program_id, *frag_shader.handle);
  ON_SCOPE_EXIT([&]() { ::glDetachShader(program_id, *frag_shader.handle); });

  TRY(link_program(LOGGER, program_id));
  LOG_TRACE("finished compiling");
  return OK_MOVE(program);
}

} // namespace

namespace
{
shader::load_r
from_spc_sourcecode(log_t& LOGGER, shader_cfg&& spc, NumLights const nl)
{
  // In this function, we treat the data fields of spc as source-code.
  auto avi = TRY(avi_from_vertex_shader(spc.vertex));

  auto vs      = spc.vertex;
  auto fs      = spc.fragment;
  auto program = TRY(from_sources(LOGGER, avi, MOVE(vs), MOVE(fs), nl));

  // SUPER INVALID, refactor this?
  shader_type sp{MOVE(program), MOVE(spc.va), MOVE(spc)};
  return OK_MOVE(sp);
}

} // namespace

namespace shader
{
shader::load_r
from_sourcecode(log_t& LOGGER, shader_cfg&& spc, NumLights const nl)
{
  LOG_TRACE("Loading shader directly from source [ndl:%lu,npl:%lu], \nv: '%s'\nf: '%s'", nl.ndl,
            nl.npl, spc.vertex, spc.fragment);
  return from_spc_sourcecode(LOGGER, MOVE(spc), nl);
}

shader::load_r
from_files(log_t& LOGGER, shader_cfg&& spc, NumLights const nl)
{
  // In this function, we treat the data fields of spc as filenames.
  auto const prefix = [](auto const& path) { return "./build-system/bin/shaders/" + path; };

  auto vs = vertex_shader_contents{TRY(common::read_file(prefix(spc.vertex)))};
  auto fs = fragment_shader_contents{TRY(common::read_file(prefix(spc.fragment)))};
  LOG_TRACE("Loading shader from files v: '%s', f: '%s'", vs, fs);

  // over-write the fields with the data read from the file, so it can be treated as source code
  // from now on.
  spc.vertex   = vs;
  spc.fragment = fs;
  return from_sourcecode(LOGGER, MOVE(spc), nl);
}

Result<NOTHING, std::string>
reload_from_files(log_t& LOGGER, enttreg_t& reg, NumLights const nl)
{
  for (auto&& pair : reg.ctx<shader_cache_3d>()) {
    auto sp_config = pair.second.copy_config();
    pair.second    = TRY(from_sourcecode(LOGGER, MOVE(sp_config), nl));
  }
  return OK_NONE;
}

} // namespace shader

namespace shader2d
{
opengl::shader_type*
lookup(enttreg_t& reg, std::string const& name)
{
  auto& cache = reg.ctx<shader_cache_2d>();
  return cache.lookup(name);
}

opengl::shader_type&
ref(enttreg_t& reg, std::string const& name)
{
  auto* ptr = lookup(reg, name);
  assert(ptr);
  return *ptr;
}

opengl::shader_type&
font(enttreg_t& reg)
{
  return ref(reg, "2dfont");
}

opengl::shader_type&
silhoutte(enttreg_t& reg)
{
  return ref(reg, "silhoutte_2d");
}

opengl::shader_type&
texture2d(enttreg_t& reg)
{
  return ref(reg, "2dtexture");
}

} // namespace shader2d

namespace shader3d
{
AdjustNumPointLightsResult
reload_shaders(log_t& LOGGER, enttreg_t& reg)
{
  auto const num_lights = shader3d::num_lights(reg);
  for (auto& pair : reg.ctx<shader_cache_3d>()) {
    auto sp_config = pair.second.copy_config();
    pair.second    = TRY(shader::from_sourcecode(LOGGER, MOVE(sp_config), num_lights));
  }
  return OK_NONE;
}

AdjustNumPointLightsResult
decrement_num_pointlights(log_t& LOGGER, enttreg_t& reg)
{
  --reg.ctx<NumLights>().npl;
  return reload_shaders(LOGGER, reg);
}

AdjustNumPointLightsResult
increment_num_pointlights(log_t& LOGGER, enttreg_t& reg)
{
  ++reg.ctx<NumLights>().npl;
  return reload_shaders(LOGGER, reg);
}

NumLights
num_lights(enttreg_t const& reg)
{
  return reg.ctx<NumLights>();
}

AdjustNumPointLightsResult
set_num_lights(log_t& LOGGER, enttreg_t& reg, NumLights const num_lights)
{
  reg.set<NumLights>(num_lights);
  return reload_shaders(LOGGER, reg);
}

opengl::shader_type*
lookup(enttreg_t& reg, std::string const& name)
{
  auto& cache = reg.ctx<shader_cache_3d>();
  return cache.lookup(name);
}

opengl::shader_type&
ref(enttreg_t& reg, std::string const& name)
{
  auto* ptr = lookup(reg, name);
  assert(ptr);
  return *ptr;
}

opengl::shader_type&
silhoutte(enttreg_t& reg)
{
  return ref(reg, "silhoutte_3d");
}
opengl::shader_type&
skybox(enttreg_t& reg)
{
  return ref(reg, "skybox");
}
opengl::shader_type&
sunshaft(enttreg_t& reg)
{
  return ref(reg, "sunshaft");
}
opengl::shader_type&
water_basic(enttreg_t& reg)
{
  return ref(reg, "water_medium");
}
opengl::shader_type&
water_medium(enttreg_t& reg)
{
  return ref(reg, "water_medium");
}
opengl::shader_type&
water_advanced(enttreg_t& reg)
{
  return ref(reg, "water_advanced");
}
opengl::shader_type&
wireframe(enttreg_t& reg)
{
  return ref(reg, "wireframe");
}

} // namespace shader3d
