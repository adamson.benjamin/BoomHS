#pragma once
#include <common/FOR.hpp>
#include <common/forward.hpp>
#include <common/move.hpp>

#include <array>
#include <cassert>
#include <cstring>
#include <iterator>
#include <string>
#include <tuple>
#include <vector>

namespace common
{
template <typename... Ts>
bool constexpr and_all(Ts... ts) noexcept
{
  return (... && ts);
}

template <typename... Ts>
bool constexpr or_all(Ts... ts) noexcept
{
  return (... || ts);
}

template <typename F, typename... Args>
void constexpr for_each(F f, Args&&... args) noexcept
{
  (f(std::forward<Args>(args)), ...);
}

// Helper function for determining if two char const* are equal
inline bool
cstrcmp(char const* a, char const* b)
{
  return 0 == ::strcmp(a, b);
}

inline bool
string_contains(std::string const& str, char const* phrase)
{
  return str.find(phrase) != std::string::npos;
}

inline auto*
memzero(void* const dest, size_t const count)
{
  return std::memset(dest, 0, count);
}

template <typename T, size_t N>
auto
vec_from_array(std::array<T, N> const& array)
{
  return std::vector<T>{array.cbegin(), array.cend()};
}

template <typename T, size_t N>
auto
vec_from_array(std::array<T, N>&& array)
{
  return std::vector<T>{array.cbegin(), array.cend()};
}

} // namespace common

namespace common
{

// Assumes the distance between 'b' and 'e' is positive and returns the distance as a size_t.
template <typename B, typename E>
auto constexpr distance_size_t(B const b, E const e)
{
  auto const d = std::distance(b, e);
  assert(d >= 0);
  return static_cast<size_t>(d);
}

// Given a reference to a value, and a pair of two values, return a reference to the item in the
// pair that is not the same as the value passed in.
//
// ie:
//
// auto a = 5, b = 10;
//
// assert(other_of(a, PAIR(a, b)) == b);
// assert(other_of(b, PAIR(a, b)) == a);
template <typename T>
constexpr T const&
other_of_two(T const& value, std::pair<T, T> const& pair)
{
  return value == pair.first ? pair.second : pair.first;
}

template <typename T, size_t N, class... Args>
constexpr auto
make_array(Args&&... args)
{
  return std::array<T, N>{{FORWARD(args)}};
}

template <typename T, class... Args>
constexpr auto
make_array(Args&&... args)
{
  auto constexpr N = sizeof...(args);
  return make_array<T, N>(FORWARD(args));
}

// Construct an array with 'N' elements from a function 'F' using arguments 'Args'.
//
// Construct an array of N elements, each element constructed by passing the function F all of the
// arguments Args.
template <size_t N, typename F, typename... Args>
auto constexpr make_array_from(F&& fn, Args&&... pack)
{
  using T = decltype(fn(FORWARD(pack)));

  std::array<T, N> arr;
  FOR(i, N) { arr[i] = fn(FORWARD(pack)); }
  return arr;
}

// Construct an array of 'N' elements invoking a function 'F' 'N' times.
//
// The function 'F' should accept a single index parameter 'i'. This function 'F' will forward the
// index value 'i' during the array construction to each 'F' invocation.
//
// The parameter 'i' is the current index of the array being constructed.
// ie: 0, 1, 2, ... up to N-1
template <size_t N, typename F>
auto constexpr make_array_from_fn_forwarding_index(F&& fn)
{
  using T = decltype(fn(N));
  std::array<T, N> arr;
  FOR(i, N) { arr[i] = fn(i); }

  return arr;
}

// Construct a std::vector<T> with 'n' rect.
template <typename T>
auto
vec_with_size(size_t const n)
{
  std::vector<T> buffer;
  buffer.resize(n);
  return buffer;
}

template <class Target = void, class... Args>
constexpr auto
concat_array(Args&&... args) noexcept
{
  auto constexpr fn = [](auto&& first, auto&&... rest) {
    using T =
        std::conditional_t<!std::is_void<Target>::value, Target, std::decay_t<decltype(first)>>;
    return std::array<T, (sizeof...(rest) + 1)>{{decltype(first)(first), decltype(rest)(rest)...}};
  };
  return std::apply(fn, std::tuple_cat(FORWARD(args)));
}

// This function gives an easier to remember name to this operation.
//
// Concatenates all elements of src to the end of dst.
//
// NOTE: dst and src must not be the same container, otherwise undefined behavior.
template <typename T>
void
concat_container(T& dst, T&& src)
{
  // https://stackoverflow.com/a/30798014/562174
  // https://en.cppreference.com/w/cpp/algorithm/move
  auto const b = std::make_move_iterator(src.begin());
  auto const e = std::make_move_iterator(src.end());
  std::move(b, e, std::back_inserter(dst));
}

template <typename T>
void
concat_vector(std::vector<T>& dst, std::vector<T>&& src)
{
  concat_container(dst, MOVE(src));
}

template <typename T>
void
concat_string(std::basic_string<T>& dst, std::basic_string<T>&& src)
{
  concat_container(dst, MOVE(src));
}

// trim from start (in place)
// void
// ltrim(std::string&);
//
//// trim from end (in place)
// void
// rtrim(std::string&);
//

// trim from both ends (in place)
void trim(std::string_view);

// Convert an array to a string.
template <typename T>
std::string
stringify(T const& array)
{
  std::string result = "{";
  FOR(i, array.size())
  {
    if (i > 0) {
      result += ", ";
    }
    auto const& v = array[i];
    result += std::to_string(v);
  }
  return result + "}";
}

} // namespace common
