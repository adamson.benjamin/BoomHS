#pragma once
#include <chrono>
#include <cstdint>

using Clock        = std::chrono::steady_clock;
using time_point_t = Clock::time_point;

using duration_t       = std::chrono::duration<int, std::milli>;
using seconds_duration = std::chrono::duration<int>;

namespace common::time
{

inline time_t
steady_clock_to_time_t(std::chrono::steady_clock::time_point const& t)
{
  using namespace std::chrono;

  auto const sys_now       = system_clock::now();
  auto const steady_now    = steady_clock::now();
  auto const diff          = t - steady_now;
  auto const diff_duration = duration_cast<system_clock::duration>(diff);
  return system_clock::to_time_t(sys_now + diff_duration);
}

} // namespace common::time
