#pragma once
#include <utility>

#define PAIR(A, B)                                                                                 \
  std::pair { A, B }

#define BREAK_IF(EXPR)                                                                             \
  if ((EXPR)) {                                                                                    \
    break;                                                                                         \
  }

#define CONTINUE_IF(EXPR)                                                                          \
  if ((EXPR)) {                                                                                    \
    continue;                                                                                      \
  }

#define RETURN_IF(EXPR)                                                                            \
  if ((EXPR)) {                                                                                    \
    return;

#define RETURN_VALUE_IF(EXPR)                                                                      \
  if ((EXPR)) {                                                                                    \
    return (EXPR);                                                                                 \
  }
