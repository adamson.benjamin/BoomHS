#pragma once
#include <type_traits>

#define FOR(Q, N) for (std::remove_cv_t<std::remove_reference_t<decltype(N)>> Q = 0; Q < N; ++Q)
