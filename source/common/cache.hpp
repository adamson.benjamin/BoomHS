#pragma once
#include <common/FOR.hpp>
#include <common/macros_container.hpp>

#include <optional>
#include <sstream>
#include <string>
#include <vector>

template <typename T>
class VectorCache
{
  std::vector<T> data_;

public:
  VectorCache() = default;
  MOVE_CONSTRUCTIBLE_ONLY(VectorCache);
  DEFINE_VECTOR_LIKE_WRAPPER_FNS(data_)
};

namespace cache
{
template <typename Cache>
std::vector<std::string>
all_shader_names(Cache const& sc)
{
  std::vector<std::string> result;
  for (auto const& it : sc) {
    result.emplace_back(it.name());
  }
  return result;
}

template <typename Cache>
std::string
all_shader_names_flattened(Cache const& cache, char const delim)
{
  auto const        sn = all_shader_names(cache);
  std::stringstream buffer;

  for (auto const& it : sn) {
    buffer << it;
    buffer << delim;
  }
  return buffer.str();
}

template <typename Cache>
std::optional<std::string>
nickname_at_index(Cache const& sc, size_t const index)
{
  auto const sn = all_shader_names(sc);
  if (index >= sn.size()) {
    return std::nullopt;
  }
  return sn[index];
}

template <typename Cache>
std::optional<size_t>
index_of_nickname(Cache const& sc, std::string const& name)
{
  auto const sn = all_shader_names(sc);
  FOR(i, sn.size())
  {
    auto const& it = sn[i];
    if (it == name) {
      return std::make_optional(i);
    }
  }
  return std::nullopt;
}

} // namespace cache
