#include <common/algorithm.hpp>

namespace common
{

void
trim(std::string_view s)
{
  auto constexpr SPECIAL_CHARS = " \t\r\v\n";

  s.remove_prefix(std::min(s.find_first_not_of(SPECIAL_CHARS), s.size()));
  s.remove_suffix((s.size() - 1) - std::min(s.find_last_not_of(SPECIAL_CHARS), s.size() - 1));
}

} // namespace common