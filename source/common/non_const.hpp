#pragma once

/// Helper macro for defining non-const versions of class member functions, where you don't have
/// to duplicate the logic inside the method, just delcare the name.
///
/// source: https://stackoverflow.com/a/55426147/562174
template <typename T>
struct NonConst;

template <typename T>
struct NonConst<T const&>
{
  using type = T&;
};

template <typename T>
struct NonConst<T const*>
{
  using type = T*;
};

#define NON_CONST(func)                                                                            \
  template <typename... TTTT>                                                                      \
  auto func(TTTT&&... a)->typename NonConst<decltype(func(a...))>::type                            \
  {                                                                                                \
    return const_cast<decltype(func(a...))>(std::as_const(*this).func(std::forward<TTTT>(a)...));  \
  }
