#pragma once
#include <common/forward.hpp>
#include <common/macros_class.hpp>
#include <common/move.hpp>

#include <array>
#include <cassert>
#include <deque>
#include <iterator>
#include <vector>

////////////////////////////////////////////////////////////////////////////////////////////////////
// BEGIN/END
#define BEGIN_END_FORWARD_FNS(CONTAINER)                                                           \
  decltype(auto) begin() { return CONTAINER.begin(); }                                             \
  decltype(auto) end() { return CONTAINER.end(); }                                                 \
                                                                                                   \
  decltype(auto) begin() const { return CONTAINER.begin(); }                                       \
  decltype(auto) end() const { return CONTAINER.end(); }                                           \
                                                                                                   \
  decltype(auto) cbegin() const { return CONTAINER.cbegin(); }                                     \
  decltype(auto) cend() const { return CONTAINER.cend(); }

////////////////////////////////////////////////////////////////////////////////////////////////////
// INDEX operator
#define INDEX_OPERATOR_FNS(CONTAINER)                                                              \
  auto const& operator[](size_t const i) const                                                     \
  {                                                                                                \
    assert(i < CONTAINER.size());                                                                  \
    return CONTAINER[i];                                                                           \
  }                                                                                                \
  auto& operator[](size_t const i)                                                                 \
  {                                                                                                \
    assert(i < CONTAINER.size());                                                                  \
    return CONTAINER[i];                                                                           \
  }

///////////////////////////////////////////////////////////////////////////////////////////////////
// Define common container-like forwarding functions.
#define DEFINE_ARRAY_LIKE_WRAPPER_FNS(CONTAINER)                                                   \
  INDEX_OPERATOR_FNS(CONTAINER)                                                                    \
  BEGIN_END_FORWARD_FNS(CONTAINER)                                                                 \
                                                                                                   \
  using CONTAINER_T            = decltype(CONTAINER);                                              \
  using value_type             = typename decltype(CONTAINER)::value_type;                         \
  using reference              = typename decltype(CONTAINER)::reference;                          \
  using const_reference        = typename decltype(CONTAINER)::const_reference;                    \
  using pointer                = typename decltype(CONTAINER)::pointer;                            \
  using const_pointer          = typename decltype(CONTAINER)::const_pointer;                      \
  using iterator               = typename decltype(CONTAINER)::iterator;                           \
  using const_iterator         = typename decltype(CONTAINER)::const_iterator;                     \
  using reverse_iterator       = typename decltype(CONTAINER)::reverse_iterator;                   \
  using const_reverse_iterator = typename decltype(CONTAINER)::const_reverse_iterator;             \
  using difference_type        = typename decltype(CONTAINER)::difference_type;                    \
  using size_type              = typename decltype(CONTAINER)::size_type;                          \
                                                                                                   \
  auto& back()                                                                                     \
  {                                                                                                \
    assert(!CONTAINER.empty());                                                                    \
    return CONTAINER.back();                                                                       \
  }                                                                                                \
  auto const& back() const                                                                         \
  {                                                                                                \
    assert(!CONTAINER.empty());                                                                    \
    return CONTAINER.back();                                                                       \
  }                                                                                                \
  auto& front()                                                                                    \
  {                                                                                                \
    assert(!CONTAINER.empty());                                                                    \
    return CONTAINER.front();                                                                      \
  }                                                                                                \
  auto const& front() const                                                                        \
  {                                                                                                \
    assert(!CONTAINER.empty());                                                                    \
    return CONTAINER.front();                                                                      \
  }                                                                                                \
  auto empty() const { return CONTAINER.empty(); }                                                 \
                                                                                                   \
  auto size() const { return CONTAINER.size(); }

///////////////////////////////////////////////////////////////////////////////////////////////////
// Define common vector-like forwarding functions.
#define DEFINE_VECTOR_LIKE_WRAPPER_FNS(CONTAINER)                                                  \
  DEFINE_ARRAY_LIKE_WRAPPER_FNS(CONTAINER)                                                         \
                                                                                                   \
  using allocator_type = typename decltype(CONTAINER)::allocator_type;                             \
                                                                                                   \
  auto capacity() const { return CONTAINER.capacity(); }                                           \
  auto clear() { CONTAINER.clear(); }                                                              \
  auto reserve(size_t const c) { CONTAINER.reserve(c); }                                           \
                                                                                                   \
  void push_back(value_type&& item) { CONTAINER.push_back(std::forward<value_type>(item)); }       \
  template <class... Args>                                                                         \
  auto& emplace(Args&&... args)                                                                    \
  {                                                                                                \
    return CONTAINER.emplace(FORWARD(args));                                                       \
  }                                                                                                \
  template <class... Args>                                                                         \
  auto& emplace_back(Args&&... args)                                                               \
  {                                                                                                \
    return CONTAINER.emplace_back(FORWARD(args));                                                  \
  }

#define DEFINE_STACK_LIKE_WRAPPR_FNS(CONTAINER)                                                    \
                                                                                                   \
  using CONTAINER_T = decltype(CONTAINER);                                                         \
  using value_type  = typename decltype(CONTAINER)::value_type;                                    \
                                                                                                   \
  bool empty() const { return CONTAINER.empty(); }                                                 \
                                                                                                   \
  auto& top() { return CONTAINER.top(); }                                                          \
                                                                                                   \
  template <class... Args>                                                                         \
  decltype(auto) emplace(Args&&... args)                                                           \
  {                                                                                                \
    CONTAINER.emplace(FORWARD(args));                                                              \
  }                                                                                                \
                                                                                                   \
  void pop() { CONTAINER.pop(); }                                                                  \
  void push(value_type const& item) { CONTAINER.push(item); }                                      \
  void push(value_type&& item) { CONTAINER.push(MOVE(item)); }

#define DEFINE_DEQUE_LIKE_WRAPPR_FNS(CONTAINER)                                                    \
  DEFINE_ARRAY_LIKE_WRAPPER_FNS(CONTAINER)                                                         \
                                                                                                   \
  template <class... Args>                                                                         \
  auto& emplace_back(Args&&... args)                                                               \
  {                                                                                                \
    return CONTAINER.emplace_back(FORWARD(args));                                                  \
  }                                                                                                \
                                                                                                   \
  void pop_back() { CONTAINER.pop_back(); }                                                        \
  void pop_front() { CONTAINER.pop_front(); }
