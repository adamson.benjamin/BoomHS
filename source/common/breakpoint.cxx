#ifdef _WIN64
#include <intrin.h>
#endif

namespace common
{
#ifdef DEBUG_BUILD
void
cpu_breakpoint()
{

// Emit the per-compiler instruction for the debugger
//
// Based on:
// https://mainisusuallyafunction.blogspot.com/2012/01/embedding-gdb-breakpoints-in-c-source.html
#ifdef _WIN64 // MSVC
  __debugbreak();
#elif defined(__GNUC___) // GCC
  asm volatile("int $3")
#elif defined(__CLANG__) // CLANG
  // UNKNOWN, GCC version didn't work for me.
#endif                   // _WIN64
}
#else

void
cpu_breakpoint()
{
  // All release builds, this compiles to nothing.
}
#endif // DEBUG_BUILD

} // namespace common
