#include "wall_clock.hpp"
#include <common/time.hpp>

#include <cassert>
#include <ctime>

using namespace common;
using namespace std;
using namespace std::chrono;

namespace
{

auto
now()
{
  return steady_clock::now();
}

} // namespace

WallClock::WallClock()
    : init_(now())
{
}

/*
int64_t
WallClock::offset_seconds() const
{
  auto const seconds = offset_.seconds;
  auto const minutes = offset_.minutes;
  auto const hours   = offset_.hours;
  auto const days    = offset_.days;
  auto const weeks   = offset_.weeks;
  auto const months  = offset_.months;
  auto const years   = offset_.years;

  return seconds
    + minutes_to_seconds(minutes)
    + hours_to_seconds(hours)
    + days_to_seconds(days)
    + weeks_to_seconds(weeks)
    + months_to_seconds(months)
    + years_to_seconds(years);
}

WallClock::total_seconds() const
{
  return elapsed_seconds() + offset_seconds();
}
*/

void
WallClock::reset()
{
  last_reset_ = now();
}

/*
int
WallClock::seconds() const
{
  auto const seconds = total_seconds();
  return seconds % 60;
}

void
WallClock::add_seconds(int const v)
{
  auto& seconds = offset_.seconds;
  seconds_ += v;
  if (seconds > 59) {
    add_minutes(1);
    seconds_ = 0;
  }
}

void
WallClock::set_seconds(int const v)
{
  offset_.seconds_ = v;
}

int
WallClock::minutes() const
{
  auto const seconds = total_seconds();
  return seconds_to_minutes(seconds) % 60;
}

void
WallClock::add_minutes(int const v)
{
  auto& minutes = offset_.minutes;
  minutes += v;
  if (minutes > 59) {
    add_hours(1);
    minutes = 0;
  }
}

void
WallClock::set_minutes(int const v)
{
  offset_.minutes = v;
}

*/
long
WallClock::hours() const
{
  return duration_cast<chrono::hours>(init_.time_since_epoch()).count();
}

/*
void
WallClock::add_hours(int const v)
{
  auto& hours = offset_.hours;
  hours += v;
  if (hours > 23) {
    add_days(1);
    hours = 0;
  }
}

void
WallClock::set_hours(int const v)
{
  offset_.hours = v;
}

int
WallClock::days() const
{
  auto const seconds = total_seconds();
  return seconds_to_days(seconds) % 7;
}

void
WallClock::set_days(int const v)
{
  offset_.days = v;
}

void
WallClock::add_days(int const v)
{
  auto& days = offset_.days;
  days += v;
  if (days > 30) {
    add_weeks(1);
    days = 0;
  }
}

int
WallClock::weeks() const
{
  auto const seconds = total_seconds();
  return seconds_to_weeks(seconds) % 4;
}

void
WallClock::add_weeks(int const v)
{
  auto& weeks = offset_.weeks;
  weeks += v;
  if (weeks > 4) {
    add_months(1);
    weeks = 0;
  }
}

void
WallClock::set_weeks(int const v)
{
  offset_.weeks = v;
}

int
WallClock::months() const
{
  auto const seconds = total_seconds();
  return seconds_to_months(seconds) % 12;
}

void
WallClock::add_months(int const v)
{
  auto& months = offset_.months;
  months += v;
  if (months > 12) {
    add_years(1);
    months = 0;
  }
}

void
WallClock::set_months(int const v)
{
  offset_.months = v;
}

int
WallClock::years() const
{
  auto const seconds = total_seconds();
  return seconds_to_years(seconds) % 365;
}

void
WallClock::add_years(int const v)
{
  offset_.years += v;
}

void
WallClock::set_years(int const v)
{
  offset_.years = v;
}
*/

void
WallClock::set_speed(int const speed)
{
  multiplier_.speed = speed;
}

duration_t
WallClock::since_beginning() const
{
  auto const diff = now() - init_;
  return duration_cast<chrono::milliseconds>(diff);
}
