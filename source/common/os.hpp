#pragma once
#include <common/macros_class.hpp>
#include <common/macros_scope_exit.hpp>
#include <common/result.hpp>
#include <common/tuple.hpp>

#include <filesystem>

#include <fstream>
#include <iostream>
#include <string>

namespace common
{

// Read the contents of a file into a string.
//
// On success, returns an Ok(std::string) filled with the contents of the file.
// On error, returns an Err(std::string) filled with a description of why reading the file failed.
Result<std::string, std::string>
read_file(char const*);

// Alias for reading a file, given a std::string as a filename.
inline auto
read_file(std::string const& s)
{
  return read_file(s.c_str());
}

inline auto
read_file(std::filesystem::path const& path)
{
  return read_file(path.string());
}

// TODO: boost::path
template <typename... Text>
void
write_file(std::filesystem::path const& path, Text const&... text)
{
  std::ifstream is;
  auto*         fb = is.rdbuf();
  fb->open(path.string().c_str(), std::ios::out);
  ON_SCOPE_EXIT([&fb]() { fb->close(); });

  std::ostream os{fb};
  auto const   write_line = [&os](auto const& line) { os << line; };

  auto const tuple_view = std::make_tuple(text...);
  common::tuple_for_each(tuple_view, write_line);
}

} // namespace common
