#include "timer.hpp"

using namespace std::chrono;

namespace common
{

Clock::duration
StopWatch::time_elapsed() const
{
  return Clock::now() - epoch_;
}

void
StopWatch::reset()
{
  epoch_ = Clock::now();
}

bool
StopWatch::has_time_elapsed(duration_t const dur) const
{
  return time_elapsed() > dur;
}

bool
Timer::is_expired() const
{
  return stopwatch_.has_time_elapsed(duration_);
}

void
Timer::set_duration(duration_t const dur)
{
  duration_ = dur;
  stopwatch_.reset();
}

} // namespace common
