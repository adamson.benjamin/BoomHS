#pragma once
#include <common/forward.hpp>
#include <common/macros_class.hpp>
#include <common/move.hpp>

#include <extlibs/fmt.hpp>
#include <extlibs/spdlog.hpp>

#include <cassert>

namespace common
{
using LoggerPointer = std::unique_ptr<spdlog::logger>;
enum class LogLevel
{
  trace = 0,
  debug,
  info,
  warn,
  error,
  MAX,
};

class LogWriter
{
  LoggerPointer logger_;

public:
  MOVE_CONSTRUCTIBLE_ONLY(LogWriter);
  explicit LogWriter(LoggerPointer&& lp)
      : logger_(MOVE(lp))
  {
  }

  template <typename... Params>
  void trace(Params&&... p)
  {
    logger_->trace(fmt::sprintf(FORWARD(p)));
  }

  template <typename... Params>
  void debug(Params&&... p)
  {
    logger_->debug(fmt::sprintf(FORWARD(p)));
  }

  template <typename... Params>
  void info(Params&&... p)
  {
    logger_->info(fmt::sprintf(FORWARD(p)));
  }

  template <typename... Params>
  void warn(Params&&... p)
  {
    logger_->warn(fmt::sprintf(FORWARD(p)));
  }

  template <typename... Params>
  void error(Params&&... p)
  {
    logger_->error(fmt::sprintf(FORWARD(p)));
  }

  void flush() { logger_->flush(); }
  void set_level(spdlog::level::level_enum const level) { logger_->set_level(level); }
};

struct Logger : public ::common::LogWriter
{
  template <typename... Args>
  Logger(Args&&... args)
      : ::common::LogWriter(FORWARD(args))
  {
  }
};
} // namespace common
struct log_t : public ::common::Logger
{
};

#define DEFINE_LOG_FN(FN_NAME)                                                                     \
  template <typename... Params>                                                                    \
  void FN_NAME(log_t& logger, Params&&... p)                                                       \
  {                                                                                                \
    logger.FN_NAME(fmt::sprintf(FORWARD(p)));                                                      \
  }                                                                                                \
  template <typename... Params>                                                                    \
  void FN_NAME(log_t* plogger, Params&&... p)                                                      \
  {                                                                                                \
    assert(plogger);                                                                               \
    FN_NAME(*plogger, FORWARD(p));                                                                 \
  }

namespace common::log::internal
{
DEFINE_LOG_FN(trace)
DEFINE_LOG_FN(debug)
DEFINE_LOG_FN(info)
DEFINE_LOG_FN(warn)
DEFINE_LOG_FN(error)
} // namespace common::log::internal

namespace common::log_factory
{

log_t
make_default(char const*, spdlog::level::level_enum);

log_t make_stderr(spdlog::level::level_enum = spdlog::level::trace);
} // namespace common::log_factory
