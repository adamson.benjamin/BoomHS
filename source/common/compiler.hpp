#pragma once
#include <assert.h>

#ifdef DEBUG_BUILD
#define FOR_DEBUG_ONLY(fn) fn()
#define FOR_RELEASE_ONLY(fn) []() {}

#elif RELEASE_BUILD
#define FOR_DEBUG_ONLY(fn) []() {}
#define FOR_RELEASE_ONLY(fn) fn()
#endif
