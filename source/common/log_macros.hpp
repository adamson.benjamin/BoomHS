#pragma once
#include <common/log_type.hpp>

#undef LOG_TRACE
#undef LOG_DEBUG
#undef LOG_INFO
#undef LOG_WARN
#undef LOG_ERROR

#ifdef DEBUG_BUILD
#define LOG_TRACE_IMPL(...) ::common::log::internal::trace(LOGGER, __VA_ARGS__)
#define LOG_DEBUG_IMPL(...) ::common::log::internal::debug(LOGGER, __VA_ARGS__)
#define LOG_INFO_IMPL(...) ::common::log::internal::info(LOGGER, __VA_ARGS__)
#define LOG_WARN_IMPL(...) ::common::log::internal::warn(LOGGER, __VA_ARGS__)
#define LOG_ERROR_IMPL(...) ::common::log::internal::error(LOGGER, __VA_ARGS__)
#else
#define LOG_TRACE_IMPL(...) []() {}
#define LOG_DEBUG_IMPL(...) []() {}
#define LOG_INFO_IMPL(...) []() {}
#define LOG_WARN_IMPL(...) []() {}
#define LOG_ERROR_IMPL(...) []() {}
#endif

#define LOG_TRACE(...) LOG_TRACE_IMPL(__VA_ARGS__)
#define LOG_DEBUG(...) LOG_DEBUG_IMPL(__VA_ARGS__)
#define LOG_INFO(...) LOG_INFO_IMPL(__VA_ARGS__)
#define LOG_WARN(...) LOG_WARN_IMPL(__VA_ARGS__)
#define LOG_ERROR(...) LOG_ERROR_IMPL(__VA_ARGS__)