#pragma once
#include <common/macros_class.hpp>
#include <common/result.hpp>
#include <common/time.hpp>

#include <chrono>

struct TimeOffset
{
  int seconds = 0, minutes = 0, hours = 0;
  int days = 0, weeks = 0, months = 0, years = 0;
};

struct TimeMultiplier
{
  int speed = 1;
};

// When the last reset occurred.
using LastResetTimePoint = time_point_t;

class WallClock
{
  time_point_t const init_;

  LastResetTimePoint last_reset_;

  // How much time has been aftificially added/subtracted from the actual time that has passed
  // since this instance was instantiated.
  TimeOffset offset_;

  // A multiplier for time elapsing, to speed-up/slow-down the passing of time.
  TimeMultiplier multiplier_;

public:
  MOVE_CONSTRUCTIBLE_ONLY(WallClock);
  WallClock();

  void reset();

  int  seconds() const;
  void add_seconds(int);
  void set_seconds(int);

  int  minutes() const;
  void add_minutes(int);
  void set_minutes(int);

  long hours() const;
  void add_hours(int);
  void set_hours(int);

  int  days() const;
  void add_days(int);
  void set_days(int);

  int  weeks() const;
  void add_weeks(int);
  void set_weeks(int);

  int  months() const;
  void add_months(int);
  void set_months(int);

  int  years() const;
  void add_years(int);
  void set_years(int);

  auto speed() const { return multiplier_.speed; }
  void set_speed(int);

  duration_t since_beginning() const;
};
