#include "log.hpp"

#include <common/algorithm.hpp>
#include <common/compiler.hpp>

#include <common/macros_class.hpp>

#include <iostream>
#include <memory>

using namespace common;

namespace
{
auto
threadsafe_sink(char const* file_path)
{
  // use multi-threaded spdlog sink
  using SinkType = spdlog::sinks::basic_file_sink_mt;
  return std::make_unique<SinkType>(file_path, true);
}

auto
threadsafe_stderr_sink()
{
  using SinkType = spdlog::sinks::stderr_color_sink_st;
  return std::make_unique<SinkType>();
}

[[noreturn]] void
on_spderr_exception(spdlog::spdlog_ex const& ex)
{
  std::cerr << "spdlog_ex: '" << ex.what() << "'\n";
  std::exit(EXIT_FAILURE);
}

LogWriter
make_logflusher(char const* file_path, spdlog::level::level_enum const level)
{
  try {
    auto stderr_sink = threadsafe_stderr_sink();
    stderr_sink->set_level(level);

    auto       file_sink = threadsafe_sink(file_path);
    auto const sinks     = common::make_array<spdlog::sink_ptr>(MOVE(file_sink), MOVE(stderr_sink));
    auto const cbegin    = std::cbegin(sinks);
    auto const cend      = std::cend(sinks);
    auto       logger    = std::make_unique<spdlog::logger>(file_path, cbegin, cend);
    logger->set_level(level);
    return LogWriter{MOVE(logger)};
  }
  catch (spdlog::spdlog_ex const& ex) {
    on_spderr_exception(ex);
  }
}

} // namespace

namespace common::log_factory
{

log_t
make_default(char const* path, spdlog::level::level_enum const level)
{
  // 1. Construct an instance of the default log group.
  // 2. Construct an instance of a logger that writes all log levels to a shared file.
  auto flusher = make_logflusher(path, level);
  return log_t{MOVE(flusher)};
}

log_t
make_stderr(spdlog::level::level_enum const level)
{
  try {
    auto stderr_sink = threadsafe_stderr_sink();
    stderr_sink->set_level(level);

    auto const sinks  = common::make_array<spdlog::sink_ptr>(MOVE(stderr_sink));
    auto       logger = std::make_unique<spdlog::logger>("", sinks.cbegin(), sinks.cend());
    logger->set_level(level);
    return log_t{LogWriter{MOVE(logger)}};
  }
  catch (spdlog::spdlog_ex const& ex) {
    on_spderr_exception(ex);
  }
}

} // namespace common::log_factory
