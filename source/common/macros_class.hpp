#pragma once
#include <common/forward.hpp>
#include <common/move.hpp>

// Define a common set of macros for use when defining classes.
//
// Most of the definitions are just an alias for doing something that is by default verbose in
// standard C++.
//
// The macros made the user's code easier to understand at a glance, reducing visual boilerplate.

////////////////////////////////////////////////////////////////////////////////////////////////////
// BASIC
#define DEFAULT_CONSTRUCTIBLE(CLASSNAME) CLASSNAME() = default;

////////////////////////////////////////////////////////////////////////////////////////////////////
// COPY/MOVE
#define COPY_CONSTRUCTIBLE(CLASSNAME) CLASSNAME(CLASSNAME const&) = default
#define COPY_ASSIGNABLE(CLASSNAME) CLASSNAME& operator=(CLASSNAME const&) = default

#define MOVE_CONSTRUCTIBLE(CLASSNAME) CLASSNAME(CLASSNAME&&) = default
#define MOVE_ASSIGNABLE(CLASSNAME) CLASSNAME& operator=(CLASSNAME&&) = default

#define MOVE_CONSTRUCTIBLE_NOEXCEPT(CLASSNAME) CLASSNAME(CLASSNAME&&) noexcept = default
#define MOVE_ASSIGNABLE_NOEXCEPT(CLASSNAME) CLASSNAME& operator=(CLASSNAME&&) noexcept = default

////////////////////////////////////////////////////////////////////////////////////////////////////
// COPY
#define NO_COPY_ASSIGN(CLASSNAME) CLASSNAME& operator=(CLASSNAME const&) = delete
#define NO_COPY_CONSTRUTIBLE(CLASSNAME) CLASSNAME(CLASSNAME const&) = delete

#define COPY_DEFAULT(CLASSNAME)                                                                    \
  COPY_CONSTRUCTIBLE(CLASSNAME);                                                                   \
  COPY_ASSIGNABLE(CLASSNAME)

#define COPY_CONSTRUCTIBLE_ONLY(CLASSNAME)                                                         \
  NO_MOVE(CLASSNAME);                                                                              \
  NO_COPY_ASSIGN(CLASSNAME);                                                                       \
  COPY_CONSTRUCTIBLE(CLASSNAME)

#define COPY_ONLY(CLASSNAME)                                                                       \
  NO_MOVE(CLASSNAME);                                                                              \
  COPY_DEFAULT(CLASSNAME)

#define NO_COPY(CLASSNAME)                                                                         \
  NO_COPY_ASSIGN(CLASSNAME);                                                                       \
  NO_COPY_CONSTRUTIBLE(CLASSNAME)

////////////////////////////////////////////////////////////////////////////////////////////////////
// MOVE
#define NO_MOVE_ASSIGN(CLASSNAME) CLASSNAME& operator=(CLASSNAME&&) = delete

#define NO_MOVE_CONSTRUTIBLE(CLASSNAME) CLASSNAME(CLASSNAME&&) = delete

#define MOVE_DEFAULT(CLASSNAME)                                                                    \
  MOVE_CONSTRUCTIBLE(CLASSNAME);                                                                   \
  MOVE_ASSIGNABLE(CLASSNAME)

#define MOVE_CONSTRUCTIBLE_ONLY(CLASSNAME)                                                         \
  NO_COPY(CLASSNAME);                                                                              \
  NO_MOVE_ASSIGN(CLASSNAME);                                                                       \
  MOVE_CONSTRUCTIBLE(CLASSNAME)

#define MOVE_DEFAULT_ONLY(CLASSNAME)                                                               \
  NO_COPY(CLASSNAME);                                                                              \
  MOVE_DEFAULT(CLASSNAME)

#define NO_MOVE(CLASSNAME)                                                                         \
  NO_MOVE_CONSTRUTIBLE(CLASSNAME);                                                                 \
  NO_MOVE_ASSIGN(CLASSNAME)

////////////////////////////////////////////////////////////////////////////////////////////////////
// COPY/MOVE
#define COPYMOVE_DEFAULT(CLASSNAME)                                                                \
  COPY_DEFAULT(CLASSNAME);                                                                         \
  MOVE_DEFAULT(CLASSNAME)

#define COPYMOVE_CONSTRUCTIBLE_NO_COPYASSIGN(CLASSNAME)                                            \
  MOVE_CONSTRUCTIBLE(CLASSNAME);                                                                   \
  COPY_CONSTRUCTIBLE(CLASSNAME);                                                                   \
  NO_MOVE_ASSIGN(CLASSNAME);                                                                       \
  NO_COPY_ASSIGN(CLASSNAME)

#define NO_COPY_OR_MOVE(CLASSNAME)                                                                 \
  NO_COPY(CLASSNAME);                                                                              \
  NO_MOVE(CLASSNAME)

#define NOCOPY_MOVE_DEFAULT(CLASSNAME)                                                             \
  MOVE_CONSTRUCTIBLE(CLASSNAME);                                                                   \
  MOVE_ASSIGNABLE(CLASSNAME);                                                                      \
  NO_COPY(CLASSNAME)

////////////////////////////////////////////////////////////////////////////////////////////////////
// COMBINED
#define NO_MOVE_OR_COPY(CLASSNAME) NO_COPY_OR_MOVE(CLASSNAME)
