#pragma once
#include <utility>

// Alias for std::move (rationale: MOVE is easier to read/grok than std::move, once the pattern is
// internalized by the programmer).
#define MOVE(A) std::move(A)
