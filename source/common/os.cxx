#include "os.hpp"

#include <fstream>
#include <iostream>
#include <sstream>

namespace common
{

Result<std::string, std::string>
read_file(char const* path)
{
  // Read the Vertex Shader code from the file
  std::stringstream sstream;
  {
    std::ifstream istream(path, std::ios::in);

    if (!istream.is_open()) {
      return Err("Error opening file at path '" + std::string{path} + "'");
    }

    std::string buffer;
    bool        first = true;
    while (std::getline(istream, buffer)) {
      if (!first) {
        sstream << "\n";
      }
      else {
        first = false;
      }
      sstream << buffer;
    }
    // explicit, dtor should do this.
    istream.close();
  }
  return Ok(sstream.str());
}

} // namespace common
