#pragma once
#include <common/macros_class.hpp>
#include <common/move.hpp>

#include <utility>

namespace core::impl
{
// Wrapper around a function that will be called when the instance of the class
// is destroyed.
//
// Must be passed an r-value, to keep semantics simple.
// Cannot be moved or copied, and this class does NOT expose the function it
// will call after it is
// constructed.
template <typename FN>
class DestroyFN
{
  FN fn_;

public:
  DestroyFN(FN&& fn)
      : fn_(std::forward<FN>(fn))
  {
  }
  ~DestroyFN() { this->fn_(); }

  NO_COPY_OR_MOVE(DestroyFN);
};

} // namespace core::impl

// Macros and helper-macros
#define ON_SCOPE_EXIT_CONSTRUCT_IN_PLACE(VAR, fn)                                                  \
  ::core::impl::DestroyFN<decltype(fn)> const VAR { fn }

#define ON_SCOPE_EXIT_MOVE_EXPR_INTO_VAR(VAR, expr)                                                \
  auto TEMPORARY##VAR = expr;                                                                      \
  ON_SCOPE_EXIT_CONSTRUCT_IN_PLACE(VAR, MOVE(TEMPORARY##VAR))

#define ON_SCOPE_EXIT_CONCAT(pre, VAR, expr) ON_SCOPE_EXIT_MOVE_EXPR_INTO_VAR(pre##VAR, (expr))
#define ON_SCOPE_EXIT_EXPAND(VAR, expr) ON_SCOPE_EXIT_CONCAT(_scopeignoreme_, VAR, expr)
#define ON_SCOPE_EXIT(expr) ON_SCOPE_EXIT_EXPAND(__COUNTER__, expr)

#define ON_SCOPE_EXIT_REF(expr) ON_SCOPE_EXIT([&]() { (expr); })
