#pragma once
//
// Bring in std::type_traits, so user code doesn't need too.
#include <type_traits>

// https://stackoverflow.com/a/53945549/562174
//
// Used in constexpr if chaining functions to ensure else clause is failure to compile.
template <class...>
constexpr std::false_type always_false{};
