#pragma once
#include <common/macros_class.hpp>
#include <common/time.hpp>

namespace common
{

class StopWatch
{
  Clock::time_point epoch_;

  Clock::duration time_elapsed() const;

public:
  COPYMOVE_DEFAULT(StopWatch);
  StopWatch() = default;

  void reset();
  bool has_time_elapsed(duration_t) const;
};

///
/// Basic timer.
class Timer
{
  common::StopWatch stopwatch_;
  duration_t        duration_;

public:
  Timer() = default;

  bool is_expired() const;

  void reset() { set_duration(duration_); }
  void set_duration(duration_t);
};

///
/// Timer that can be enabled/disabled.
class OnOffTimer
{
  Timer timer_;
  bool  enabled_ = false;

public:
  bool is_expired() const { return timer_.is_expired(); }
  bool is_enabled() const { return enabled_; }

  void turn_on() { enabled_ = true; }
  void turn_off() { enabled_ = false; }

  void reset() { timer_.reset(); }
  void set_duration(duration_t const dur) { timer_.set_duration(dur); }
};

/// Timer that switches between two states, A and B.
class TwoStateTimer
{
  OnOffTimer timer_;
  bool       state_ = false;

public:
  bool is_expired() const { return timer_.is_expired(); }
  bool is_enabled() const { return timer_.is_enabled(); }

  void reset() { timer_.reset(); }
  void set_duration(duration_t const dur) { timer_.set_duration(dur); }

  void turn_on() { timer_.turn_on(); }
  void turn_off() { timer_.turn_off(); }
  void toggle() { state_ ^= true; }

  bool state_a() const { return is_enabled() && state_; }
  bool state_b() const { return is_enabled() && !state_; }

  void set_state_a() { state_ = true; }
  void set_state_b() { state_ = false; }
};

} // namespace common
