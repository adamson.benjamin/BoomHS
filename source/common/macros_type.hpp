#pragma once
#include <type_traits>

// Alias macro for determining if two types are equivalent. Less boilerplate, easier to grok.
#define TYPES_MATCH(A, B) std::is_same<A, B>::value

template <class T, class... Rest>
inline constexpr bool all_all_same = (std::is_same_v<T, Rest> && ...);