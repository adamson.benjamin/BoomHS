#pragma once
#include <common/algorithm.hpp>
#include <common/macros_class.hpp>

#include <extlibs/fmt.hpp>
#include <extlibs/oktal.hpp>

// OK_MOVE
//
// Shortcut for common idiom:
//    auto st = ...;
//    return Ok(MOVE(st));
//
// becomes
//
//    auto st = ...;
//    return OK_MOVE(st);
#define OK_MOVE(...) Ok(MOVE(__VA_ARGS__))

// ERR_MOVE
//
// Shortcut for common idiom:
//    auto st = ...;
//    return Err(MOVE(st));
//
// becomes
//
//    auto st = ...;
//    return ERR_MOVE(st);
#define ERR_MOVE(...) Err(MOVE(__VA_ARGS__))

// OK_PAIR
//
// Shortcut for common idiom:
//    auto st = ...;
//    return Ok(PAIR(st));
//
// becomes
//
//    auto st = ...;
//    return OK_PAIR(st);
#define OK_PAIR(A, B) Ok(PAIR(A, B))

//#define OK_TUPLE_MOVEALL(...) Ok(MOVE_ALL_TUPLE(__VA_ARGS__))
#define OK_PAIR_MOVEALL(A, B) Ok(MOVE_ALL_PAIR(A, B))

//
// Shortcut macro for returning a formatted string directly as an error.
// Normally you have to write out code like this:
//     return Err(fmt::sprintf("Error reason thing %i '%s' %i", 38, get_error_reason(), 42));
//
// Instead, using this macro, you can write out the following
//
// usage example:
//    return ERR_SPRINTF("Error reason thing %i '%s' %i", 38, get_error_reason(), 42);
#define ERR_SPRINTF(...) Err(fmt::sprintf(__VA_ARGS__))

// Rust-like try!() macro, not portable to MSVC (but we can use clang on windows now).
#define TRY(...)                                                                                   \
  ({                                                                                               \
    auto INSIDE_TRY_MACRO_RES = __VA_ARGS__;                                                       \
    if (INSIDE_TRY_MACRO_RES.is_err()) {                                                           \
      using res_t = decltype(INSIDE_TRY_MACRO_RES);                                                \
      using E     = typename res_t::E;                                                             \
      return types::Err<E>{INSIDE_TRY_MACRO_RES.unwrap_error()};                                   \
    }                                                                                              \
    INSIDE_TRY_MACRO_RES.unwrap();                                                                 \
  })

namespace common
{

//
// A dummy structure. Used to indicate "Nothing"
struct none_t
{
};

} // namespace common

#define NOTHING ::common::none_t
#define OK_NONE Ok(NOTHING{})
#define ERR_NONE Err(NOTHING{})
