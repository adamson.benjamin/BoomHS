#pragma once
#include <common/forward.hpp>
#include <common/macros_class.hpp>
#include <common/move.hpp>

#include <cassert>
#include <type_traits>

// A wrapper around a resource.
//
// Internally tracks when it is time to destroy the resource.
//
// The last non-moved-from instance of this type owns the resource, and will invoke destroy_fn()
//
// It is undefined behavior to reference the internal reference after an move_only_type has been
// moved-from.
namespace common::detail
{
template <typename R, auto destroy_fn>
class move_only_type
{
private:
  R    r_;
  bool owner_ = true;

public:
  NO_COPY(move_only_type);

  template <typename... Args>
  explicit move_only_type(Args&&... args)
      : r_{R{FORWARD(args)}}
  {
  }

  ~move_only_type()
  {
    if (owner_) {
      destroy_fn(r_);
    }
  }

  auto& operator=(move_only_type&& other)
  {
    assert(&other != this);

    owner_ = other.owner_;
    r_     = MOVE(other.r_);

    // This instance takes ownership of the resource from "other"
    other.owner_ = false;
    return *this;
  }

  move_only_type(move_only_type&& other)
      : r_(MOVE(other.r_))
      , owner_(other.owner_)
  {
    // This instance takes ownership of the resource from "other"
    other.owner_ = false;
  }

  auto&       ref() { return r_; }
  auto const& ref() const { return r_; }

  auto const* operator-> () const { return &r_; }
  auto*       operator-> () { return &r_; }

  auto&       operator*() { return ref(); }
  auto const& operator*() const { return ref(); }
};

} // namespace common::detail

template <typename R, auto destroy_fn>
using move_only_type = ::common::detail::move_only_type<R, destroy_fn>;